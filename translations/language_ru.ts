<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AccessRightsWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/AccessRightsWidgets/AccessRightsWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/AccessRightsWidgets/AccessRightsWidget.ui" line="57"/>
        <source>Сотрудники</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/AccessRightsWidgets/AccessRightsWidget.ui" line="84"/>
        <source>Создать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/AccessRightsWidgets/AccessRightsWidget.ui" line="91"/>
        <source>Удалить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/AccessRightsWidgets/AccessRightsWidget.ui" line="98"/>
        <source>Обновить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/AccessRightsWidgets/AccessRightsWidget.ui" line="115"/>
        <source>Права доступа</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/AccessRightsWidgets/AccessRightsWidget.ui" line="154"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AttachmentWidget</name>
    <message>
        <location filename="../src/Widgets/AttachmentWidget/AttachmentWidget.ui" line="14"/>
        <source>Attachment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/AttachmentWidget/AttachmentWidget.ui" line="38"/>
        <source>Attachment file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/AttachmentWidget/AttachmentWidget.ui" line="64"/>
        <source>Скачать и открыть</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/AttachmentWidget/AttachmentWidget.ui" line="71"/>
        <source>Отмена</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClientsWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ClientsWidget/ClientsWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ClientsWidget/ClientsWidget.ui" line="97"/>
        <source>Время с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ClientsWidget/ClientsWidget.ui" line="109"/>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ClientsWidget/ClientsWidget.ui" line="181"/>
        <source>до</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ClientsWidget/ClientsWidget.ui" line="121"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ClientsWidget/ClientsWidget.ui" line="157"/>
        <source>Дата с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ClientsWidget/ClientsWidget.ui" line="188"/>
        <source>Сейчас</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ClientsWidget/ClientsWidget.ui" line="278"/>
        <source>Характер</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComplaintInfoWidget</name>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="37"/>
        <source>Review</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="44"/>
        <source>work</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="51"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="58"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="79"/>
        <source>Номер жалобы:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="86"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="123"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="143"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="231"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="307"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="327"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="347"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="369"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="395"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="415"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="437"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="465"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="493"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="528"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="604"/>
        <source>XXX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="99"/>
        <source>Характер жалобы:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="116"/>
        <source>Номер заказа:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="136"/>
        <source>Адрес:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="158"/>
        <source>Суть жалобы:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="180"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="187"/>
        <source>Файлы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="209"/>
        <source>Прикрепленные файлы:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="224"/>
        <source>Дата:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="244"/>
        <source>Назначена на:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="258"/>
        <source>Вид:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="272"/>
        <source>Статус:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="286"/>
        <source>Департамент:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="300"/>
        <source>Приоритет:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="320"/>
        <source>Кто оформил:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="340"/>
        <source>Откуда:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="360"/>
        <source>Водитель (id):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="379"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="614"/>
        <source>Анкета</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="388"/>
        <source>Чей водитель:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="408"/>
        <source>Парк/Колонна:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="428"/>
        <source>Положительных:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="447"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="475"/>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="503"/>
        <source>Посмотреть</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="456"/>
        <source>Отрицательных:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="484"/>
        <source>Всего:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="512"/>
        <source>Характеры
отрицательных
жалоб:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="521"/>
        <source>Рейтинг:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="595"/>
        <source>Прикреплённые файлы:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="668"/>
        <source>Отзывы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="682"/>
        <source>История</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.ui" line="701"/>
        <source>Отзывы:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComplaintsWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="134"/>
        <source>Дата с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="168"/>
        <source>Статус</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="180"/>
        <source>На кого</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="210"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="259"/>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="271"/>
        <source>до</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="278"/>
        <source>Сейчас</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="290"/>
        <source>Характер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="317"/>
        <source>Время с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.ui" line="333"/>
        <source>Excel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateComplaintDialog</name>
    <message>
        <location filename="../src/Widgets/CreateComplaintDialog/CreateComplaintDialog.ui" line="14"/>
        <source>Оформить жалобу</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateComplaintDialog/CreateComplaintDialog.ui" line="38"/>
        <source>Описание жалобы:&lt;span style=&quot;color:#FF0000;&quot;&gt;*&lt;/span&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateComplaintDialog/CreateComplaintDialog.ui" line="48"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateComplaintDialog/CreateComplaintDialog.ui" line="65"/>
        <source>Компания:&lt;span style=&quot;color:#FF0000;&quot;&gt;*&lt;/span&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateComplaintDialog/CreateComplaintDialog.ui" line="84"/>
        <source>На кого:&lt;span style=&quot;color:#FF0000;&quot;&gt;*&lt;/span&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateComplaintDialog/CreateComplaintDialog.ui" line="103"/>
        <source>Характер жалобы:&lt;span style=&quot;color:#FF0000;&quot;&gt;*&lt;/span&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateComplaintDialog/CreateComplaintDialog.ui" line="122"/>
        <source>Номер заказа:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateComplaintDialog/CreateComplaintDialog.ui" line="141"/>
        <source>Дополнительные комментарии:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateComplaintDialog/CreateComplaintDialog.ui" line="262"/>
        <source>Attachments</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateUserDialog</name>
    <message>
        <location filename="../src/Widgets/CreateUserDialog/CreateUserDialog.ui" line="14"/>
        <source>Добавить нового сотрудника</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateUserDialog/CreateUserDialog.ui" line="20"/>
        <source>Имя</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateUserDialog/CreateUserDialog.ui" line="30"/>
        <source>Логин:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateUserDialog/CreateUserDialog.ui" line="40"/>
        <source>Пароль:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateUserDialog/CreateUserDialog.ui" line="50"/>
        <source>Должность:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateUserDialog/CreateUserDialog.ui" line="60"/>
        <source>Компания сотрудника:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/CreateUserDialog/CreateUserDialog.ui" line="70"/>
        <source>Отдел:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DriversWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.ui" line="91"/>
        <source>Время с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.ui" line="103"/>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.ui" line="175"/>
        <source>до</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.ui" line="115"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.ui" line="151"/>
        <source>Дата с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.ui" line="182"/>
        <source>Сейчас</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.ui" line="257"/>
        <source>Характер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.ui" line="279"/>
        <source>Excel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../src/Widgets/LoginDialog/LoginDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/LoginDialog/LoginDialog.ui" line="34"/>
        <source>Пользователь:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/LoginDialog/LoginDialog.ui" line="44"/>
        <source>Пароль:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogistsWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.ui" line="97"/>
        <source>Время с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.ui" line="109"/>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.ui" line="181"/>
        <source>до</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.ui" line="121"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.ui" line="157"/>
        <source>Дата с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.ui" line="188"/>
        <source>Сейчас</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.ui" line="278"/>
        <source>Характер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.ui" line="285"/>
        <source>Excel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="14"/>
        <source>Контроль качества</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="36"/>
        <source>Администрирование</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="46"/>
        <source>Обращения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="82"/>
        <source>ТФ - ГФ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="115"/>
        <source>Жалобы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="146"/>
        <source>Клиенты</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="160"/>
        <source>Логисты</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="189"/>
        <source>Операторы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="218"/>
        <source>Водители</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="247"/>
        <source>Все</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="280"/>
        <source>Отзывы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/MainWindow.ui" line="309"/>
        <source>Достаевский</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OperatorsWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.ui" line="91"/>
        <source>Время с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.ui" line="103"/>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.ui" line="175"/>
        <source>до</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.ui" line="115"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.ui" line="151"/>
        <source>Дата с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.ui" line="182"/>
        <source>Сейчас</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.ui" line="257"/>
        <source>Характер</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.ui" line="279"/>
        <source>Excel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecallInfoWidget</name>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="37"/>
        <source>Complaint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="44"/>
        <source>Create complaint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="51"/>
        <source>close recall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="70"/>
        <source>Номер отзыва:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="77"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="97"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="117"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="137"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="157"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="177"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="200"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="220"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="240"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="260"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="286"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="311"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="334"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="354"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="374"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="396"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="424"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="452"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="487"/>
        <source>XXX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="90"/>
        <source>Номер заказа:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="110"/>
        <source>Номер авто:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="130"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Водитель предложил &lt;/p&gt;&lt;p&gt;пристегнуть ремень:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="150"/>
        <source>В салоне было чисто:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="170"/>
        <source>Водитель вёл машину аккуратно:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="193"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Водитель предложил &lt;/p&gt;&lt;p&gt;отрегулировать температуру &lt;/p&gt;&lt;p&gt;воздуха в салоне&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="213"/>
        <source>В салоне был приятный запах:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="233"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Водитель предложил включить &lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;радиостанцию по Вашему вкусу:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="253"/>
        <source>Дата создания:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="277"/>
        <source>Загруженные файлы:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="293"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="318"/>
        <source>Анкета</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="302"/>
        <source>Водитель (id):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="327"/>
        <source>ФИО водителя:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="347"/>
        <source>Чей водитель:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="367"/>
        <source>Парк/Колонна:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="387"/>
        <source>Положительных:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="406"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="434"/>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="462"/>
        <source>Посмотреть</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="415"/>
        <source>Отрицательных:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="443"/>
        <source>Всего:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="471"/>
        <source>Характеры
отрицательных
жалоб:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="480"/>
        <source>Рейтинг:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="516"/>
        <source>Отзыв:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="575"/>
        <source>Коментарии</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="589"/>
        <source>История</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/RecallInfoWidget/RecallInfoWidget.ui" line="608"/>
        <source>Коментарий:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReviewsWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.ui" line="83"/>
        <source>Показать закрытые отзывы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.ui" line="104"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.ui" line="129"/>
        <source>Дата с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.ui" line="141"/>
        <source>Время с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.ui" line="206"/>
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.ui" line="218"/>
        <source>до</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.ui" line="225"/>
        <source>Сейчас</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.ui" line="259"/>
        <source>Excel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScrollWidget</name>
    <message>
        <location filename="../src/Widgets/ScrollWidget/ScrollWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkerInformationWidget</name>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="39"/>
        <source>ОБЩАЯ ИНФОРМАЦИЯ ПО ВОДИТЕЛЮ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="63"/>
        <source>ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="77"/>
        <source>ФИО водителя:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="91"/>
        <source>Парк/колона:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="105"/>
        <source>Принят на работу:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="149"/>
        <source>АВТО</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="173"/>
        <source>Номер:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="187"/>
        <source>Марка:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="201"/>
        <source>Модель:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="215"/>
        <source>Телефон:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="229"/>
        <source>Доп. номера:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="285"/>
        <source>СТАТИСТИКА ПО ВОДИТЕЛЮ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="309"/>
        <source>Количество жалоб:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="325"/>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="350"/>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="375"/>
        <source>Просмотреть</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="334"/>
        <source>Положительных:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="359"/>
        <source>Отрицательных:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="384"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Характеры&lt;/p&gt;&lt;p&gt;отрицательных&lt;/p&gt;&lt;p&gt;жалоб:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="416"/>
        <source>ФОТО</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="438"/>
        <source>Скачать анкету</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="475"/>
        <source>Скачать жалобу в формате Excel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="520"/>
        <source>Отзывы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="534"/>
        <source>История</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="553"/>
        <source>Отзывы:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/WorkerInformationWidget/WorkerInformationWidget.ui" line="609"/>
        <source>Сохранить</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qat_client::ComplaintInfoWidget</name>
    <message>
        <location filename="../src/Widgets/ComplaintInfoWidget/ComplaintInfoWidget.cpp" line="260"/>
        <source> / </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qat_client::ComplaintsWidget</name>
    <message>
<<<<<<< HEAD
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.cpp" line="211"/>
=======
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.cpp" line="216"/>
>>>>>>> master
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.cpp" line="213"/>
=======
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.cpp" line="218"/>
>>>>>>> master
        <source>Excel (*.xlsx)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qat_client::CreateUserController</name>
    <message>
        <location filename="../src/Controllers/CreateUserController.cpp" line="133"/>
        <source>Unknown error.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qat_client::DriversWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.cpp" line="174"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/DriversWidget/DriversWidget.cpp" line="176"/>
        <source>Excel (*.xlsx)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qat_client::LogistsWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.cpp" line="177"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/LogistsWidget/LogistsWidget.cpp" line="179"/>
        <source>Excel (*.xlsx)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qat_client::OperatorsWidget</name>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.cpp" line="177"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.cpp" line="179"/>
        <source>Excel (*.xlsx)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qat_client::ReviewsWidget</name>
    <message>
<<<<<<< HEAD
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.cpp" line="152"/>
=======
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.cpp" line="154"/>
>>>>>>> master
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.cpp" line="154"/>
=======
        <location filename="../src/Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.cpp" line="156"/>
>>>>>>> master
        <source>Excel (*.xlsx)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
