#pragma once


#define ID_DEP_QAT 1
#define CONST_RATING 4.9


namespace qat_client
{

    enum Statuses
    {
        _new = 1,
        in_the_work,
        demolished,
        it_requires_notification,
        is_closed
    };

    enum SmsTemplates
    {
        description = 0,
        id_template,
        message_text
    };

}
