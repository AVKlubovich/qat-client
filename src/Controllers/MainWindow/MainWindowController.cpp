#include "Common.h"
#include "MainWindowController.h"

#include "Widgets/MainWindow/MainWindow.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/Users/Users.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "session-controller/Session.h"

#include "utils/Settings/SettingsFactory.h"

#include "Models/ComplaintsModel.h"
#include "Models/CheckedModel.h"
#include "Models/UsersTableModel.h"
#include "Models/ReviewsModel.h"

#include "permissions/UserPermission.h"
#include "Controllers/ResponceChecker.h"

#include "progress-indicator/ProgressIndicator.h"

using namespace qat_client;

MainWindowController::MainWindowController(QObject *parent)
    : QObject(parent)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _requestsManager->init();
//    _fetchTimer.setSingleShot(true);
}

MainWindowController::~MainWindowController()
{
    _modelDepartmentsInfo->deleteLater();
    _modelInfoReviews->deleteLater();
    _modelInfo->deleteLater();
    _modelSourceInfo->deleteLater();
    _mainWindow->deleteLater();
}

void MainWindowController::setOptions(const Options & options)
{
    _options = options;
}

void MainWindowController::setDBManager(const database::DBManagerShp &dbManager)
{
    _dbManager = dbManager;

    _driversWidgetController.setDbManager(_dbManager);
    _operatorsWidgetController.setDbManager(_dbManager);
    _logistsWidgetController.setDbManager(_dbManager);
    _clientsWidgetController.setDbManager(_dbManager);
    _complaintsWidgetController.setDbManager(_dbManager);
    _reviewsWidgetController.setDbManager(_dbManager);
    _accessRightsController.setDbManager(_dbManager);
}

void MainWindowController::setSession(const session_controller::SessionShp &session)
{
    _session = session;
}

void MainWindowController::setVersion(const QString &version)
{
    _version = version;
}

void MainWindowController::run()
{
    _mainWindow = new MainWindow();
    _mainWindow->setWindowTitle(QString("Отдел контроля качества %1. Удачной работы, %2")
                                .arg(_version)
                                .arg(_session->userName()));
    _mainWindow->showMaximized();

    _indicator = new progress_indicator::ProgressIndicator(_mainWindow);
    _indicator->setSizeWidgetDefault();
//    _indicator->move(_mainWindow->width()/2, _mainWindow->height()/2);
    _indicator->move(QApplication::desktop()->screen()->rect().center() - _indicator->rect().center());

    _modelInfoReviews = new QStandardItemModel();
    _modelInfo = new QStandardItemModel();
    _modelDepartmentsInfo = new QStandardItemModel();
    _modelSourceInfo = new QStandardItemModel();

    _driversWidgetController.setDriversWidget(_mainWindow->getDriversWidget());
    _operatorsWidgetController.setOperatorsWidget(_mainWindow->getOperatorsWidget());
    _logistsWidgetController.setLogistsWidget(_mainWindow->getLogistsWidget());
    _clientsWidgetController.setClientsWidget(_mainWindow->getClientsWidget());
    _complaintsWidgetController.setComplaintsWidget(_mainWindow->getComplaintsWidget());
    _reviewsWidgetController.setReviewsWidget(_mainWindow->getReviewsWidget());
    _accessRightsController.setAccessRightsWidget(_mainWindow->getAccessRightsWidget());

    createConnectMainWindowStatusBar();

    _indicator->show();
    _indicator->startAnimation();
    commandGetNewDBData();
}

void MainWindowController::commandGetNewDBData()
{
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_new_db_data";
        requestMap["date_last_query"] = QDateTime::currentDateTime();
        requestMap["idUser"] = permissions::UserPermission::instance().idUser();

        network::RequestShp request(new network::DefaultRequest("get_new_db_data", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &MainWindowController::onGetNewDbData);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_new_db_data", reconnectorObj);
    reconnectorObj.run();
}

void MainWindowController::commandFetchNewData()
{
    _fetchTimer->stop();
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "fetch_new_data";
        requestMap["last_fetch_date"] = _lastFetch.toString("yyyy-MM-dd hh:mm:ss");
        requestMap["idUser"] = permissions::UserPermission::instance().idUser();

        network::RequestShp request(new network::DefaultRequest("fetch_new_data", QVariant::fromValue(requestMap)));
        //TODO: it is annoying, need a mechanizm to avoid errors and logs pollution
        _requestsManager->execute(request, this, &MainWindowController::onFetchNewData);
    };

    Reconnector reconnectorObj(request);
    _reconnectMap.insert("fetch_new_data", reconnectorObj);
    reconnectorObj.run();
}

void MainWindowController::tabChangedComplaints(const Settings& data)
{
    _settings = data;

    switch (data._tabIndex)
    {
        case 0:
        {
            auto target = data._mapComplaints[data._tabIndexComplaints];
            if (target == "clients")
            {
                _clientsWidgetController.run();
                return;
            }
            if (target == "logists")
            {
                _logistsWidgetController.run();
                return;
            }
            if (target == "operators")
            {
                _operatorsWidgetController.run();
                return;
            }
            if (target == "drivers")
            {
                _driversWidgetController.run();
                return;
            }
            _complaintsWidgetController.run();
        }
            break;
        case 1:
            _reviewsWidgetController.run();
            break;
        default:
            break;
    }
}

void MainWindowController::onGetNewDbData(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_new_db_data"];
        reconnector.run();

        return;
    }

    const auto& response = responseObj->toVariant().toMap();
    const auto& commandType = response["head"].toMap()["type"].toString();

    if (commandType.trimmed() != "get_new_db_data")
        return;

    const auto& responseMap = response["body"].toMap();

    if (responseMap.contains("last_fetch_date"))
        _lastFetch = QDateTime::fromString(responseMap["last_fetch_date"].toString(), "yyyy-MM-dd hh:mm:ss");

    const auto& future = QtConcurrent::run(this, &MainWindowController::parseNewDbData, responseMap);
    QFutureWatcher<void> *fWatcher = new QFutureWatcher<void>();
    connect(fWatcher, &QFutureWatcher<void>::finished, this, &MainWindowController::onParseNewDbDataFinished);
    connect(fWatcher, &QFutureWatcher<void>::finished, fWatcher, &QFutureWatcher<void>::deleteLater);
    fWatcher->setFuture(future);

    emit signalShowMessageStatusBar("Данные загружаются", 2); // TODO 2 sec
}

void MainWindowController::parseNewDbData(const QVariantMap& inData)
{
    const auto& dbWraper = _dbManager->getDBWraper();

    QList<QSqlQuery> queriesList = database::DBHelpers::createInsertQueries(inData, dbWraper);
    for (QSqlQuery& query : queriesList)
    {
        const bool result = query.exec();
        if (!result)
        {
            qDebug() << __FUNCTION__ << query.lastError().text();
            qDebug() << __FUNCTION__ << query.lastQuery();
        }
    }
}

void MainWindowController::onParseNewDbDataFinished()
{
    _indicator->deleteLater();
    _indicator = nullptr;

    initControllers();
//    selectCountReccord();
}

void MainWindowController::createConnectMainWindowStatusBar()
{
    connect(this, &MainWindowController::signalShowMessageStatusBar,
            _mainWindow, &MainWindow::showMessageStatusBar);

    connect(&_complaintsWidgetController, &ComplaintsWidgetController::signalShowMessageStatusBarComplaints,
            _mainWindow, &MainWindow::showMessageStatusBar);

    connect(&_reviewsWidgetController, &ReviewsWidgetController::signalShowMessageStatusBarReviews,
            _mainWindow, &MainWindow::showMessageStatusBar);

    connect(&_driversWidgetController, &DriversWidgetController::signalShowMessageStatusBarDrivers,
            _mainWindow, &MainWindow::showMessageStatusBar);

    connect(&_operatorsWidgetController, &OperatorsWidgetController::signalShowMessageStatusBarOperators,
            _mainWindow, &MainWindow::showMessageStatusBar);

    connect(&_logistsWidgetController, &LogistsWidgetController::signalShowMessageStatusBarLogists,
            _mainWindow, &MainWindow::showMessageStatusBar);

    // TODO подсчёт жалоб и отзывов
    connect(&_complaintsWidgetController, &ComplaintsWidgetController::signalStatusBarValue,
            _mainWindow, &MainWindow::setValueStatusBar);

    connect(&_reviewsWidgetController, &ReviewsWidgetController::signalStatusBarValue,
            _mainWindow, &MainWindow::setValueStatusBar);

    connect(&_driversWidgetController, &DriversWidgetController::signalStatusBarValue,
            _mainWindow, &MainWindow::setValueStatusBar);

    connect(&_operatorsWidgetController, &OperatorsWidgetController::signalStatusBarValue,
            _mainWindow, &MainWindow::setValueStatusBar);

    connect(&_logistsWidgetController, &LogistsWidgetController::signalStatusBarValue,
            _mainWindow, &MainWindow::setValueStatusBar);
}

void MainWindowController::initControllers()
{
    _reviewsWidgetController.init();
    _complaintsWidgetController.init();
    _driversWidgetController.init();
    _operatorsWidgetController.init();
    _logistsWidgetController.init();
    _clientsWidgetController.init();
    _accessRightsController.init();
    _complaintsWidgetController.run();

    // выгрузка водителей, операторов и логистов
    connect(&_driversWidgetController, &DriversWidgetController::signalRefresh,
            &_complaintsWidgetController, &ComplaintsWidgetController::commandGetSelectComplaintsPublic);
    connect(&_operatorsWidgetController, &OperatorsWidgetController::signalRefresh,
            &_complaintsWidgetController, &ComplaintsWidgetController::commandGetSelectComplaintsPublic);
    connect(&_logistsWidgetController, &LogistsWidgetController::signalRefresh,
            &_complaintsWidgetController, &ComplaintsWidgetController::commandGetSelectComplaintsPublic);

    // завершение обновления
    connect(&_complaintsWidgetController, &ComplaintsWidgetController::finishedRefreshComplaints,
            &_driversWidgetController, &DriversWidgetController::finishedRefreshComplaints);
    connect(&_complaintsWidgetController, &ComplaintsWidgetController::finishedRefreshComplaints,
            &_logistsWidgetController, &LogistsWidgetController::finishedRefreshComplaints);
    connect(&_complaintsWidgetController, &ComplaintsWidgetController::finishedRefreshComplaints,
            &_operatorsWidgetController, &OperatorsWidgetController::finishedRefreshComplaints);

    // поиск по водителю
    connect(&_driversWidgetController, &DriversWidgetController::signalOpenComplaintsWorker,
            this, &MainWindowController::openComplaintsFromWorker);
    connect(&_operatorsWidgetController, &OperatorsWidgetController::signalOpenComplaintsWorker,
            this, &MainWindowController::openComplaintsFromWorker);
    connect(&_logistsWidgetController, &LogistsWidgetController::signalOpenComplaintsWorker,
            this, &MainWindowController::openComplaintsFromWorker);

    _fetchTimer = new QTimer(this);
    connect(_fetchTimer, &QTimer::timeout, this, &MainWindowController::commandFetchNewData);

    connect(_mainWindow, &qat_client::MainWindow::signalTabChanged,
        this, &MainWindowController::tabChangedComplaints);

    connect(&_reviewsWidgetController, &ReviewsWidgetController::signalIdComplaintOpen,
            this, &MainWindowController::openComplaintFromReview);

    connect(&_complaintsWidgetController, &ComplaintsWidgetController::signalIdReviewOpen,
            this, &MainWindowController::openReviewFromComplaint);

    connect(_mainWindow, &MainWindow::signalClickedStatistic,
            this, &MainWindowController::createStatistic);

    connect(_mainWindow, &MainWindow::signalClickedExcel,
            this, &MainWindowController::createExcelFile);

    connect(_mainWindow, &MainWindow::signalClickedStatisticReviews,
            this, &MainWindowController::createStatisticReviews);

    connect(_mainWindow, &MainWindow::signalClickedExcelReviews,
            this, &MainWindowController::createExcelFileReviews);

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Client");

    _options.fetchData = settings["FetchData"].toInt();

    _fetchTimer->start(_options.fetchData);
    createModelCompany();
    createModelStatisticTarget();
}

void MainWindowController::onFetchNewData(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["fetch_new_data"];
        reconnector.run();

        return;
    }

    const auto& responseJson = responseObj->bodyToVariant().toMap();

    if (responseJson.contains("complaints"))
    {
        if (parseObjFetchNewData(responseJson["complaints"].toMap()))
        {
            _complaintsWidgetController.refreshFetch();
//            qDebug() << "[DEBUG]" << "END PARSER COMPLAINT" << endl;
        }
    }
    if (responseJson.contains("reviews"))
    {
        if (parseObjFetchNewData(responseJson["reviews"].toMap()))
        {
            _reviewsWidgetController.refreshFetch();
//            qDebug() << "[DEBUG]" << "END PARSER REVIEW" << endl;
        }
    }

    if (responseJson.contains("last_fetch_date"))
        _lastFetch = responseJson["last_fetch_date"].toDateTime();

//    selectCountReccord();
    _fetchTimer->start(_options.fetchData);
//    qDebug() << "[DEBUG]" << "TIMER START" << endl;
}

void MainWindowController::onStatistic(network::ResponseShp responseObj)
{
    const auto& response = responseObj->bodyToVariant().toMap();
    createInfoModelStatistic(response["companies_values"].toMap());
    createDepartmantsModelStatistic(response["departments"].toList());
    createSourceModelStatistic(response["source"].toList());
}

void MainWindowController::onStatisticReviews(network::ResponseShp responseObj)
{
    const auto& response = responseObj->bodyToVariant().toMap();
    createInfoModelStatisticReviews(response);
}

bool MainWindowController::parseObjFetchNewData(const QVariantMap & obj)
{
    const auto dbWraper = _dbManager->getDBWraper();
    bool isRefresh = false;

//    qDebug() << "[DEBUG]" << __FUNCTION__ << "START PARSER" << endl;

    if (obj.contains("insert"))
    {
        Q_ASSERT(this->thread() == QThread::currentThread());
        auto insertQuery = dbWraper->query();
        const auto& jarrInsert = obj["insert"].toList();


//        qDebug() << "[DEBUG]" << __FUNCTION__ << "START INSERT" << jarrInsert.count() << endl;
        if (jarrInsert.count() > 0)
        {
//            QList<QSqlQuery> sqlList = database::DBHelpers::createInsertQueriesPerTable(jarrInsert, dbWraper);
            for (const auto item : jarrInsert)
            {
                const auto jobj = item.toMap();
                if (!jobj.contains("table") ||
                    !jobj.contains("data"))
                    continue;

                const auto& table = jobj["table"].toString();
                const auto& data = jobj["data"].toMap();
                if (data.count() == 0 ||
                    table.isEmpty())
                    continue;

                auto listKeys = data.keys();
                const auto& stringKeys = QStringList(listKeys).join(", ");

                QStringList listValues;
                for (auto key : listKeys)
                    listValues.append("'" + data.value(key).toString() + "'");

                const auto& stringValues = listValues.join(", ");
                const QString& sqlQuery = "INSERT OR REPLACE INTO " + table + " (" + stringKeys + ") VALUES (" + stringValues + ")";

                insertQuery.prepare(sqlQuery);
                if (!insertQuery.exec())
                {
                    qDebug() << __FUNCTION__ << "[SQL ERROR]" << insertQuery.lastError() << sqlQuery;
//                    Q_ASSERT(false);
                }
            }

//            qDebug() << "[DEBUG]" << __FUNCTION__ << "END INSERT" << endl;
            isRefresh = true;
        }
    }

    if (obj.contains("update"))
    {
        Q_ASSERT(this->thread() == QThread::currentThread());
        auto updateQuery = dbWraper->query();
        const auto& jarrUpdate = obj["update"].toList();

//        qDebug() << "[DEBUG]" << __FUNCTION__ << "START UPDATE" << jarrUpdate.count() << endl;
        if (jarrUpdate.count() > 0)
        {
            for (const auto& item : jarrUpdate)
            {
                const auto& jobj = item.toMap();
                if (!jobj.contains("table") ||
                    !jobj.contains("data") ||
                    !jobj.contains("condition"))
                    continue;

                const auto& table = jobj["table"].toString();
                const auto& data = jobj["data"].toMap();
                const auto& condition = jobj["condition"].toMap();
                if (data.count() == 0 ||
                    table.isEmpty())
                    continue;

                auto sqlQuery = "UPDATE " + table + " SET ";
                for (auto it = data.begin(); it != data.end(); it++)
                    sqlQuery += it.key() + "='" + it.value().toString() + "', ";
                sqlQuery = sqlQuery.mid(0, sqlQuery.length() - 2);

                if (condition.keys().count() > 0)
                {
                    sqlQuery += " WHERE ";
                    for (auto it = condition.begin(); it != condition.end(); it++)
                        sqlQuery += it.key() + "='" + it.value().toString() + "' AND ";
                    sqlQuery = sqlQuery.mid(0, sqlQuery.length() - 5);
                }

                updateQuery.prepare(sqlQuery);
                if (!updateQuery.exec())
                {
                    qDebug() << __FUNCTION__ << "[SQL ERROR]" << updateQuery.lastError() << sqlQuery;
//                    Q_ASSERT(false);
                }
            }
        }

//        qDebug() << "[DEBUG]" << __FUNCTION__ << "END UPDATE" << endl;
        isRefresh = true;
    }

    if (obj.contains("delete"))
    {
        auto deleteQuery = dbWraper->query();
        const auto& jarrDelete = obj["delete"].toList();

//        qDebug() << "[DEBUG]" << __FUNCTION__ << "START DELETE" << jarrDelete.count() << endl;
        if (jarrDelete.count() > 0)
        {
            for (const auto item : jarrDelete)
            {
                const auto& jobj = item.toMap();
                if (!jobj.contains("table") ||
                    !jobj.contains("condition"))
                    continue;

                const auto& table = jobj["table"].toString();
                const auto& condition = jobj["condition"].toMap();
                if (condition.count() == 0 ||
                    table.isEmpty())
                    continue;

                auto sqlQuery = "DELETE FROM \"" + table + "\"";
                if (condition.keys().count() > 0)
                {
                    sqlQuery += " WHERE ";
                    for (auto it = condition.begin(); it != condition.end(); it++)
                        sqlQuery += it.key() + "='" + it.value().toString() + "' AND ";
                    sqlQuery = sqlQuery.mid(0, sqlQuery.length() - 5);
                }

                deleteQuery.prepare(sqlQuery);
                const auto result = dbWraper->execQuery(deleteQuery);
                if (!result)
                {
                    qDebug() << __FUNCTION__ << "[SQL ERROR]" << deleteQuery.lastError();
//                    qDebug() << __FUNCTION__ << "query failed";
//                    Q_ASSERT(false);
                }
            }
        }

//        qDebug() << "[DEBUG]" << __FUNCTION__ << "END DELETE" << endl;
        isRefresh = true;
    }

    return isRefresh;
}

void MainWindowController::createSourceModelStatistic(QVariantList body)
{
    _modelSourceInfo->clear();
    quint64 allCount = 0;
    for (const auto& elem : body)
    {
        const auto& map = elem.toMap();
        const auto& source = map["name_source"].toString();
        const auto count = map["complaints_count"].toLongLong();

        QList<QStandardItem*> rowItems;
        rowItems.append(new QStandardItem(source));
        rowItems.append(new QStandardItem(QString::number(count)));
        _modelSourceInfo->appendRow(rowItems);
        allCount += count;
    }

    QList<QStandardItem*> rowItems;
    rowItems.append(new QStandardItem("Всего"));
    rowItems.append(new QStandardItem(QString::number(allCount)));
    _modelSourceInfo->appendRow(rowItems);
    _modelSourceInfo->setHeaderData(0, Qt::Horizontal, "Откуда");
    _modelSourceInfo->setHeaderData(1, Qt::Horizontal, "Количество");
    _mainWindow->setModelSourceStatistic(_modelSourceInfo);
}

void MainWindowController::createDepartmantsModelStatistic(QVariantList body)
{
    _modelDepartmentsInfo->clear();
    quint64 allCount = 0;
    for (const auto& elem : body)
    {
        const auto& map = elem.toMap();
        const auto& department = map["name"].toString();
        const auto count = map["complaints_count"].toLongLong();

        QList<QStandardItem*> rowItems;
        rowItems.append(new QStandardItem(department));
        rowItems.append(new QStandardItem(QString::number(count)));
        _modelDepartmentsInfo->appendRow(rowItems);
        allCount += count;
    }

    QList<QStandardItem*> rowItems;
    rowItems.append(new QStandardItem("Всего"));
    rowItems.append(new QStandardItem(QString::number(allCount)));
    _modelDepartmentsInfo->appendRow(rowItems);
    _modelDepartmentsInfo->setHeaderData(0, Qt::Horizontal, "Отдел");
    _modelDepartmentsInfo->setHeaderData(1, Qt::Horizontal, "Количество");
    _mainWindow->setModelDepartmentsStatistic(_modelDepartmentsInfo);
}

void MainWindowController::createInfoModelStatistic(QVariantMap body)
{
    _modelInfo->clear();
    for (auto company = body.begin(); company != body.end(); ++company)
    {
        auto item = new QStandardItem(company.key());

        auto font = item->font();
        font.setPixelSize(20);
        font.setBold(true);
        font.setItalic(true);
        item->setFont(font);

        _modelInfo->appendRow(item);
        const auto& companyMap = company.value().toMap();
        for (auto nature = companyMap.begin(); nature != companyMap.end(); ++nature)
        {
            QList <QStandardItem*> list;
            const auto& mapNature = nature.value().toMap();
            list.append(new QStandardItem(nature.key()));

            quint64 count = 0;
            for (auto source = mapNature.begin(); source != mapNature.end(); ++source)
            {
                switch (source.key().toInt()) {
                    case 1:
                        list.append(new QStandardItem(source.value().toString()));
                        count += source.value().toLongLong();
                        break;
                    case 2:
                        list.append(new QStandardItem(source.value().toString()));
                        count += source.value().toLongLong();
                        break;
                    case 3:
                        list.append(new QStandardItem(source.value().toString()));
                        count += source.value().toLongLong();
                        break;
                    case 4:
                        list.append(new QStandardItem(source.value().toString()));
                        count += source.value().toLongLong();
                        break;
                    case 5:
                        list.append(new QStandardItem(source.value().toString()));
                        count += source.value().toLongLong();
                        break;
                    default:
                        break;
                }
            }
            list.append(new QStandardItem(QString::number(count)));
            _modelInfo->appendRow(list);
        }
    }

    _modelInfo->setHeaderData(0, Qt::Horizontal, "Характер");
    _modelInfo->setHeaderData(1, Qt::Horizontal, "Новая");
    _modelInfo->setHeaderData(2, Qt::Horizontal, "В работе");
    _modelInfo->setHeaderData(3, Qt::Horizontal, "Разобранна");
    _modelInfo->setHeaderData(4, Qt::Horizontal, "Требует оповещения");
    _modelInfo->setHeaderData(5, Qt::Horizontal, "Закрыта");
    _modelInfo->setHeaderData(6, Qt::Horizontal, "Всего");
    _mainWindow->setModelInfoStatistic(_modelInfo);
}

void MainWindowController::createInfoModelStatisticReviews(QVariantMap body)
{
    _modelInfoReviews->clear();
    for (auto info = body.begin(); info != body.end(); ++info)
    {
        QList <QStandardItem*> list;
        const auto& mapInfo = info.value().toMap();
        list.append(new QStandardItem(info.key()));
        list.append(new QStandardItem(mapInfo["Положительных"].toString()));
        list.append(new QStandardItem(mapInfo["Отрицательных"].toString()));
        const auto count = mapInfo["Отрицательных"].toLongLong() + mapInfo["Положительных"].toLongLong();
        list.append(new QStandardItem(QString::number(count)));
        _modelInfoReviews->appendRow(list);
    }

    _modelInfoReviews->setHeaderData(0, Qt::Horizontal, "Парк");
    _modelInfoReviews->setHeaderData(1, Qt::Horizontal, "Положительных");
    _modelInfoReviews->setHeaderData(2, Qt::Horizontal, "Отрицательных");
    _modelInfoReviews->setHeaderData(3, Qt::Horizontal, "Всего");
    _mainWindow->setModelInfoStatistic(_modelInfoReviews);
}

void MainWindowController::createModelCompany()
{
    auto model = new CheckedModel();
    model->setQuery("SELECT name, id FROM company WHERE isVisible = 1",
                    *(_dbManager->getConnection().data()));

    _mainWindow->setCompany(model);
}

void MainWindowController::createModelStatisticTarget()
{
    auto model = new CheckedModel();
    model->setQuery("SELECT data_target, id FROM complaints_target WHERE isVisible = 1",
                    *(_dbManager->getConnection().data()));

    _mainWindow->setStatisticTarget(model);
}

bool MainWindowController::companyByName(const QString &name)
{
    const auto& dbWraper = _dbManager->getDBWraper();
    auto query = dbWraper->query();
    query.prepare("SELECT id FROM company WHERE name = :name");
    query.bindValue(":name", name);
    if (!dbWraper->execQuery(query))
        qDebug() << endl << query.lastError();

    return query.first();
}

void MainWindowController::selectCountReccord()
{
    const auto& dbWraper = _dbManager->getDBWraper();
    auto query = dbWraper->query();

    query.prepare("SELECT count(*) as count FROM complaints");
    if (!dbWraper->execQuery(query))
        qDebug() << __FUNCTION__ << "[ERROR QUERY]" << query.lastError();

    query.first();
    const auto complaints = query.value("count").toULongLong();

    query.prepare("SELECT count(*) as count FROM reviews");
    if (!dbWraper->execQuery(query))
        qDebug() << __FUNCTION__ << "[ERROR QUERY]" << query.lastError();

    query.first();
    const auto reviews = query.value("count").toULongLong();

//    _mainWindow->setValueStatusBar(complaints, reviews);
}

void MainWindowController::openReviewFromComplaint(const quint64 id)
{
    _reviewsWidgetController.setFindDataReview(id);
    _mainWindow->setTabReview();
}

void MainWindowController::openComplaintFromReview(const quint64 id)
{
    _complaintsWidgetController.setFindDataComplaint(id);
    _mainWindow->setTabComplaint();
}

void MainWindowController::openComplaintsFromWorker(const quint64 idWorker, const int keyWotker, const int statusTarget)
{
    _mainWindow->setTabComplaint();
    _complaintsWidgetController.openComplaintsWorker(idWorker, keyWotker, statusTarget);
}

void MainWindowController::createStatistic(const QDateTime dateStart,
                                           const QDateTime dateEnd,
                                           QVariantList idsComany,
                                           QVariantList idsTarget)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "statistic_complaints";
    requestMap["companies"] = idsComany;
    requestMap["targets"] = idsTarget;
    requestMap["date_start"] = dateStart;
    requestMap["date_end"] = dateEnd;
    requestMap["id_user"] = permissions::UserPermission::instance().idUser();

    network::RequestShp request(new network::DefaultRequest("statistic_complaints", QVariant::fromValue(requestMap)));
    _requestsManager->execute(request, this, &MainWindowController::onStatistic);
}

void MainWindowController::createExcelFile(const QString &fileName)
{
#ifdef Q_OS_WIN
    QMap <int, QString> type;
    type[0] = "Общая статистика";
    type[1] = "Не закрытых";
    type[2] = "Откуда";

    QXlsx::Document doc;
    for (auto i = 0; i != 3; ++i)
    {
        doc.addSheet(type[i]);
        doc.setColumnWidth(1,  1, 100);
        doc.setColumnWidth(2,  2, 25);
        if (!i)
        {
            doc.setColumnWidth(3,  3, 25);
            doc.setColumnWidth(4,  4, 25);
            doc.setColumnWidth(5,  5, 25);
            doc.setColumnWidth(6,  6, 25);
            doc.setColumnWidth(6,  6, 25);
        }

        QXlsx::Format capitonStyle;
        capitonStyle.setFontName("Arial");
        capitonStyle.setFontSize(10);
        capitonStyle.setFontColor(Qt::black);
        capitonStyle.setFontBold(true);
        capitonStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
        capitonStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        capitonStyle.setPatternBackgroundColor(QColor::fromRgb(146,208,80));
        capitonStyle.setBorderColor(Qt::black);
        capitonStyle.setBorderStyle(QXlsx::Format::BorderThin);

        if (i == 0)
        {
            doc.write(1, 1,  "Характер",                  capitonStyle);
            doc.write(1, 2,  "Новая",                     capitonStyle);
            doc.write(1, 3,  "В работе",                  capitonStyle);
            doc.write(1, 4,  "Разобранна",                capitonStyle);
            doc.write(1, 5,  "Требует оповещения",        capitonStyle);
            doc.write(1, 6,  "Закрыта",                   capitonStyle);
            doc.write(1, 7,  "Всего",                     capitonStyle);
        }

        if (i == 1)
        {
            doc.write(1, 1,  "Отдел",                     capitonStyle);
            doc.write(1, 2,  "Количество",                capitonStyle);
        }

        if (i == 2)
        {
            doc.write(1, 1,  "Откуда",                    capitonStyle);
            doc.write(1, 2,  "Количество",                capitonStyle);
        }

        doc.setRowHeight(1, 1, 20);
    }

    QXlsx::Format baseDataStyle;
    baseDataStyle.setFontName("Arial");
    baseDataStyle.setFontColor(Qt::black);
    baseDataStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    // info
    for (auto i = 0; i != 3; ++i)
    {
        doc.selectSheet(type[i]);
        QStandardItemModel * model;

        switch (i) {
            case 0:
                model = _modelInfo;
                break;
            case 1:
                model = _modelDepartmentsInfo;
                break;
            case 2:
                model = _modelSourceInfo;
                break;
            default:
                break;
        }

        for (auto rowIndex = 0; rowIndex < model->rowCount(); ++rowIndex)
            for (auto columnIndex = 0; columnIndex < model->columnCount(); ++columnIndex)
            {
                if (!columnIndex)
                {
                    const auto & data = model->index(rowIndex, columnIndex).data().toString();
                    if (companyByName(data))
                    {
                        QXlsx::Format dataStyle;
                        dataStyle.setFontName("Arial");
                        dataStyle.setFontSize(12);
                        dataStyle.setFontColor(Qt::red);
                        dataStyle.setFontBold(true);
                        dataStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
                        dataStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
                        doc.write(rowIndex + 2, columnIndex + 1, data, dataStyle);
                        continue;
                    }
                }
                const auto & data = model->index(rowIndex, columnIndex).data();
                doc.write(rowIndex + 2, columnIndex + 1, data, baseDataStyle);
            }
    }

    if (!doc.saveAs(fileName))
        qDebug() << "Error save excel file";
#endif
}

void MainWindowController::createStatisticReviews(const QDateTime dateStart, const QDateTime dateEnd, QVariantList ids)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "statistic_reviews";
    requestMap["driver"] = ids;
    requestMap["date_start"] = dateStart;
    requestMap["date_end"] = dateEnd;
    requestMap["idUser"] = permissions::UserPermission::instance().idUser();

    network::RequestShp request(new network::DefaultRequest("statistic_reviews", QVariant::fromValue(requestMap)));
    _requestsManager->execute(request, this, &MainWindowController::onStatisticReviews);
}

void MainWindowController::createExcelFileReviews(const QString &fileName)
{
#ifdef Q_OS_WIN
    QXlsx::Document doc;
    doc.addSheet("Статистика");
    doc.setColumnWidth(1,  1, 100);
    doc.setColumnWidth(2,  2, 25);
    doc.setColumnWidth(3,  3, 25);
    doc.setColumnWidth(4,  4, 25);

    QXlsx::Format capitonStyle;
    capitonStyle.setFontName("Arial");
    capitonStyle.setFontSize(10);
    capitonStyle.setFontColor(Qt::black);
    capitonStyle.setFontBold(true);
    capitonStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    capitonStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    capitonStyle.setPatternBackgroundColor(QColor::fromRgb(146,208,80));
    capitonStyle.setBorderColor(Qt::black);
    capitonStyle.setBorderStyle(QXlsx::Format::BorderThin);

    doc.write(1, 1,  "Парк",                 capitonStyle);
    doc.write(1, 2,  "Положительных",        capitonStyle);
    doc.write(1, 3,  "Отрицательных",        capitonStyle);
    doc.write(1, 4,  "Всего",                capitonStyle);

    doc.setRowHeight(1, 1, 20);

    QXlsx::Format baseDataStyle;
    baseDataStyle.setFontName("Arial");
    baseDataStyle.setFontColor(Qt::black);
    baseDataStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);


    for (auto rowIndex = 0; rowIndex < _modelInfoReviews->rowCount(); ++rowIndex)
    {
        for (auto columnIndex = 0; columnIndex < _modelInfoReviews->columnCount(); ++columnIndex)
        {
            const auto & data = _modelInfoReviews->index(rowIndex, columnIndex).data();
            doc.write(rowIndex + 2, columnIndex + 1, data, baseDataStyle);
        }
    }

    if (!doc.saveAs(fileName))
        qDebug() << "Error save excel file";
#endif
}
