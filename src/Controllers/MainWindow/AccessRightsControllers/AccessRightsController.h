﻿#pragma once

#include "../../CreateUserController.h"
#include "../../../Models/UsersTableModel.h"

#include "../../Reconnector.h"

namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class AccessRightsWidget;

    class CheckedModel;
    class DepartmentCheckedModel;


    class AccessRightsController : public QObject
    {
        Q_OBJECT

        enum class TypeTemplate
        {
            not_selected,
            sms_template,
            email_template
        };

    public:
        AccessRightsController();
        ~AccessRightsController();

        void setDbManager(database::DBManagerShp dbManager);
        void setAccessRightsWidget(AccessRightsWidget * accessRightsWidget);

    public slots:
        void init();
//        void clear();

    private slots:
        // NOTE: send commands
        void commandCreateMapPermissions();
        void commandSaveUserPermissions();

        void commandGetSavePermissions();

        void commandGetSavePermissionsIndex(const QModelIndex &index);
        void commandGetTemplates(int index);
        void commandInsertTempalte(int idCompany);
        void commandUpdateTempalte(int idTemplate, const QString &description, const QString &textTemplate);

        // NOTE: user accounts control slots
        void createUserControllerRun();//run diallog for create new user account
        void getUserAccountList();//send request to update user account list
        void onGetUserAccountListResponse(network::ResponseShp);//response to getUserAccountList() call
        void onGetTemplatesResponse(network::ResponseShp responseObj);//response to getUserAccountList() call
        void onDoubleClickedUser(const int row);
        void onBtnDeleteUserClicked(const int row);
        void onResponceDeleteUser(network::ResponseShp);
        void onCreateTestReview(network::ResponseShp) {}

        // NEW
        void findName(const QString name = "");
        void onClickDepartment(const QModelIndex &index);
        void refreshDepartments(const QModelIndex & index);

    private slots:
        void saveAddCommand(const QString & text, const QString & table);
        void saveUpdateCommand(const QString & text, const QString & table, const int id = -1);
        void saveAddNatureCommand   (const QString & text, const int day, const QString &essense,
                                     const QString & table, const QString &cost,
                                     const int rating = 0, int type = 0);
        void saveUpdateNatureCommand(const QString & text, const int day, const QString &essense,
                                     const QString & table, const QString &cost,
                                     const int id = -1, const int rating = 0, int type = 0);
        void deleteCommand(const int id, const QString & table);
        void createDepartamentsModel(const int idCompany);

        void createTestReview();
        void slotSelectTypeTempale(int type);
        void commandRemoveTemplate(int idTemplate);

    signals:
        void signalIdComplaintOpen(const quint64 & id);

    private:
        void fillingModelOfAccessRights();
        void onGetUserPermissionsResponse(network::ResponseShp response);
        void onGetUserPermissionsListResponse(network::ResponseShp response);
        void onResponseInsertTempalte(network::ResponseShp responseObj);
        void onResponseUpdateTempalte(network::ResponseShp responseObj);
        void onResponseDeleteTempalte(network::ResponseShp responseObj);

        // mask generation widget
        void connectMethod();
        void createCompanyModel();
        void createWorkerModel();
        void createNatureModel(const int idCompany, const int idTarget);
        void onGetTableUpdate(network::ResponseShp response);

        // new methods
        void createDepartmentModel();
        void openDepartmentPermissions(quint64 idDepartment);

    private:
        network::RequestsManagerShp _requestsManager;
        database::DBManagerShp _dbManager;

        AccessRightsWidget *_accessRightsWidget = nullptr;

        // NOTE: controllers
        CreateUserController _createUserController;

        UsersTableModel* _usersTableModel = nullptr;
        network::Users _users;

        QSqlQueryModel *_usersModel;
        QSqlQueryModel *_companyModel;
        QSqlQueryModel *_workerModel;
        QSqlQueryModel *_natureModel;
        QSqlQueryModel *_departmetModel;
        QStandardItemModel *_templateModel;
        CheckedModel* _commandsModel;
        DepartmentCheckedModel* _departmentsModel;

        QMap <QStandardItem*, QMap <QStandardItem*, quint64>> _mapCompany;
        QMap <quint64, QSet<quint64>> _mapPermissions;

        QModelIndex _isSelectDepartment;
        quint64 _idUser;
        TypeTemplate _typeTempalte = TypeTemplate::not_selected;

        QMap<QString, Reconnector> _reconnectMap;

        bool _flagSave = true;

        const int COLUMN_NAME_DEPARTMENTS = 0;
    };

}
