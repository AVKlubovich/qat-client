#include "Common.h"
#include "AccessRightsController.h"

#include "Widgets/MainWindow/AccessRightsWidgets/AccessRightsWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/Users/Users.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "Models/CheckedModel.h"
#include "Models/DepartmentCheckedModel.h"
#include "permissions/UserPermission.h"
#include "Controllers/ResponceChecker.h"

#include "Definitions.h"


using namespace qat_client;

AccessRightsController::AccessRightsController()
    : QObject(nullptr)
    , _requestsManager(network::RequestsManagerShp::create())
{
}

AccessRightsController::~AccessRightsController()
{
}

void AccessRightsController::setDbManager(database::DBManagerShp dbManager)
{
    _dbManager = dbManager;
}

void AccessRightsController::setAccessRightsWidget(AccessRightsWidget * accessRightsWidget)
{
    _accessRightsWidget = accessRightsWidget;
}

void AccessRightsController::init()
{
    _usersTableModel = new UsersTableModel();
    _accessRightsWidget->setUsersTableModel(_usersTableModel);

    _usersModel = new QSqlQueryModel();
    _accessRightsWidget->setUsersModel(_usersModel);

    _commandsModel = new CheckedModel();
    _accessRightsWidget->setCommandsModel(_commandsModel);

    _departmentsModel = new DepartmentCheckedModel();
    connect(_departmentsModel, &DepartmentCheckedModel::finishUpdate,
            _accessRightsWidget, &AccessRightsWidget::setResetTreeDepartments);
    _accessRightsWidget->setDepartamentsModel(_departmentsModel);


    fillingModelOfAccessRights();

//    connect(_accessRightsWidget, &AccessRightsWidget::signalSelectCompany,
//        this, &AccessRightsController::selectCompanyPermissions);

//    connect(_accessRightsWidget, &qat_client::AccessRightsController::sigmalListUsersClicked,
//            this, &AccessRightsController::selectListUsersClicked);

    connect(_accessRightsWidget, &AccessRightsWidget::signalClickedDepartment,
            this, &AccessRightsController::onClickDepartment);

    // TODO refresh departments
    connect(_accessRightsWidget, &AccessRightsWidget::signalRefreshDepartments,
            this, &AccessRightsController::refreshDepartments);

    connect(_accessRightsWidget, &AccessRightsWidget::signalRefreshUsers,
            this, &AccessRightsController::getUserAccountList);

    connect(_accessRightsWidget, &AccessRightsWidget::signalCreateUser,
            this, &AccessRightsController::createUserControllerRun);

    connect(_accessRightsWidget, &AccessRightsWidget::signalDoubleClicked,
            this, &AccessRightsController::onDoubleClickedUser);

    connect(_accessRightsWidget, &qat_client::AccessRightsWidget::signalDeleteUser,
            this, &AccessRightsController::onBtnDeleteUserClicked);

    connect(_accessRightsWidget, &qat_client::AccessRightsWidget::signalBtnSearch,
            this, &AccessRightsController::findName);

//    connect(_accessRightsWidget, &qat_client::AccessRightsWidget::signalCheckedAll,
//            this, [this]()
//    {
//        _commandsModel->checkedAll();
//        _accessRightsWidget->setResetListCommand();
//        commandGetSavePermissions();
//    });

    connect(_accessRightsWidget, &qat_client::AccessRightsWidget::signalCheckedAll,
            this, [this]()
    {
        _commandsModel->checkedAll();
        _accessRightsWidget->setResetListCommand();
    });

    connect(_commandsModel, &CheckedModel::selectionChanged,
            this, &AccessRightsController::commandCreateMapPermissions);

    connect(_accessRightsWidget, &AccessRightsWidget::signalClickedBtnSaveUserPermissions,
            this, &AccessRightsController::commandSaveUserPermissions);

//    connect(_commandsModel, &CheckedModel::selectionChanged,
//            this, &AccessRightsController::commandGetSavePermissions);

    connect(_departmentsModel, &DepartmentCheckedModel::selectionChanged,
            this, &AccessRightsController::commandGetSavePermissionsIndex);

    connect(_accessRightsWidget, &AccessRightsWidget::signalBtnCreateTestReviewClicked,
            this, &AccessRightsController::createTestReview);

    connect(_accessRightsWidget, &AccessRightsWidget::signalCbNCompany,
            this, &AccessRightsController::commandGetTemplates);

    connect(_accessRightsWidget, &AccessRightsWidget::signalBtnClickedInsertTemplate,
            this, &AccessRightsController::commandInsertTempalte);

    connect(_accessRightsWidget, &AccessRightsWidget::signalBtnClickedSaveTemplate,
            this, &AccessRightsController::commandUpdateTempalte);

    connect(_accessRightsWidget, &AccessRightsWidget::signalTypeTemplate,
            this, &AccessRightsController::slotSelectTypeTempale);

    connect(_accessRightsWidget, &AccessRightsWidget::signalBtnClickedRemoveTemplate,
            this, &AccessRightsController::commandRemoveTemplate);
    connectMethod();
}

void AccessRightsController::commandCreateMapPermissions()
{
    // TODO _mapPermissions only user
    auto item = _departmentsModel->itemFromIndex(_isSelectDepartment);
    if (item == nullptr)
        return;

    if (item->data(Qt::UserRole + 2).toBool())
    {
        auto map = _mapCompany.value(item);
        for (auto it = map.begin(); it != map.end(); ++it)
            _mapPermissions[it.value()] = _commandsModel->checkedRealIndex();
    }
    else
        _mapPermissions[item->data(Qt::UserRole + 1).toInt()] = _commandsModel->checkedRealIndex();

    _flagSave = false;
    _accessRightsWidget->isEnabledBtnSaveUserPermissions(_flagSave);
}

void AccessRightsController::commandSaveUserPermissions()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "save_permissions_new";

    requestMap["idUser"] = permissions::UserPermission::instance().idUser();

    QMessageBox msgBox;
    if (_idUser != 0)
        requestMap["id_user"] = _idUser;
    else
    {
        msgBox.setText("Выберите пользователя");
        msgBox.exec();

        commandSaveUserPermissions();
        return;
    }

    QVariantMap mapVariant;
    for (auto it = _mapPermissions.begin(); it != _mapPermissions.end(); ++it)
    {
        QVariantList list;
        for (auto item : it.value())
            list.append(item);
        mapVariant[QString::number(it.key())] = list;
    }
    requestMap["permissions"] = mapVariant;

    _flagSave = true;
    _accessRightsWidget->isEnabledBtnSaveUserPermissions(_flagSave);

    network::RequestShp request(new network::DefaultRequest("save_permissions_new", QVariant::fromValue(requestMap)));
    _requestsManager->execute(request, this, &AccessRightsController::onGetUserPermissionsResponse);
}

void AccessRightsController::commandGetSavePermissions()
{
//    auto request = [this]()
//    {

        auto senderObj = qobject_cast<CheckedModel*>(sender());

        QVariantMap requestMap;
        requestMap["type_command"] = "save_permissions";

        QVariantList listDepartment;
        auto listDepartmentsCompany = _departmentsModel->getCheckedDepartments(_isSelectDepartment);
        if (listDepartmentsCompany.count() == 0)
        {
            auto item = _departmentsModel->itemFromIndex(_isSelectDepartment);
            if (item == nullptr)
                return;
            listDepartment.append(item->data(Qt::UserRole + 1));
            requestMap["idsDepartament"] = listDepartment;
        }
        else
        {
            requestMap["idsDepartament"] = listDepartmentsCompany;
        }

        requestMap["idUser"] = permissions::UserPermission::instance().idUser();
        requestMap["id_user"] = _idUser;

        if (_departmentsModel->isChecked(_isSelectDepartment))
            requestMap["idCommands"] = _commandsModel->checkedRealIndexMapKeys();
        else
            requestMap["idCommands"] = "";

        if (_commandsModel->checkedRealIndexMapKeys().isEmpty())
        {
//            if ((_departmentsModel->isChecked(_isSelectDepartment)) && (!senderObj))

            if (_departmentsModel->isChecked(_isSelectDepartment))
                if (!senderObj)
                    return;
        }

        QMessageBox msgBox;
        if (_accessRightsWidget->isCurrentListUserIndex())
        {
            network::RequestShp request(new network::DefaultRequest("save_permissions", QVariant::fromValue(requestMap)));
            _requestsManager->execute(request, this, &AccessRightsController::onGetUserPermissionsListResponse);

            _accessRightsWidget->setEnabled(false);
        }
        else
        {
            msgBox.setText("Выберите пользователя");
            msgBox.exec();
        }
//    };

//    Reconnector reconnectorObj(request);
//    _reconnectMap.insert("save_permissions", reconnectorObj);
//    reconnectorObj.run();
}

void AccessRightsController::commandGetSavePermissionsIndex(const QModelIndex &index)
{
    _isSelectDepartment = index;

    if (_departmentsModel->isChecked(_isSelectDepartment))
    {
        _commandsModel->clearChecked();
        _accessRightsWidget->setResetListCommand();
        _accessRightsWidget->isExpandedTreeDepartment();
    }
}

void AccessRightsController::commandGetTemplates(int index)
{
    if (_typeTempalte == TypeTemplate::not_selected)
        return;

    if (_typeTempalte == TypeTemplate::sms_template)
    {
        int idCompany = _companyModel->index(index, 1).data().toInt();
        QVariantMap requestMap;
        requestMap["type_command"] = "get_sms_templates";
        requestMap["id_company"] = idCompany;

        network::RequestShp request(new network::DefaultRequest("get_sms_templates", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &AccessRightsController::onGetTemplatesResponse);
    }
    else if (_typeTempalte == TypeTemplate::email_template)
    {
        int idCompany = _companyModel->index(index, 1).data().toInt();
        QVariantMap requestMap;
        requestMap["type_command"] = "get_email_templates";
        requestMap["id_company"] = idCompany;

        network::RequestShp request(new network::DefaultRequest("get_email_templates", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &AccessRightsController::onGetTemplatesResponse);
    }
}

void AccessRightsController::commandInsertTempalte(int idCompany)
{
    if (_typeTempalte == TypeTemplate::not_selected)
        return;

    if (_typeTempalte == TypeTemplate::sms_template)
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "insert_sms_template";
        requestMap["id_company"] = idCompany;
        requestMap["description"] = "description";
        requestMap["text_message"] = QString();

        network::RequestShp request(new network::DefaultRequest("insert_sms_template", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &AccessRightsController::onResponseInsertTempalte);
    }
    else if (_typeTempalte == TypeTemplate::email_template)
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "insert_email_template";
        requestMap["id_company"] = idCompany;
        requestMap["description"] = "description";
        requestMap["text_message"] = QString();

        network::RequestShp request(new network::DefaultRequest("insert_email_template", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &AccessRightsController::onResponseInsertTempalte);
    }
}

void AccessRightsController::commandUpdateTempalte(int idTemplate, const QString &description, const QString &textTemplate)
{
    if (_typeTempalte == TypeTemplate::not_selected)
        return;

    if (_typeTempalte == TypeTemplate::sms_template)
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "update_sms_template";
        requestMap["id"] = idTemplate;
        requestMap["description"] = description;
        requestMap["text_message"] = textTemplate;

        network::RequestShp request(new network::DefaultRequest("update_sms_template", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &AccessRightsController::onResponseUpdateTempalte);
    }
    else if (_typeTempalte == TypeTemplate::email_template)
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "update_email_template";
        requestMap["id"] = idTemplate;
        requestMap["description"] = description;
        requestMap["text_message"] = textTemplate;

        network::RequestShp request(new network::DefaultRequest("update_email_template", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &AccessRightsController::onResponseUpdateTempalte);
    }
}

void AccessRightsController::onGetUserAccountListResponse(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_user_list"];
        reconnector.run();

        return;
    }

    _users.clear();
    _users.fromVariant(responseObj->bodyToVariant());

    _usersTableModel->setUsers(_users);
}

void AccessRightsController::onGetTemplatesResponse(network::ResponseShp responseObj)
{
    auto listTemplates = responseObj->bodyToVariant().toMap().value("templates").toList();
    _templateModel->clear();

    for (auto &item : listTemplates)
    {
        QList <QStandardItem*> list;
        const auto& mapItem = item.toMap();
        list.append(new QStandardItem(mapItem.value("description").toString()));
        list.append(new QStandardItem(mapItem.value("id").toString()));
        list.append(new QStandardItem(mapItem.value("text_message").toString()));
        _templateModel->appendRow(list);
    }
}

void AccessRightsController::onDoubleClickedUser(const int row)
{
    network::User user = _users.at(row);
    _createUserController.setDataUserControler(user);
    _createUserController.setDbManager(_dbManager);
    _createUserController.run();
}

void AccessRightsController::onBtnDeleteUserClicked(const int row)
{
    network::User user = _users.at(row);
    QVariantMap requestMap;
    requestMap["type_command"] = "delete_user";
    requestMap["id"] = user.id();
    requestMap["idUser"] = permissions::UserPermission::instance().idUser();
    network::RequestShp request(new network::DefaultRequest("delete_user", QVariant::fromValue(requestMap)));
    _requestsManager->execute(request, this, &AccessRightsController::onResponceDeleteUser);
}

void AccessRightsController::onResponceDeleteUser(network::ResponseShp)
{
    getUserAccountList();
}

void AccessRightsController::fillingModelOfAccessRights()
{
    _commandsModel->setQuery("SELECT name, id FROM commands",
                             *(_dbManager->getConnection().data()));

    findName();
    _departmentsModel->blockSignals(true);
    createDepartmentModel();
    _departmentsModel->blockSignals(false);
}

void AccessRightsController::onGetUserPermissionsResponse(network::ResponseShp response)
{
    if (checkResponse(response) != 1)
    {
        auto& reconnector = _reconnectMap["save_permissions"];
        reconnector.run();
    }
}

void AccessRightsController::onGetUserPermissionsListResponse(network::ResponseShp response)
{
    _accessRightsWidget->setEnabled(true);

    if (checkResponse(response) != 1)
    {
        auto& reconnector = _reconnectMap["save_permissions"];
        reconnector.run();

        return;
    }

    const auto responseList = response->bodyToVariant().toList();
    _mapPermissions.clear();

    for (auto element : responseList)
    {
        auto idDepartment = element.toMap()["id_department"].toLongLong();
        auto idPermission = element.toMap()["id_command"].toLongLong();

        _mapPermissions[idDepartment].insert(idPermission);
    }

    _departmentsModel->blockSignals(true);
    _departmentsModel->setCheckedDepartments(_mapPermissions.keys());
    _departmentsModel->blockSignals(false);

    _accessRightsWidget->setResetTreeDepartments();
    _accessRightsWidget->isExpandedTreeDepartment();
    _accessRightsWidget->isEnabledDepartments(true);
}

void AccessRightsController::onResponseInsertTempalte(network::ResponseShp responseObj)
{
    auto mapTemplates = responseObj->bodyToVariant().toMap();

    QList <QStandardItem*> list;
    list.append(new QStandardItem(mapTemplates.value("description").toString()));
    list.append(new QStandardItem(mapTemplates.value("id").toString()));
    list.append(new QStandardItem(mapTemplates.value("message_text").toString()));
    _templateModel->appendRow(list);
}

void AccessRightsController::onResponseUpdateTempalte(network::ResponseShp responseObj)
{
    auto mapTemplates = responseObj->bodyToVariant().toMap();
    const QString id = mapTemplates.value("id").toString();

    auto listItems = _templateModel->findItems(id, Qt::MatchRegExp, id_template);
    QStandardItem *item = listItems.first();
    const int row = item->index().row();

    item = _templateModel->item(row, description);
    item->setText(mapTemplates.value("description").toString());

    item = _templateModel->item(row, message_text);
    item->setText(mapTemplates.value("text_message").toString());
}

void AccessRightsController::onResponseDeleteTempalte(network::ResponseShp responseObj)
{
    auto mapTemplates = responseObj->bodyToVariant().toMap();
    const QString id = mapTemplates.value("id").toString();

    auto listItems = _templateModel->findItems(id, Qt::MatchRegExp, id_template);
    QStandardItem *item = listItems.first();
    const int row = item->index().row();

    _templateModel->removeRow(row);
}

void AccessRightsController::connectMethod()
{
    _companyModel = new QSqlQueryModel();
    _accessRightsWidget->setCompanyModel(_companyModel);

    _workerModel = new QSqlQueryModel();
    _accessRightsWidget->setWorkerModel(_workerModel);

    _departmetModel = new QSqlQueryModel();
    _accessRightsWidget->setDepartamentModel(_departmetModel);

    _natureModel = new QSqlQueryModel();
    _accessRightsWidget->setNatureModel(_natureModel);

    _templateModel = new QStandardItemModel();
    _accessRightsWidget->setTempalateSmsModel(_templateModel);

    createCompanyModel();
    createWorkerModel();

    connect(_accessRightsWidget, &AccessRightsWidget::signalRefreshNature,
            this, &AccessRightsController::createNatureModel);

    connect(_accessRightsWidget, &AccessRightsWidget::signalChangedCompany,
            this, &AccessRightsController::createDepartamentsModel);

    connect(_accessRightsWidget, &AccessRightsWidget::signalSaveAdd,
            this, &AccessRightsController::saveAddCommand);

    connect(_accessRightsWidget, &AccessRightsWidget::signalSaveUpdate,
            this, &AccessRightsController::saveUpdateCommand);

    connect(_accessRightsWidget, &AccessRightsWidget::signalSaveNatureAdd,
            this, &AccessRightsController::saveAddNatureCommand);

    connect(_accessRightsWidget, &AccessRightsWidget::signalSaveNatureUpdate,
            this, &AccessRightsController::saveUpdateNatureCommand);

    connect(_accessRightsWidget, &AccessRightsWidget::signalDelete,
            this, &AccessRightsController::deleteCommand);
}

void AccessRightsController::createCompanyModel()
{
    const auto queryStr = QString("SELECT name, id FROM company WHERE isVisible = 1 ORDER BY id");
    auto query = _dbManager->getDBWraper()->query();
    query.prepare(queryStr);
    query.exec();

    _companyModel->setQuery(query);
}

void AccessRightsController::createWorkerModel()
{
    const auto queryStr = QString("SELECT data_target, id FROM complaints_target WHERE isVisible = 1 ORDER BY id");
    auto query = _dbManager->getDBWraper()->query();
    query.prepare(queryStr);
    query.exec();

    _workerModel->setQuery(query);
}

void AccessRightsController::createDepartamentsModel(const int idCompany)
{
    const auto queryStr = QString("SELECT name, id FROM departments WHERE isVisible = 1 AND id_company = :idCompany ORDER BY id");
    auto query = _dbManager->getDBWraper()->query();
    query.prepare(queryStr);
    query.bindValue(":idCompany", idCompany);
    if (!query.exec())
        qDebug() << query.lastError();

    _departmetModel->setQuery(query);
}

void AccessRightsController::createNatureModel(const int idCompany, const int idTarget)
{
    const auto queryStr = QString("SELECT data_nature, hint_essence, days_to_review, id, default_rating, cost, type FROM complaints_nature"
                                  " WHERE id_company = %1 AND id_target = %2 AND id_visiable = 1 ORDER BY id")
                          .arg(QString::number(idCompany)).arg(QString::number(idTarget));
    auto query = _dbManager->getDBWraper()->query();
    query.prepare(queryStr);
    query.exec();

    _natureModel->setQuery(query);
}

void AccessRightsController::onGetTableUpdate(network::ResponseShp responseObj)
{
    auto response = responseObj->toVariant().toMap();
    const auto responseMap = response["body"].toMap();

    const auto dbWraper = _dbManager->getDBWraper();

    QList<QSqlQuery> queriesList = database::DBHelpers::createInsertQueries(responseMap, dbWraper);
    for (QSqlQuery& query : queriesList)
    {
        const bool result = query.exec();
        if (!result)
        {
            qDebug() << __FUNCTION__ << query.lastError().text();
            qDebug() << __FUNCTION__ << query.lastQuery();
        }
    }

    createCompanyModel();
    createWorkerModel();
    createDepartamentsModel(_accessRightsWidget->getModelIndexCompany());
    createNatureModel(_accessRightsWidget->getModelIndexCompany(), _accessRightsWidget->getModelIndexTarget());
    _accessRightsWidget->hideWidgets();
}

void AccessRightsController::createDepartmentModel()
{
    auto dbWraper = _dbManager->getDBWraper();
    auto newQuery = dbWraper->query();
    newQuery.prepare("SELECT name, id FROM company WHERE isVisible = 1");
    bool result = dbWraper->execQuery(newQuery);
    if (!result)
    {
        qDebug() << "ERROR READ TABLE COMPANY" << endl;
        Q_ASSERT(true);
    }

    const auto companiesList = database::DBHelpers::queryToVariant(newQuery);

    newQuery.prepare("SELECT name, id, id_company FROM departments WHERE isVisible = 1 ORDER BY id");
    result = dbWraper->execQuery(newQuery);
    if (!result)
    {
        qDebug() << "ERROR READ TABLE COMPANY" << endl;
        Q_ASSERT(true);
    }

    const auto departmentsList = database::DBHelpers::queryToVariant(newQuery);

    QList< QStandardItem* > listItems;
    QMap <QStandardItem*, quint64> mapItemsId;
    for (auto company : companiesList)
    {
        auto nameCompany = company.toMap()["name"].toString();
        auto idCompany = company.toMap()["id"].toLongLong();

        auto itemCompany = new QStandardItem(nameCompany);
        itemCompany->setData(idCompany, Qt::UserRole + 1);
        itemCompany->setData(true, Qt::UserRole + 2);

        mapItemsId.clear();
        for (auto department : departmentsList)
        {
            auto nameDepartment      = department.toMap()["name"].toString();
            auto idDepartment        = department.toMap()["id"].toLongLong();
            auto idCompanyDepartment = department.toMap()["id_company"].toLongLong();

            if (idCompany == idCompanyDepartment)
            {
                auto itemDepartment = new QStandardItem(nameDepartment);
                itemDepartment->setData(idDepartment, Qt::UserRole + 1);
                itemDepartment->setData(false, Qt::UserRole + 2);
                itemDepartment->setData(idCompanyDepartment, Qt::UserRole + 3);

                mapItemsId[itemDepartment] = idDepartment;
                itemCompany->setChild(itemCompany->rowCount(), itemDepartment);
            }
        }
        _mapCompany[itemCompany] = mapItemsId;
        listItems.append(itemCompany);
    }

    _departmentsModel->appendColumn(listItems);
    _departmentsModel->setHeaderData(COLUMN_NAME_DEPARTMENTS, Qt::Horizontal, QVariant("Департаменты"));
}

void AccessRightsController::openDepartmentPermissions(quint64 idDepartment)
{
    if (_mapPermissions.contains(idDepartment))
    {
        _commandsModel->setChecked(_mapPermissions[idDepartment].values());
        _accessRightsWidget->setResetListCommand();
    }
}

void AccessRightsController::onClickDepartment(const QModelIndex &index)
{
    QString text = "";
    if (_departmentsModel->isChecked(index))
    {
         _isSelectDepartment = index;
        auto item = _departmentsModel->itemFromIndex(index);
        if (_mapCompany.contains(item))
        {
            text = "Укажите права для депортаментов компании: " + item->text();
            _commandsModel->clearChecked();
            _accessRightsWidget->setResetListCommand();
        }

        else
        {
            _commandsModel->clearChecked();
            _accessRightsWidget->setResetListCommand();

            text = "Укажите права для депортамента: " + item->text();
            openDepartmentPermissions(item->data(Qt::UserRole + 1).toLongLong());
        }
        _accessRightsWidget->isEnabledPermissions(true);
    }
    else
    {
        _commandsModel->clearChecked();
        _accessRightsWidget->setResetListCommand();

        if (index.data(Qt::UserRole + 2).toBool())
        {
            auto item = _departmentsModel->itemFromIndex(_isSelectDepartment);
            if (item == nullptr)
                return;

            auto map = _mapCompany.value(item);
            for (auto it = map.begin(); it != map.end(); ++it)
                _mapPermissions.remove(it.value());
        }
        else
            _mapPermissions.remove(index.data(Qt::UserRole + 1).toULongLong());

        _flagSave = false;
        _accessRightsWidget->isEnabledBtnSaveUserPermissions(_flagSave);
        _isSelectDepartment = QModelIndex();

        text = "Выберите департамент или компанию";
        _accessRightsWidget->isEnabledPermissions(false);
    }

    _accessRightsWidget->setTextDepartment(text);
}

void AccessRightsController::refreshDepartments(const QModelIndex &index)
{
    const auto newUser = _usersModel->index(index.row(), _usersModel->columnCount() - 1).data().toLongLong();
    if (newUser == _idUser)
        return;

    if (!_flagSave)
    {
        QMessageBox msgBox;
        msgBox.setText("Сохранить права доступа?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();

        if (ret == QMessageBox::Save)
            commandSaveUserPermissions();
    }

    _flagSave = true;
    _accessRightsWidget->isEnabledBtnSaveUserPermissions(_flagSave);

    _accessRightsWidget->isEnabledPermissions(false);
    _commandsModel->clearChecked();
    _accessRightsWidget->setResetListCommand();

    _idUser = newUser;
    QVariantMap map;
    map["command"] = "get_user_permissions";
    map["id_user"] =  _idUser;

    network::RequestShp request(new network::DefaultRequest("get_user_permissions", map));
    _requestsManager->execute(request, this, &AccessRightsController::onGetUserPermissionsListResponse);
}

void AccessRightsController::saveAddCommand(const QString &text, const QString &table)
{
    QVariantMap map;
    map["command"] = "add_data";
    map["idUser"] = permissions::UserPermission::instance().idUser();
    map["table"] = table;
    map["name"] = text;
    if (table == "departments")
        map["idCompany"] = _accessRightsWidget->getModelIndexCompany();

    network::RequestShp request(new network::DefaultRequest("add_data", map));
    _requestsManager->execute(request, this, &AccessRightsController::onGetTableUpdate);
}

void AccessRightsController::saveUpdateCommand(const QString &text, const QString &table, const int id)
{
    QVariantMap map;
    map["command"] = "update_data";
    map["idUser"] = permissions::UserPermission::instance().idUser();
    map["table"] = table;
    map["name"] = text;
    map["id"] = id;
    if (table == "departments")
        map["idCompany"] = _accessRightsWidget->getModelIndexCompany();

    network::RequestShp request(new network::DefaultRequest("update_data", map));
    _requestsManager->execute(request, this, &AccessRightsController::onGetTableUpdate);
}

void AccessRightsController::saveAddNatureCommand(const QString &text, const int day, const QString &essense,
                                                  const QString &table, const QString &cost, const int rating, int type)
{
    QVariantMap map;
    map["command"] = "add_data";
    map["idUser"] = permissions::UserPermission::instance().idUser();
    map["table"] = table;
    map["name"] = text;
    map["day"] = day;
    map["essence"] = essense;
    map["idCompany"] = _accessRightsWidget->getModelIndexCompany();
    map["idTarget"] =  _accessRightsWidget->getModelIndexTarget();
    map["default_rating"] = rating;
    map["cost"] = cost;
    map["type"] = type;

    network::RequestShp request(new network::DefaultRequest("add_data", map));
    _requestsManager->execute(request, this, &AccessRightsController::onGetTableUpdate);
}

void AccessRightsController::saveUpdateNatureCommand(const QString &text, const int day, const QString &essense,
                                                     const QString &table, const QString &cost, const int id, const int rating, int type)
{
    QVariantMap map;
    map["command"] = "update_data";
    map["idUser"] = permissions::UserPermission::instance().idUser();
    map["table"] = table;
    map["name"] = text;
    map["day"] = day;
    map["essence"] = essense;
    map["id"] = id;
    map["idCompany"] = _accessRightsWidget->getModelIndexCompany();
    map["idTarget"] =  _accessRightsWidget->getModelIndexTarget();
    map["default_rating"] = rating;
    map["cost"] = cost;
    map["type"] = type;

    network::RequestShp request(new network::DefaultRequest("update_data", map));
    _requestsManager->execute(request, this, &AccessRightsController::onGetTableUpdate);
}

void AccessRightsController::deleteCommand(const int id, const QString &table)
{
    QVariantMap map;
    map["command"] = "delete_data";
    map["idUser"] = permissions::UserPermission::instance().idUser();
    map["table"] = table;
    map["id"] = id;

    network::RequestShp request(new network::DefaultRequest("delete_data", map));
    _requestsManager->execute(request, this, &AccessRightsController::onGetTableUpdate);
}

void AccessRightsController::findName(const QString name)
{
    QString sql = QString("SELECT name, id_department, id FROM users WHERE isVisible = 0 AND name LIKE '%1%'").arg(name);
    _usersModel->setQuery(sql, *(_dbManager->getConnection().data()));
}

void AccessRightsController::createUserControllerRun()
{
    _createUserController.setDbManager(_dbManager);
    _createUserController.run();
}

void AccessRightsController::getUserAccountList()
{
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["idUser"] = permissions::UserPermission::instance().idUser();

        network::RequestShp request(new network::DefaultRequest("get_user_list", requestMap));
        _requestsManager->execute(request, this, &AccessRightsController::onGetUserAccountListResponse);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_user_list", reconnectorObj);
    reconnectorObj.run();
}

void AccessRightsController::createTestReview()
{
    QVariantMap requestMap;
    QVariantMap car;
    QVariantMap driver;
    QVariantMap answer;

    requestMap["id_company"] = 3;
    requestMap["order_id"] = 25515251;
    requestMap["id_source"] = 3;
    requestMap["comment"] = "TEST DEVELOPER";

    answer["1"] = 0;
    answer["2"] = 1;
    answer["3"] = 2;
    answer["4"] = 0;
    answer["5"] = 1;
    answer["6"] = 2;

    driver["id"] = 123;
    driver["family"] = "test";
    driver["name"] = "test";
    driver["sec_fam"] = "test";
    driver["date_come"] = "2016-01-01";
    driver["our"] = 1;
    driver["park"] = 1;
    driver["column"] = 1;
    driver["city"] = 1;
    driver["form"] = 1;
    driver["rating"] = 4;
    driver["cnt_rating"] = 0;

    car["id"] = 123456;
    car["number"] = "123test123";
    car["model"] = "opel";
    car["color"] = "red";

    requestMap["type_command"] = "insert_review";
    requestMap["car"] = car;
    requestMap["driver"] = driver;
    requestMap["answers"] = answer;
    requestMap["idUser"] = permissions::UserPermission::instance().idUser();

    network::RequestShp request(new network::DefaultRequest("insert_review", QVariant::fromValue(requestMap)));
    _requestsManager->execute(request, this, &AccessRightsController::onCreateTestReview);
}

void AccessRightsController::slotSelectTypeTempale(int type)
{
    _typeTempalte = (TypeTemplate)type;
    if (_typeTempalte != TypeTemplate::not_selected)
        _accessRightsWidget->onChangedNCompany();
}

void AccessRightsController::commandRemoveTemplate(int idTemplate)
{
    if (_typeTempalte == TypeTemplate::not_selected)
        return;

    if (_typeTempalte == TypeTemplate::sms_template)
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "delete_sms_template";
        requestMap["id"] = idTemplate;

        network::RequestShp request(new network::DefaultRequest("delete_sms_template", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &AccessRightsController::onResponseDeleteTempalte);
    }
    else if (_typeTempalte == TypeTemplate::email_template)
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "delete_email_template";
        requestMap["id"] = idTemplate;

        network::RequestShp request(new network::DefaultRequest("delete_email_template", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &AccessRightsController::onResponseDeleteTempalte);
    }
}
