﻿#pragma once


#include "Controllers/CreateComplaintController.h"
#include "Controllers/ComplaintInfoController.h"
#include "Controllers/RecallInfoController.h"
#include "Controllers/CreateUserController.h"
#include "Models/UsersTableModel.h"
#include "ComplaintsControllers/ComplaintsWidgetController.h"
#include "ReviewsControllers/ReviewsWidgetController.h"
#include "ComplaintsControllers/DriversWidgetController.h"
#include "ComplaintsControllers/OperatorsWidgetController.h"
#include "ComplaintsControllers/LogistsWidgetController.h"
#include "ComplaintsControllers/ClientsWidgetController.h"
#include "AccessRightsControllers/AccessRightsController.h"

#include "../Reconnector.h"


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace session_controller
{
    class Session;
    typedef QSharedPointer<Session> SessionShp;
}

namespace progress_indicator {
    class ProgressIndicator;
}


namespace qat_client
{

    class MainWindow;

    class CheckedModel;


    class MainWindowController : public QObject
    {
        Q_OBJECT

    public:
        struct Options
        {
            int fetchData;
        };

        struct Settings
        {
            Settings()
            {
                clear();
            }

            int _tabIndex;
            int _tabIndexComplaints;
            QMap<int, QString> _mapComplaints;

            void clear()
            {
                _tabIndex = 0;
                _tabIndexComplaints = 0;
                _mapComplaints.clear();
            }
        };

    public:
        MainWindowController(QObject *parent = nullptr);
        ~MainWindowController();

        void setOptions(const Options& options);
        void setDBManager(const database::DBManagerShp& dbManager);
        void setSession(const session_controller::SessionShp& session);
        void setVersion(const QString & version);

        void run();

    private slots:
        // NOTE: send commands
        void commandGetNewDBData();
        void commandFetchNewData();
        void tabChangedComplaints(const Settings& data);

        // NOTE: data processing
        void onGetNewDbData(network::ResponseShp);
        void onFetchNewData(network::ResponseShp);
        void onStatistic(network::ResponseShp);
        void onStatisticReviews(network::ResponseShp responseObj);
        
        // NOTE: transfer of control to controllers
        void openReviewFromComplaint(const quint64 id);
        void openComplaintFromReview(const quint64 id);
        void openComplaintsFromWorker(const quint64 idWorker, const int keyWotker, const int statusTarget);
        void createStatistic(const QDateTime dateStart, const QDateTime dateEnd, QVariantList idsComany, QVariantList idsTarget);
        void createExcelFile(const QString & fileName);
        void createStatisticReviews(const QDateTime dateStart, const QDateTime dateEnd, QVariantList ids);
        void createExcelFileReviews(const QString & fileName);

        void onParseNewDbDataFinished();
        void createConnectMainWindowStatusBar();

    signals:
        void signalShowMessageStatusBar(const QString &msg, quint32 sec = 0);

    private:
        void parseNewDbData(const QVariantMap& inData);
        void initControllers();
        bool parseObjFetchNewData(const QVariantMap & obj);
        void createSourceModelStatistic(QVariantList body);
        void createDepartmantsModelStatistic(QVariantList body);
        void createInfoModelStatistic(QVariantMap body);
        void createInfoModelStatisticReviews(QVariantMap body);
        void createModelCompany();
        void createModelStatisticTarget();

        bool companyByName(const QString& name);
        void selectCountReccord();

    private:
        database::DBManagerShp _dbManager;
        network::RequestsManagerShp _requestsManager;
        session_controller::SessionShp _session;
        progress_indicator::ProgressIndicator *_indicator;

        MainWindow *_mainWindow = nullptr;
        Options _options;
        QDateTime _lastFetch;
        QTimer *_fetchTimer;
        Settings _settings;

        QString _version;

        // NOTE: controllers
        ComplaintsWidgetController _complaintsWidgetController;
        DriversWidgetController _driversWidgetController;
        OperatorsWidgetController _operatorsWidgetController;
        LogistsWidgetController _logistsWidgetController;
        ReviewsWidgetController _reviewsWidgetController;
        ClientsWidgetController _clientsWidgetController;
        AccessRightsController _accessRightsController;

        QMap<QString, Reconnector> _reconnectMap;

        QStandardItemModel* _modelInfo;
        QStandardItemModel* _modelInfoReviews;
        QStandardItemModel* _modelDepartmentsInfo;
        QStandardItemModel* _modelSourceInfo;
    };

    typedef QSharedPointer<MainWindowController> MainWindowControllerShp;

}
