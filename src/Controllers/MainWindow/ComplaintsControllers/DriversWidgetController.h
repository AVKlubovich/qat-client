﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/WorkerInformationController/WorkerInformationController.h"


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class DriversWidget;

    class CheckedModel;


    class DriversWidgetController : public QObject
    {
        Q_OBJECT

    public:
        struct SettingsDrivers
        {
            SettingsDrivers()
            {
                clear();
            }
            
            QDateTime _dateStart;
            QDateTime _dateEnd;
            bool _isSetDateEnd;
            QList<quint64> _nature;
            QList<quint64> _companies;
            QString _like;

            void clear()
            {
                _nature.clear();
                _companies.clear();
                _like = "";
                _isSetDateEnd = false;
                _dateStart = QDateTime::currentDateTime().addDays(-4);
                _dateStart.setTime(QTime(0, 0));
                _dateEnd = QDateTime::currentDateTime().addSecs(3000);
                _isSetDateEnd = false;
            }
        };

    public:
        DriversWidgetController();
        ~DriversWidgetController();

        void setDbManager(database::DBManagerShp dbManager);
        void setDriversWidget(DriversWidget * driversWidget);

    private:
        void createInfoStatusBar(const QString &params, const QVariantList &sqlParameters);

    public slots:
        void init();
        void run();
        void update();

        void fillingEditsInfoComplaint(const quint64 id);
        void finishedRefreshComplaints();

    private slots:
        // NOTE: send commands
        void commandGetSelectDrivers(const SettingsDrivers & data);

        // NOTE: data processing
        void refreshDrivers(const SettingsDrivers& data);

        // NOTE: transfer of control to controllers
        void createWorkerInfoControllerRun();
        void fillNature();

        void createExcelFile(const QString &fileName);

    signals:
        void signalRefresh(const QDateTime dateTime);
        void signalOpenComplaintsWorker(const quint64 idWorker, const int keyWorker, const int statusType);
        void updateCompanies();

        void signalStatusBarValue(const QMap<int, int> &map, bool flag);
        void signalShowMessageStatusBarDrivers(const QString &msg, quint32 sec = 0);

    private:
        network::RequestsManagerShp _requestsManager;
        database::DBManagerShp _dbManager;

        DriversWidget *_driversWidget = nullptr;
        QDateTime _lastDateTimeDbaseDrivers;
        SettingsDrivers _settings;

        // NOTE: controllers
        WorkerInformationController _workerInfoController;

        // NOTE: models
        QSqlQueryModel* _driversModel;

        CheckedModel* _companiesModel;
        CheckedModel* _natureModel;

        static const QString _defaultSqlDrivers;
        static const QString _defaultSqlDriversCount;

        bool _isInintSettings = false;
        static const QString _defaultSqlExcel;

        bool _flagRefreshDrivers = false;
    };

}
