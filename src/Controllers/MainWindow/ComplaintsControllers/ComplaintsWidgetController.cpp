#include "Common.h"
#include "ComplaintsWidgetController.h"

#include "Widgets/MainWindow/ComplaintsWidgets/ComplaintsWidget/ComplaintsWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/Users/Users.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "utils/Sql/SqlString.h"

#include "Models/ComplaintsModel.h"
#include "Models/CheckedModel.h"
#include "Models/CheckedStandardModel.h"

#include "permissions/UserPermission.h"
#include "Controllers/ResponceChecker.h"


using namespace qat_client;

const QString ComplaintsWidgetController::_defaultSqlComplaints(QString(
        "SELECT "
            "complaints.id AS '№', "
            "complaints_status.name_status AS 'Статус', "
            "%1 AS 'Дата создания', "
            "company.name AS 'Компания', "
            "complaints_source.name_source AS 'Откуда', "
            "departments.name AS 'Отдел', "
            "complaints_type.name AS 'Тип', "
            "complaints.id_order AS '№ заказа', "
            "complaints_nature.data_nature AS 'Характер', "
            "complaints_target.data_target AS 'На кого', "

            "CASE WHEN complaints_nature.id_target = 1 THEN "
            " driver.surname || ' ' || driver.firstname || ' ' || driver.middlename "
            "ELSE "
                  " CASE WHEN complaints_nature.id_target = 2 THEN operator.fio"
                  " ELSE CASE WHEN complaints_nature.id_target = 3 THEN logist.fio"
                        " ELSE NULL "
                        " END "
                  "END "
             "END AS 'ФИО', "

            "complaints_nature.id_target, "
            "complaints.feedback, "
            "complaints.days_to_process, "
            "complaints.id_status "
        "FROM complaints "
        "INNER JOIN complaints_status ON (complaints.id_status = complaints_status.id) "
        "INNER JOIN complaints_source ON (complaints.id_source = complaints_source.id) "
        "INNER JOIN departments ON (complaints.id_department = departments.id) "
        "INNER JOIN complaints_type ON (complaints.id_type = complaints_type.id) "
        "INNER JOIN company ON (company.id = complaints.id_company) "
        "INNER JOIN complaints_nature ON (complaints_nature.id = complaints.id_nature) "
        "INNER JOIN complaints_target ON (complaints_nature.id_target = complaints_target.id) "
        " LEFT JOIN complaints_orders_drivers AS driver ON (driver.id_complaint = complaints.id)"
        " LEFT JOIN complaints_orders_logists AS logist ON (logist.id_complaint = complaints.id)"
        " LEFT JOIN complaints_orders_operators AS operator ON (operator.id_complaint = complaints.id) "
        ).arg(sql_utils::stringSqlDate));

const QString ComplaintsWidgetController::_defaultSqlExcel(
        "SELECT"
        " c.id AS id,"
        " c.id_order AS idOrder,"
        " %1,"
        " target.data_target AS target,"
        " nature.data_nature AS nature,"
        " c.date_create AS dateCreate,"
        " c.essence AS essence,"
        " status.name_status AS status,"
        " department.name AS nameDepartment,"
        " auto.number AS carNumber,"
        " c.id_company AS idCompany"
        " FROM complaints AS c"
        " LEFT JOIN %2 AS driver ON(c.id = driver.id_complaint)"
        " INNER JOIN complaints_nature AS nature ON (nature.id = c.id_nature)"
        " INNER JOIN complaints_target AS target ON (target.id = nature.id_target)"
        " INNER JOIN complaints_status AS status ON (status.id = c.id_status)"
        " INNER JOIN departments AS department ON (c.id_department = department.id)"
        " LEFT JOIN complaints_orders_cars AS auto ON (c.id = auto.id_complaint)"
        " WHERE c.id = :idComplaint"
        );

ComplaintsWidgetController::ComplaintsWidgetController()
    : QObject(nullptr)
    , _requestsManager(network::RequestsManagerShp::create())
{
}

ComplaintsWidgetController::~ComplaintsWidgetController()
{
    if (_complaintsModel)
        _complaintsModel->deleteLater();
}

void ComplaintsWidgetController::setDbManager(database::DBManagerShp dbManager)
{
    _dbManager = dbManager;
}

void ComplaintsWidgetController::setComplaintsWidget(ComplaintsWidget * complaintsWidget)
{
    _complaintsWidget = complaintsWidget;
}

void ComplaintsWidgetController::init()
{
    _complaintsModel = new ComplaintsModel();
    _complaintsWidget->setComplaintsModel(_complaintsModel);

    _statusesModel = new CheckedModel();
    _complaintsWidget->setStatusesModel(_statusesModel);

    _sourcesModel = new CheckedModel();
    _complaintsWidget->setSourcesModel(_sourcesModel);

    _targetsModel = new CheckedModel();
    _complaintsWidget->setTargetsModel(_targetsModel);

    _companiesModel = new CheckedModel();
    _complaintsWidget->setCompaniesModel(_companiesModel);

    _natureModel = new CheckedModel();
    _complaintsWidget->setNatureModel(_natureModel);

    _feedBackModel = new CheckedStandardModel();
    _complaintsWidget->setFeedBackModel(_feedBackModel);
    createItemsFeedBackModel();

    createComplaintInfoControllerRun();

    connect(_complaintsWidget, &ComplaintsWidget::signalCreateComplaint,
            this, &ComplaintsWidgetController::createComplaintControllerRun);

    connect(_complaintsWidget, &ComplaintsWidget::signalSelectComplaints,
            this, &ComplaintsWidgetController::commandGetSelectComplaints);

    connect(_complaintsWidget, &ComplaintsWidget::signalFillNature,
            this, &ComplaintsWidgetController::fillNature);

    connect(_complaintsWidget, &ComplaintsWidget::signalUpdateSourceModel,
            this, &ComplaintsWidgetController::fillSources);

    connect(_complaintsWidget, &ComplaintsWidget::complaintSelected,
            this, &ComplaintsWidgetController::fillingEditsInfoComplaint);

    connect(_complaintsWidget, &ComplaintsWidget::signalCreateComplaintsExcelFile,
            this, &ComplaintsWidgetController::createExcelFile);

    connect(_complaintsWidget, &ComplaintsWidget::signalResetModel,
            this, &ComplaintsWidgetController::sResetBtnClicked);

    connect(this, &ComplaintsWidgetController::updateCompanies,
            _complaintsWidget, &ComplaintsWidget::updateCompanies);

    connect(_complaintsWidget, &ComplaintsWidget::signalCheckedAll,
            [this](){
        _companiesModel->checkedAll();
        emit updateCompanies();
    });
    connect(_complaintsWidget, &ComplaintsWidget::signalUncheckedAll,
            [this](){
        _companiesModel->checkedClearAll();
        emit updateCompanies();
    });

    auto func = [this]()
    {
        _complaintInfoController.hideWidget();
    };
    connect(_complaintsWidget, &ComplaintsWidget::signalHideWidget, this, func);

    connect(&_complaintInfoController, &ComplaintInfoController::signalIdReviewOpen,
            this, &ComplaintsWidgetController::signalIdReviewOpen);
    connect(&_complaintInfoController, &ComplaintInfoController::signalInfoControllerFindComplaints,
            this, &ComplaintsWidgetController::slotFindCompalints);
}

void ComplaintsWidgetController::run()
{
    if (_isInintSettings)
    {
        update();
        return;
    }
    _isInintSettings = true;

    // NOTE: желательно заюзать врапер и у него метод query()
    _statusesModel->setQuery("SELECT name_status, id FROM complaints_status",
                             *(_dbManager->getConnection().data()));
    _statusesModel->setCheckedAll();

    _targetsModel->setQuery("SELECT data_target, id FROM complaints_target WHERE isVisible = 1",
                            *(_dbManager->getConnection().data()));
    _targetsModel->setCheckedAll();

    _companiesModel->setQuery("SELECT name, id FROM company WHERE isVisible = 1",
                              *(_dbManager->getConnection().data()));
    _companiesModel->setCheckedAll();

    fillSources();

    _settings._dateStart = QDateTime::currentDateTime().addDays(-4);
    _settings._dateStart.setTime(QTime(0, 0));
    _settings._dateEnd = QDateTime::currentDateTime().addSecs(3000);
    _settings._isSetDateEnd = false;
    _lastDateTimeDbaseComplaints = _settings._dateStart;
    _settings._companies = _companiesModel->getListId();
    _complaintsWidget->setSettings(_settings, true);

    QStringList feedBack;
    auto list = _feedBackModel->getCheckedList();
    for (auto it : list)
        feedBack.append(QString::number(it));
    _settings._feedback = feedBack;

    refreshComplaints(_settings);
}

void ComplaintsWidgetController::update()
{
    commandGetSelectComplaints(_settings);
}

void ComplaintsWidgetController::commandGetSelectComplaints(const SettingsComplaints & data)
{
    _settings = data;

    if (data._dateStart < _lastDateTimeDbaseComplaints)
    {
        auto request = [this]()
        {
            QVariantMap requestMap;
            requestMap["type_command"] = "get_select_complaints";
            requestMap["date_last_query"] = QDateTime::currentDateTime();
            requestMap["date_start"] = _settings._dateStart.toString("yyyy-MM-dd hh:mm:ss");
            requestMap["date_end"] = _lastDateTimeDbaseComplaints.toString("yyyy-MM-dd hh:mm:ss");
            requestMap["idUser"] = permissions::UserPermission::instance().idUser();
            network::RequestShp request(new network::DefaultRequest("get_select_complaints", QVariant::fromValue(requestMap)));
            _requestsManager->execute(request, this, &ComplaintsWidgetController::onGetSelectComplaints);
            _lastDateTimeDbaseComplaints = _settings._dateStart;
        };

        Reconnector reconnectorObj(request);

        _reconnectMap.insert("get_select_complaints", reconnectorObj);
        reconnectorObj.run();
    }
    else
    {
        refreshComplaints(_settings);
    }
}

void ComplaintsWidgetController::slotFindCompalints(QVariantList listIdsComplaints)
{
    _findIdsComplaints = listIdsComplaints;

    auto request = [this, listIdsComplaints]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_select_ids_complaints";
        requestMap["list_ids"] = listIdsComplaints;
        requestMap["idUser"] = permissions::UserPermission::instance().idUser();
        network::RequestShp request(new network::DefaultRequest("get_select_ids_complaints", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &ComplaintsWidgetController::onGetSelectComplaints);
    };

    Reconnector reconnectorObj(request);
    _reconnectMap.insert("get_select_ids_complaints", reconnectorObj);
    reconnectorObj.run();
}

void ComplaintsWidgetController::onGetSelectComplaints(network::ResponseShp responseObj)
{
    const auto& responseMap = responseObj->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& body = responseMap["body"].toMap();
    const auto& typeCommand = head.value("type").toString();

    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap[typeCommand];
        reconnector.run();

        return;
    }

//    const auto& future = QtConcurrent::run(this, &ComplaintsWidgetController::parseSelectComplaints, body);
//    QFutureWatcher<void> *fWatcher = new QFutureWatcher<void>();
//    if (typeCommand == "get_select_complaints")
//        connect(fWatcher, &QFutureWatcher<void>::finished, this, &ComplaintsWidgetController::onParseSelectComplaintsFinished);
//    else
//        connect(fWatcher, &QFutureWatcher<void>::finished, this, &ComplaintsWidgetController::onParseSelectIdsComplaintsFinished);
//    connect(fWatcher, &QFutureWatcher<void>::finished, fWatcher, &QFutureWatcher<void>::deleteLater);
//    fWatcher->setFuture(future);

    parseSelectComplaints(body);

    emit signalShowMessageStatusBarComplaints("Данные загружаются", 2);

    if (typeCommand == "get_select_complaints")
        onParseSelectComplaintsFinished();
    else
        onParseSelectIdsComplaintsFinished();
}

void ComplaintsWidgetController::parseSelectComplaints(const QVariantMap& inData)
{
    const auto& dbWraper = _dbManager->getDBWraper();

    QList<QSqlQuery> queriesList = database::DBHelpers::createInsertQueries(inData, dbWraper);
    for (QSqlQuery& query : queriesList)
    {
        if (!query.exec())
        {
            qDebug() << __FUNCTION__ << query.lastError().text() << query.lastQuery();
        }
    }
}

void ComplaintsWidgetController::onParseSelectComplaintsFinished()
{
    refreshComplaints(_settings);
    emit finishedRefreshComplaints();
}

void ComplaintsWidgetController::onParseSelectIdsComplaintsFinished()
{
    refreshFindComplaints();
    emit finishedRefreshComplaints();
}

void ComplaintsWidgetController::refreshComplaints(const SettingsComplaints & data)
{
    _flagStartFetch = true;

    QStringList sqlQueries;
    QList<QVariant> sqlParameters;
    sql_utils::SqlString sqlStr;


    if (data._companies.count() == 1)
        sqlStr.addSqlParameters(data._nature, "complaints", "_nature", sqlQueries, sqlParameters);

    sqlStr.addSqlParameters(data._statuses, "complaints", "_status", sqlQueries, sqlParameters);
    sqlStr.addSqlParameters(data._targets, "complaints", "_target", sqlQueries, sqlParameters);
    sqlStr.addSqlParameters(data._companies, "complaints", "_company", sqlQueries, sqlParameters);
    sqlStr.addSqlParameters(data._sources, "complaints", "_source", sqlQueries, sqlParameters);
    sqlStr.addSqlLike(data._like, "complaints", sqlQueries, sqlParameters);

    const auto stringDepartmentsUser = permissions::UserPermission::instance().getDepartmentsUser().join(", ");

    const QDateTime& endDate(!data._isSetDateEnd ? QDateTime::currentDateTime().addSecs(3000) : data._dateEnd);

    QString params = "WHERE (complaints.date_create "
                     "BETWEEN strftime('%Y-%m-%dT%H:%M:%S', DATETIME('" + data._dateStart.toString("yyyy-MM-dd hh:mm:ss") + "')) " +
                     "AND strftime('%Y-%m-%dT%H:%M:%S', DATETIME("
                     "'" + endDate.toString("yyyy-MM-dd hh:mm:ss") + "'" + "))) " +
                     (sqlQueries.count() > 0 ? ("AND (" + sqlQueries.join(" AND ") + ")") : "") +
                     " AND complaints.id_department IN (" + stringDepartmentsUser + ") "
                     " AND complaints.feedback IN (" + data._feedback.join(",") + ")"
                     "%1 "
                     "ORDER BY complaints.date_create DESC";
    const QString& currentSqlComplaints = _defaultSqlComplaints + params.arg("");

    auto newQuery = _dbManager->getDBWraper()->query();
    if (!newQuery.prepare(currentSqlComplaints))
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }

    for (int i = 0; i < sqlParameters.count(); i++)
    {
        newQuery.bindValue(i, sqlParameters[i]);
    }

    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    _complaintsModel->setQuery(newQuery);
    _complaintsWidget->updateComplaints();

    if (!newQuery.first())
        emit signalShowMessageStatusBarComplaints("По вашему запросу жалоб не найдено", 5);

    if (_settings._idSelectedComplaint != 0)
    {
        fillingEditsInfoComplaint(_settings._idSelectedComplaint);
        _settings._idSelectedComplaint = 0;
    }

    createInfoStatusBar(params, sqlParameters);
}

void ComplaintsWidgetController::refreshFindComplaints()
{
    _flagStartFetch = false;

    QStringList listIds;
    for (auto id : _findIdsComplaints)
        listIds << id.toString();

    const QString& currentSqlComplaints = _defaultSqlComplaints + QString(
                                       "WHERE complaints.id IN (%1)"
                                       "ORDER BY complaints.date_create DESC")
                                   .arg(listIds.join(", "));

    auto newQuery = _dbManager->getDBWraper()->query();
    if (!newQuery.prepare(currentSqlComplaints))
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }

    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    _complaintsModel->setQuery(newQuery);
    _complaintsWidget->updateComplaints();
    _complaintsWidget->setResetBtn(false);
    _complaintInfoController.hideWidget();

    if (!newQuery.first())
        emit signalShowMessageStatusBarComplaints("По вашему запросу жалоб не найдено", 5);
}

void ComplaintsWidgetController::fillNature()
{
    const QString& sql = "SELECT data_nature, id FROM complaints_nature WHERE id_visiable = 1 AND id_company = ?";
    auto newQuery = _dbManager->getDBWraper()->query();
    if (!newQuery.prepare(sql))
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    auto it = _companiesModel->checkedRealIndex().begin();
    newQuery.bindValue(0, *it);
    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    _natureModel->setQuery(newQuery);
    _natureModel->setCheckedAll();

    _complaintsWidget->updateNature();
}

void ComplaintsWidgetController::fillSources()
{
    QStringList list;

    for (auto item : _companiesModel->checkedRealIndex())
        list.append(QString::number(item));

    if (_companiesModel->checkedRealIndex().count())
        list.append("0");

    const QString& sql = QString("SELECT name_source, id FROM complaints_source WHERE id_company IN (%1)")
                         .arg(list.join(","));
    auto newQuery = _dbManager->getDBWraper()->query();
    newQuery.prepare(sql);

    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }

    _sourcesModel->setQuery(newQuery);
    _sourcesModel->setCheckedAll();

    _complaintsWidget->updateSources();
}

void ComplaintsWidgetController::createExcelFile(const QString &fileName)
{
#ifdef Q_OS_WIN
    // map company
    QMap<quint64, QString> mapSheet = _companiesModel->getCheckedRealIndexMap();
    QMap<quint64, quint64> mapRowIndex;

    // create excel doc
    QXlsx::Document doc;

    // create list in excel file and create header
    for (auto i = mapSheet.begin(); i != mapSheet.end(); ++i)
    {
        mapRowIndex[i.key()] = 1;

        QString &nameCompany = i.value().left(i.value().indexOf(" ("));
        doc.addSheet(nameCompany);
        doc.setColumnWidth(1,  1, 15);
        doc.setColumnWidth(2,  2, 15);
        doc.setColumnWidth(3,  3, 40);
        doc.setColumnWidth(4,  4, 20);
        doc.setColumnWidth(5,  5, 20);
        doc.setColumnWidth(6,  6, 15);
        doc.setColumnWidth(7,  7, 20);
        doc.setColumnWidth(8,  8, 25);
        doc.setColumnWidth(9,  9, 50);
        doc.setColumnWidth(10, 10, 22);
        doc.setColumnWidth(11, 11, 20);

        QXlsx::Format capitonStyle;
        capitonStyle.setFontName("Arial");
        capitonStyle.setFontSize(10);
        capitonStyle.setFontColor(Qt::black);
        capitonStyle.setFontBold(true);
        capitonStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
        capitonStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        capitonStyle.setPatternBackgroundColor(QColor::fromRgb(146,208,80));
        capitonStyle.setBorderColor(Qt::black);
        capitonStyle.setBorderStyle(QXlsx::Format::BorderThin);

        doc.write(1, 1,  "№ жалобы",                 capitonStyle);
        doc.write(1, 2,  "№ заказа",                 capitonStyle);
        doc.write(1, 3,  "ФИО",                      capitonStyle);
        doc.write(1, 4,  "На кого",                  capitonStyle);
        doc.write(1, 5,  "Принадлежность",           capitonStyle);
        doc.write(1, 6,  "№ авто",                   capitonStyle);
        doc.write(1, 7,  "Характер жалобы",          capitonStyle);
        doc.write(1, 8,  "Дата",                     capitonStyle);
        doc.write(1, 9,  "Суть",                     capitonStyle);
        doc.write(1, 10, "Статус",                   capitonStyle);
        doc.write(1, 11, "Департамент",              capitonStyle);
        doc.setRowHeight(1, 1, 20);
    }

    // create text format
    QXlsx::Format baseDataStyle;
    baseDataStyle.setFontName("Arial");
    baseDataStyle.setFontColor(Qt::black);
    baseDataStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    for (auto rowIndex = 0; rowIndex < _complaintsModel->rowCount(); ++rowIndex)
    {
        QString sqlQuery;
        switch (_complaintsModel->index(rowIndex, _complaintsModel->columnCount()-4).data().toInt())
        {
            case 1:
                sqlQuery = _defaultSqlExcel.arg("driver.surname || ' ' || driver.firstname || ' ' || driver.middlename AS driverName, "
                                                "driver.whose AS whoseDriver")
                           .arg("complaints_orders_drivers");
                break;
            case 2:
                sqlQuery = _defaultSqlExcel.arg("driver.fio AS driverName")
                           .arg("complaints_orders_logists");
                break;
            case 3:
                sqlQuery = _defaultSqlExcel.arg("driver.fio AS driverName")
                           .arg("complaints_orders_operators");
                break;
            default:
                break;
        }

        auto newQuery = _dbManager->getDBWraper()->query();
        if (!newQuery.prepare(sqlQuery))
        {
            qDebug() << "ERROR:" << newQuery.lastError().text();
            Q_ASSERT(false);
        }


        newQuery.bindValue(":idComplaint", _complaintsModel->index(rowIndex, 0).data());
        if (!newQuery.exec())
        {
            qDebug() << "ERROR:" << newQuery.lastError().text();
            Q_ASSERT(false);
        }

        if (newQuery.first())
        {
            auto idComplaint    = newQuery.value("id").toLongLong();
            auto idOrder        = newQuery.value("idOrder").toLongLong();
            auto driverName     = newQuery.value("driverName").toString();
            auto target         = newQuery.value("target").toString();
            auto whoseDriver    = newQuery.value("whoseDriver").toInt();
            auto carNumber      = newQuery.value("carNumber").toString();
            auto nature         = newQuery.value("nature").toString();
            auto dateCreate     = newQuery.value("dateCreate").toDateTime().toString("yyyy.MM.dd hh:mm:ss");
            auto essence        = newQuery.value("essence").toString();
            auto status         = newQuery.value("status").toString();
            auto nameDepartment = newQuery.value("nameDepartment").toString();
            auto idCompany      = newQuery.value("idCompany").toInt();

            if (mapRowIndex.contains(idCompany))
            {
                QString &nameCompany = mapSheet[idCompany].left(mapSheet[idCompany].indexOf(" ("));
                doc.selectSheet(nameCompany);
                _overallRowIndex = mapRowIndex[idCompany];
                ++_overallRowIndex;
                mapRowIndex[idCompany] = _overallRowIndex;
            }
            else
                continue;

            doc.write(_overallRowIndex, 1,  idComplaint,    baseDataStyle);
            doc.write(_overallRowIndex, 2,  idOrder,        baseDataStyle);
            doc.write(_overallRowIndex, 3,  driverName,     baseDataStyle);
            doc.write(_overallRowIndex, 4,  target,         baseDataStyle);
            if(!whoseDriver)
                doc.write(_overallRowIndex, 5,  "Наш",      baseDataStyle);
            else
                doc.write(_overallRowIndex, 5,  "Частник",  baseDataStyle);
            doc.write(_overallRowIndex, 6,  carNumber,      baseDataStyle);
            doc.write(_overallRowIndex, 7,  nature,         baseDataStyle);
            doc.write(_overallRowIndex, 8,  dateCreate,     baseDataStyle);
            doc.write(_overallRowIndex, 9,  essence,        baseDataStyle);
            doc.write(_overallRowIndex, 10, status,         baseDataStyle);
            doc.write(_overallRowIndex, 11, nameDepartment, baseDataStyle);

        }
    }

    if (!doc.saveAs(fileName))
        qDebug() << __FUNCTION__ << QString("Не удалось сохранить документ %").arg(fileName);
#endif
}

void ComplaintsWidgetController::sResetBtnClicked()
{
    _flagStartFetch = true;
    _complaintsWidget->setResetBtn(true);
    refreshComplaints(_settings);       
}

void ComplaintsWidgetController::onOpenNextComplaint()
{
    _complaintsWidget->selectNextComplaint();
}

void ComplaintsWidgetController::fillingEditsInfoComplaint(const quint64 id)
{
    _complaintInfoController.fillingEditsInfo(id);
}

void ComplaintsWidgetController::commandGetSelectComplaintsPublic(QDateTime dateTime)
{
    if (dateTime < _lastDateTimeDbaseComplaints)
    {
        auto request = [this, dateTime]()
        {
            QVariantMap requestMap;
            requestMap["type_command"] = "get_select_complaints";
            requestMap["date_last_query"] = QDateTime::currentDateTime();
            requestMap["date_start"] = dateTime.toString("yyyy-MM-dd hh:mm:ss");
            requestMap["date_end"] = _lastDateTimeDbaseComplaints.toString("yyyy-MM-dd hh:mm:ss");
            requestMap["idUser"] = permissions::UserPermission::instance().idUser();
            network::RequestShp request(new network::DefaultRequest("get_select_complaints", QVariant::fromValue(requestMap)));
            _requestsManager->execute(request, this, &ComplaintsWidgetController::onGetSelectComplaints);
            _lastDateTimeDbaseComplaints = dateTime;
        };

        Reconnector reconnectorObj(request);
        _reconnectMap.insert("get_select_complaints", reconnectorObj);
        reconnectorObj.run();
    }
    else
        emit finishedRefreshComplaints();
}

void ComplaintsWidgetController::openComplaintsWorker(const quint64 idWorker, const int keyWorker, const int statusType)
{
    QMap<int, QString> mapWorkers;
    mapWorkers[1] = "drivers";
    mapWorkers[2] = "operators";
    mapWorkers[3] = "logists";

    enum Workers
    {
        drivers = 1,
        operators,
        logists
    };

    QString parametrs;
    switch (keyWorker) {
        case drivers:
            parametrs = " driver.surname || ' ' || driver.firstname || ' ' || driver.middlename AS 'ФИО', ";
            break;
        case operators:
            parametrs = " driver.fio AS 'ФИО', ";
            break;
        case logists:
            parametrs = " driver.fio AS 'ФИО', ";
            break;
        default:
            break;
    }

    enum StatusType
    {
        AllComplaints = 0,
        PositiveComplaints,
        NegativeComplaints
    };

    QString type;
    switch (statusType) {
        case AllComplaints:
            type = "1, 2";
            break;
        case PositiveComplaints:
            type = "1";
            break;
        case NegativeComplaints:
            type = "2";
            break;
        default:
            break;
    }

    const QString& sql = QString("SELECT "
                                 "complaints.id AS '№', "
                                 "complaints_status.name_status AS 'Статус', "
                                 "strftime('%d-%m-%Y %H:%M:%S', complaints.date_create) AS 'Дата создания', "
                                 "company.name AS 'Компания', "
                                 "complaints_source.name_source AS 'Откуда', "
                                 "departments.name AS 'Отдел', "
                                 "complaints_type.name AS 'Тип', "
                                 "complaints.id_order AS '№ заказа', "
                                 "complaints_nature.data_nature AS 'Характер', "
                                 "complaints_target.data_target AS 'На кого', "
                                 "%1 "
                                 "complaints_nature.id_target, "
                                 "complaints.days_to_process, "
                                 "complaints.id_status "
                                 "FROM complaints "
                                 "INNER JOIN complaints_status ON (complaints.id_status = complaints_status.id) "
                                 "INNER JOIN complaints_source ON (complaints.id_source = complaints_source.id) "
                                 "INNER JOIN departments ON (complaints.id_department = departments.id) "
                                 "INNER JOIN complaints_type ON (complaints.id_type = complaints_type.id) "
                                 "INNER JOIN company ON (company.id = complaints.id_company) "
                                 "INNER JOIN complaints_nature ON (complaints_nature.id = complaints.id_nature) "
                                 "INNER JOIN complaints_target ON (complaints_nature.id_target = complaints_target.id) "
                                 "INNER JOIN complaints_orders_%2 AS driver ON (driver.id_complaint = complaints.id) "
                                 "WHERE driver.id_worker = :idWorker AND complaints.id_type IN (%3) ORDER BY complaints.date_create DESC"
                                ).arg(parametrs).arg(mapWorkers[keyWorker]).arg(type);

    auto newQuery = _dbManager->getDBWraper()->query();
    if (!newQuery.prepare(sql))
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }

    newQuery.bindValue(":idWorker", idWorker);

    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }

    _complaintsModel->setQuery(newQuery);
    _complaintsWidget->updateComplaints();

    if (!newQuery.first())
    {
        emit signalShowMessageStatusBarComplaints("По вашему запросу жалоб не найдено", 5);
        refreshComplaints(_settings);
    }
}

void ComplaintsWidgetController::refreshFetch()
{
    if (!_flagStartFetch)
        return;

    if (_complaintsWidget->getChbIsSetDateEnd()->isChecked())
        refreshComplaints(_settings);
}

void ComplaintsWidgetController::createItemsFeedBackModel()
{
    QList<QStandardItem*> items;
    items.append(new QStandardItem("Не указано"));
    items.append(new QStandardItem("Да"));
    items.append(new QStandardItem("Нет"));

    _feedBackModel->appendColumn(items);
    _feedBackModel->checkedAll();
}

void ComplaintsWidgetController::createInfoStatusBar(QString &params, const QVariantList &sqlParameters)
{
    if (!_complaintsWidget->isVisible())
        return;

    const QString sqlQuery = "SELECT id_status, count(id_status) AS count "
                             "FROM complaints "
                             "INNER JOIN complaints_nature ON (complaints_nature.id = complaints.id_nature) "
                             "INNER JOIN complaints_target ON (complaints_target.id = complaints_nature.id_target) "
                             + params.arg("GROUP BY complaints.id_status ");

    auto query = _dbManager->getDBWraper()->query();
    if (!query.prepare(sqlQuery))
    {
        qDebug() << "ERROR:" << query.lastError().text();
        Q_ASSERT(false);
    }

    for (int i = 0; i < sqlParameters.count(); i++)
        query.bindValue(i, sqlParameters[i]);

    if (!query.exec())
    {
        qDebug() << "ERROR:" << query.lastError().text();
        Q_ASSERT(false);
    }

    int allCount = 0;
    QMap<int, int> mapStatusBar =
    {
        {0, 0},
        {1, 0},
        {2, 0},
        {3, 0},
        {4, 0},
        {5, 0}
    };

    while (query.next()) {
        mapStatusBar[query.value(0).toInt()] = query.value(1).toInt();
        allCount += query.value(1).toInt();
    }
    mapStatusBar[0] = allCount;

    emit signalStatusBarValue(mapStatusBar, true);
}

void ComplaintsWidgetController::setFindDataComplaint(const quint64 id)
{
    // NOTE: желательно заюзать врапер и у него метод query()
    _statusesModel->setQuery("SELECT name_status, id FROM complaints_status",
                             *(_dbManager->getConnection().data()));

    _targetsModel->setQuery("SELECT data_target, id FROM complaints_target",
                            *(_dbManager->getConnection().data()));

    _companiesModel->setQuery("SELECT name, id FROM company WHERE isVisible = 1",
                              *(_dbManager->getConnection().data()));

    _settings._dateStart = QDateTime(QDate(2014, 1, 1), QTime(0, 0));
    _settings._dateStart.setTime(QTime(0, 0));
    _settings._dateEnd = QDateTime::currentDateTime().addSecs(3000);
    _settings._isSetDateEnd = false;
    _lastDateTimeDbaseComplaints = _settings._dateStart;
    _settings._companies = _companiesModel->getListId();
    _settings._like = QString::number(id);
    _settings._idSelectedComplaint = id;
    _complaintsWidget->setSettings(_settings);
}

void ComplaintsWidgetController::createComplaintInfoControllerRun()
{
    _complaintInfoController.setDBManager(_dbManager);
    _complaintInfoController.setRequestsManager(_requestsManager);
    _complaintInfoController.run();
    connect(&_complaintInfoController, &ComplaintInfoController::signalOpenComplaintsWorker,
            this, &ComplaintsWidgetController::openComplaintsWorker);
    connect(&_complaintInfoController, &ComplaintInfoController::signalOnenNextComplaint,
            this, &ComplaintsWidgetController::onOpenNextComplaint);
    _complaintsWidget->setComplaintInfoWidget(_complaintInfoController.getWidget());
    _complaintInfoController.hideWidget();
}

void ComplaintsWidgetController::createComplaintControllerRun()
{
    _createComplaintController.setRequestsManager(_requestsManager);
    _createComplaintController.setDBManager(_dbManager);
    _createComplaintController.run();
}
