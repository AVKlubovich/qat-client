﻿#pragma once

#include "Controllers/ComplaintInfoController.h"

#include "../../Reconnector.h"


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class ClientsWidget;

    class CheckedModel;


    class ClientsWidgetController : public QObject
    {
        Q_OBJECT

    public:
        struct SettingsClients
        {
            SettingsClients()
            {
                clear();
            }
            
            QDateTime _dateStart;
            QDateTime _dateEnd;
            bool _isSetDateEnd;
            QList<quint64> _nature;
            QList<quint64> _companies;
            QString _like;

            void clear()
            {
                _nature.clear();
                _companies.clear();
                _like = "";
                _isSetDateEnd = false;
                _dateStart = QDateTime::currentDateTime().addDays(-4);
                _dateStart.setTime(QTime(0, 0));
                _dateEnd = QDateTime::currentDateTime().addSecs(3000);
                _isSetDateEnd = false;
            }
        };

    public:
        ClientsWidgetController();
        ~ClientsWidgetController();

        void setDbManager(database::DBManagerShp dbManager);
        void setClientsWidget(ClientsWidget * LogistsWidget);

    public slots:
        void init();
        void run();
        void update();

        void fillingEditsInfoComplaint(const quint64 id);

    private slots:
        // NOTE: send commands
        void commandGetSelectClients(const SettingsClients & data);

        // NOTE: data processing
        void onGetSelectClients(network::ResponseShp);
        void refreshClients(const SettingsClients& data);

        // NOTE: transfer of control to controllers
        void createComplaintInfoControllerRun();
        void fillNature();

    private:
        network::RequestsManagerShp _requestsManager;
        database::DBManagerShp _dbManager;

        qat_client::ClientsWidget *_clientsWidget = nullptr;
        QDateTime _lastDateTimeDbaseClients;
        SettingsClients _settings;

        // NOTE: controllers
        ComplaintInfoController _complaintInfoController;

        // NOTE: models
        QSqlQueryModel* _clientsModel;

        CheckedModel* _companiesModel;
        CheckedModel* _natureModel;

        static const QString _defaultSqlClients;

        bool _isInintSettings = false;

        QMap<QString, Reconnector> _reconnectMap;
    };

}
