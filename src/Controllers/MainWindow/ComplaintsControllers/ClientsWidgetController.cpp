#include "Common.h"
#include "ClientsWidgetController.h"

#include "Widgets/MainWindow/ComplaintsWidgets/ClientsWidget/ClientsWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/Users/Users.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "utils/Sql/SqlString.h"

#include "Models/ComplaintsModel.h"
#include "Models/CheckedModel.h"
#include "Controllers/ResponceChecker.h"


using namespace qat_client;

const QString ClientsWidgetController::_defaultSqlClients(
    "SELECT "
    "clients.id AS '№', "
    "clients.phone AS 'Телефон', "
    "clients.fio AS 'ФИО', "
    "clients.id_complaint AS '№ Жалобы', "
    "IFNULL(users.name, '') AS 'Назначена на', "
    "strftime('%d-%m-%Y %H:%M:%S', clients.date_create) AS 'Дата', "
    "complaints_nature.data_nature AS 'Характер жалобы' "
    "FROM clients "
    "INNER JOIN company ON (company.id = clients.id_company) "
    "INNER JOIN complaints_nature ON (complaints_nature.id = clients.id_nature) "
    "LEFT JOIN users ON (clients.id_customer = users.id) ");

ClientsWidgetController::ClientsWidgetController()
    : QObject(nullptr)
    , _requestsManager(network::RequestsManagerShp::create())
{
}

ClientsWidgetController::~ClientsWidgetController()
{
    if (_clientsModel)
        _clientsModel->deleteLater();
}

void ClientsWidgetController::setDbManager(database::DBManagerShp dbManager)
{
    _dbManager = dbManager;
}

void ClientsWidgetController::setClientsWidget(ClientsWidget * clientsWidget)
{
    _clientsWidget = clientsWidget;
}

void ClientsWidgetController::init()
{
    _clientsModel = new QSqlQueryModel();
    _clientsWidget->setClientsModel(_clientsModel);
    
    _companiesModel = new CheckedModel();
    _clientsWidget->setCompaniesModel(_companiesModel);

    _natureModel = new CheckedModel();
    _clientsWidget->setNatureModel(_natureModel);

    createComplaintInfoControllerRun();

    connect(_clientsWidget, &ClientsWidget::signalSelectClients,
            this, &ClientsWidgetController::commandGetSelectClients);
    connect(_clientsWidget, &ClientsWidget::signalFillNature,
            this, &ClientsWidgetController::fillNature);
    connect(_clientsWidget, &ClientsWidget::complaintSelected, this,
            &ClientsWidgetController::fillingEditsInfoComplaint);

    auto func = [this]()
    {
        _complaintInfoController.hideWidget();
    };
    connect(_clientsWidget, &ClientsWidget::signalHideWidget, this, func);
}

void ClientsWidgetController::run()
{
    if (_isInintSettings)
    {
        update();
        return;
    }
    _isInintSettings = true;
    
    _companiesModel->setQuery("SELECT name, id FROM company",
                              *(_dbManager->getConnection().data()));
    
    _settings._dateStart = QDateTime::currentDateTime().addDays(-4);
    _settings._dateStart.setTime(QTime(0, 0));
    _settings._dateEnd = QDateTime::currentDateTime().addSecs(3000);
    _settings._isSetDateEnd = false;
    _lastDateTimeDbaseClients = _settings._dateStart;
    _settings._companies = _companiesModel->getListId();
    _clientsWidget->setSettings(_settings);

    refreshClients(_settings);
}

void ClientsWidgetController::update()
{
    commandGetSelectClients(_settings);
}

void ClientsWidgetController::commandGetSelectClients(const SettingsClients & data)
{
    _settings = data;

    if (!_lastDateTimeDbaseClients.isValid())
        _lastDateTimeDbaseClients = QDateTime::currentDateTime();

    if (_settings._dateStart < _lastDateTimeDbaseClients)
    {
        auto request = [this]()
        {
            QVariantMap requestMap;
            requestMap["type_command"] = "get_select_clients";
            requestMap["date_last_query"] = QDateTime::currentDateTime();
            requestMap["date_start"] = _settings._dateStart.toString("yyyy-MM-dd hh:mm:ss");
            requestMap["date_end"] = _lastDateTimeDbaseClients.toString("yyyy-MM-dd hh:mm:ss");
            network::RequestShp request(new network::DefaultRequest("get_select_clients", QVariant::fromValue(requestMap)));
            _requestsManager->execute(request, this, &ClientsWidgetController::onGetSelectClients);
            _lastDateTimeDbaseClients = _settings._dateStart;
        };

        Reconnector reconnectorObj(request);

        _reconnectMap.insert("get_select_clients", reconnectorObj);
        reconnectorObj.run();
    }
    else
    {
        refreshClients(_settings);
    }
}

void ClientsWidgetController::onGetSelectClients(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) < 0 )
        return;

    const auto dbWraper = _dbManager->getDBWraper();
    auto insertQuery = dbWraper->query();

    const auto& clients = responseObj->bodyToVariant().toMap()["clients"].toList();
    for (const auto& client : clients)
    {
        const auto& map = client.toMap();

        const QStringList& listKeys = map.keys();
        const QString& stringKeys = listKeys.join(", ");

        QStringList listValues;
        for (const auto& key : listKeys)
            listValues.append("'" + map.value(key).toString() + "'");

        const QString& stringValues = listValues.join(", ");

        const QString& sqlQuery = "INSERT OR REPLACE INTO clients (" + stringKeys + ") VALUES (" + stringValues + ")";

        insertQuery.prepare(sqlQuery);
        const auto result = dbWraper->execQuery(insertQuery);
        if (!result)
        {
            qDebug() << insertQuery.lastError();
            qDebug() << __FUNCTION__ << "query failed";
            Q_ASSERT(false);
        }
    }

    refreshClients(_settings);
}

void ClientsWidgetController::refreshClients(const SettingsClients& data)
{
    QStringList sqlQueries;
    QList<QVariant> sqlParameters;
    sql_utils::SqlString sqlStr;

    sqlStr.addSqlParameters(data._companies, "clients", "_company", sqlQueries, sqlParameters);
    sqlStr.addSqlLike(data._like, "clients", sqlQueries, sqlParameters);

    const QString& currentSqClients = _defaultSqlClients +
        "WHERE (clients.date_create " +
        "BETWEEN strftime('%Y-%m-%dT%H:%M:%S', DATETIME('" + data._dateStart.toString("yyyy-MM-dd hh:mm:ss") + "')) " +
        "AND strftime('%Y-%m-%dT%H:%M:%S', DATETIME(" + (!data._isSetDateEnd ? "'now', 'localtime'" : (QString("'") + data._dateEnd.toString("yyyy-MM-dd hh:mm:ss") + "'")) + "))) " +
        (sqlQueries.count() > 0 ? ("AND (" + sqlQueries.join(" OR ") + ")") : "") +
        " ORDER BY clients.date_create DESC";
    auto newQuery = _dbManager->getDBWraper()->query();
    if (!newQuery.prepare(currentSqClients))
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }

    for (int i = 0; i < sqlParameters.count(); i++)
        newQuery.bindValue(i, sqlParameters[i]);

    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }

    _clientsModel->setQuery(newQuery);
    _clientsWidget->updateClients();
}

void ClientsWidgetController::fillNature()
{
    const QString& sql = "SELECT data_nature, id FROM complaints_nature WHERE id_visiable = 1 AND id_company = ?";
    auto newQuery = _dbManager->getDBWraper()->query();
    if (!newQuery.prepare(sql))
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    auto it = _companiesModel->checkedRealIndex().begin();
    newQuery.bindValue(0, *it);
    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    _natureModel->setQuery(newQuery);
    _clientsWidget->updateNature();
}

void ClientsWidgetController::fillingEditsInfoComplaint(const quint64 id)
{
    _complaintInfoController.fillingEditsInfo(id);
}

void ClientsWidgetController::createComplaintInfoControllerRun()
{
    _complaintInfoController.setDBManager(_dbManager);
    _complaintInfoController.setRequestsManager(_requestsManager);
    _complaintInfoController.run();
    _clientsWidget->setComplaintInfoWidget(_complaintInfoController.getWidget());
    _complaintInfoController.hideWidget();
}
