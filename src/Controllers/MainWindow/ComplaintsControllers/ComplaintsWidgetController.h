﻿#pragma once

#include "Controllers/ComplaintInfoController.h"
#include "Controllers/CreateComplaintController.h"

#include "../../Reconnector.h"


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class ComplaintsWidget;

    class CheckedModel;
    class CheckedStandardModel;


    class ComplaintsWidgetController : public QObject
    {
        Q_OBJECT

    public:
        struct SettingsComplaints
        {
            SettingsComplaints()
            {
                clear();
            }
            
            QDateTime _dateStart;
            QDateTime _dateEnd;
            bool _isSetDateEnd;
            QList<quint64> _nature;
            QList<quint64> _targets;
            QList<quint64> _statuses;
            QList<quint64> _companies;
            QList<quint64> _sources;
            QList<QString> _feedback;
            QString _like;
            quint64 _idSelectedComplaint;

            void clear()
            {
                _idSelectedComplaint = 0;
                _nature.clear();
                _targets.clear();
                _statuses.clear();
                _companies.clear();
                _like = "";
                _isSetDateEnd = false;
                _dateStart = QDateTime::currentDateTime().addDays(-4);
                _dateStart.setTime(QTime(0, 0));
                _dateEnd = QDateTime::currentDateTime().addSecs(3000);
                _isSetDateEnd = false;
            }
        };

    public:
        ComplaintsWidgetController();
        ~ComplaintsWidgetController();

        void setDbManager(database::DBManagerShp dbManager);
        void setComplaintsWidget(ComplaintsWidget* complaintsWidget);
        void setFindDataComplaint(const quint64 id);
        void openComplaintsWorker(const quint64 idWorker, const int keyWorker, const int statusType);
        void refreshFetch();

    private:
        void createItemsFeedBackModel();
        void createInfoStatusBar(QString &params, const QVariantList &sqlParameters);

    public slots:
        void init();
        void run();
        void update();

        void fillingEditsInfoComplaint(const quint64 id);

        void commandGetSelectComplaintsPublic(QDateTime dateTime);

    private slots:
        // NOTE: send commands
        void commandGetSelectComplaints(const SettingsComplaints& data);
        void slotFindCompalints(QVariantList listIdsComplaints);

        // NOTE: data processing
        void onGetSelectComplaints(network::ResponseShp);
        void parseSelectComplaints(const QVariantMap& inData);
        void onParseSelectComplaintsFinished();
        void onParseSelectIdsComplaintsFinished();
        void refreshComplaints(const SettingsComplaints& data);
        void refreshFindComplaints();

        // NOTE: transfer of control to controllers
        void createComplaintInfoControllerRun();
        void createComplaintControllerRun();

        void fillNature();
        void fillSources();

        //NOTE: create excel files
        void createExcelFile(const QString& fileName);
        void sResetBtnClicked();
        void onOpenNextComplaint();


    signals:
        void signalIdReviewOpen(const quint64 id);
        void finishedRefreshComplaints();
        void updateCompanies();

        void signalStatusBarValue(const QMap<int, int> &map, bool flag);
        void signalShowMessageStatusBarComplaints(const QString &msg, quint32 sec = 0);

    private:
        network::RequestsManagerShp _requestsManager;
        database::DBManagerShp _dbManager;

        ComplaintsWidget *_complaintsWidget = nullptr;
        QDateTime _lastDateTimeDbaseComplaints;
        SettingsComplaints _settings;

        // NOTE: controllers
        CreateComplaintController _createComplaintController;
        ComplaintInfoController _complaintInfoController;

        // NOTE: models
        QSqlQueryModel* _complaintsModel;

        CheckedModel* _statusesModel;
        CheckedModel* _targetsModel;
        CheckedModel* _companiesModel;
        CheckedModel* _natureModel;
        CheckedModel* _sourcesModel;
        CheckedStandardModel* _feedBackModel;

        static const QString _defaultSqlComplaints;
        static const QString _defaultSqlExcel;

        bool _isInintSettings = false;
        int _overallRowIndex;

        QMap<QString, Reconnector> _reconnectMap;

        QVariantList _findIdsComplaints;
        bool _flagStartFetch = true;
    };

}
