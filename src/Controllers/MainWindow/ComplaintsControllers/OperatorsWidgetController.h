﻿#pragma once

#include "../../WorkerInformationController/WorkerInformationController.h"


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class OperatorsWidget;

    class CheckedModel;


    class OperatorsWidgetController : public QObject
    {
        Q_OBJECT

    public:
        struct SettingsOperators
        {
            SettingsOperators()
            {
                clear();
            }

            QDateTime _dateStart;
            QDateTime _dateEnd;
            bool _isSetDateEnd;
            QList<quint64> _nature;
            QList<quint64> _companies;
            QString _like;

            void clear()
            {
                _nature.clear();
                _companies.clear();
                _like = "";
                _isSetDateEnd = false;
                _dateStart = QDateTime::currentDateTime().addDays(-4);
                _dateStart.setTime(QTime(0, 0));
                _dateEnd = QDateTime::currentDateTime().addSecs(3000);
                _isSetDateEnd = false;
            }
        };

    public:
        OperatorsWidgetController();
        ~OperatorsWidgetController();

        void setDbManager(database::DBManagerShp dbManager);
        void setOperatorsWidget(OperatorsWidget * operatorsWidget);

    private:
        void createInfoStatusBar(QString &params, const QVariantList &sqlParameters);

    public slots:
        void init();
        void run();
        void update();

        void fillingEditsInfoComplaint(const quint64 id);
        void finishedRefreshComplaints();

    signals:
        void signalRefresh(const QDateTime dateTime);
        void signalOpenComplaintsWorker(const quint64 idWorker, const int keyWorker, const int statusType);
        void updateCompanies();

        void signalStatusBarValue(const QMap<int, int> &map, bool flag);
        void signalShowMessageStatusBarOperators(const QString &msg, quint32 sec = 0);

    private slots:
        // NOTE: send commands
        void commandGetSelectOperators(const SettingsOperators & data);
        void createExcelFile(const QString &fileName);

        // NOTE: data processing
        void refreshOperators(const SettingsOperators& data);

        // NOTE: transfer of control to controllers
        void createWorkerInfoControllerRun();
        void fillNature();

    private:
        network::RequestsManagerShp _requestsManager;
        database::DBManagerShp _dbManager;

        OperatorsWidget *_operatorsWidget = nullptr;
        QDateTime _lastDateTimeDbaseOperators;
        SettingsOperators _settings;

        // NOTE: controllers
        WorkerInformationController _workerInfoController;

        // NOTE: models
        QPointer<QSqlQueryModel> _operatorsModel;

        CheckedModel* _companiesModel;
        CheckedModel* _natureModel;

        static const QString _defaultSqlOperators;
        static const QString _defaultSqlOperatorsCount;

        bool _isInintSettings = false;
        static const QString _defaultSqlExcel;

        bool _flagRefreshOperators = false;
    };
}
