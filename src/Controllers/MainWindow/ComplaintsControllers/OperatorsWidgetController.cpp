#include "Common.h"
#include "OperatorsWidgetController.h"

#include "Widgets/MainWindow/ComplaintsWidgets/OperatorsWidget/OperatorsWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/Users/Users.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "utils/Sql/SqlString.h"

#include "Models/ComplaintsModel.h"
#include "Models/CheckedModel.h"

#include "permissions/UserPermission.h"


using namespace qat_client;

const QString OperatorsWidgetController::_defaultSqlOperators(
        "SELECT"
        " drivers.id_complaint AS 'ID',"
        " drivers.fio AS 'ФИО',"
        " CASE WHEN cars.id IS NOT NULL THEN cars.number ELSE NULL END AS '№ авто',"
        " drivers.id_complaint AS '№ жалобы',"
        " strftime('%d-%m-%Y %H:%M:%S', complaints.date_create) AS 'Дата',"
        " complaints.id_order AS '№ Заказа', "
        " complaints_nature.data_nature AS 'Характер жалобы'"
        " FROM complaints_orders_operators AS drivers"
        " INNER JOIN complaints ON (complaints.id = drivers.id_complaint)"
        " LEFT JOIN complaints_orders_cars AS cars ON (complaints.id = cars.id_complaint)"
        " INNER JOIN complaints_nature ON (complaints.id_nature = complaints_nature.id)");

const QString OperatorsWidgetController::_defaultSqlOperatorsCount(
        "SELECT complaints.id_status, count(complaints.id_status) AS count"
        " FROM complaints_orders_operators AS drivers"
        " INNER JOIN complaints ON (complaints.id = drivers.id_complaint)"
        " LEFT JOIN complaints_orders_cars AS cars ON (complaints.id = cars.id_complaint)"
        " INNER JOIN complaints_nature ON (complaints.id_nature = complaints_nature.id)");

const QString OperatorsWidgetController::_defaultSqlExcel(
        "SELECT"
        " c.id AS id,"
        " c.id_order AS idOrder,"
        " driver.fio AS driverName, "
        " target.data_target AS target,"
        " nature.data_nature AS nature,"
        " c.date_create AS dateCreate,"
        " c.essence AS essence,"
        " status.name_status AS status,"
        " department.name AS nameDepartment,"
        " auto.number AS carNumber,"
        " c.id_company AS idCompany"
        " FROM complaints AS c"
        " INNER JOIN complaints_orders_operators AS driver ON(c.id = driver.id_complaint)"
        " INNER JOIN complaints_nature AS nature ON (nature.id = c.id_nature)"
        " INNER JOIN complaints_target AS target ON (target.id = nature.id_target)"
        " INNER JOIN complaints_status AS status ON (status.id = c.id_status)"
        " INNER JOIN departments AS department ON (c.id_department = department.id)"
        " LEFT JOIN complaints_orders_cars AS auto ON (c.id = auto.id_complaint)"
        " WHERE c.id = :idComplaint"
        );

OperatorsWidgetController::OperatorsWidgetController()
    : QObject(nullptr)
    , _requestsManager(network::RequestsManagerShp::create())
{
}

OperatorsWidgetController::~OperatorsWidgetController()
{
    if (_operatorsModel)
        _operatorsModel->deleteLater();
}

void OperatorsWidgetController::setDbManager(database::DBManagerShp dbManager)
{
    _dbManager = dbManager;
}

void OperatorsWidgetController::setOperatorsWidget(OperatorsWidget * operatorsWidget)
{
    _operatorsWidget = operatorsWidget;
}

void OperatorsWidgetController::createInfoStatusBar(QString &params, const QVariantList &sqlParameters)
{
    if (!_operatorsWidget->isVisible())
        return;

    const QString sqlQuery = _defaultSqlOperatorsCount + params.arg("GROUP BY complaints.id_status ");
    auto query = _dbManager->getDBWraper()->query();
    if (!query.prepare(sqlQuery))
    {
        qDebug() << "ERROR:" << query.lastError().text();
        Q_ASSERT(false);
    }

    for (int i = 0; i < sqlParameters.count(); i++)
        query.bindValue(i, sqlParameters[i]);

    if (!query.exec())
    {
        qDebug() << "ERROR:" << query.lastError().text();
        Q_ASSERT(false);
    }

    int allCount = 0;
    QMap<int, int> mapStatusBar =
    {
        {0, 0},
        {1, 0},
        {2, 0},
        {3, 0},
        {4, 0},
        {5, 0}
    };

    while (query.next()) {
        mapStatusBar[query.value(0).toInt()] = query.value(1).toInt();
        allCount += query.value(1).toInt();
    }
    mapStatusBar[0] = allCount;

    emit signalStatusBarValue(mapStatusBar, true);
}

void OperatorsWidgetController::init()
{
    _operatorsModel = new QSqlQueryModel();
    _operatorsWidget->setOperatorsModel(_operatorsModel);
    
    _companiesModel = new CheckedModel();
    _operatorsWidget->setCompaniesModel(_companiesModel);

    _natureModel = new CheckedModel();
    _operatorsWidget->setNatureModel(_natureModel);

    createWorkerInfoControllerRun();

    connect(_operatorsWidget, &OperatorsWidget::signalCreateComplaintsExcelFile,
            this, &OperatorsWidgetController::createExcelFile);

    connect(_operatorsWidget, &OperatorsWidget::signalSelectOperators,
            this, &OperatorsWidgetController::commandGetSelectOperators);
    connect(_operatorsWidget, &OperatorsWidget::signalFillNature,
            this, &OperatorsWidgetController::fillNature);
    connect(_operatorsWidget, &OperatorsWidget::complaintSelected, this,
            &OperatorsWidgetController::fillingEditsInfoComplaint);

    connect(this, &OperatorsWidgetController::updateCompanies,
            _operatorsWidget, &OperatorsWidget::updateCompanies);

    connect(_operatorsWidget, &OperatorsWidget::signalCheckedAll,
            [this](){
        _companiesModel->checkedAll();
        emit updateCompanies();
    });
    connect(_operatorsWidget, &OperatorsWidget::signalUncheckedAll,
            [this](){
        _companiesModel->checkedClearAll();
        emit updateCompanies();
    });

    auto func = [this]()
    {
        _workerInfoController.hideWidget();
    };
    connect(_operatorsWidget, &OperatorsWidget::signalHideWidget, this, func);
}

void OperatorsWidgetController::run()
{
    if (_isInintSettings)
    {
        update();
        return;
    }
    _isInintSettings = true;
    
    _companiesModel->setQuery("SELECT name, id FROM company WHERE isVisible = 1",
                              *(_dbManager->getConnection().data()));
    
    _settings._dateStart = QDateTime::currentDateTime().addDays(-4);
    _settings._dateStart.setTime(QTime(0, 0));
    _settings._dateEnd = QDateTime::currentDateTime().addSecs(3000);
    _settings._isSetDateEnd = false;
    _lastDateTimeDbaseOperators = _settings._dateStart;
    _settings._companies = _companiesModel->getListId();
    _operatorsWidget->setSettings(_settings);
}

void OperatorsWidgetController::update()
{
    commandGetSelectOperators(_settings);
}

void OperatorsWidgetController::commandGetSelectOperators(const SettingsOperators & data)
{
    _settings = data;

    _flagRefreshOperators = true;
    emit signalRefresh(data._dateStart);
}

void OperatorsWidgetController::refreshOperators(const SettingsOperators& data)
{
    QStringList sqlQueries;
    QList<QVariant> sqlParameters;
    sql_utils::SqlString sqlStr;

    sqlStr.addSqlParameters(data._companies, "complaints", "_company", sqlQueries, sqlParameters);
    sqlStr.addSqlLike(data._like, "complaints", sqlQueries, sqlParameters);

    const auto stringDepartmentsUser = permissions::UserPermission::instance().getDepartmentsOperatorsUser().join(", ");

    QString params = "WHERE (complaints.date_create "
                     "BETWEEN strftime('%Y-%m-%dT%H:%M:%S', DATETIME('" + data._dateStart.toString("yyyy-MM-dd hh:mm:ss") + "')) " +
                     "AND strftime('%Y-%m-%dT%H:%M:%S', DATETIME(" + (!data._isSetDateEnd ? "'now', 'localtime'" : (QString("'") +
                     data._dateEnd.toString("yyyy-MM-dd hh:mm:ss") + "'")) + "))) " +
                     (sqlQueries.count() > 0 ? ("AND (" + sqlQueries.join(" OR ") + ")") : "") +
                     " AND complaints.id_department IN (" + stringDepartmentsUser + ") "
                     " AND complaints_nature.id_target = 2 ORDER BY complaints.date_create DESC";

    const QString& currentSqOperators = _defaultSqlOperators + params.arg("");
    auto newQuery = _dbManager->getDBWraper()->query();
    if (!newQuery.prepare(currentSqOperators))
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    for (int i = 0; i < sqlParameters.count(); i++)
        newQuery.bindValue(i, sqlParameters[i]);

    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    _operatorsModel->setQuery(newQuery);
    _operatorsWidget->updateOperators();

    if (!newQuery.first())
        emit signalShowMessageStatusBarOperators("По вашему запросу жалоб не найдено", 5);

    createInfoStatusBar(params, sqlParameters);
}

void OperatorsWidgetController::fillNature()
{
    const QString& sql = "SELECT data_nature, id FROM complaints_nature WHERE id_visiable = 1 AND id_company = ?";
    auto newQuery = _dbManager->getDBWraper()->query();
    if (!newQuery.prepare(sql))
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    auto it = _companiesModel->checkedRealIndex().begin();
    newQuery.bindValue(0, *it);
    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    _natureModel->setQuery(newQuery);
    _operatorsWidget->updateNature();
}

void OperatorsWidgetController::fillingEditsInfoComplaint(const quint64 id)
{
    const int TAB_OPERATOR = 2;
    _workerInfoController.fillingEditsInfo(id, TAB_OPERATOR);
}

void OperatorsWidgetController::createWorkerInfoControllerRun()
{
    _workerInfoController.setDBManager(_dbManager);
    _workerInfoController.setRequestsManager(_requestsManager);
    connect(&_workerInfoController, &WorkerInformationController::signalOpenComplaintsWorker,
            this, &OperatorsWidgetController::signalOpenComplaintsWorker);
    _workerInfoController.run();
    _operatorsWidget->setComplaintInfoWidget(_workerInfoController.getWidget());
    _workerInfoController.hideWidget();
}

void OperatorsWidgetController::createExcelFile(const QString &fileName)
{
#ifdef Q_OS_WIN
    // map company
    QMap<quint64, QString> mapSheet = _companiesModel->getCheckedRealIndexMap();
    QMap<quint64, quint64> mapRowIndex;

    // create excel doc
    QXlsx::Document doc;

    // create list in excel file and create header
    for (auto i = mapSheet.begin(); i != mapSheet.end(); ++i)
    {
        mapRowIndex[i.key()] = 1;

        QString &nameCompany = i.value().left(i.value().indexOf(" ("));
        doc.addSheet(nameCompany);
        doc.setColumnWidth(1,  1, 15);
        doc.setColumnWidth(2,  2, 15);
        doc.setColumnWidth(3,  3, 40);
        doc.setColumnWidth(4,  4, 20);
        doc.setColumnWidth(5,  5, 20);
        doc.setColumnWidth(6,  6, 15);
        doc.setColumnWidth(7,  7, 20);
        doc.setColumnWidth(8,  8, 25);
        doc.setColumnWidth(9,  9, 50);
        doc.setColumnWidth(10, 10, 22);
        doc.setColumnWidth(11, 11, 20);

        QXlsx::Format capitonStyle;
        capitonStyle.setFontName("Arial");
        capitonStyle.setFontSize(10);
        capitonStyle.setFontColor(Qt::black);
        capitonStyle.setFontBold(true);
        capitonStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
        capitonStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        capitonStyle.setPatternBackgroundColor(QColor::fromRgb(146,208,80));
        capitonStyle.setBorderColor(Qt::black);
        capitonStyle.setBorderStyle(QXlsx::Format::BorderThin);

        doc.write(1, 1,  "№ жалобы",                 capitonStyle);
        doc.write(1, 2,  "№ заказа",                 capitonStyle);
        doc.write(1, 3,  "ФИО",                      capitonStyle);
        doc.write(1, 4,  "На кого",                  capitonStyle);
        doc.write(1, 5,  "Принадлежность",           capitonStyle);
        doc.write(1, 6,  "№ авто",                   capitonStyle);
        doc.write(1, 7,  "Характер жалобы",          capitonStyle);
        doc.write(1, 8,  "Дата",                     capitonStyle);
        doc.write(1, 9,  "Суть",                     capitonStyle);
        doc.write(1, 10, "Статус",                   capitonStyle);
        doc.write(1, 11, "Департамент",              capitonStyle);
        doc.setRowHeight(1, 1, 20);
    }

    // create text format
    QXlsx::Format baseDataStyle;
    baseDataStyle.setFontName("Arial");
    baseDataStyle.setFontColor(Qt::black);
    baseDataStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    uint overallRowIndex = 0;

    for (auto rowIndex = 0; rowIndex < _operatorsModel->rowCount(); ++rowIndex)
    {
        auto newQuery = _dbManager->getDBWraper()->query();
        if (!newQuery.prepare(_defaultSqlExcel))
        {
            qDebug() << "ERROR:" << newQuery.lastError().text();
            Q_ASSERT(false);
        }

        newQuery.bindValue(":idComplaint", _operatorsModel->index(rowIndex, 0).data());
        if (!newQuery.exec())
        {
            qDebug() << "ERROR:" << newQuery.lastError().text();
            Q_ASSERT(false);
        }

        if (newQuery.first())
        {
            auto idComplaint    = newQuery.value("id").toLongLong();
            auto idOrder        = newQuery.value("idOrder").toLongLong();
            auto driverName     = newQuery.value("driverName").toString();
            auto target         = newQuery.value("target").toString();
            auto whoseDriver    = newQuery.value("whoseDriver").toInt();
            auto carNumber      = newQuery.value("carNumber").toString();
            auto nature         = newQuery.value("nature").toString();
            auto dateCreate     = newQuery.value("dateCreate").toDateTime().toString("yyyy.MM.dd hh:mm:ss");
            auto essence        = newQuery.value("essence").toString();
            auto status         = newQuery.value("status").toString();
            auto nameDepartment = newQuery.value("nameDepartment").toString();
            auto idCompany      = newQuery.value("idCompany").toInt();

            if (mapRowIndex.contains(idCompany))
            {
                QString &nameCompany = mapSheet[idCompany].left(mapSheet[idCompany].indexOf(" ("));
                doc.selectSheet(nameCompany);
                overallRowIndex = mapRowIndex[idCompany];
                ++overallRowIndex;
                mapRowIndex[idCompany] = overallRowIndex;
            }
            else
                continue;

            doc.write(overallRowIndex, 1,  idComplaint,    baseDataStyle);
            doc.write(overallRowIndex, 2,  idOrder,        baseDataStyle);
            doc.write(overallRowIndex, 3,  driverName,     baseDataStyle);
            doc.write(overallRowIndex, 4,  target,         baseDataStyle);
            if(!whoseDriver)
                doc.write(overallRowIndex, 5,  "Наш",      baseDataStyle);
            else
                doc.write(overallRowIndex, 5,  "Частник",  baseDataStyle);
            doc.write(overallRowIndex, 6,  carNumber,      baseDataStyle);
            doc.write(overallRowIndex, 7,  nature,         baseDataStyle);
            doc.write(overallRowIndex, 8,  dateCreate,     baseDataStyle);
            doc.write(overallRowIndex, 9,  essence,        baseDataStyle);
            doc.write(overallRowIndex, 10, status,         baseDataStyle);
            doc.write(overallRowIndex, 11, nameDepartment, baseDataStyle);

        }
    }

    if (!doc.saveAs(fileName))
        QMessageBox::warning(nullptr, "Ошибка", "Не получилось записать файл");
#endif
}

void OperatorsWidgetController::finishedRefreshComplaints()
{
    if (_flagRefreshOperators)
        refreshOperators(_settings);
    _flagRefreshOperators = false;
}
