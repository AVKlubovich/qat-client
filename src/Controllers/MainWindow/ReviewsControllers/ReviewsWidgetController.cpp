#include "Common.h"
#include "ReviewsWidgetController.h"

#include "Widgets/MainWindow/ReviewsWidgets/ReviewsWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/Users/Users.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "utils/Sql/SqlString.h"

#include "Models/ComplaintsModel.h"
#include "Models/CheckedModel.h"
#include "Models/ReviewsModel.h"

#include "permissions/UserPermission.h"

#include "Controllers/ResponceChecker.h"


using namespace qat_client;

const QString ReviewsWidgetController::_defaultSqlReviews(QString(
    "SELECT "
    "reviews.id AS '№', "
    "%1 AS 'Дата создания', "
    "company.name AS 'Компания', "
    "reviews_source.name_source AS 'Откуда', "
    "reviews.id_order AS '№ заказа', "
    "CASE WHEN reviews.type = 0 THEN 'Положительная' "
    "ELSE 'Отрицательная' END AS Тип, "
    "reviews.id_status "
    "FROM reviews "
    "INNER JOIN reviews_source ON (reviews.id_source = reviews_source.id) "
    "INNER JOIN company ON (company.id = reviews.id_company) ").arg(sql_utils::stringSqlDate));

const QString ReviewsWidgetController::_defaultSqlExcel(
        "SELECT"
            " r.id AS id,"
            " r.id_order AS idOrder,"
            " driver.surname || ' ' || driver.firstname || ' ' || driver.middlename AS driverName,"
            " r.comment AS essence,"
            " answer.id_question AS idQ,"
            " answer.answer AS answer,"
            " driver.id_park AS park,"
            " r.id_status AS idStatus,"
            " r.id_company AS idCompany,"
            "CASE WHEN r.type = 0 THEN 'Положительная' "
            "ELSE 'Отрицательная' END AS Type "
        " FROM"
        " reviews AS r"
        " LEFT JOIN reviews_orders_drivers AS driver ON (r.id = driver.id_review)"
        " INNER JOIN reviews_answer AS answer ON (r.id = answer.id_reviews)"
        " WHERE r.id = :idReview"
        " ORDER BY idQ"
        );

ReviewsWidgetController::ReviewsWidgetController()
    : QObject(nullptr)
    , _requestsManager(network::RequestsManagerShp::create())
{
}

ReviewsWidgetController::~ReviewsWidgetController()
{
    if (_reviewsModel)
        _reviewsModel->deleteLater();

//    if (_complaintsWidget)
//        _complaintsWidget->deleteLater();
}

void ReviewsWidgetController::setDbManager(database::DBManagerShp dbManager)
{
    _dbManager = dbManager;
}

void ReviewsWidgetController::setReviewsWidget(ReviewsWidget * reviewsWidget)
{
    _reviewsWidget = reviewsWidget;
}

void ReviewsWidgetController::init()
{
    _reviewsModel = new ReviewsModel();
    _reviewsWidget->setReviewsModel(_reviewsModel);

    _companiesModel = new CheckedModel();
    _reviewsWidget->setCompaniesModel(_companiesModel);

    _sourceModel = new CheckedModel();
    _reviewsWidget->setSourcesModel(_sourceModel);

    createRecallInfoControllerRun();

    connect(_reviewsWidget, &ReviewsWidget::signalSelectReviews,
            this, &ReviewsWidgetController::commandGetSelectReviews);

    connect(_reviewsWidget, &ReviewsWidget::reviewsSelected, this,
            &ReviewsWidgetController::fillingEditsInfoRecall);

    connect(_reviewsWidget, &ReviewsWidget::signalCreateReviewsExcelFile,
            this, &ReviewsWidgetController::createExcelFile);

    connect(this, &ReviewsWidgetController::updateCompanies,
            _reviewsWidget, &ReviewsWidget::updateCompanies);

    connect(_reviewsWidget, &ReviewsWidget::signalCheckedAll,
            [this]()
    {
        _companiesModel->checkedAll();
        emit updateCompanies();
    });

    connect(_reviewsWidget, &ReviewsWidget::signalUncheckedAll,
            [this]()
    {
        _companiesModel->checkedClearAll();
        emit updateCompanies();
    });

    auto func = [this]()
    {
        _recallInfoController.hideWidget();
    };
    connect(_reviewsWidget, &ReviewsWidget::signalHideWidget, this, func);

    connect(&_recallInfoController, &RecallInfoController::signalCreateComplaint,
            this, &ReviewsWidgetController::createComplaintOfReview);

    connect(&_recallInfoController, &RecallInfoController::signalIdComplaintOpen,
            this, &ReviewsWidgetController::signalIdComplaintOpen);

    connect(&_recallInfoController, &RecallInfoController::signalOpenNextReviewInfoController,
            _reviewsWidget, &ReviewsWidget::slotOpenNextItemReview);
}

void ReviewsWidgetController::run()
{
    if (_isInintSettings)
    {
        update();
        return;
    }
    _isInintSettings = true;

    _companiesModel->setQuery("SELECT name, id FROM company WHERE isVisible = 1",
                              *(_dbManager->getConnection().data()));

    _sourceModel->setQuery("SELECT name_source, id FROM reviews_source",
                           *(_dbManager->getConnection().data()));

    _settings._dateStart = QDateTime::currentDateTime().addDays(-4);
    _settings._dateStart.setTime(QTime(0, 0));
    _settings._dateEnd = QDateTime::currentDateTime().addSecs(3000);
    _settings._isSetDateEnd = false;
    _lastDateTimeDbaseReviews = _settings._dateStart;
    _settings._companies = _companiesModel->getListId();
    _settings._sources = _sourceModel->getListId();
    _reviewsWidget->setSettings(_settings);

    refreshReviews(_settings);
}

void ReviewsWidgetController::update()
{
    commandGetSelectReviews(_settings);
}

void ReviewsWidgetController::commandGetSelectReviews(const SettingsReviews & data)
{
    _settings = data;

    if (!_lastDateTimeDbaseReviews.isValid())
        _lastDateTimeDbaseReviews = QDateTime::currentDateTime();

    if (data._dateStart < _lastDateTimeDbaseReviews)
    {
        auto request = [this]()
        {
            QVariantMap requestMap;
            requestMap["type_command"] = "get_select_reviews";
            requestMap["date_last_query"] = QDateTime::currentDateTime();
            requestMap["date_start"] = _settings._dateStart.toString("yyyy-MM-dd hh:mm:ss");
            requestMap["date_end"] = _lastDateTimeDbaseReviews.toString("yyyy-MM-dd hh:mm:ss");
            requestMap["idUser"] = permissions::UserPermission::instance().idUser();
            network::RequestShp request(new network::DefaultRequest("get_select_reviews", QVariant::fromValue(requestMap)));
            _requestsManager->execute(request, this, &ReviewsWidgetController::onGetSelectReviews);
            _lastDateTimeDbaseReviews = _settings._dateStart;
        };

        Reconnector reconnectorObj(request);

        _reconnectMap.insert("get_select_reviews", reconnectorObj);
        reconnectorObj.run();
    }
    else
    {
        refreshReviews(_settings);
    }
}

void ReviewsWidgetController::onGetSelectReviews(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_select_reviews"];
        reconnector.run();
        return;
    }

    const auto dbWraper = _dbManager->getDBWraper();
    auto insertQuery = dbWraper->query();

    const auto& reviews = responseObj->bodyToVariant().toMap()["reviews"].toList();
    for (const auto& review : reviews)
    {
        const auto& map = review.toMap();

        const QStringList& listKeys = map.keys();
        const QString& stringKeys = listKeys.join(", ");

        QStringList listValues;
        for (const auto& key : listKeys)
            listValues.append("'" + map.value(key).toString() + "'");

        const QString& stringValues = listValues.join(", ");

        const QString& sqlQuery = "INSERT OR REPLACE INTO reviews (" + stringKeys + ") VALUES (" + stringValues + ")";

        insertQuery.prepare(sqlQuery);
        const auto result = dbWraper->execQuery(insertQuery);
        if (!result)
        {
            qDebug() << insertQuery.lastError();
            qDebug() << __FUNCTION__ << "query failed";
            Q_ASSERT(false);
        }
    }

    refreshReviews(_settings);
}

void ReviewsWidgetController::refreshReviews(const SettingsReviews & data)
{
    QStringList sqlQueries;
    QList<QVariant> sqlParameters;
    sql_utils::SqlString sqlStr;

    sqlStr.addSqlParametersReview(data._companies, "reviews", "_company", sqlQueries, sqlParameters);
    sqlStr.addSqlParametersReview(data._sources, "reviews", "_source", sqlQueries, sqlParameters);
    sqlStr.addSqlLikeReview(data._like, "reviews", sqlQueries, sqlParameters);

    QString params = "WHERE (reviews.date_create "
                     "BETWEEN strftime('%Y-%m-%dT%H:%M:%S', DATETIME('" + data._dateStart.toString("yyyy-MM-dd hh:mm:ss") + "')) "
                     "AND strftime('%Y-%m-%dT%H:%M:%S', DATETIME(" + (!data._isSetDateEnd ? "'now', 'localtime'" : (QString("'") + data._dateEnd.toString("yyyy-MM-dd hh:mm:ss") + "'")) + "))) " +
//                     "AND " + sqlQuery +
                     (sqlQueries.count() > 0 ? ("AND (" + sqlQueries.join(" AND ") + ")") : "") +
//                     (sqlQueries.count() > 0 ? (" AND (" + sqlQueries.join(" OR ") + ")") : "") +
                     (!data._isCloseReviews ? " AND reviews.id_status = '0'" : "") +
                     "%1 "
                     " ORDER BY reviews.date_create DESC";

    const QString& currentSqlReviews = _defaultSqlReviews + params.arg("");

    auto newQuery = _dbManager->getDBWraper()->query();
    if (!newQuery.prepare(currentSqlReviews))
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }
    for (int i = 0; i < sqlParameters.count(); i++)
        newQuery.bindValue(i, sqlParameters[i]);

    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        Q_ASSERT(false);
    }

    _reviewsModel->setQuery(newQuery);
    _reviewsWidget->updateReviews();

    if (!newQuery.first())
    {
//        QMessageBox msg;
//        msg.setText("По вашему запросу отзывов не найдено");
//        msg.exec();

        emit signalShowMessageStatusBarReviews("По вашему запросу отзывов не найдено", 5);
    }

    if (_settings._idSelectedReview != 0)
    {
        fillingEditsInfoRecall(_settings._idSelectedReview);
        _settings._idSelectedReview = 0;
    }

    createInfoStatusBar(params, sqlParameters);
}

void ReviewsWidgetController::createComplaintOfReview(const quint64 idOrder,
                                                      const quint64 idCompany,
                                                      const quint64 idReview,
                                                      const quint64 idSource,
                                                      const QString& comment)
{
    _createComplaintController.setDataReview(idOrder, idCompany, idReview, idSource, comment);
    createComplaintControllerRun();
}

void ReviewsWidgetController::createExcelFile(const QString &fileName)
{
#ifdef Q_OS_WIN
    // map company
    QMap<quint64, QString> mapSheet = _companiesModel->getCheckedRealIndexMap();
    QMap<quint64, quint64> mapRowIndex;

    enum
    {
        question_1 = 0,
        question_2,
        question_3,
        question_4,
        question_5,
        question_6
    };

    // create excel doc
    QXlsx::Document doc;

    // create list in excel file and create header
    for (auto i = mapSheet.begin(); i != mapSheet.end(); ++i)
    {
        mapRowIndex[i.key()] = 1;

        QString &nameCompany = i.value().left(i.value().indexOf(" ("));
        doc.addSheet(nameCompany);
        doc.setColumnWidth(1, 1, 10);
        doc.setColumnWidth(2, 2, 10);
        doc.setColumnWidth(3, 3, 40);
        doc.setColumnWidth(4, 4, 30);
        doc.setColumnWidth(5, 5, 85);
        doc.setColumnWidth(6, 6, 25);
        doc.setColumnWidth(7, 7, 25);
        doc.setColumnWidth(8, 8, 25);


        QXlsx::Format capitonStyle;
        capitonStyle.setFontName("Arial");
        capitonStyle.setFontSize(10);
        capitonStyle.setFontColor(Qt::black);
        capitonStyle.setFontBold(true);
        capitonStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
        capitonStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        capitonStyle.setPatternBackgroundColor(QColor::fromRgb(146,208,80));
        capitonStyle.setBorderColor(Qt::black);
        capitonStyle.setBorderStyle(QXlsx::Format::BorderThin);

        doc.write(1, 1,  "№ отзыва",                 capitonStyle);
        doc.write(1, 2,  "№ заказа",                 capitonStyle);
        doc.write(1, 3,  "ФИО водителя / логиста",   capitonStyle);
        doc.write(1, 4,  "Суть отзыва",              capitonStyle);
        doc.write(1, 5,  "Ответы на вопросы",        capitonStyle);
        doc.write(1, 6,  "АТП",                      capitonStyle);
        doc.write(1, 7,  "Тип",                      capitonStyle); // положительная/отрицательная
        doc.write(1, 8,  "Статус",                   capitonStyle);

        doc.setRowHeight(1, 1, 20);
    }

    // create text format
    QXlsx::Format baseDataStyle;
    baseDataStyle.setFontName("Arial");
    baseDataStyle.setFontColor(Qt::black);
    baseDataStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    for (auto rowIndex = 0; rowIndex < _reviewsModel->rowCount(); ++rowIndex)
    {
        auto newQuery = _dbManager->getDBWraper()->query();
        if (!newQuery.prepare(_defaultSqlExcel))
        {
            qDebug() << "ERROR:" << newQuery.lastError().text();
            Q_ASSERT(false);
        }
        newQuery.bindValue(":idReview", _reviewsModel->index(rowIndex, 0).data());
        if (!newQuery.exec())
        {
            qDebug() << "ERROR:" << newQuery.lastError().text();
            Q_ASSERT(false);
        }

        bool state = true;
        while (newQuery.next())
        {
            // определение компании
            const auto idCompany = newQuery.value("idCompany").toULongLong();
            if (mapRowIndex.contains(idCompany))
            {
                QString &nameCompany = mapSheet[idCompany].left(mapSheet[idCompany].indexOf(" ("));
                doc.selectSheet(nameCompany);
                overallRowIndex = static_cast<qint32>(mapRowIndex[idCompany]);
                ++overallRowIndex;
                mapRowIndex[idCompany] = overallRowIndex;
            }
            else
                continue;

            // запись основных данных
            if (state)
            {
                auto idReview       = newQuery.value("id").toInt();
                auto idOrder        = newQuery.value("idOrder").toLongLong();
                auto driverName     = newQuery.value("driverName").toString();
                auto essence        = newQuery.value("essence").toString();
                auto park           = newQuery.value("park").toLongLong();
                auto idStatus       = newQuery.value("idStatus").toLongLong();
                auto type           = newQuery.value("type").toString();

                doc.write(overallRowIndex, 1,  idReview,      baseDataStyle);
                doc.write(overallRowIndex, 2,  idOrder,       baseDataStyle);
                doc.write(overallRowIndex, 3,  driverName,    baseDataStyle);
                doc.write(overallRowIndex, 4,  essence,       baseDataStyle);
                doc.write(overallRowIndex, 6,  park,          baseDataStyle);
                doc.write(overallRowIndex, 7,  type,          baseDataStyle);
                if (idStatus)
                    doc.write(overallRowIndex, 8,  "Закрыта", baseDataStyle);
                else
                    doc.write(overallRowIndex, 8,  "Открыта", baseDataStyle);
                state = false;
            }

            // вопросы ответы
            QString question;
            QString text;
            auto answer = newQuery.value("answer").toString();

            switch (newQuery.value("idQ").toInt())
            {
                case question_1:
                    question = "Водитель предложил пристегнуть ремень: ";
                    text = question + answer;
                    doc.write(overallRowIndex, 5,  text,    baseDataStyle);
                    break;
                case question_2:
                    question = "В салоне было чисто: ";
                    text = question + answer;
                    doc.write(overallRowIndex, 5,  text,    baseDataStyle);
                    break;
                case question_3:
                    question = "Водитель вёл машину аккуратно: ";
                    text = question + answer;
                    doc.write(overallRowIndex, 5,  text,    baseDataStyle);
                    break;
                case question_4:
                    question = "Водитель предложил отрегулировать температуру воздуха в салоне: ";
                    text = question + answer;
                    doc.write(overallRowIndex, 5,  text,    baseDataStyle);
                    break;
                case question_5:
                    question = "В салоне был приятный запах: ";
                    text = question + answer;
                    doc.write(overallRowIndex, 5,  text,    baseDataStyle);
                    break;
                case question_6:
                    question = "Водитель предложил включить радиостанцию по Вашему вкусу: ";
                    text = question + answer;
                    doc.write(overallRowIndex, 5,  text,    baseDataStyle);
                    break;
                default:
                    break;
            }
        }
    }

    if (!doc.saveAs(fileName))
        qDebug() << "ПИЗДЕЦ";
#endif
}

void ReviewsWidgetController::createComplaintControllerRun()
{
    _createComplaintController.setRequestsManager(_requestsManager);
    _createComplaintController.setDBManager(_dbManager);
    _createComplaintController.run();
}

void ReviewsWidgetController::createRecallInfoControllerRun()
{
    _recallInfoController.setDBManager(_dbManager);
    _recallInfoController.setRequestsManager(_requestsManager);
    _recallInfoController.run();
    _reviewsWidget->setReviewsInfoWidget(_recallInfoController.getWidget());
    _recallInfoController.hideWidget();
}

void ReviewsWidgetController::fillingEditsInfoRecall(const quint64 id)
{
    _recallInfoController.fillingEditsInfo(id);
}

void ReviewsWidgetController::setFindDataReview(const quint64 id)
{
    if (!_companiesModel->rowCount())
        _companiesModel->setQuery("SELECT name, id FROM company WHERE isVisible = 1",
                                  *(_dbManager->getConnection().data()));

    if (!_sourceModel->rowCount())
        _sourceModel->setQuery("SELECT name_source, id FROM reviews_source",
                               *(_dbManager->getConnection().data()));

    _settings._dateStart = QDateTime(QDate(2014, 1, 1), QTime(0, 0));
    _settings._dateStart.setTime(QTime(0, 0));
    _settings._dateEnd = QDateTime::currentDateTime().addSecs(3000);
    _settings._isSetDateEnd = false;
    _lastDateTimeDbaseReviews = _settings._dateStart;
    _settings._companies = _companiesModel->getListId();
    _settings._sources = _sourceModel->getListId();
    _settings._like = QString::number(id);
    _settings._idSelectedReview = id;
    _reviewsWidget->setSettings(_settings);

    _isInintSettings = true;
}

void ReviewsWidgetController::refreshFetch()
{
    if (_reviewsWidget->getChbIsSetDateEnd()->isChecked())
        refreshReviews(_settings);
}

void qat_client::ReviewsWidgetController::createInfoStatusBar(QString &params, const QVariantList &sqlParameters)
{
    if (!_reviewsWidget->isVisible())
        return;

    const QString sqlQuery = "SELECT id_status + 6, count(id_status) AS count FROM reviews "
                             + params.arg("GROUP BY reviews.id_status ");

    auto query = _dbManager->getDBWraper()->query();
    if (!query.prepare(sqlQuery))
    {
        qDebug() << "ERROR:" << query.lastError().text();
        Q_ASSERT(false);
    }

    for (int i = 0; i < sqlParameters.count(); i++)
        query.bindValue(i, sqlParameters[i]);

    if (!query.exec())
    {
        qDebug() << "ERROR:" << query.lastError().text();
        Q_ASSERT(false);
    }

    int allCount = 0;
    QMap<int, int> mapStatusBar =
    {
        {0, 0},
        {6, 0},
        {7, 0}
    };

    while (query.next()) {
        mapStatusBar[query.value(0).toInt()] = query.value(1).toInt();
        allCount += query.value(1).toInt();
    }
    mapStatusBar[0] = allCount;

    emit signalStatusBarValue(mapStatusBar, false);
}
