﻿#pragma once

#include "../../RecallInfoController.h"
#include "../../CreateComplaintController.h"

#include "../../Reconnector.h"


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class ReviewsWidget;

    class CheckedModel;


    class ReviewsWidgetController : public QObject
    {
        Q_OBJECT

    public:
        struct SettingsReviews
        {
            SettingsReviews()
            {
                clear();
            }
            
            QDateTime _dateStart;
            QDateTime _dateEnd;
            bool _isSetDateEnd;
            QList<quint64> _companies;
            QList<quint64> _sources;
            bool _isCloseReviews;
            QString _like;
            quint64 _idSelectedReview;

            void clear()
            {
                _idSelectedReview = 0;
                _companies.clear();
                _like = "";
                _isSetDateEnd = false;
                _dateStart = QDateTime::currentDateTime().addDays(-4);
                _dateStart.setTime(QTime(0, 0));
                _dateEnd = QDateTime::currentDateTime().addSecs(3000);
                _isSetDateEnd = false;
            }
        };

    public:
        ReviewsWidgetController();
        ~ReviewsWidgetController();

        void setDbManager(database::DBManagerShp dbManager);
        void setReviewsWidget(ReviewsWidget * reviewsWidget);
        void setFindDataReview(const quint64 id);
        void refreshFetch();

    private:
        void createInfoStatusBar(QString &params, const QVariantList &sqlParameters);
        
    public slots:
        void init();
        void run();
        void update();

        void fillingEditsInfoRecall(const quint64 id);

    private slots:
        // NOTE: send commands
        void commandGetSelectReviews(const SettingsReviews & data);

        // NOTE: data processing
        void onGetSelectReviews(network::ResponseShp);

        void refreshReviews(const SettingsReviews& data);

        // NOTE: transfer of control to controllers
        void createRecallInfoControllerRun();
        void createComplaintControllerRun();
        void createComplaintOfReview(const quint64 idOrder,
                                     const quint64 idCompany,
                                     const quint64 idReview,
                                     const quint64 idSource,
                                     const QString& comment);
        void createExcelFile(const QString & fileName);

    signals:
        void signalIdComplaintOpen(const quint64 id);
        void updateCompanies();

        void signalStatusBarValue(const QMap<int, int> &map, bool flag);
        void signalShowMessageStatusBarReviews(const QString &msg, quint32 sec = 0);

    private:
        network::RequestsManagerShp _requestsManager;
        database::DBManagerShp _dbManager;

        ReviewsWidget *_reviewsWidget = nullptr;
        QDateTime _lastDateTimeDbaseReviews;
        SettingsReviews _settings;

        // NOTE: controllers
        RecallInfoController _recallInfoController;
        CreateComplaintController _createComplaintController;

        // NOTE: models
        QSqlQueryModel* _reviewsModel;
        CheckedModel* _companiesModel;
        CheckedModel* _sourceModel;

        static const QString _defaultSqlReviews;
        static const QString _defaultSqlExcel;

        bool _isInintSettings = false;
        int overallRowIndex;

        QMap<QString, Reconnector> _reconnectMap;
    };
}
