﻿#pragma once

#include "Widgets/ScrollWidget/ScrollWidget.h"
#include "Models/StatusModel.h"
#include "Reconnector.h"


namespace qat_client
{
    class ComplaintInfoWidget;
    class ScrollWidget;
    class CreateComplaintDialog;
}

namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class ComplaintInfoController : public QObject
    {
        Q_OBJECT

    public:
        struct UpdateParametrs
        {
            quint64 id;
            int idNature;
            QString rating;
            quint64 idUser;
            int idType;
            int idStatus;
            quint64 idDepartament;
            int isWork;
            QString essence;
            QString comment;
            quint64 idOrder;
            int daysToReview;
        };

    public:
        ComplaintInfoController();
        ~ComplaintInfoController() = default;

        void setRequestsManager(const network::RequestsManagerShp& rManager);
        void setDBManager(const database::DBManagerShp& dbManager);

        QWidget *getWidget();
        void hideWidget();
        void showWidget();

    signals:
        void signalRefreshModel();
        void signalIdReviewOpen(const quint64 id);
        void signalOpenComplaintsWorker(const quint64 idWorker, const int keyWorker, const int statusType);
        void signalInfoControllerFindComplaints(QVariantList list);
        void signalOnenNextComplaint();

    public slots:
        void run();

        void fillingEditsInfo(const quint64 id);

        void commandTakenInfoOperation(bool isWork);
        void commandUpdateComplaint(const UpdateParametrs settings);
        void commandGetSmsTemplates();
        void commandSendSmsToServer(const QString &smsText, const QString &phone, const QDateTime &date, int type, double mark = 0);
        void onTakenInfoOperation(network::ResponseShp responseObj);
        void onUpdateComplaint(network::ResponseShp responseObj);
        void onGetSmsTemplates(network::ResponseShp responseObj);
        void onSendSms(network::ResponseShp responseObj);

    private slots:
        void commandGetSelectComplaints(const QString &idTarget, int statusType);
        void commandGetHistoryList();
        void requestBtnChallenge();

    private:
        void createInfoModel();
        void createLinkModel();
        void createTypeModel(const int &idType);
        void createStatusModel(const int &idStatus);
        void createUsersModel(const quint64 &idUser);
        void createNatureModel(const int idTarget, const int idCompany, const int idNature);
        void createDepartamentModel(const quint64 idCompany, const quint64 idDepartament);
        int searchRow(QAbstractItemModel *model, const int id);

        void responseInfoStatistic(network::ResponseShp responseObj);
        void responseComments(network::ResponseShp responseObj);
        void responseHistory(network::ResponseShp responseObj);
        void onGetSelectComplaints(network::ResponseShp responseObj);
        void parseGetSelectComplaints(const QVariantMap& inData);
        void onParseGetSelectComplaintsFinished();
        void responceBtnChallenge(network::ResponseShp responseObj);
        void hideHistory();

    private:
        ComplaintInfoWidget* _complaintInfoWidget = nullptr;
        ScrollWidget* _scrollWidget = nullptr;

        QSqlQueryModel* _infoModel = nullptr;
        QSqlQueryModel* _typeModel = nullptr;
        StatusModel* _statusModel = nullptr;
        QSqlQueryModel* _usersModel = nullptr;
        QSqlQueryModel* _natureModel = nullptr;
        QSqlQueryModel* _departamentModel = nullptr;
        QSqlQueryModel* _commentsModel = nullptr;
        QSqlQueryModel* _linkModel = nullptr;
        QStandardItemModel *_templateModel = nullptr;
        QStandardItemModel *_phoneModel = nullptr;

        network::RequestsManagerShp _requestManager;
        database::DBManagerShp _dbManager;

        UpdateParametrs _settings;
        quint64 _idComplaint;

        quint64 _keyWorker;
        QString _valueWorker;
        quint64 _idWorker;
        int _statusType;

        QMap<QString, Reconnector> _reconnectMap;
    };

}
