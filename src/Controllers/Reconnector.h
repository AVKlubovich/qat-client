﻿#pragma once


namespace qat_client
{

    class Reconnector
    {
    public:
        Reconnector() = default;
        Reconnector(std::function<void(void)> reconnectMethod);
        ~Reconnector() = default;

        void run();
        const quint8 count() const;

    private:
        std::function<void(void)> _reconnectMethod;
        quint8 _counter = 0;
    };

}
