#include "Common.h"

#include "CreateUserController.h"
#include "network-core/RequestsManager/Users/RequestCreateUser.h"
#include "network-core/RequestsManager/Users/RequestUpdateUser.h"
#include "network-core/RequestsManager/ResponseGeneric.h"

#include "database/DBManager.h"


using namespace qat_client;

CreateUserController::CreateUserController()
    : QObject(nullptr)
{
    connect(&_createUserDialog, &CreateUserDialog::onCreateUser,
            this, &CreateUserController::createUser);

    connect(&_createUserDialog, &CreateUserDialog::onUpdateUser,
            this, &CreateUserController::updateUser);

    connect(&_requestsManager, SIGNAL(responseReady(QSharedPointer<network::Response>)),
            this, SLOT(responseCreateUserReady(QSharedPointer<network::Response>)));

    connect(&_createUserDialog, &CreateUserDialog::signalIdComapny,
            this, &CreateUserController::setIdDepartment);

    _companyModel = new QSqlQueryModel();
    _createUserDialog.setCompanyModel(_companyModel);

    _departmentModel = new QSqlQueryModel();
    _createUserDialog.setDepartmentModel(_departmentModel);
}

void CreateUserController::run()
{
    _companyModel->setQuery("SELECT name, id FROM company WHERE isVisible = 1",
                             *(_dbManager->getConnection().data()));
    bool res = _createUserDialog.exec();
    if (res == QDialog::Accepted)
    {
        ;//TODO: update user list table
    }
    else
    {
        ;//TODO: clean up _requestManager
    }
}

void CreateUserController::setDbManager(database::DBManagerShp dbManager)
{
    _dbManager = dbManager;
}

void CreateUserController::setDataUserControler(network::User &user)
{
    _idUser = user.id();
    _createUserDialog.setDataUser(user);
}

void CreateUserController::createUser()
{
    QString userName, userLogin, userPassword, userPosition, userCompany, userDepartment;
    _createUserDialog.getUserData(&userName, &userLogin, &userPassword, &userPosition, &userCompany, &userDepartment);

    if (userName.isEmpty() ||
        userLogin.isEmpty() ||
        userPassword.isEmpty() ||
        userPosition.isEmpty() ||
        userCompany.isEmpty() ||
        userDepartment.isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setText("Заполните все поля");
        msgBox.exec();
        _createUserDialog.setDisabledButtonSave();
        return;
    }

    //Send login request to server
    network::RequestCreateUser requestCreateUser(userName, userLogin, userPassword, userPosition,userCompany,userDepartment);
    _requestsManager.sendRequest(requestCreateUser);
}

void CreateUserController::updateUser()
{
    QString userName, userLogin, userPassword, userPosition, userCompany, userDepartment;
    _createUserDialog.getUserData(&userName, &userLogin, &userPassword, &userPosition, &userCompany, &userDepartment);

    if (userName.isEmpty() ||
        userLogin.isEmpty() ||
        userPassword.isEmpty() ||
        userPosition.isEmpty() ||
        userCompany.isEmpty() ||
        userDepartment.isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setText("Заполните все поля");
        msgBox.exec();
        _createUserDialog.setDisabledButtonSave();
        return;
    }

    //Send login request to server
    network::RequestUpdateUser requestUpdateUser(_idUser, userName, userLogin, userPassword, userPosition, userCompany, userDepartment);
    _requestsManager.sendRequest(requestUpdateUser);
}

void CreateUserController::responseCreateUserReady(QSharedPointer<network::Response> response)
{
    QString msg;
    //check success
    QSharedPointer<network::ResponseGeneric> responseGeneric = qSharedPointerDynamicCast<network::ResponseGeneric>(response);
    if (responseGeneric)
    {
        if (responseGeneric->status() == network::ResponseGeneric::StatusSuccess)
        {
            this->_createUserDialog.accept();
            return;
        }
        else
        {
            msg = responseGeneric->message();
        }
    }

    //check errors
    QSharedPointer<network::ResponseServerError> responseServerError = qSharedPointerDynamicCast<network::ResponseServerError>(response);
    if (responseServerError)
        msg = responseServerError->getErrorMessage();

    if (msg.isEmpty())
        msg = tr("Unknown error.");

    QMessageBox msgBox;
    msgBox.setText(msg);
    msgBox.exec();
    //retry
    _createUserDialog.execRetry();
}

void CreateUserController::responseCreateUserFailed(QSharedPointer<network::ResponseServerError> response)
{
    QMessageBox msgBox;
    msgBox.setText(response->getErrorMessage());
    msgBox.exec();
    //retry to login
    _createUserDialog.execRetry();
}

void CreateUserController::setIdDepartment(const int &index)
{
    const QString& sqlQuery = "SELECT name, id FROM departments WHERE isVisible = 1 AND id_company = "
                              + _companyModel->index(index, 1).data().toString();
    _departmentModel->setQuery(sqlQuery,
                               *(_dbManager->getConnection().data()));
}
