﻿#pragma once


#include "network-core/RequestsManager/Response.h"


namespace qat_client
{

    inline int checkResponse(network::ResponseShp response)
    {
        const auto& mapResponce = response->toVariant().toMap();
        const auto& head = mapResponce["head"].toMap();
        const auto& body = mapResponce["body"].toMap();

        const auto status = body["status"].toInt();
        const auto& errorString = body["error_string"].toString();

        switch (status)
        {
            case -1:
                break;
            case -2:
            {
                auto type = head["type"].toString();
                QMessageBox msg;
                msg.setText(errorString.arg(type));
                msg.exec();
                break;
            }
            default:
                break;
        }

        return 1;
//        return status;
    }

}
