﻿#pragma once

#include "Widgets/ScrollWidget/ScrollWidget.h"
#include "../Reconnector.h"


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class WorkerInformationWidget;

    class ScrollWidget;


    class WorkerInformationController : public QObject
    {
        Q_OBJECT

    public:
        WorkerInformationController();
        ~WorkerInformationController() = default;

        void setRequestsManager(const network::RequestsManagerShp& rManager);
        void setDBManager(const database::DBManagerShp& dbManager);

        QWidget *getWidget();
        void hideWidget();
        void showWidget();

        void fillingEditsInfo(const quint64 idComplaint, const int idWorker);

    public slots:
        void run();

    signals:
        void signalOpenComplaintsWorker(const quint64 idWorker, const int keyWorker, const int statusType);

    private:
        void createInfoModel();
        void createPhoneModel();

        void responseInfoStatistic(network::ResponseShp responseObj);
        void onGetSelectComplaints(network::ResponseShp responseObj);
        void parseGetSelectComplaints(const QVariantMap& inData);
        void onParseGetSelectComplaintsFinished();
        void responseComments(network::ResponseShp responseObj);
        void responceSaveComment(network::ResponseShp responseObj);
        void responseHistory(network::ResponseShp responseObj);
        void hideHistory();

    private slots:
        void commandGetSelectComplaints(const QString& idTarget, int statusType);
        void commandSaveComment(const QString& comment);
        void commandGetHistoryList();

        void createExcelFile(const QString& fileName);

    private:
        WorkerInformationWidget* _workerInfoWidget = nullptr;
        ScrollWidget* _scrollWidget = nullptr;

        network::RequestsManagerShp _requestManager;
        database::DBManagerShp _dbManager;

        QSqlQueryModel* _infoModel = nullptr;
        QSqlQueryModel* _phonesModel = nullptr;

        quint64 _idComplaint;
        int _keyWorker;
        int _statusType;
        QString _valueWorker;
        quint64 _idWorker;

        QMap<QString, Reconnector> _reconnectMap;
    };

}
