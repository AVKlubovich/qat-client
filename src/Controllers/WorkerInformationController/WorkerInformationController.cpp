#include "Common.h"
#include "WorkerInformationController.h"

#include "Widgets/WorkerInformationWidget/WorkerInformationWidget.h"
#include "Widgets/AttachmentWidget/AttachmentWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"

#include "permissions/UserPermission.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "Controllers/ResponceChecker.h"


using namespace qat_client;

WorkerInformationController::WorkerInformationController()
    : QObject(nullptr)
{
}

void WorkerInformationController::setRequestsManager(const network::RequestsManagerShp &rManager)
{
    _requestManager = rManager;
}

void WorkerInformationController::setDBManager(const database::DBManagerShp &dbManager)
{
    _dbManager = dbManager;
}

QWidget *WorkerInformationController::getWidget()
{
    return _scrollWidget;
}

void WorkerInformationController::hideWidget()
{
    _scrollWidget->hide();
}

void WorkerInformationController::showWidget()
{
    _scrollWidget->show();
}

void WorkerInformationController::fillingEditsInfo(const quint64 idComplaint, const int idWorker)
{
    QMap<int, QString> mapWorkers;
    mapWorkers[1] = "drivers";
    mapWorkers[2] = "operators";
    mapWorkers[3] = "logists";

    _keyWorker = idWorker;
    _valueWorker = mapWorkers[idWorker];

    enum
    {
        drivers = 1,
        operators,
        logists
    };

    _idComplaint = idComplaint;
    QString sqlQuery =
            "SELECT"
            " driver.id_worker AS idDriver,"
            " %1"
            " CASE WHEN car.id IS NOT NULL THEN car.number ELSE NULL END AS numberCar,"
            " car.model AS modelCar,"
            " car.color AS colorCar,"
            " complaints.id_company"
            " FROM"
            " %2 AS driver"
            " LEFT JOIN complaints_orders_cars AS car ON (driver.id_complaint = car.id_complaint)"
            " INNER JOIN complaints ON (driver.id_complaint = complaints.id)"
            " INNER JOIN company ON (company.id = complaints.id_company)"
            " %3"
            " WHERE driver.id_complaint = :idComplaint";

    QString parametrs;
    QString table;
    QString park = "";
    switch (idWorker) {
        case drivers:
            parametrs = " driver.surname || ' ' || driver.firstname || ' ' || driver.middlename AS driverName,"
                        " company.name || ' - '|| park.park_name AS namePark, "
                        " driver.column AS driverColumn, "
                        " driver.date_come AS dateComeDriver, ";
            table = "complaints_orders_drivers";
            park = " LEFT JOIN parks AS park ON (park.id = driver.id_park AND park.id_company = complaints.id_company)";
            break;
        case operators:
            parametrs = " driver.fio AS driverName,"
                        " NULL AS namePark,"
                        " NULL AS dateComeDriver,";
            table = "complaints_orders_operators";
            break;
        case logists:
            parametrs = " driver.fio AS driverName,"
                        " NULL AS namePark,"
                        " NULL AS dateComeDriver,";
            table = "complaints_orders_logists";
            break;
        default:
            break;
    }

    sqlQuery = sqlQuery.arg(parametrs).arg(table).arg(park);

    auto infoQuery = _dbManager->getDBWraper()->query();

    infoQuery.prepare(sqlQuery);
    infoQuery.bindValue(":idComplaint", idComplaint);
    infoQuery.exec();

    infoQuery.first();
    const quint64 idDriver = infoQuery.value("idDriver").toLongLong();

    _infoModel->setQuery(infoQuery);
    _workerInfoWidget->setInfoModel(_infoModel);

    infoQuery.prepare("SELECT * FROM complaints_orders_driver_phones "
                      "WHERE id_complaint = :idComplaint");
    infoQuery.bindValue(":idComplaint", idComplaint);
    infoQuery.exec();
    _phonesModel->setQuery(infoQuery);
    _workerInfoWidget->setPhonesModel(_phonesModel);

    if (_infoModel->rowCount())
        showWidget();

    // TODO запрос на сервер статистика
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "worker_statistic";
        requestMap["worker"] = mapWorkers[idWorker];
        requestMap["id"] = idDriver;
        network::RequestShp request(new network::DefaultRequest("worker_statistic", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &WorkerInformationController::responseInfoStatistic);
    }

    {
        auto request = [this]()
        {
            QVariantMap requestMap;
            requestMap["type_command"] = "get_comments";
            requestMap["schema"] = "complaints_schema";
            requestMap["id"] = _idComplaint;
            network::RequestShp request(new network::DefaultRequest("get_comments", QVariant::fromValue(requestMap)));
            _requestManager->execute(request, this, &WorkerInformationController::responseComments);
        };

        Reconnector reconnectorObj(request);

        _reconnectMap.insert("get_comments", reconnectorObj);
        reconnectorObj.run();
    }
}

void WorkerInformationController::responseComments(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_comments"];
        reconnector.run();

        return;
    }

    const auto& listComments = responseObj->bodyToVariant().toList();
    _workerInfoWidget->setComments(listComments);
}

void WorkerInformationController::responceSaveComment(network::ResponseShp responseObj)
{
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_comments";
        requestMap["schema"] = "complaints_schema";
        requestMap["id"] = _idComplaint;
        network::RequestShp request(new network::DefaultRequest("get_comments", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &WorkerInformationController::responseComments);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_comments", reconnectorObj);
    reconnectorObj.run();
}

void WorkerInformationController::responseHistory(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_history"];
        reconnector.run();
        return;
    }

    const auto& listHistory = responseObj->bodyToVariant().toList();
    _workerInfoWidget->setHistory(listHistory);
}

void WorkerInformationController::hideHistory()
{
    const auto& listDep = permissions::UserPermission::instance().getDepartmentsUser();
    const QString& sqlQuery = QString("SELECT * FROM departments WHERE id IN (%1) AND (name LIKE 'OKK%' OR name LIKE 'OКК%')")
                          .arg(listDep.join(", "));
    auto infoQuery = _dbManager->getDBWraper()->query();
    infoQuery.prepare(sqlQuery);
    infoQuery.exec();

    _workerInfoWidget->setPermissionsWidget(!infoQuery.first());
}

void WorkerInformationController::run()
{
    if (_workerInfoWidget)
        _workerInfoWidget->deleteLater();

    _workerInfoWidget = new WorkerInformationWidget();

    connect(_workerInfoWidget, &WorkerInformationWidget::signalTargetComplaints,
            this, &WorkerInformationController::commandGetSelectComplaints);

    connect(_workerInfoWidget, &WorkerInformationWidget::signalSaveComment,
            this, &WorkerInformationController::commandSaveComment);

    connect(_workerInfoWidget, &WorkerInformationWidget::signalCreateComplaintsExcelFile,
            this, &WorkerInformationController::createExcelFile);

    _scrollWidget = new ScrollWidget();
    _scrollWidget->setWidget(_workerInfoWidget);

    createInfoModel();
    createPhoneModel();

    connect(_workerInfoWidget, &WorkerInformationWidget::signalDownloadHistory,
            this, &WorkerInformationController::commandGetHistoryList);
    hideHistory();
}

void WorkerInformationController::createInfoModel()
{
    if (_infoModel)
        _infoModel->deleteLater();

    _infoModel = new QSqlQueryModel(this);
}

void WorkerInformationController::createPhoneModel()
{
    if (_phonesModel)
        _phonesModel->deleteLater();

    _phonesModel = new QSqlQueryModel(this);
}

void WorkerInformationController::responseInfoStatistic(network::ResponseShp responseObj)
{
    const auto& mapStatistic = responseObj->bodyToVariant().toMap();
    _workerInfoWidget->setStatistic(mapStatistic);
}

void WorkerInformationController::commandGetSelectComplaints(const QString &idTarget, int statusType)
{
    _idWorker = idTarget.toLongLong();
    _statusType = statusType;
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_select_complaints";
        requestMap["date_last_query"] = QDateTime::currentDateTime();
        requestMap["date_start"] = "2000-01-01 00:00:00";
        requestMap["date_end"] = QDateTime::currentDateTime().addSecs(3000).toString("yyyy-MM-dd hh:mm:ss");
        requestMap["table"] = _valueWorker;
        requestMap["id"] = _idWorker;
        requestMap["type"] = _statusType;
        requestMap["idUser"] = permissions::UserPermission::instance().idUser();

        network::RequestShp request(new network::DefaultRequest("get_select_complaints", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &WorkerInformationController::onGetSelectComplaints);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_select_complaints", reconnectorObj);
    reconnectorObj.run();
}

void WorkerInformationController::commandSaveComment(const QString &comment)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "add_comment";
    requestMap["id"] = _idComplaint;
    requestMap["comment"] = comment;
    requestMap["schema"] = "complaints_schema";
    requestMap["id_user"] = permissions::UserPermission::instance().idUser();

    network::RequestShp request(new network::DefaultRequest("add_comment", QVariant::fromValue(requestMap)));
    _requestManager->execute(request, this, &WorkerInformationController::responceSaveComment);
}

void WorkerInformationController::commandGetHistoryList()
{
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_history";
        requestMap["schema"] = "complaints_schema";
        requestMap["id"] = _idComplaint;
        network::RequestShp request(new network::DefaultRequest("get_history", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &WorkerInformationController::responseHistory);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_comments", reconnectorObj);
    reconnectorObj.run();
}

void WorkerInformationController::createExcelFile(const QString &fileName)
{
#ifdef Q_OS_WIN
    QXlsx::Document doc;

    doc.setColumnWidth(1, 1, 15);
    doc.setColumnWidth(2, 2, 15);
    doc.setColumnWidth(3, 3, 15);
    doc.setColumnWidth(4, 4, 40);
    doc.setColumnWidth(5, 5, 40);
    doc.setColumnWidth(6, 6, 15);
    doc.setColumnWidth(7, 7, 25);
    doc.setColumnWidth(8, 8, 40);
    doc.setColumnWidth(9, 9, 20);

    QXlsx::Format capitonStyle;
    capitonStyle.setFontName("Arial");
    capitonStyle.setFontSize(10);
    capitonStyle.setFontColor(Qt::black);
    capitonStyle.setFontBold(true);
    capitonStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    capitonStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    capitonStyle.setPatternBackgroundColor(QColor::fromRgb(146, 208, 80));
    capitonStyle.setBorderColor(Qt::black);
    capitonStyle.setBorderStyle(QXlsx::Format::BorderThin);

    doc.write(1, 1, "№ жалобы",                           capitonStyle);
    doc.write(1, 2, "№ заказа",                           capitonStyle);
    doc.write(1, 3, "№ водителя",                         capitonStyle);
    doc.write(1, 4, "ФИО водителя / оператора / логиста", capitonStyle);
    doc.write(1, 5, "Колонна",                            capitonStyle);
    doc.write(1, 6, "Парк",                               capitonStyle);
    doc.write(1, 7, "Дата",                               capitonStyle);
    doc.write(1, 8, "Характер жалобы",                    capitonStyle);
    doc.write(1, 9, "Статус",                             capitonStyle);

    doc.setRowHeight(1, 1, 20);

    QXlsx::Format baseDataStyle;
    baseDataStyle.setFontName("Arial");
    baseDataStyle.setFontColor(Qt::black);
    baseDataStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    QString sqlExcel = QString(
        "SELECT "
        "id_worker, "
        "surname || ' ' || firstname || ' ' || middlename AS driver_name, "
        "park_name, "
        "whose, "
        "id_order, "
        "essence, "
        "name_status, "
        "date_create "
        "FROM complaints_orders_%1 "
        "INNER JOIN parks "
        "ON (parks.id = id_park) "
        "INNER JOIN complaints "
        "ON (complaints.id = id_complaint) "
        "INNER JOIN complaints_status "
        "ON (complaints_status.id = id_status) "
        "WHERE "
        "id_complaint = :idComplaint")
        .arg(_valueWorker);

    auto newQuery = _dbManager->getDBWraper()->query();
    newQuery.prepare(sqlExcel);
    newQuery.bindValue(":idComplaint", _idComplaint);

    if (!newQuery.exec())
    {
        qDebug() << "ERROR:" << newQuery.lastError().text();
        qDebug() << newQuery.lastQuery();
        Q_ASSERT(false);
    }

    if (newQuery.first())
    {
        const auto idOrder = newQuery.value("id_order").toULongLong();
        const auto idDriver = newQuery.value("id_worker").toULongLong();
        const auto& driverName = newQuery.value("driver_name").toString();
        const auto& parkName = newQuery.value("park_name").toString();
        const auto whoseDriver = newQuery.value("whose").toInt();
        const auto& essence = newQuery.value("essence").toString();
        const auto& nameStatus = newQuery.value("name_status").toString();
        const auto& dateCreate = newQuery.value("date_create").toDateTime().toString("yyyy.MM.dd hh:mm:ss");

        doc.write(2, 1, _idComplaint,   baseDataStyle);
        doc.write(2, 2, idOrder,        baseDataStyle);
        doc.write(2, 3, idDriver,       baseDataStyle);
        doc.write(2, 4, driverName,     baseDataStyle);
        if (!whoseDriver)
            doc.write(2, 5, QString("Наш").toUtf8(),     baseDataStyle);
        else
            doc.write(2, 5, QString("Частник").toUtf8(), baseDataStyle);
        doc.write(2, 6, parkName,       baseDataStyle);
        doc.write(2, 7, dateCreate,     baseDataStyle);
        doc.write(2, 8, essence,        baseDataStyle);
        doc.write(2, 9, nameStatus,     baseDataStyle);
    }

    if (!doc.saveAs(fileName))
        QMessageBox::warning(nullptr, "Ошибка", "Не получилось записать файл");
#endif
}

void WorkerInformationController::onGetSelectComplaints(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_select_complaints"];
        reconnector.run();

        return;
    }

    const auto& response = responseObj->toVariant().toMap();
    const QString& commandType = response["head"].toMap()["type"].toString();

    if (commandType.trimmed() != "get_select_complaints")
        return;

    const auto& responseMap = response["body"].toMap();

    const auto& future = QtConcurrent::run(this, &WorkerInformationController::parseGetSelectComplaints, responseMap);
    QFutureWatcher<void> *fWatcher = new QFutureWatcher<void>();
    connect(fWatcher, &QFutureWatcher<void>::finished, this, &WorkerInformationController::onParseGetSelectComplaintsFinished);
    connect(fWatcher, &QFutureWatcher<void>::finished, fWatcher, &QFutureWatcher<void>::deleteLater);
    fWatcher->setFuture(future);
}

void WorkerInformationController::parseGetSelectComplaints(const QVariantMap& inData)
{
    const auto& dbWraper = _dbManager->getDBWraper();

    QList<QSqlQuery> queriesList = database::DBHelpers::createInsertQueries(inData, dbWraper);
    for (QSqlQuery& query : queriesList)
    {
        const bool result = query.exec();
        if (!result)
        {
            qDebug() << __FUNCTION__ << query.lastError().text();
            qDebug() << __FUNCTION__ << query.lastQuery();
        }
    }
}

void WorkerInformationController::onParseGetSelectComplaintsFinished()
{
    emit signalOpenComplaintsWorker(_idWorker, _keyWorker, _statusType);
}
