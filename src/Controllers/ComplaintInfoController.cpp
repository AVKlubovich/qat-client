#include "Common.h"
#include "ComplaintInfoController.h"

#include "Widgets/ComplaintInfoWidget/ComplaintInfoWidget.h"
#include "Widgets/AttachmentWidget/AttachmentWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "permissions/UserPermission.h"

#include "Controllers/ResponceChecker.h"

#include "utils/Sql/SqlString.h"


using namespace qat_client;

ComplaintInfoController::ComplaintInfoController()
    : QObject(nullptr)
{
}

void ComplaintInfoController::setRequestsManager(const network::RequestsManagerShp& rManager)
{
    _requestManager = rManager;
}

void ComplaintInfoController::setDBManager(const database::DBManagerShp& dbManager)
{
    _dbManager = dbManager;
}

void ComplaintInfoController::run()
{
    if (_complaintInfoWidget)
        _complaintInfoWidget->deleteLater();

    _complaintInfoWidget = new ComplaintInfoWidget();
    _scrollWidget = new ScrollWidget();
    _scrollWidget->setWidget(_complaintInfoWidget);

    createInfoModel();
    createLinkModel();

    connect(_complaintInfoWidget, &ComplaintInfoWidget::signalInWork,
            this , &ComplaintInfoController::commandTakenInfoOperation);
    connect(_complaintInfoWidget, &ComplaintInfoWidget::signalSaveComplaint,
            this , &ComplaintInfoController::commandUpdateComplaint);
    connect(_complaintInfoWidget, &ComplaintInfoWidget::signalIdReveiw,
            this, &ComplaintInfoController::signalIdReviewOpen);
    connect(_complaintInfoWidget, &ComplaintInfoWidget::signalTargetComplaints,
            this, &ComplaintInfoController::commandGetSelectComplaints);
    connect(_complaintInfoWidget, &ComplaintInfoWidget::signalDownloadHistory,
            this, &ComplaintInfoController::commandGetHistoryList);
    connect(_complaintInfoWidget, &ComplaintInfoWidget::signalBtnChallenge,
            this, &ComplaintInfoController::requestBtnChallenge);
    connect(_complaintInfoWidget, &ComplaintInfoWidget::signalFindComplaints,
            this, &ComplaintInfoController::signalInfoControllerFindComplaints);
    connect(_complaintInfoWidget, &ComplaintInfoWidget::signalOpenSmsDialog,
            this, &ComplaintInfoController::commandGetSmsTemplates);
    connect(_complaintInfoWidget, &ComplaintInfoWidget::signalSendSmsToServer,
            this, &ComplaintInfoController::commandSendSmsToServer);
    hideHistory();
}

QWidget *ComplaintInfoController::getWidget()
{
    return _scrollWidget;
}

void ComplaintInfoController::hideWidget()
{
    _scrollWidget->hide();
}

void ComplaintInfoController::showWidget()
{
    _scrollWidget->show();
}

void ComplaintInfoController::fillingEditsInfo(const quint64 id)
{
    QMap<quint64, QString> mapWorkers;
    mapWorkers[1] = "drivers";
    mapWorkers[2] = "operators";
    mapWorkers[3] = "logists";

    enum
    {
        drivers = 1,
        operators,
        logists
    };

    _idComplaint = id;
    const auto infoQueryStr = QString(
        "SELECT "
            "complaints.id_nature, "
            "%1 AS datetime, "
            "complaints.id_user, "
            "complaints.id_type, "
            "complaints.id_status, "
            "complaints.id_department, "
            "users.name, "
            "complaints_source.name_source, "
            "complaints.id_company, "
            "complaints.id, "
            "complaints.id_order, "
            "complaints_nature.id_target, "
            "complaints.is_work, "
            "complaints.essence, "
            "complaints.days_to_process, "
            "complaints.other_user_create, "
            "complaints.rating, "
            "complaints.feedback "
        "FROM complaints "
        "INNER JOIN complaints_nature ON (complaints.id_nature = complaints_nature.id) "
        "INNER JOIN complaints_source ON (complaints.id_source = complaints_source.id) "
        "INNER JOIN users ON (complaints.id_user_create = users.id) "
        "WHERE complaints.id = :id").arg(sql_utils::stringSqlDate);

    auto infoQuery = _dbManager->getDBWraper()->query();
    infoQuery.prepare(infoQueryStr);
    infoQuery.bindValue(":id", id);
    infoQuery.exec();
    _infoModel->setQuery(infoQuery);

    infoQuery.prepare("SELECT id_review FROM complaint_to_review WHERE id_complaint = :id");
    infoQuery.bindValue(":id", id);
    infoQuery.exec();
    _linkModel->setQuery(infoQuery);

    if (_infoModel->rowCount())
    {
        const int idStatus = _infoModel->index(0, 4).data().toInt();
        const int idType = _infoModel->index(0, 3).data().toInt();
        const int idNature = _infoModel->index(0, 0).data().toInt();
        const quint64 idDepartament = _infoModel->index(0, 5).data().toLongLong();
        const quint64 idUser = _infoModel->index(0, 2).data().toLongLong();
        const int idCompany = _infoModel->index(0, 8).data().toInt();
        const int idTarget = _infoModel->index(0, 11).data().toInt();
        const int isWork = _infoModel->index(0, 12).data().toInt();

        _complaintInfoWidget->fillComplaintInfo(_infoModel);
        _complaintInfoWidget->setIdComplaint(_idComplaint);

        _complaintInfoWidget->setIsWork(isWork);
        createUsersModel(idUser);
        createTypeModel(idType);
        createStatusModel(idStatus);
        createNatureModel(idTarget, idCompany, idNature);
        createDepartamentModel(idCompany, idDepartament);

        // driver info
        _keyWorker = idTarget;
        _valueWorker = mapWorkers[idTarget];
        QString parametrs;
        QString table;
        QString park;
        switch (_keyWorker)
        {
            case drivers:
                if (idCompany == 3)
                {
                    parametrs = " driver.id_worker AS id,"
                                " driver.surname || ' ' || driver.firstname || ' ' || driver.middlename AS driverName,"
                                " park.park_name AS namePark,"
                                " driver.column AS driverColumn,"
                                " driver.whose AS whoseDriver,"
                                " driver.date_come AS dateComeDriver,"
                                " driver.rating AS ratingDriver";
                    table = "complaints_orders_drivers";
                    park = " LEFT JOIN parks AS park ON (park.id = driver.id_park AND park.id_company = complaints.id_company) "
                           " INNER JOIN complaints ON (complaints.id = id_complaint) ";
                }
                else
                {
                    parametrs = " driver.id_worker AS id,"
                                " driver.surname || ' ' || driver.firstname || ' ' || driver.middlename AS driverName,"
                                " NULL AS namePark,"
                                " NULL AS driverColumn,"
                                " driver.whose AS whoseDriver,"
                                " driver.date_come AS dateComeDriver,"
                                " driver.rating AS ratingDriver";
                    table = "complaints_orders_drivers";
                    park = " INNER JOIN complaints ON (complaints.id = id_complaint) ";
                }
                break;
            case operators:
                parametrs = " driver.id_worker AS id,"
                            " driver.fio AS driverName,"
                            " NULL AS namePark,"
                            " NULL AS driverColumn,"
                            " NULL AS whoseDriver,"
                            " NULL AS dateComeDriver,"
                            " NULL AS ratingDriver";
                table = "complaints_orders_operators";
                break;
            case logists:
                parametrs = " driver.id_worker AS id,"
                            " driver.fio AS driverName,"
                            " NULL AS namePark,"
                            " NULL AS driverColumn,"
                            " NULL AS whoseDriver,"
                            " NULL AS dateComeDriver,"
                            " NULL AS ratingDriver";
                table = "complaints_orders_logists";
                break;
            default:
                break;
        }
        const auto sqlInfoDriver = QString("SELECT %1 FROM %2 AS driver %3 WHERE id_complaint = :idComplaint")
                                   .arg(parametrs).arg(table).arg(park);

        infoQuery.prepare(sqlInfoDriver);
        infoQuery.bindValue(":idComplaint", id);
        if (!infoQuery.exec())
            qDebug() << infoQuery.lastError() << endl;

        auto driverModel = new QSqlQueryModel();
        driverModel->setQuery(infoQuery);
        _complaintInfoWidget->infoDriverComplaint(driverModel);

        const int ROW_FIRST = 0;
        const int COLUMN_ID_COMPLAINT = 0;

        if (driverModel->rowCount())
        {
            QVariantMap requestMap;
            requestMap["type_command"] = "worker_statistic";
            requestMap["worker"] = mapWorkers[_keyWorker];
            requestMap["id"] = driverModel->index(ROW_FIRST, COLUMN_ID_COMPLAINT).data();
            network::RequestShp request(new network::DefaultRequest("worker_statistic", QVariant::fromValue(requestMap)));
            _requestManager->execute(request, this, &ComplaintInfoController::responseInfoStatistic);
        }

        {
            QVariantMap requestMap;
            requestMap["type_command"] = "get_comments";
            requestMap["schema"] = "complaints_schema";
            requestMap["id"] = id;
            network::RequestShp request(new network::DefaultRequest("get_comments", QVariant::fromValue(requestMap)));
            _requestManager->execute(request, this, &ComplaintInfoController::responseComments);
        }

        showWidget();
    }
    else
    {
        hideWidget();
    }
}

void ComplaintInfoController::responseInfoStatistic(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) < 0 )
        return;

    const auto& mapStatistic = responseObj->bodyToVariant().toMap();
    _complaintInfoWidget->setStatistic(mapStatistic);
}

void ComplaintInfoController::responseComments(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_comments"];
        reconnector.run();

        return;
    }

    const auto& listComments = responseObj->bodyToVariant().toList();
    _complaintInfoWidget->setComments(listComments);
}

void ComplaintInfoController::responseHistory(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_history"];
        reconnector.run();

        return;
    }

    const auto listHistory = responseObj->bodyToVariant().toList();
    _complaintInfoWidget->setHistory(listHistory);
}

void ComplaintInfoController::commandTakenInfoOperation(bool isWork)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "taken_info_operation";
    requestMap["idUser"] = permissions::UserPermission::instance().idUser();
    requestMap["id"] = _idComplaint;
    requestMap["work"] = static_cast<quint8>(isWork);

    network::RequestShp request(new network::DefaultRequest("taken_info_operation", QVariant::fromValue(requestMap)));
    _requestManager->execute(request, this, &ComplaintInfoController::onTakenInfoOperation);
}

void ComplaintInfoController::commandUpdateComplaint(const UpdateParametrs settings)
{
    auto request = [settings, this]()
    {
        QVariantMap requestMap;
        requestMap["id"] = settings.id;
        requestMap["id_nature"] = settings.idNature;
        requestMap["id_user"] = settings.idUser;
        requestMap["id_type"] = settings.idType;
        requestMap["id_status"] = settings.idStatus;
        requestMap["id_department"] = settings.idDepartament;
        requestMap["isWork"] = 0;
        requestMap["essence"] = settings.essence;
        requestMap["id_order"] = settings.idOrder;
        requestMap["comment"] = settings.comment;
        requestMap["id"] = settings.id;
        requestMap["day"] = settings.daysToReview;
        requestMap["id_user_create"] = permissions::UserPermission::instance().idUser();
        requestMap["idUser"] = permissions::UserPermission::instance().idUser();
        requestMap["rating"] = settings.rating;
        requestMap["type_command"] = "update_complaint";

        network::RequestShp request(new network::DefaultRequest("update_complaint", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &ComplaintInfoController::onUpdateComplaint);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("update_complaint", reconnectorObj);
    reconnectorObj.run();
}

void ComplaintInfoController::commandGetSmsTemplates()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "get_sms_templates";
    requestMap["id_company"] = _infoModel->index(0, 8).data().toInt(); // TODO
//    requestMap["id_company"] = 3;
    network::RequestShp request(new network::DefaultRequest("get_sms_templates", QVariant::fromValue(requestMap)));
    _requestManager->execute(request, this, &ComplaintInfoController::onGetSmsTemplates);
}

void ComplaintInfoController::commandSendSmsToServer(const QString &smsText, const QString &phone, const QDateTime &date, int type, double mark)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "send_sms";
    requestMap["sms_text"] = smsText; // TODO
    requestMap["phone_number"] = phone;
    requestMap["id_complaint"] = _idComplaint;
    requestMap["time_of_send"] = date.toString("yyyy-MM-dd hh:mm:ss");
    requestMap["id_company"] = _infoModel->index(0, 8).data().toInt();
    requestMap["type_message"] = type;
    requestMap["mark"] = mark;
    network::RequestShp request(new network::DefaultRequest("send_sms", QVariant::fromValue(requestMap)));
    _requestManager->execute(request, this, &ComplaintInfoController::onSendSms);
}

void ComplaintInfoController::onTakenInfoOperation(network::ResponseShp responseObj)
{
}

void ComplaintInfoController::onUpdateComplaint(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["update_complaint"];
        reconnector.run();

        return;
    }

    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_comments";
        requestMap["schema"] = "complaints_schema";
        requestMap["id"] = _idComplaint;
        network::RequestShp request(new network::DefaultRequest("get_comments", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &ComplaintInfoController::responseComments);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_comments", reconnectorObj);
    reconnectorObj.run();

    hideWidget();

    emit signalOnenNextComplaint();
}

void ComplaintInfoController::onGetSmsTemplates(network::ResponseShp responseObj)
{
    auto listTemplates = responseObj->bodyToVariant().toMap().value("templates").toList();

    if (_templateModel)
        _templateModel->deleteLater();

    _templateModel = new QStandardItemModel();
    for (auto &item : listTemplates)
    {
        QList <QStandardItem*> list;
        const auto& mapItem = item.toMap();
        list.append(new QStandardItem(mapItem.value("description").toString()));
        list.append(new QStandardItem(mapItem.value("id").toString()));
        list.append(new QStandardItem(mapItem.value("text_message").toString()));
        _templateModel->appendRow(list);
    }

    auto infoQuery = _dbManager->getDBWraper()->query();
    infoQuery.prepare("SELECT c.client_number, p.phone "
                      "FROM complaints AS c "
                      "INNER JOIN complaints_orders_driver_phones AS p ON (c.id = p.id_complaint) "
                      "WHERE c.id = :idComplaint");
    infoQuery.bindValue(":idComplaint", _idComplaint);
    if (!infoQuery.exec())
        qDebug() << infoQuery.lastError() << endl;

    QStringList listPhonesClient;
    QStringList listPhonesDriver;
    while (infoQuery.next())
    {
        const QString &numberDriver = infoQuery.value("phone").toString();
        if (!listPhonesDriver.contains(numberDriver) && numberDriver != "0")
            listPhonesDriver.append(numberDriver);

        const QString &numberClient = infoQuery.value("client_number").toString();
        if (!listPhonesClient.contains(numberClient))
            listPhonesClient.append(numberClient);
    }

    QList <QStandardItem*> items;
    for (const QString &str : listPhonesDriver)
        items.append(new QStandardItem(str + " водитель"));

    for (const QString &str : listPhonesClient)
        items.append(new QStandardItem(str + " клиент"));

    if (_phoneModel)
        _phoneModel->deleteLater();
    _phoneModel = new QStandardItemModel();
    _phoneModel->appendColumn(items);

    _complaintInfoWidget->openSmsDialog(_templateModel, _phoneModel);
}

void ComplaintInfoController::onSendSms(network::ResponseShp responseObj)
{
}

void ComplaintInfoController::createInfoModel()
{
    if (_infoModel)
        _infoModel->deleteLater();

    _infoModel = new QSqlQueryModel(this);
    _complaintInfoWidget->setComplaintInfoModel(_infoModel);
}

void ComplaintInfoController::createLinkModel()
{
    if (_linkModel)
        _linkModel->deleteLater();

    _linkModel = new QSqlQueryModel(this);
    _complaintInfoWidget->setButtonLinkModel(_linkModel);
}

void ComplaintInfoController::createTypeModel(const int &idType)
{
    if (_typeModel)
        _typeModel->deleteLater();

    _typeModel = new QSqlQueryModel(this);

    const QString& typeQueryStr = QString(
        "SELECT name, id FROM complaints_type");

    auto typeQuery = _dbManager->getDBWraper()->query();
    typeQuery.prepare(typeQueryStr);
    typeQuery.exec();
    _typeModel->setQuery(typeQuery);

    const int position = searchRow(_typeModel, idType);
    _complaintInfoWidget->setTypeModel(_typeModel, position);
}

void ComplaintInfoController::createStatusModel(const int &idStatus)
{
    if (_statusModel)
        _statusModel->deleteLater();

    _statusModel = new StatusModel();

    auto statusQuery = _dbManager->getDBWraper()->query();

    {
        const QString& sqlDep = QString("SELECT id FROM departments"
                                        " WHERE id = 1");
//                               " WHERE departments.isVisible = 1 AND departments.name LIKE 'OKK%' OR departments.name LIKE 'ОКК%'";
        statusQuery.prepare(sqlDep);
        statusQuery.exec();
        _statusModel->setDepartmentOkk(database::DBHelpers::queryToVariant(statusQuery));
    }

    const QString& statusQueryStr = QString("SELECT name_status, id FROM complaints_status ORDER BY id");
    statusQuery.prepare(statusQueryStr);
    statusQuery.exec();
    _statusModel->setStatus(database::DBHelpers::queryToVariant(statusQuery));

    const int position = searchRow(_statusModel, idStatus);
    _complaintInfoWidget->setStatusModel(_statusModel, position);
}

void ComplaintInfoController::createUsersModel(const quint64 &idUser)
{
    if (_usersModel)
        _usersModel->deleteLater();

    _usersModel = new QSqlQueryModel(this);

    const QString& usersQueryStr = QString(
        "SELECT name, id FROM users");

    auto usersQuery = _dbManager->getDBWraper()->query();
    usersQuery.prepare(usersQueryStr);
    usersQuery.exec();
    _usersModel->setQuery(usersQuery);

    const int position = searchRow(_usersModel, idUser);
    _complaintInfoWidget->setUsersModel(_usersModel, position);
}

void ComplaintInfoController::createNatureModel(const int idTarget, const int idCompany, const int idNature)
{
    if (_natureModel)
        _natureModel->deleteLater();

    _natureModel = new QSqlQueryModel(this);

    const QString& natureQueryStr = QString(
        "SELECT data_nature, days_to_review, cost, id FROM complaints_nature WHERE id_target = "
        + QString::number(idTarget) + " AND "
        "id_company = " + QString::number(idCompany));

    auto natureQuery = _dbManager->getDBWraper()->query();
    natureQuery.prepare(natureQueryStr);
    natureQuery.exec();
    _natureModel->setQuery(natureQuery);

    const int position = searchRow(_natureModel, idNature);
    _complaintInfoWidget->setNatureModel(_natureModel, position);
}

void ComplaintInfoController::createDepartamentModel(const quint64 idCompany, const quint64 idDepartament)
{
    if (_departamentModel)
        _departamentModel->deleteLater();

    _departamentModel = new QSqlQueryModel(this);

    const QString& departamentsQueryStr = QString(
        "SELECT name, id FROM departments WHERE (isVisible = 1 AND id_company = :idCompany) OR id = 1");

    auto departamentsQuery = _dbManager->getDBWraper()->query();
    departamentsQuery.prepare(departamentsQueryStr);
    departamentsQuery.bindValue(":idCompany", QString::number(idCompany));
    departamentsQuery.exec();
    _departamentModel->setQuery(departamentsQuery);

    const int position = searchRow(_departamentModel, idDepartament);
    _complaintInfoWidget->setDepartamentModel(_departamentModel, position);
}

int ComplaintInfoController::searchRow(QAbstractItemModel *model, const int id)
{
    for (auto i = 0; i < model->rowCount(); i++)
    {
        auto index = model->index(i, model->columnCount() - 1, QModelIndex());
        if (index.data(Qt::DisplayRole).toInt() == id)
            return i;
    }
    return -1;
}

void ComplaintInfoController::commandGetSelectComplaints(const QString &idTarget, int statusType)
{
    _idWorker = idTarget.toLongLong();
    _statusType = statusType;
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_select_complaints";
        requestMap["date_last_query"] = QDateTime::currentDateTime();
        requestMap["date_start"] = "2000-01-01 00:00:00";
        requestMap["date_end"] = QDateTime::currentDateTime().addSecs(3000).toString("yyyy-MM-dd hh:mm:ss");
        requestMap["table"] = _valueWorker;
        requestMap["id"] = _idWorker;
        requestMap["type"] = _statusType;
        requestMap["idUser"] = permissions::UserPermission::instance().idUser();

        network::RequestShp request(new network::DefaultRequest("get_select_complaints", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &ComplaintInfoController::onGetSelectComplaints);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_select_complaints", reconnectorObj);
    reconnectorObj.run();
}

void ComplaintInfoController::commandGetHistoryList()
{
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_history";
        requestMap["schema"] = "complaints_schema";
        requestMap["id"] = _idComplaint;
        network::RequestShp request(new network::DefaultRequest("get_history", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &ComplaintInfoController::responseHistory);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_comments", reconnectorObj);
    reconnectorObj.run();
}

void ComplaintInfoController::requestBtnChallenge()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "complaint_challenge";
    requestMap["schema"] = "complaints_schema";
    requestMap["id"] = _idComplaint;
    requestMap["id_user"] = permissions::UserPermission::instance().idUser();
    network::RequestShp request(new network::DefaultRequest("complaint_challenge", QVariant::fromValue(requestMap)));
    _requestManager->execute(request, this, &ComplaintInfoController::responceBtnChallenge);
}

void ComplaintInfoController::onGetSelectComplaints(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_select_complaints"];
        reconnector.run();

        return;
    }

    const auto& response = responseObj->toVariant().toMap();
    const auto& commandType = response["head"].toMap()["type"].toString();

    if (commandType.trimmed() != "get_select_complaints")
        return;

    const auto& responseMap = response["body"].toMap();

    const auto& future = QtConcurrent::run(this, &ComplaintInfoController::parseGetSelectComplaints, responseMap);
    QFutureWatcher<void> *fWatcher = new QFutureWatcher<void>();
    connect(fWatcher, &QFutureWatcher<void>::finished, this, &ComplaintInfoController::onParseGetSelectComplaintsFinished);
    connect(fWatcher, &QFutureWatcher<void>::finished, fWatcher, &QFutureWatcher<void>::deleteLater);
    fWatcher->setFuture(future);
}

void ComplaintInfoController::parseGetSelectComplaints(const QVariantMap& inData)
{
    const auto& dbWraper = _dbManager->getDBWraper();

    QList<QSqlQuery> queriesList = database::DBHelpers::createInsertQueries(inData, dbWraper);
    for (QSqlQuery& query : queriesList)
    {
        const bool result = query.exec();
        if (!result)
        {
            qDebug() << __FUNCTION__ << query.lastError().text();
            qDebug() << __FUNCTION__ << query.lastQuery();
        }
    }
}

void ComplaintInfoController::onParseGetSelectComplaintsFinished()
{
    emit signalOpenComplaintsWorker(_idWorker, _keyWorker, _statusType);
}

void ComplaintInfoController::responceBtnChallenge(network::ResponseShp responseObj)
{
    const auto map = responseObj->bodyToVariant().toMap();
    if (map["status"].toInt() < 0)
    {
        QMessageBox::warning(nullptr, map["error_type"].toString(), map["error_string"].toString());
        return;
    }
}

void ComplaintInfoController::hideHistory()
{
    const auto& listDep = permissions::UserPermission::instance().getDepartmentsUser();

    const QString& sqlQuery = QString("SELECT * FROM departments WHERE id IN (%1) "
                                      "AND (name LIKE 'OKK%' "  // english
                                      "OR name LIKE 'ОКК%')")   // russian
                              .arg(listDep.join(", "));

    auto infoQuery = _dbManager->getDBWraper()->query();
    infoQuery.prepare(sqlQuery);
    infoQuery.exec();

    _complaintInfoWidget->setPermissionsWidget(!infoQuery.first());
}
