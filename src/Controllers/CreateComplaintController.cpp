﻿#include "Common.h"
#include "CreateComplaintController.h"

#include "Widgets/CreateComplaintDialog/CreateComplaintDialog.h"
#include "Widgets/AttachmentWidget/AttachmentWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"

#include "file-attachment/FileAttachmentWidget.h"

#include "permissions/UserPermission.h"

#include "Controllers/ResponceChecker.h"

#include "Definitions.h"

using namespace qat_client;

CreateComplaintController::CreateComplaintController()
    : QObject(nullptr)
{
    _matchTableSources = QMap<int, int>
    {
        {1, 2},   // Android отзывы
        {2, 3},   // iOS отзывы
        {3, 1},   // программа ОКК
        {4, 6},   // Android Сообщение в ОКК
        {5, 7},   // iOS Сообщение в ОКК
        {6, 4},   // Android Сообщение в Тех. поддержку
        {7, 5},   // iOS Сообщение в Тех. поддержку
        {8, 11},  // Android Клиент забыл
        {9, 12},  // iOS Клиент забыл
        {10, 13}, // Email Таксовичкоф
        {11, 14}, // Email Грузовичкоф
    };
}

void CreateComplaintController::setRequestsManager(const network::RequestsManagerShp& rManager)
{
    _requestsManager = rManager;
}

void CreateComplaintController::setDBManager(const database::DBManagerShp& dbManager)
{
    _dbManager = dbManager;
}

void CreateComplaintController::setDataReview(const quint64 idOrder,
                                              const quint64 idCompany,
                                              const quint64 idReview,
                                              const quint64 idSource,
                                              const QString &comment)
{
    _flagCreateComplaintOfReview = true;
    _idOrder = idOrder;
    _idCompany = idCompany;
    _idReview = idReview;
    _idSource = idSource;
    _comment = comment;
}

void CreateComplaintController::run()
{
    _createComplaintDialog = new CreateComplaintDialog();
    connect(_createComplaintDialog, &CreateComplaintDialog::companyChanged,
            this, &CreateComplaintController::onCompanyChanged);
    connect(_createComplaintDialog, &CreateComplaintDialog::targetChanged,
            this, &CreateComplaintController::onTargetChanged);
    connect(_createComplaintDialog, &CreateComplaintDialog::natureChanged,
            this, &CreateComplaintController::onNatureCnahged);
    connect(_createComplaintDialog, &CreateComplaintDialog::createClicked,
            this, &CreateComplaintController::onCreateClicked);
    connect(_createComplaintDialog, &CreateComplaintDialog::closed,
            this, [this] (){clearVariables();});
    connect(_createComplaintDialog, &CreateComplaintDialog::finished,
            _createComplaintDialog, &CreateComplaintDialog::deleteLater);

    if (_fileWidget)
    {
        _fileWidget->deleteLater();
        _fileWidget = nullptr;
    }

    _fileWidget = new file_attach::FileAttachmentWidget();
    _fileWidget->setWindowFlags(Qt::Popup);
    _createComplaintDialog->setFileAttachWidget(_fileWidget);

    if (_companyModel)
        _companyModel->clear();
    else
        _companyModel = new QSqlQueryModel();
    _createComplaintDialog->setCompanyModel(_companyModel);

    if (_targetModel)
        _targetModel->clear();
    else
        _targetModel = new QSqlQueryModel();
    _createComplaintDialog->setTargetModel(_targetModel);

    if (_natureModel)
        _natureModel->clear();
    else
        _natureModel = new QSqlQueryModel();
    _createComplaintDialog->setNatureModel(_natureModel);

    fillCompanyModel();

    if (_flagCreateComplaintOfReview)
    {
        _createComplaintDialog->setIdOrder(_idOrder);
        _createComplaintDialog->setIdCompany(_idCompany);
        _createComplaintDialog->setComment(_comment);
    }

    _createComplaintDialog->exec();
}

void CreateComplaintController::onCompanyChanged(const quint64 index)
{
//    const auto companyId = _companyModel->data(_companyModel->index(index, 1)).toLongLong();

//    const auto queryStr = QString("SELECT data_target, id FROM complaints_target WHERE id_company = :companyId");
    const auto queryStr = QString("SELECT data_target, id FROM complaints_target WHERE isVisible = 1");
    auto query = _dbManager->getDBWraper()->query();
    query.prepare(queryStr);
//    query.bindValue(":companyId", companyId);
    query.exec();

    _targetModel->setQuery(query);
    _createComplaintDialog->updateTraget();
}

void CreateComplaintController::onTargetChanged(const quint64 index)
{
    const auto targetId = _targetModel->data(_targetModel->index(index, 1)).toLongLong();
    const auto curCompanyInd = _createComplaintDialog->getCompanyIndex();
    const auto companyId = _companyModel->data(_companyModel->index(curCompanyInd, 1)).toLongLong();

    const auto queryStr = QString("SELECT data_nature, id, days_to_review, hint_essence, cost, type FROM complaints_nature "
                                  "WHERE id_target = :targetId AND id_company = :companyId AND \"id_visiable\" = 1");
    auto query = _dbManager->getDBWraper()->query();
    query.prepare(queryStr);
    query.bindValue(":targetId", targetId);
    query.bindValue(":companyId", companyId);
    query.exec();

    _natureModel->setQuery(query);
    _createComplaintDialog->updateNature();
}

void CreateComplaintController::onNatureCnahged(const quint64 natureId)
{
    _createComplaintDialog->updateEssence();
}

void CreateComplaintController::onCreateClicked()
{
    const auto curCompanyInd = _createComplaintDialog->getCompanyIndex();
    const auto companyId = _companyModel->data(_companyModel->index(curCompanyInd, 1)).toLongLong();

//    const auto curTargetInd = _createComplaintDialog->getTargetIndex();
//    const auto targetId = _targetModel->data(_targetModel->index(curTargetInd, 1)).toLongLong();

    const auto curNatureInd = _createComplaintDialog->getNatureIndex();
    const auto natureId = _natureModel->data(_natureModel->index(curNatureInd, 1)).toLongLong();
    QString rating = QString::number(CONST_RATING - _natureModel->data(_natureModel->index(curNatureInd, 4)).toFloat(), 'f', 1);
    const auto orderId = _createComplaintDialog->getOrder();
    const auto essence = _createComplaintDialog->getEssence();
    const auto comment = _createComplaintDialog->getComment();
    const auto days = _createComplaintDialog->getDaysToReview();
    const auto feedback = _createComplaintDialog->getFeedBack();

    auto request = [this, companyId, orderId, natureId, rating, essence, comment, days, feedback]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "insert_complaint";
        requestMap["id_company"] = companyId;

        if (_flagCreateComplaintOfReview)
        {
            if (_idSource == 11) // TODO КАСТЫЛЬ!!!!!
                requestMap["id_company"] = 2;
            requestMap["id_source"] = _matchTableSources.value(_idSource);
        }
        else
            requestMap["id_source"] = 1;

        requestMap["id_order"] = orderId;
        requestMap["id_status"] = 1;

        auto query = _dbManager->getDBWraper()->query();
        query.prepare("SELECT id FROM departments WHERE isVisible = 1 AND id_company = :idCompany ORDER BY id LIMIT 1");
        query.bindValue(":idCompany", requestMap["id_company"].toInt());
        query.exec();

        if (query.first())
            requestMap["id_department"] = query.value("id").toInt();

        requestMap["id_nature"] = natureId;
        requestMap["id_user_create"] = permissions::UserPermission::instance().idUser();
        requestMap["idUser"] = permissions::UserPermission::instance().idUser();
        requestMap["hint_essence"] = essence;
        requestMap["id_type"] = 2;
        requestMap["comment"] = comment;
        requestMap["day"] = days;
        requestMap["feedback"] = feedback;

        requestMap["rating"] = rating;
        if (_idReview)
            requestMap["review"] = _idReview;

        network::RequestShp request(new network::DefaultRequest("insert_complaint", QVariant::fromValue(requestMap)));
        _requestsManager->execute(request, this, &CreateComplaintController::onCreateResponse);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("insert_complaint", reconnectorObj);
    reconnectorObj.run();
}

void CreateComplaintController::onCreateResponse(const network::ResponseShp response)
{
    if (checkResponse(response) != 1)
    {
        auto& reconnector = _reconnectMap["insert_complaint"];
        reconnector.run();

        return;
    }

    const auto responseMap = response->bodyToVariant().toMap();

    const auto status = responseMap["status"].toLongLong();
    if (status != 1)
    {
        qDebug() << responseMap["error_str"].toString();
        QMessageBox::warning(nullptr, "Warning", "Complaint do not create");
        return;
    }

    const auto idComplaint = responseMap["id_complaint"].toLongLong();
    _fileWidget->uploadAllFiles(idComplaint);
}

void CreateComplaintController::fillCompanyModel()
{
    const auto queryStr = QString("SELECT name, id FROM company WHERE isVisible = 1 ORDER BY id");
    auto query = _dbManager->getDBWraper()->query();
    query.prepare(queryStr);
    query.exec();

    _companyModel->setQuery(query);
}

void CreateComplaintController::clearVariables()
{
    _idCompany = 0;
    _idOrder = 0;
    _idSource = 0;
    _idReview = 0;
    _comment.clear();
    _flagCreateComplaintOfReview = false;
}
