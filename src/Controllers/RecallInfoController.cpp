#include "Common.h"
#include "RecallInfoController.h"

#include "Widgets/RecallInfoWidget/RecallInfoWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"

#include "permissions/UserPermission.h"
#include "Controllers/ResponceChecker.h"

#include "utils/Sql/SqlString.h"


using namespace qat_client;

RecallInfoController::RecallInfoController()
    : QObject(nullptr)
{
}

void RecallInfoController::setRequestsManager(const network::RequestsManagerShp& rManager)
{
    _requestManager = rManager;
}

void RecallInfoController::setDBManager(const database::DBManagerShp& dbManager)
{
    _dbManager = dbManager;
}

void RecallInfoController::run()
{
    if (_recallInfoWidget)
        _recallInfoWidget->deleteLater();
    _recallInfoWidget = new RecallInfoWidget();
    _scrollWidget = new ScrollWidget();
    _scrollWidget->setWidget(_recallInfoWidget);

    createInfoModel();
    createLinkModel();

    connect(this, &RecallInfoController::signalInfoModel,
            _recallInfoWidget, &RecallInfoWidget::fillComplaintInfo);
    connect(_recallInfoWidget, &RecallInfoWidget::signalClicedCloseRecall,
            this, &RecallInfoController::commandCloseRecall);
    connect(_recallInfoWidget, &RecallInfoWidget::signalCreateComplaint,
            this, &RecallInfoController::signalCreateComplaint);
    connect(_recallInfoWidget, &RecallInfoWidget::signalIdComplaint,
            this, &RecallInfoController::signalIdComplaintOpen);
    connect(_recallInfoWidget, &RecallInfoWidget::signalCreateComment,
            this, &RecallInfoController::commandSaveComment);
    connect(_recallInfoWidget, &RecallInfoWidget::signalDownloadHistory,
            this, &RecallInfoController::commandGetHistoryList);
    hideHistory();
}

QWidget *RecallInfoController::getWidget()
{
    return _scrollWidget;
}

void RecallInfoController::hideWidget()
{
    _scrollWidget->hide();
}

void RecallInfoController::showWidget()
{
    _scrollWidget->show();
}

void RecallInfoController::fillingEditsInfo(const quint64 id)
{
    _idReview = id;
    QMap<int, QString> mapWorkers;
    mapWorkers[1] = "drivers";
    mapWorkers[2] = "operators";
    mapWorkers[3] = "logists";

    enum
    {
        drivers = 1,
        operators,
        logists
    };

    const auto infoQueryStr = QString(
                "SELECT "
                       "reviews.id, "
                       "reviews.id_order, "
                       "%1 AS datetime, "
                       "reviews.id_company, "
                       "reviews.id_status, "
                       "reviews.id_source, "
                       "reviews.comment, "
                       "CASE WHEN reviews.type = 0 THEN 'Положительная' "
                       "ELSE 'Отрицательная' END, "
                       "reviews_answer.id_question AS id_question, "
                       "reviews_answer.answer, "
                       "reviews.user_email "
                "FROM reviews "
                "LEFT JOIN reviews_answer ON (reviews.id = reviews_answer.id_reviews) "
                "WHERE reviews.id = :id ORDER BY id_question").arg(sql_utils::stringSqlDate);

    auto infoQuery = _dbManager->getDBWraper()->query();
    infoQuery.prepare(infoQueryStr);
    infoQuery.bindValue(":id", id);
    infoQuery.exec();
    _infoModel->setQuery(infoQuery);

    infoQuery.prepare("SELECT id_complaint FROM complaint_to_review WHERE id_review = :id");
    infoQuery.bindValue(":id", id);
    infoQuery.exec();
    _linkModel->setQuery(infoQuery);

    if (_infoModel->rowCount())
    {
        emit signalInfoModel();

        // driver info
        _keyWorker = 1;
        _valueWorker = mapWorkers[_keyWorker];
        QString parametrs;
        QString table;
        QString park = "";
        switch (_keyWorker) {
            case drivers:
                parametrs = " driver.id AS id,"
                            " driver.surname || ' ' || driver.firstname || ' ' || driver.middlename AS driverName,"
                            " park.park_name || ' / ' ||driver.column  AS namePark,"
                            " CASE WHEN driver.whose = 0 THEN 'Наш' ELSE 'Частный' END AS whoseDriver,"
                            " driver.date_come AS dateComeDriver,"
                            " driver.rating AS ratingDriver";
                table = "reviews_orders_drivers";
                park = " INNER JOIN parks AS park ON (park.id = driver.id_park)";
                break;
            case operators:
                parametrs = " driver.id AS id,"
                            " driver.fio AS driverName,"
                            " NULL AS namePark,"
                            " NULL AS whoseDriver,"
                            " NULL AS dateComeDriver,"
                            " NULL AS ratingDriver";
                table = "reviews_orders_operators";
                break;
            case logists:
                parametrs = " driver.id AS id,"
                            " driver.fio AS driverName,"
                            " NULL AS namePark,"
                            " NULL AS whoseDriver,"
                            " NULL AS dateComeDriver,"
                            " NULL AS ratingDriver";
                table = "reviews_orders_logists";
                break;
            default:
                break;
        }
        const QString& sqlInfoDriver = QString("SELECT  %1,car.number AS numberCar FROM %2 AS driver"
                                               " LEFT JOIN reviews_orders_cars AS car ON (car.id_review = driver.id_review)"
                                               " %3 WHERE driver.id_review = :idReview").arg(parametrs).arg(table).arg(park);

        infoQuery.prepare(sqlInfoDriver);
        infoQuery.bindValue(":idReview", id);
        if (!infoQuery.exec())
            qDebug() << infoQuery.lastError() << endl;

        auto driverModel = new QSqlQueryModel();
        driverModel->setQuery(infoQuery);
        _recallInfoWidget->setDriverInfoModel(driverModel);

        const int ROW_FIRST = 0;
        const int COLUMN_ID_COMPLAINT = 0;

        {
            QVariantMap requestMap;
            requestMap["type_command"] = "worker_statistic";
            requestMap["worker"] = mapWorkers[_keyWorker];
            requestMap["id"] = driverModel->index(ROW_FIRST, COLUMN_ID_COMPLAINT).data();
            network::RequestShp request(new network::DefaultRequest("worker_statistic", QVariant::fromValue(requestMap)));
            _requestManager->execute(request, this, &RecallInfoController::responseInfoStatistic);
        }

        {
            auto request = [this]()
            {
                QVariantMap requestMap;
                requestMap["type_command"] = "get_comments";
                requestMap["schema"] = "reviews_schema";
                requestMap["id"] = _idReview;
                network::RequestShp request(new network::DefaultRequest("get_comments", QVariant::fromValue(requestMap)));
                _requestManager->execute(request, this, &RecallInfoController::responseComments);
            };

            Reconnector reconnectorObj(request);

            _reconnectMap.insert("get_comments", reconnectorObj);
            reconnectorObj.run();
        }

        showWidget();
    }
    else
    {
        hideWidget();
    }
}

void RecallInfoController::responseInfoStatistic(network::ResponseShp responseObj)
{
    const auto mapStatistic = responseObj->bodyToVariant().toMap();
    _recallInfoWidget->setStatistic(mapStatistic);
}

void RecallInfoController::responseComments(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_comments"];
        reconnector.run();

        return;
    }

    const auto mapComments = responseObj->bodyToVariant().toList();
    _recallInfoWidget->setComments(mapComments);
}

void RecallInfoController::responceSaveComment(network::ResponseShp responseObj)
{
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_comments";
        requestMap["schema"] = "reviews_schema";
        requestMap["id"] = _idReview;
        network::RequestShp request(new network::DefaultRequest("get_comments", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &RecallInfoController::responseComments);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_comments", reconnectorObj);
    reconnectorObj.run();
}

void RecallInfoController::responseHistory(network::ResponseShp responseObj)
{
    if (checkResponse(responseObj) != 1)
    {
        auto& reconnector = _reconnectMap["get_history"];
        reconnector.run();

        return;
    }

    const auto listHistory = responseObj->bodyToVariant().toList();
    _recallInfoWidget->setHistory(listHistory);
}

void RecallInfoController::commandCloseRecall(const quint64 id)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "change_in_status";
    requestMap["idUser"] = permissions::UserPermission::instance().idUser();
    requestMap["id"] = id;
    requestMap["idStatus"] = 1;

    network::RequestShp request(new network::DefaultRequest("change_in_status", QVariant::fromValue(requestMap)));
    _requestManager->execute(request, this, &RecallInfoController::onChangeInStatus);
}

void RecallInfoController::commandSaveComment(const QString &comment)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "add_comment";
    requestMap["id"] = _idReview;
    requestMap["comment"] = comment;
    requestMap["schema"] = "reviews_schema";
    requestMap["id_user"] = permissions::UserPermission::instance().idUser();

    network::RequestShp request(new network::DefaultRequest("add_comment", QVariant::fromValue(requestMap)));
    _requestManager->execute(request, this, &RecallInfoController::responceSaveComment);
}

void RecallInfoController::commandGetHistoryList()
{
    auto request = [this]()
    {
        QVariantMap requestMap;
        requestMap["type_command"] = "get_history";
        requestMap["schema"] = "reviews_schema";
        requestMap["id"] = _idReview;
        network::RequestShp request(new network::DefaultRequest("get_history", QVariant::fromValue(requestMap)));
        _requestManager->execute(request, this, &RecallInfoController::responseHistory);
    };

    Reconnector reconnectorObj(request);

    _reconnectMap.insert("get_comments", reconnectorObj);
    reconnectorObj.run();
}

void RecallInfoController::onChangeInStatus(network::ResponseShp responseObj)
{
    emit signalOpenNextReviewInfoController();
}

void RecallInfoController::createInfoModel()
{
    if (_infoModel)
        _infoModel->deleteLater();

    _infoModel = new QSqlQueryModel(this);
    _recallInfoWidget->setRecallInfoModel(_infoModel);
}

void RecallInfoController::createLinkModel()
{
    if (_linkModel)
        _linkModel->deleteLater();

    _linkModel = new QSqlQueryModel(this);
    _recallInfoWidget->setButtonLinkModel(_linkModel);
}

void RecallInfoController::hideHistory()
{
    const auto & listDep = permissions::UserPermission::instance().getDepartmentsUser();

    const QString& sqlQuery = QString("SELECT * FROM departments WHERE id IN (%1) AND (name LIKE 'OKK%' OR name LIKE 'OКК%')")
                              .arg(listDep.join(", "));
    auto infoQuery = _dbManager->getDBWraper()->query();
    infoQuery.prepare(sqlQuery);
    infoQuery.bindValue(":list", listDep);
    infoQuery.exec();

    _recallInfoWidget->setPermissionsWidget(!infoQuery.first());
}
