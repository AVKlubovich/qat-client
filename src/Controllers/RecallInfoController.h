﻿#pragma once

#include "Widgets/ScrollWidget/ScrollWidget.h"
#include "Reconnector.h"


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class ScrollWidget;
    class RecallInfoWidget;

    class RecallInfoController : public QObject
    {
        Q_OBJECT

    public:
        struct UpdateParametrs
        {
            quint64 id;
            int idNature;
            quint64 idUser;
            int idType;
            int idStatus;
            int idDepartament;
            int isWork;
            QString essence;
            QString comment;
        };

    public:
        RecallInfoController();
        ~RecallInfoController() = default;

        void setRequestsManager(const network::RequestsManagerShp& rManager);
        void setDBManager(const database::DBManagerShp& dbManager);

        QWidget *getWidget();
        void hideWidget();
        void showWidget();

    public slots:
        void run();

        void fillingEditsInfo(const quint64 id);
        void commandCloseRecall(const quint64 id);

    private slots:
        void commandSaveComment(const QString &comment);
        void commandGetHistoryList();

    signals:
        void signalInfoModel();
        void signalCreateComplaint(const quint64 idOrder, const quint64 idCompany,
                                   const quint64 idReview, const quint64 idSource, const QString& comment);
        void signalIdComplaintOpen(const quint64 idComplaint);

        void signalOpenNextReviewInfoController();

    private:
        void createInfoModel();
        void createLinkModel();
        void onChangeInStatus(network::ResponseShp responseObj);
        void responseInfoStatistic(network::ResponseShp responseObj);
        void responseComments(network::ResponseShp responseObj);
        void responceSaveComment(network::ResponseShp responseObj);
        void responseHistory(network::ResponseShp responseObj);
        void hideHistory();

    private:
        RecallInfoWidget* _recallInfoWidget = nullptr;
        ScrollWidget* _scrollWidget = nullptr;

        QSqlQueryModel* _infoModel = nullptr;
        QSqlQueryModel* _linkModel = nullptr;

        network::RequestsManagerShp _requestManager;
        database::DBManagerShp _dbManager;

        int _keyWorker;
        QString _valueWorker;
        quint64 _idWorker;
        int _statusType;
        quint64 _idReview;

        QMap<QString, Reconnector> _reconnectMap;
    };

}
