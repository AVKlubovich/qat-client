﻿#pragma once

#include "Reconnector.h"


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace file_attach
{
    class FileAttachmentWidget;
}


namespace qat_client
{

    class CreateComplaintDialog;


    class CreateComplaintController : public QObject
    {
        Q_OBJECT

        // TODO <review id source, complaint id source> ДОБАВИТЬ В БД ТАБЛИЦУ СООТВЕТСВИЯ РЕСУРСОВ ЛИБО ВЫНЕСТИ В ОБЩУЮ ТАБЛИЦУ!!!
        QMap <int, int> _matchTableSources;

    public:
        struct Options
        {
            int fetchData;
        };

    public:
        explicit CreateComplaintController();
        ~CreateComplaintController() = default;

        void setRequestsManager(const network::RequestsManagerShp& rManager);
        void setDBManager(const database::DBManagerShp& dbManager);
        void setDataReview(const quint64 idOrder,
                           const quint64 idCompany,
                           const quint64 idReview,
                           const quint64 idSource,
                           const QString& comment);

    public slots:
        void run();

    private slots:
        void onCompanyChanged(const quint64 index);
        void onTargetChanged(const quint64 index);
        void onNatureCnahged(const quint64 natureId);

        void onCreateClicked();

    public:
        void onCreateResponse(const network::ResponseShp response);

    private:
        void fillCompanyModel();
//        void fillTargetModel();
//        void fillNatureModel();
        void clearVariables();

    private:
        CreateComplaintDialog* _createComplaintDialog = nullptr;

        file_attach::FileAttachmentWidget *_fileWidget = nullptr;
        network::RequestsManagerShp _requestsManager;
        database::DBManagerShp _dbManager;

        // NOTE: models
        QSqlQueryModel* _companyModel = nullptr;
        QSqlQueryModel* _targetModel = nullptr;
        QSqlQueryModel* _natureModel = nullptr;

        bool _flagCreateComplaintOfReview = false;
        quint64 _idOrder;
        quint64 _idReview = 0;
        quint64 _idCompany;
        quint64 _idSource;
        QString _comment;

        QMap<QString, Reconnector> _reconnectMap;
    };

}
