#include "Common.h"
#include "VersionController.h"

#include "Widgets/MainWindow/MainWindow.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/Users/Users.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"

#include "session-controller/Session.h"

#include "utils/Settings/SettingsFactory.h"

#include "Models/ComplaintsModel.h"
#include "Models/CheckedModel.h"
#include "Models/UsersTableModel.h"
#include "Models/ReviewsModel.h"

#include "permissions/UserPermission.h"

#include "Controllers/ResponceChecker.h"

#include "Core/Core.h"


using namespace qat_client;

VersionController::VersionController(QObject *parent)
    : QObject(parent)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _requestsManager->init();
}

VersionController::~VersionController()
{
    _versionWidget->deleteLater();
}

void VersionController::onGetVersion(network::ResponseShp responce)
{
    const auto & versionList = responce->bodyToVariant().toList();

    QString version = "";
    if (versionList.count())
        version = versionList.first().toMap()["version"].toString();
    else
    {
        _getVersion = false;
        QMessageBox msgBox;
        msgBox.setText("Сервер не доступен.");
        msgBox.exec();

        emit signalGetVersion();
        return;
    }

    const qreal newVersion = version.toDouble();
    const qreal currVersion = _version.toDouble();
//    if (_version != version)
//    if (currVersion < newVersion)
    if ((currVersion - newVersion) > std::numeric_limits<double>::epsilon() ||
        std::fabs(currVersion - newVersion) < std::numeric_limits<double>::epsilon())
    {
        _getVersion = true;
    }
    else
    {
        _getVersion = false;
        _versionWidget = new VersionWidget(_version, version);
        _versionWidget->exec();
    }

    emit signalGetVersion();
}

bool VersionController::loadVersion()
{
    network::RequestShp request(new network::DefaultRequest("get_version", QVariant()));
    _requestsManager->execute(request, this, &VersionController::onGetVersion);
    QEventLoop loop;
    connect(this, &VersionController::signalGetVersion, &loop, &QEventLoop::quit);
    loop.exec();
    return _getVersion;
}

const QString VersionController::getVersion()
{
    return _version;
}
