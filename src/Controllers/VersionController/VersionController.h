﻿#pragma once

#include "Widgets/Version/VersionWidget.h"


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace qat_client
{
    class VersionWidget;

    class VersionController : public QObject
    {
        Q_OBJECT

    public:
        VersionController(QObject *parent = nullptr);
        ~VersionController();

        bool loadVersion();
        const QString getVersion();

    private:
        void onGetVersion(network::ResponseShp responce);

    signals:
        void signalGetVersion();

    private:
        network::RequestsManagerShp _requestsManager;
        VersionWidget * _versionWidget = nullptr;

        const QString _version = "1.33";
        bool _getVersion;
    };

    typedef QSharedPointer<VersionController> VersionControllerShp;

}
