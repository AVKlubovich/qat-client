﻿#pragma once

#include "Widgets/CreateUserDialog/CreateUserDialog.h"
#include "network-core/RequestsManager/RequestsManager.h"
#include "Reconnector.h"


namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace qat_client
{

    class CreateUserController : public QObject
    {
        Q_OBJECT

    public:
        explicit CreateUserController();

        void run();

        void setDbManager(database::DBManagerShp dbManager);
        void setDataUserControler(network::User &user);

    private slots:
        void createUser();
        void updateUser();

        void responseCreateUserReady(QSharedPointer<network::Response> response);
        void responseCreateUserFailed(QSharedPointer<network::ResponseServerError> response);
        void setIdDepartment(const int& index);

    private:
        CreateUserDialog _createUserDialog;
        network::RequestsManager _requestsManager;

        database::DBManagerShp _dbManager;

        QPointer<QSqlQueryModel> _companyModel;
        QPointer<QSqlQueryModel> _departmentModel;

        quint64 _idUser;

        QMap<QString, Reconnector> _reconnectMap;
    };

}
