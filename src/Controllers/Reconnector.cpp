#include "Common.h"
#include "Reconnector.h"


using namespace qat_client;

Reconnector::Reconnector(std::function<void ()> reconnectMethod)
    : _reconnectMethod(reconnectMethod)
{
}

void Reconnector::run()
{
    ++_counter;
    _reconnectMethod();
}

const quint8 Reconnector::count() const
{
    return _counter;
}
