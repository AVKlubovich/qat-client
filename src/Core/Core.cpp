﻿#include "Common.h"
#include "Core.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"

#include "utils/Logging/Logger.h"
#include "utils/Logging/LoggerMessages.h"
#include "utils/Logging/Devices/FileDevice.h"
#include "utils/Logging/Devices/DebuggerDevice.h"
#include "utils/Logging/Devices/ConsoleDevice.h"

#include "database/DBManager.h"
#include "database/DatabaseCreator.h"

#include "session-controller/Session.h"
#include "session-controller/SessionController.h"

#include "../Controllers/MainWindow/MainWindowController.h"
#include "../Controllers/VersionController/VersionController.h"

#include "multimedia/musicplayer.h"

using namespace qat_client;

Core::Core()
{
}

bool Core::init()
{
    readConfig();

    if (!initVersion())
    {
        qDebug() << "Version does not initialize";
        return false;
    }

    if (!initLogger())
    {
        qDebug() << "Logger does not initialize";
        return false;
    }

    if (!initDBManager())
    {
        qDebug() << "DBManager does not initialize";
        return false;
    }

    if (!initSession())
    {
        qDebug() << "Session does not initialize";
        return false;
    }

    if (!initWindowController())
    {
        qWarning() << "Could not initialize server";
        return false;
    }

    return true;
}

void Core::run()
{
    _windowController->run();
}

void Core::done()
{
    _windowController.clear();
    _session.clear();
    _dbManager.clear();
    _logger.clear();
    _versionController.clear();
}

void Core::readConfig()
{
    QDir::setCurrent( QCoreApplication::applicationDirPath() );

    utils::Settings::Options config = { "configuration/client.ini", true };
    utils::SettingsFactory::instance().registerSettings("client", config);

    auto settings = utils::SettingsFactory::instance().settings("client");
    settings =
    {
        // Client
        { "Client/FetchData", 5000 },
        { "Client/FilesFolder", "QATFiles" },

        // Connection
        { "Connection/Scheme", "http" },
        { "Connection/Host", "localhost" },
        { "Connection/Port", "81" },

        // Logs
        { "Log/EnableLog", true },
        { "Log/FlushInterval", 1000 },
        { "Log/PrefixName", "okk_client.log" },
        { "Log/Dir", "./logs/" },
        { "Log/MaxSize", 134217728 }, // 100 mb

        // DBManager
        { "DBManager/DBName", "file:memdb1?mode=memory&cache=shared" },
        { "DBManager/DBType", "QSQLITE" },

        // Connection to PHP
        { "PHP/Url", "http://192.168.202.242:81/QAT.php" },

        // Update
        { "Info/IpUpdate", "192.168.211.32" },
        { "Info/PortUpdate", "21" },
        { "Info/IntType", "0" },
        { "Info/Timeout", "40000" },
        { "Info/IsDomestic", "1" },
    };

    utils::SettingsFactory::instance().setCurrentSettings("client");

#ifdef QT_DEBUG
    utils::Settings::Options configProd = { "configuration/client_prod.ini", true };
    utils::SettingsFactory::instance().registerSettings("client_prod", configProd);

    utils::Settings::Options configLocal = { "configuration/client_local.ini", true };
    utils::SettingsFactory::instance().registerSettings("client_local", configLocal);

    utils::Settings::Options configProdDbInFile = { "configuration/client_prod_db_in_file.ini", true };
    utils::SettingsFactory::instance().registerSettings("client_prod_db_in_file", configProdDbInFile);

    utils::Settings::Options client_212143 = { "configuration/client_212143.ini", true };
    utils::SettingsFactory::instance().registerSettings("client_212143", client_212143);

    utils::Settings::Options client_212143_db_in_file = { "configuration/client_212143_db_in_file.ini", true };
    utils::SettingsFactory::instance().registerSettings("client_212143_db_in_file", client_212143_db_in_file);

//    utils::SettingsFactory::instance().setCurrentSettings("client_prod");
//    utils::SettingsFactory::instance().setCurrentSettings("client_local");
//    utils::SettingsFactory::instance().setCurrentSettings("client_202.242");
//    utils::SettingsFactory::instance().setCurrentSettings("client_212143_db_in_file");
#endif
}

bool Core::initLogger()
{
    // TODO: http://134.17.26.128:8080/browse/OKK-125
    return true;

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Log");

    if (!settings["Enable"].toBool())
    {
        qDebug() << "logger disabled";
        return true;
    }

    _logger = QSharedPointer<utils::Logger>::create();

    const auto loggerOptions = QSharedPointer<utils::LoggerMessages::Options>::create();
    loggerOptions->timerInterval = settings["FlushInterval"].toInt();
    if (!_logger->init(loggerOptions))
        return false;

    // FileDevice
    const auto fileOptions = QSharedPointer<utils::FileDevice::FileOptions>::create();
    fileOptions->maxSize = settings["MaxSize"].toLongLong();
    fileOptions->prefixName = settings["PrefixName"].toString(); //"okk_server.log";
    fileOptions->directory = settings["Dir"].toString();

    if (!_logger->addDevice(fileOptions))
        return false;

    // DebuggerDevice
    const auto debuggerDevice = QSharedPointer<utils::DebuggerDevice::DebuggerOptions>::create();

    if (!_logger->addDevice(debuggerDevice))
        return false;

    // ConsoleDevice
    const auto consoleDevice = QSharedPointer<utils::ConsoleDevice::ConsoleOptions>::create();

    if (!_logger->addDevice(consoleDevice))
        return false;

    qDebug() << "initLogger";
    return true;
}

bool Core::initDBManager()
{
    _dbManager = database::DBManagerShp::create();
    auto settings = utils::SettingsFactory::instance().currentSettings();
    database::DBManager::DBSettings dbSettings;
    settings.beginGroup("DBManager");
    dbSettings.database = settings["DBName"].toString();
    dbSettings.dbType= settings["DBType"].toString();
    _dbManager->initSettings(dbSettings);

    database::DatabaseCreator dbCreator;
    const auto createResult = dbCreator.createDatabase(_dbManager->getConnection(), QString());
    if (!createResult)
        return false;

    return true;
}

bool Core::initSession()
{
    _session = session_controller::SessionShp::create();
    auto sessionController = session_controller::SessionControllerShp::create(_session);

#ifdef QT_DEBUG
    QString login("admin");
    QString password("adminq321");
    sessionController->setDefaultCredentials(login, password);
#endif

    return sessionController->requestUserCredentials();
}

bool Core::initWindowController()
{
    _windowController = qat_client::MainWindowControllerShp::create();
    _windowController->setDBManager(_dbManager);
    _windowController->setSession(_session);
    _windowController->setVersion(_versionController->getVersion());

    return true;
}

bool Core::initVersion()
{
    _versionController = qat_client::VersionControllerShp::create();
    return _versionController->loadVersion();
}
