﻿#pragma once

#include "utils/BaseClasses/Singleton.h"


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace database
{
    class DBManager;
    typedef QSharedPointer<DBManager> DBManagerShp;
}

namespace session_controller
{
    class Session;
    typedef QSharedPointer<Session> SessionShp;
}

namespace utils
{
    class Logger;
    typedef QSharedPointer<Logger> LoggerShp;
}

namespace qat_client
{

    class MainWindowController;
    typedef QSharedPointer<MainWindowController> MainWindowControllerShp;

    class VersionController;
    typedef QSharedPointer<VersionController> VersionControllerShp;

    class Core
        : public utils::Singleton<Core>
    {
    public:
        Core();
        ~Core() = default;

        bool init();
        void run();
        void done();

    private:
        void readConfig();
        bool initLogger();
        bool initDBManager();
        bool initSession();
        bool initWindowController();
        bool initVersion();

    private:
        database::DBManagerShp _dbManager;
        utils::LoggerShp _logger;
        session_controller::SessionShp _session;
        qat_client::MainWindowControllerShp _windowController;
        qat_client::VersionControllerShp _versionController;
    };

}
