﻿#include "Common.h"
#include "StatusModel.h"
#include "permissions/UserPermission.h"


using namespace qat_client;

StatusModel::StatusModel(QSqlQueryModel *parent)
    : QStandardItemModel(parent)
{
}

void StatusModel::setDepartmentOkk(QVariantList list)
{
    _departmentOkk = list;
}

void StatusModel::setStatus(QVariantList list)
{
    quint64 row = 0;
    for (const auto& status : list)
    {
        QList<QStandardItem*> list;
        const auto& mapStatus = status.toMap();
        const auto& id = mapStatus["id"].toInt();

        if (id == 1)
        {
            if (!permissions::UserPermission::instance().getUserAdminComplaints())
                continue;
        }

        if (id == 5)
        {
            bool flag = false;
            auto list = permissions::UserPermission::instance().getListDepartments();
            for (const auto &dep : _departmentOkk)
            {
                const auto& idDep = dep.toMap()["id"].toInt();
                if (list.contains(idDep))
                    flag = true;
            }
            if (!flag)
                return;
        }

        list.append(new QStandardItem(mapStatus["name_status"].toString()));
        list.append(new QStandardItem(mapStatus["id"].toString()));
        this->insertRow(row, list);
        ++row;
    }
}
