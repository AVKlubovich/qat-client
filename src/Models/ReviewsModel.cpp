﻿#include "Common.h"
#include "ReviewsModel.h"


using namespace qat_client;

ReviewsModel::ReviewsModel(QSqlQueryModel *parent)
    : QSqlQueryModel(parent)
{
}

QVariant ReviewsModel::data(const QModelIndex &index, int role) const
{
    QVariant v = QSqlQueryModel::data(index, role);

    QModelIndex ind = this->index(index.row(), this->columnCount() - 1);

    if (role == Qt::BackgroundRole)
    {
        switch (ind.data(Qt::DisplayRole).toInt())
        {
            case 0:
                return QVariant(QColor(192, 211, 190));
                break;
            case 1:
                return QVariant(QColor(226, 224, 212));
                break;
            default:
                break;
        }
    }

    return v;
}
