﻿#pragma once


namespace qat_client
{

    class StatusModel : public QStandardItemModel
    {
        Q_OBJECT

    public:
        explicit StatusModel(QSqlQueryModel *parent = nullptr);
        ~StatusModel() = default;

        void setDepartmentOkk(QVariantList list);
        void setStatus(QVariantList list);
        void setSelectStatusClose();

    private:
        void createModel();

    private:
        QVariantList _departmentOkk;
        QVariantList _listStatus;
    };

}

