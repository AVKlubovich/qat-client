﻿#include "Common.h"
#include "DepartmentCheckedModel.h"


using namespace qat_client;

DepartmentCheckedModel::DepartmentCheckedModel(QObject* parent)
    : QStandardItemModel(parent)
{
}

Qt::ItemFlags DepartmentCheckedModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = Qt::ItemIsEnabled;
    flags |= Qt::ItemIsUserCheckable;

    return flags;
}

QVariant DepartmentCheckedModel::data(const QModelIndex &index, int role) const
{
    const auto originalData = QStandardItemModel::data(index, role);
    switch (role)
    {
        case Qt::CheckStateRole:
            if (!originalData.isValid())
                return checkedRole(index);
        default:
            break;
    }

    return originalData;
}

bool DepartmentCheckedModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid() || role != Qt::CheckStateRole)
        return false;

    const auto isSet = value.toInt() == Qt::Checked;

    if (isSet)
    {
        if (!_notSelectCompany)
            checkedAllGroup(index, value);
        _checkedIndex.insert(index);
    }
    else
    {
        if (!_notSelectCompany)
            checkedAllGroup(index, value);
        _checkedIndex.remove(index);
    }

    _notSelectCompany = false;

    emit selectionChanged(index);
    return true;
}

bool DepartmentCheckedModel::isChecked(const QModelIndex &index)
{
    if (_checkedIndex.contains(index))
        return true;

    return false;
}

bool DepartmentCheckedModel::isChecked(const quint64 idDepartment)
{
    for (const auto& ind : _checkedIndex)
    {
        if (data(ind, Qt::DisplayRole).toInt() == idDepartment)
        {
            return data(ind, Qt::CheckStateRole).toBool();
        }
    }
}

void DepartmentCheckedModel::setCheckedDepartments(QList<quint64> idDepartments)
{
    _checkedIndex.clear();
    for (int iParent = 0; iParent < rowCount(); ++iParent)
    {
        auto indexParent = index(iParent, 0);
        auto itemParent = itemFromIndex(index(iParent, 0));

        bool state = false;
        for (int i = 0; i < itemParent->rowCount(); ++i)
        {
            auto idDepartment = itemParent->child(i)->data(Qt::UserRole + 1).toLongLong();
            auto indexChild = indexFromItem(itemParent->child(i));
            if (idDepartments.contains(idDepartment))
            {
                state = true;
                setData(indexChild, Qt::Checked, Qt::CheckStateRole);
            }
        }

        if (state)
        {
            _notSelectCompany = true;
            setData(indexParent, Qt::Checked, Qt::CheckStateRole);
        }
    }
}

QVariantList DepartmentCheckedModel::getCheckedDepartments(const QModelIndex &indexCompany)
{
    QVariantList list;
    auto item = itemFromIndex(indexCompany);

    if (item == nullptr)
        return QVariantList();

    qDebug() << item->rowCount();
    if (item->rowCount())
    {
        for (int row = 0; row < item->rowCount(); ++row)
        {
            auto child = indexCompany.child(row, 0);
            if (_checkedIndex.contains(child))
            {
                auto itemChild = itemFromIndex(child);
                list.append(itemChild->data(Qt::UserRole + 1));
            }
        }

        return list;
    }
    return QVariantList();
}

void DepartmentCheckedModel::checkedAllGroup(const QModelIndex &index, const QVariant &value)
{
    auto item = this->itemFromIndex(index);
    if (item->rowCount())
    {
        for (int row = 0; row < item->rowCount(); ++row)
            this->setData(index.child(row, 0), value,  Qt::CheckStateRole);

        emit finishUpdate();
    }
}

QVariant DepartmentCheckedModel::checkedRole(const QModelIndex &index) const
{
    const auto checked = _checkedIndex.contains(index) ? Qt::Checked : Qt::Unchecked;
    return checked;
}

