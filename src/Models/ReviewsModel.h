﻿#pragma once


namespace qat_client
{

    class ReviewsModel : public QSqlQueryModel
    {
        Q_OBJECT

    public:
        explicit ReviewsModel(QSqlQueryModel *parent = nullptr);
        ~ReviewsModel() = default;

        QVariant data(const QModelIndex &index, int role) const override;
    };

}

