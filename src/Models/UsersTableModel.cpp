#include "Common.h"
#include "UsersTableModel.h"


using namespace qat_client;

UsersTableModel::UsersTableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

int UsersTableModel::rowCount(const QModelIndex&) const
{
    return _users.size();
}

int UsersTableModel::columnCount(const QModelIndex&) const
{
    return 5;
}

QVariant UsersTableModel::data(const QModelIndex &index, int role) const
{
    enum
    {
        login,
        name,
        position,
        company,
        department
    };

    if (role == Qt::DisplayRole)
    {
        const network::User & user = _users.at(index.row());
        switch (index.column())
        {
        case login:
            return user.login();
            break;
        case name:
            return user.name();
            break;
        case position:
            return user.officer();
            break;
        case company:
            return user.department();
            break;
        case department:
            return user.company();
            break;
        default:
            qWarning() << "Unexpected colomn index";
        }

        return QString("Unknown");
    }
    return QVariant();
}

void UsersTableModel::setUsers(const network::Users & users)
{
    beginResetModel();
    _users = users;
    endResetModel();
}

QVariant UsersTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    enum
    {
        login,
        name,
        position,
        company,
        department
    };

    if ((orientation == Qt::Horizontal) && (role == Qt::DisplayRole))
    {
        switch (section) {
        case login:
            return QString("Логин");
            break;
        case name:
            return QString("ФИО");
            break;
        case position:
            return QString("Должность");
            break;
        case company:
            return QString("Департамент");
            break;
        case department:
            return QString("Компания");
            break;
        default:
            break;
        }
    }

    return QVariant();
}
