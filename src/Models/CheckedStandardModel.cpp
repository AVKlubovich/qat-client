﻿#include "Common.h"
#include "CheckedStandardModel.h"


using namespace qat_client;

CheckedStandardModel::CheckedStandardModel(QObject* parent)
    : QStandardItemModel(parent)
{
    _checkedItems.reserve( this->rowCount() );
}

Qt::ItemFlags CheckedStandardModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = Qt::ItemIsEnabled;
    flags |= Qt::ItemIsUserCheckable;

    return flags;
}

QVariant CheckedStandardModel::data(const QModelIndex &index, int role) const
{
    const auto originalData = QStandardItemModel::data(index, role);
    switch (role)
    {
        case Qt::CheckStateRole:
            if (!originalData.isValid())
                return checkedRole( index );
        default:
            break;
    }

    return originalData;
}

bool CheckedStandardModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid() || role != Qt::CheckStateRole)
        return false;

    const auto isSet = value.toInt() == Qt::Checked;

    if (isSet)
    {
        _checkedItems.insert( index.row() );
        _checkedIndex.insert(index);
    }
    else
    {
        _checkedItems.remove( index.row() );
        _checkedIndex.remove(index);
    }

    if (_stateSignal)
        emit selectionChanged();

    _stateSignal = true;

    return true;
}

QVariant CheckedStandardModel::checkedRole(const QModelIndex &index) const
{
    const auto checked = _checkedItems.contains( index.row() ) ? Qt::Checked : Qt::Unchecked;
    return checked;
}

void CheckedStandardModel::clearChecked()
{
    _checkedItems.clear();
    _checkedIndex.clear();
}

void CheckedStandardModel::checkedAll()
{
    clearChecked();
    for (auto row = 0; row < this->rowCount(); ++row)
    {
        auto indexName = this->index(row, 0);
        _stateSignal = false;
        this->setData(indexName, Qt::Checked, Qt::CheckStateRole);
    }

    emit selectionChanged();
}

void CheckedStandardModel::checkedClearAll()
{
    for (auto row = this->rowCount(); row < 0; --row)
    {
        auto indexName = this->index(row, 0);
        _stateSignal = false;
        this->setData(indexName, Qt::Unchecked, Qt::CheckStateRole);
    }
    clearChecked();
}

bool CheckedStandardModel::isChecked(const QModelIndex &index)
{
    if (_checkedIndex.contains( index ))
        return true;

    return false;
}

QList<qint64> CheckedStandardModel::getCheckedList()
{
    return _checkedItems.toList();
}
