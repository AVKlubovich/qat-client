﻿#include "Common.h"
#include "ComplaintsModel.h"

#include "Definitions.h"


using namespace qat_client;

ComplaintsModel::ComplaintsModel(QSqlQueryModel *parent)
    : QSqlQueryModel(parent)
{
}

QVariant ComplaintsModel::data(const QModelIndex &index, int role) const
{
    QVariant v = QSqlQueryModel::data(index, role);

    QModelIndex indexStatus = this->index(index.row(), this->columnCount() - 1);
    QModelIndex indexDateHot = this->index(index.row(), this->columnCount() - 2);

    if (role == Qt::BackgroundRole)
    {
        if ((indexDateHot.isValid()) && (indexStatus.data().toInt() != 5))
        {
            const auto dateHot = indexDateHot.data().toDateTime();
            const auto dateTime = QDateTime::currentDateTime().addSecs(3000);
            auto hoursInSec = dateTime.secsTo(dateHot);
            if ((hoursInSec > 0) && (hoursInSec < 57600))
                return QVariant(QColor(230, 92, 79));
            if (hoursInSec <= 0)
                return QVariant(QColor(219, 46, 74));
        }

        switch (indexStatus.data(Qt::DisplayRole).toInt())
        {
            case _new:
                return QVariant(QColor(192, 211, 190));
                break;
            case in_the_work:
                return QVariant(QColor(253, 250, 183));
                break;
            case demolished:
                return QVariant(QColor(245, 179, 213));
                break;
            case it_requires_notification:
                return QVariant(QColor(Qt::white));
                break;
            case is_closed:
                return QVariant(QColor(226, 224, 212));
                break;
            default:
                break;
        }
    }

    return v;
}
