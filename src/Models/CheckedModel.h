﻿#pragma once


namespace qat_client
{

    class CheckedModel : public QSqlQueryModel
    {
        Q_OBJECT

    public:
        CheckedModel(QObject *parent = nullptr);

        Qt::ItemFlags flags(const QModelIndex & index) const override;
        QVariant data(const QModelIndex &index, int role) const override;
        bool setData(const QModelIndex& index, const QVariant & value, int role = Qt::EditRole) override;

        const QSet<qint64>& checkedStatus() const;
        const QSet<quint64>& checkedRealIndex() const;
        QVariantMap checkedRealIndexMapKeys() const;
        qint64 checkedCount() const;
        void setChecked(const QList<quint64>& listId);
        void setCheckedAll();
        QList<quint64> getListId();
        QVariantList getCheckedRealId();

        QMap<quint64, QString> getCheckedRealIndexMap() const;

        void clearChecked();
        void checkedAll();
        void checkedClearAll();
        bool isChecked(const QModelIndex &index);

    signals:
        void selectionChanged();
        void signalSelectAll();

    private:
        QVariant checkedRole(const QModelIndex& index) const;

    private:
        QSet<qint64> _checkedItems;
        QSet<QModelIndex> _checkedIndex;
        QSet<quint64> _checkedRealIndex;
        QVariantMap _checkedRealIndexMapKeys;
        QMap<quint64, QString> _checkedRealIndexMap;
        QVariantList _listId;

        bool _stateSignal = true;
    };

}

