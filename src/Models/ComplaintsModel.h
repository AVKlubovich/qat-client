﻿#pragma once


namespace qat_client
{

    class ComplaintsModel : public QSqlQueryModel
    {
        Q_OBJECT

    public:
        explicit ComplaintsModel(QSqlQueryModel *parent = nullptr);
        ~ComplaintsModel() = default;

        QVariant data(const QModelIndex &index, int role) const override;
    };

}

