﻿#pragma once


namespace qat_client
{

    class CheckedStandardModel : public QStandardItemModel
    {
        Q_OBJECT

    public:
        CheckedStandardModel(QObject *parent = nullptr);

        Qt::ItemFlags flags(const QModelIndex & index) const override;
        QVariant data(const QModelIndex &index, int role) const override;
        bool setData(const QModelIndex& index, const QVariant & value, int role = Qt::EditRole) override;

        void clearChecked();
        void checkedAll();
        void checkedClearAll();
        bool isChecked(const QModelIndex &index);

        QList<qint64> getCheckedList();

    signals:
        void selectionChanged();
        void signalSelectAll();

    private:
        QVariant checkedRole(const QModelIndex& index) const;

    private:
        QSet<qint64> _checkedItems;
        QSet<QModelIndex> _checkedIndex;
        bool _stateSignal = true;
    };

}

