﻿#include "Common.h"
#include "CheckedModel.h"


using namespace qat_client;

CheckedModel::CheckedModel(QObject* parent)
    : QSqlQueryModel(parent)
{
    _checkedItems.reserve( this->rowCount() );
}

Qt::ItemFlags CheckedModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = Qt::ItemIsEnabled;
    flags |= Qt::ItemIsUserCheckable;

    return flags;
}

QVariant CheckedModel::data(const QModelIndex &index, int role) const
{
    const auto originalData = QSqlQueryModel::data(index, role);
    switch (role)
    {
        case Qt::CheckStateRole:
            if (!originalData.isValid())
                return checkedRole( index );
        default:
            break;
    }

    return originalData;
}

bool CheckedModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid() || role != Qt::CheckStateRole)
        return false;

    const auto isSet = value.toInt() == Qt::Checked;

    if (isSet)
    {
        _checkedItems.insert( index.row() );
        _checkedIndex.insert(index);
        auto indexID = this->index(index.row(), this->columnCount() - 1, QModelIndex());
        _checkedRealIndex.insert(indexID.data(Qt::DisplayRole).toULongLong());
        _listId.append(indexID.data(Qt::DisplayRole));
        const int FIRST_COLUMN = 0;
        _checkedRealIndexMapKeys[indexID.data(Qt::DisplayRole).toString()] = 0;
        _checkedRealIndexMap[indexID.data(Qt::DisplayRole).toInt()] =
                this->index(index.row(), FIRST_COLUMN).data().toString();
    }
    else
    {
        _checkedItems.remove( index.row() );
        _checkedIndex.remove(index);
        auto indexID = this->index(index.row(), this->columnCount() - 1, QModelIndex());
        _checkedRealIndex.remove( indexID.data(Qt::DisplayRole).toULongLong() );
        _listId.removeAll(indexID.data(Qt::DisplayRole));
        _checkedRealIndexMapKeys.remove(indexID.data(Qt::DisplayRole).toString());
        _checkedRealIndexMap.remove(indexID.data(Qt::DisplayRole).toInt());
    }

    if (_stateSignal)
        emit selectionChanged();

    _stateSignal = true;

    return true;
}

void CheckedModel::setChecked(const QList<quint64> & listId)
{
    clearChecked();
    for (int j = 0; j < listId.count(); j++)
    {
        QModelIndex indexName, indexId;
        for (int i = 0; (indexId = this->index(i, this->columnCount() - 1)).isValid()
             && (indexName = this->index(i, 0)).isValid(); i++)
        {
            if (listId.at(j) == indexId.data(Qt::DisplayRole).toULongLong())
            {
                _stateSignal = false;
                this->setData(indexName, Qt::Checked, Qt::CheckStateRole);
            }
        }
    }
}

void CheckedModel::setCheckedAll()
{
    const auto& listId = getListId();
    (void)listId;

    setChecked(getListId());

    emit signalSelectAll();
}

QList<quint64> CheckedModel::getListId()
{
    QList<quint64> list;
    for (int i = 0; i < this->rowCount(); i++)
    {
        auto index = this->index(i, this->columnCount() - 1);
        if (index.isValid())
            list.append(index.data(Qt::DisplayRole).toULongLong());
    }

    return list;
}

QVariantList CheckedModel::getCheckedRealId()
{
    return _listId;
}

const QSet<qint64> &CheckedModel::checkedStatus() const
{
    return _checkedItems;
}

const QSet<quint64> &CheckedModel::checkedRealIndex() const
{
    return _checkedRealIndex;
}

qint64 CheckedModel::checkedCount() const
{
    return _checkedRealIndex.count();
}

QVariant CheckedModel::checkedRole(const QModelIndex &index) const
{
    const auto checked = _checkedItems.contains( index.row() ) ? Qt::Checked : Qt::Unchecked;
    return checked;
}

QVariantMap CheckedModel::checkedRealIndexMapKeys() const
{
    return _checkedRealIndexMapKeys;
}

QMap<quint64, QString> CheckedModel::getCheckedRealIndexMap() const
{
    return _checkedRealIndexMap;
}

void CheckedModel::clearChecked()
{
    _checkedItems.clear();
    _checkedIndex.clear();
    _checkedRealIndex.clear();
    _checkedRealIndexMapKeys.clear();
    _checkedRealIndexMap.clear();
}

void CheckedModel::checkedAll()
{
    clearChecked();
    for (auto row = 0; row < this->rowCount(); ++row)
    {
        auto indexName = this->index(row, 0);
        _stateSignal = false;
        this->setData(indexName, Qt::Checked, Qt::CheckStateRole);
    }

    emit selectionChanged();
}

void CheckedModel::checkedClearAll()
{
    for (auto row = this->rowCount(); row < 0; --row)
    {
        auto indexName = this->index(row, 0);
        _stateSignal = false;
        this->setData(indexName, Qt::Unchecked, Qt::CheckStateRole);
    }
    clearChecked();
}

bool CheckedModel::isChecked(const QModelIndex &index)
{
    if (_checkedIndex.contains( index ))
        return true;

    return false;
}
