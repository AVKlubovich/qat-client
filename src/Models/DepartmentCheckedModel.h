﻿#pragma once


namespace qat_client
{

    class DepartmentCheckedModel : public QStandardItemModel
    {
        Q_OBJECT

    public: // TODO model
        DepartmentCheckedModel(QObject *parent = nullptr);

        Qt::ItemFlags flags(const QModelIndex & index) const override;
        QVariant data(const QModelIndex &index, int role) const override;
        bool setData(const QModelIndex& index, const QVariant & value, int role = Qt::EditRole) override;

        bool isChecked(const QModelIndex & index);
        bool isChecked(const quint64 idDepartment);
        void setCheckedDepartments(QList <quint64> idDepartments);
        QVariantList getCheckedDepartments(const QModelIndex & indexCompany);

    signals:
        void selectionChanged(const QModelIndex &index);
        void finishUpdate();

    private:
        QVariant checkedRole(const QModelIndex& index) const;
        void checkedAllGroup(const QModelIndex &index, const QVariant &value);

    private:
        QSet<QModelIndex> _checkedIndex;
        bool _notSelectCompany = false;
    };

}

