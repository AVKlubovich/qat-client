﻿#pragma once

#include "network-core/RequestsManager/Users/Users.h"


namespace qat_client
{

    class UsersTableModel : public QAbstractTableModel
    {
        Q_OBJECT

    public:
        UsersTableModel(QObject *parent = nullptr);
        ~UsersTableModel() = default;

        int rowCount(const QModelIndex &parent = QModelIndex()) const;
        int columnCount(const QModelIndex &parent = QModelIndex()) const;
        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
        void setUsers(const network::Users & users);

        QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    private:
        network::Users _users;
    };

}
