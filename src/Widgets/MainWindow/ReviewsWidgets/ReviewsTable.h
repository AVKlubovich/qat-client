﻿#pragma once


namespace qat_client
{

    class ReviewsTable : public QTableView
    {
        Q_OBJECT

    public:
        explicit ReviewsTable(QWidget * parent = nullptr);
        ~ReviewsTable();

        void nextItemReview();

    signals:
        //complaints management
        void reviewSelected(const quint64 id);

    protected slots:
        void currentChanged(const QModelIndex & current, const QModelIndex & previous);

    private:
        QModelIndex _lastModelImdex;
    };

}
