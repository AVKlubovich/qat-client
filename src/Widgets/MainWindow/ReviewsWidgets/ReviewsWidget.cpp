﻿#include "Common.h"

#include "ReviewsWidget.h"
#include "ui_ReviewsWidget.h"

#include "Models/CheckedModel.h"
#include "permissions/UserPermission.h"


using namespace qat_client;

ReviewsWidget::ReviewsWidget(QWidget * parent)
    : QWidget(parent)
    , _ui(new Ui::ReviewsWidget)
    , _companiesModel(nullptr)
    , _sourcesModel(nullptr)
{
    _ui->setupUi(this);

    connect(_ui->reviewsTable, &ReviewsTable::reviewSelected,
            this, [this](const quint64 id) {
                    emit reviewsSelected(id);
                  });

    connect(_ui->reviewsTable, &QTableView::clicked,
            this, &ReviewsWidget::onReviewClicked);

//    _ui->reviewsTable->setItemDelegateForColumn(1, new DateTimeEditDelegate());

    auto chechStateF = [this](int state)
    {
        if (state == Qt::Unchecked ||
            state == Qt::PartiallyChecked)
            _ui->dateEnd->setEnabled(true);
        else
            _ui->dateEnd->setEnabled(false);

    };
    connect(_ui->chbIsSetDateEnd, &QCheckBox::stateChanged, this, chechStateF);

    connect(_ui->reviewsTable, &QTableView::customContextMenuRequested,
            this, &ReviewsWidget::signalHideWidget);
}

ReviewsWidget::~ReviewsWidget()
{
    delete _ui;
}

void ReviewsWidget::setReviewsModel(QAbstractItemModel* model)
{
    QSortFilterProxyModel *proxy = new QSortFilterProxyModel(this);
    proxy->setSourceModel(model);

    _ui->reviewsTable->setModel(proxy);
}

void ReviewsWidget::setCompaniesModel(CheckedModel * companiesModel)
{
    _ui->lvCompanies->setModel(companiesModel);
    _companiesModel = companiesModel;
}

void ReviewsWidget::setSourcesModel(CheckedModel *model)
{
    _ui->cbSource->setModel(model);
    _sourcesModel = model;
}

void ReviewsWidget::setSettings(ReviewsWidgetController::SettingsReviews settings)
{
    _settings = settings;

    if (_companiesModel != nullptr)
        _companiesModel->setChecked(settings._companies);

    if (_sourcesModel != nullptr)
        _sourcesModel->setChecked(settings._sources);

    _ui->editFind->setText(settings._like);

    _ui->dateStart->setDateTime(_settings._dateStart);

    if (_settings._isSetDateEnd)
    {
        _ui->dateEnd->setEnabled(true);
        _ui->dateEnd->setDateTime(_settings._dateEnd);
        _ui->chbIsSetDateEnd->setChecked(false);
    }
    else
    {
        _ui->dateEnd->setEnabled(false);
        _ui->dateEnd->setDateTime(QDateTime::currentDateTime());
        _ui->chbIsSetDateEnd->setChecked(true);
    }

    _ui->chbCloseReviews->setChecked(_settings._isCloseReviews);

    // auto refresh
    connect(_ui->lvCompanies, &QListView::clicked,
            this, &ReviewsWidget::refreshClicked);

    connect(qobject_cast<qat_client::CheckedModel *>(_ui->cbSource->model()), &CheckedModel::selectionChanged,
        this, &ReviewsWidget::refreshClicked);

    connect(_ui->chbCloseReviews, &QCheckBox::clicked,
            this, &ReviewsWidget::refreshClicked);

    connect(_ui->chbIsSetDateEnd, &QCheckBox::clicked,
            this, &ReviewsWidget::refreshClicked);

    connect(_ui->editFind, &QLineEdit::textChanged,
            this, &ReviewsWidget::refreshClicked);

    connect(_ui->dateStart, &QDateEdit::dateChanged,
            this, &ReviewsWidget::refreshClicked);

    connect(_ui->dateEnd, &QDateEdit::dateChanged,
            this, &ReviewsWidget::refreshClicked);

    connect(_ui->btnCheckedAll, &QPushButton::clicked,
            this, &ReviewsWidget::signalCheckedAll);

    connect(_ui->btnUncheckedAll, &QPushButton::clicked,
            this, &ReviewsWidget::signalUncheckedAll);
}

void ReviewsWidget::setReviewsInfoWidget(QWidget* widget)
{
    QSplitter * spliter = new QSplitter(Qt::Vertical);
    spliter->addWidget(_ui->reviewsTable);
    spliter->addWidget(widget);
    _ui->gridReviews->layout()->addWidget(spliter);
    _ui->gridReviews->setStretch(1, 1); // растяжение только элемента с индексом 1
}

QCheckBox *ReviewsWidget::getChbIsSetDateEnd()
{
    return _ui->chbIsSetDateEnd;
}

void ReviewsWidget::updateCompanies()
{
    _ui->lvCompanies->reset();
    refreshClicked();
}

void ReviewsWidget::slotOpenNextItemReview()
{
    _ui->reviewsTable->nextItemReview();
}

void ReviewsWidget::updateReviews()
{
    _ui->reviewsTable->viewport()->update();
    _ui->reviewsTable->update(QModelIndex());

    _ui->cbSource->updateTextHints();

    enum
    {
        status = 6,
    };

    _ui->reviewsTable->setColumnHidden(status, true);
    _ui->reviewsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void ReviewsWidget::refreshClicked()
{
    setSettings();
    emit signalSelectReviews(_settings);
}

void ReviewsWidget::onReviewClicked(const QModelIndex &index)
{
    const auto& idIndex = _ui->reviewsTable->model()->index(index.row(), 0, QModelIndex());
    emit reviewsSelected(idIndex.data(Qt::DisplayRole).toULongLong());
}

void ReviewsWidget::setSettings()
{
    _settings.clear();
    if (_companiesModel != nullptr)
        _settings._companies = _companiesModel->checkedRealIndex().toList();

    if (_sourcesModel != nullptr)
        _settings._sources = _sourcesModel->checkedRealIndex().toList();

    _settings._dateStart = QDateTime(_ui->dateStart->dateTime());
    if (!_ui->chbIsSetDateEnd->isChecked())
    {
        _settings._isSetDateEnd = true;
        _settings._dateEnd = QDateTime(_ui->dateEnd->dateTime());
    }
    else
    {
        _settings._isSetDateEnd = false;
    }

    if (_ui->chbCloseReviews->isChecked())
        _settings._isCloseReviews = true;
    else
        _settings._isCloseReviews = false;

    _settings._like = _ui->editFind->text();
}

void qat_client::ReviewsWidget::on_btnExcel_clicked()
{
    const QString& fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                          "reviews " + QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss") + ".xlsx",
                                                          tr("Excel (*.xlsx)"));

    if (!fileName.isEmpty())
        emit signalCreateReviewsExcelFile(fileName);
}

void ReviewsWidget::setPermissionsWidget()
{
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();
}
