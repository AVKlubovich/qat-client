﻿#include "Common.h"
#include "ReviewsTable.h"


using namespace qat_client;

ReviewsTable::ReviewsTable(QWidget * parent)
    : QTableView(parent)
{
}

ReviewsTable::~ReviewsTable()
{
}

void ReviewsTable::nextItemReview()
{
    this->selectRow(_lastModelImdex.row() + 1);
}

void ReviewsTable::currentChanged(const QModelIndex & current, const QModelIndex & previous)
{
    _lastModelImdex = current;

    QTableView::currentChanged(current, previous);
    const auto& idIndex = model()->index(current.row(), 0, QModelIndex());
    emit reviewSelected(idIndex.data(Qt::DisplayRole).toULongLong());
}
