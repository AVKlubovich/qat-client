﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/MainWindow/ReviewsControllers/ReviewsWidgetController.h"


namespace Ui
{
    class ReviewsWidget;
}

namespace qat_client
{

    class CheckedModel;

    class ReviewsWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit ReviewsWidget(QWidget * parent = nullptr);
        ~ReviewsWidget();

        void setReviewsModel(QAbstractItemModel* model);
        void setCompaniesModel(CheckedModel * companiesModel);
        void setSourcesModel(CheckedModel *model);
        void setSettings(ReviewsWidgetController::SettingsReviews settings);
        void updateReviews();
        void setReviewsInfoWidget(QWidget* widget);
        QCheckBox * getChbIsSetDateEnd();

    signals:
        //complaints management
        void signalSelectReviews(const ReviewsWidgetController::SettingsReviews & settings);
        void reviewsSelected(const quint64 id);
        void signalCreateReviewsExcelFile(const QString & fileName);
        void signalHideWidget();

        void signalCheckedAll();
        void signalUncheckedAll();

    public slots:
        void slotOpenNextItemReview();

        void updateCompanies();

    private slots:
        void onReviewClicked(const QModelIndex& index);
        void refreshClicked();

        void on_btnExcel_clicked();

    private:
        void setSettings();
        void setPermissionsWidget();

    private:
        Ui::ReviewsWidget *_ui;
        ReviewsWidgetController::SettingsReviews _settings;
        CheckedModel * _companiesModel;
        CheckedModel * _sourcesModel;
    };

}
