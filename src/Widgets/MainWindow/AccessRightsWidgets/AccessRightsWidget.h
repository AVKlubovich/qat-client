﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/MainWindow/AccessRightsControllers/AccessRightsController.h"


namespace Ui
{
    class AccessRightsWidget;
}

namespace qat_client
{

    class CheckedModel;

    class AccessRightsWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit AccessRightsWidget(QWidget * parent = nullptr);
        ~AccessRightsWidget();

        void setUsersTableModel(QAbstractItemModel* model);
        void setUsersModel(QAbstractItemModel* model);
        void setDepartamentsModel(QAbstractItemModel* model);
        void setTargetsModel(CheckedModel * targetsModel);
        void setCommandsModel(CheckedModel * commandsModel);

        void setDepartamentUser(QModelIndex indexDepartament);
        void setResetListCommand();
        void setResetTreeDepartments();

        void setTextDepartment(const QString & text);
        void isEnabledPermissions(bool isEnabled);
        void isEnabledDepartments(bool isEnabled);
        void isEnabledBtnSaveUserPermissions(bool isEnabled);
        void isExpandedTreeDepartment();
        bool isCurrentListUserIndex();

        void setCompanyModel(QAbstractItemModel *model);
        void setWorkerModel(QAbstractItemModel *model);
        void setNatureModel(QAbstractItemModel *model);
        void setDepartamentModel(QAbstractItemModel *model);

        const int getModelIndexCompany();
        const int getModelIndexTarget();
        void hideWidgets();

        void setTempalateSmsModel(QAbstractItemModel *model);
        void onChangedNCompany();

    private:
        void connectMethod();
        void setPermission();

    signals:
        //user accounts signals
        void signalRefreshUsers();//notify UI need to undate user accounts list
        void signalCreateUser();//UI request to create new user
        void signalDeleteUser(const int row);

//        void signalSelectCompany(const int idIndex);
        void sigmalListUsersClicked(QModelIndex index);
        void signalDoubleClicked(const int row);
        void signalRefreshDepartments(const QModelIndex & index);

        // new signals
        void signalBtnSearch(const QString & name);
        void signalClickedDepartment(const QModelIndex & index);
        void signalCheckedAll();
        void signalChangedCompany(const int idCompany);
        void signalClickedBtnSaveUserPermissions();

        void signalDelete(const int id, const QString & table);
        void signalSaveAdd(const QString & text, const QString & table);
        void signalSaveUpdate(const QString & text, const QString & table, const int id = -1);
        void signalSaveNatureAdd(const QString & text, const int day, const QString &essense, const QString & table, const QString &cost, const int rating = 0, int type = 0);
        void signalSaveNatureUpdate(const QString & text, const int day, const QString &essense, const QString & table, const QString &cost, const int id = -1, const int rating = 0, int type = 0);

        void signalRefreshNature(const int idCompany, const int idTarget);

        void signalBtnCreateTestReviewClicked();
        void signalCbNCompany(int index);
        void signalTypeTemplate(int type);
        void signalBtnClickedInsertTemplate(int idCompany);
        void signalBtnClickedRemoveTemplate(int idTemplate);
        void signalBtnClickedSaveTemplate(int idCompany, const QString &description, const QString &smsText);

    private slots:
        void onBtnDeleteUserClicked();
        void hideBtnCompany();
        void hideBtnNature();
        void hideBtnWorker();
        void hideBtnDepartments();

        void onClickedListTemplate(const QModelIndex &index);

    private:
        Ui::AccessRightsWidget *_ui;

        int _rowUser;
        QModelIndex _userIndex;

        bool flagUpdateCompany = false;
        bool flagUpdateNature = false;
        bool flagUpdateWorker = false;
        bool flagUpdateDepartment = false;

        bool _flagUpdateUser = false;
    };

}
