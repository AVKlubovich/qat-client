#include "Common.h"

#include "AccessRightsWidget.h"
#include "ui_AccessRightsWidget.h"

#include "Models/CheckedModel.h"
#include "permissions/UserPermission.h"
#include "Definitions.h"


using namespace qat_client;

AccessRightsWidget::AccessRightsWidget(QWidget * parent)
    : QWidget(parent)
    , _ui(new Ui::AccessRightsWidget)
{
    _ui->setupUi(this);

    _ui->editCost->setInputMask("9.9");

    _ui->tableViewUsers->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    connect(_ui->btnCreateUser, &QPushButton::clicked,
            this, &AccessRightsWidget::signalCreateUser);

    connect(_ui->btnRefreshUsers, &QPushButton::clicked,
            this, &AccessRightsWidget::signalRefreshUsers);

    connect(_ui->listUsers, &QListView::clicked,
            this, &AccessRightsWidget::sigmalListUsersClicked);

    auto searchF = [this]()
    {
        emit signalBtnSearch(_ui->editFind->text());
    };
    connect(_ui->editFind, &QLineEdit::textChanged, searchF);

    connect(_ui->treeDepartaments, &QTreeView::clicked,
            this, &AccessRightsWidget::signalClickedDepartment);

    connect(_ui->listUsers, &QListView::clicked,
            this,
    [=](QModelIndex index)
    {
        _userIndex = index;
        emit signalRefreshDepartments(index);
    });

    connect(_ui->btnSaveUserPermissions, &QPushButton::clicked,
            this, &AccessRightsWidget::signalClickedBtnSaveUserPermissions);

    connect(_ui->btnDeleteUser, &QPushButton::clicked,
            this, &AccessRightsWidget::onBtnDeleteUserClicked);

    connect(_ui->tableViewUsers, &QTableView::clicked, this,
            [=](QModelIndex index)
    {
        _rowUser = index.row();
    });

    connect(_ui->tableViewUsers, &QTableView::doubleClicked,
            [=](QModelIndex index)
    {
        const int row = index.row();
        if (_flagUpdateUser)
            emit signalDoubleClicked(row);
    });

    connect(_ui->btnAllChecked, &QPushButton::clicked,
            this, &AccessRightsWidget::signalCheckedAll);

    connect(_ui->btnTestReview, &QPushButton::clicked,
            this, &AccessRightsWidget::signalBtnCreateTestReviewClicked);

    connect(_ui->cbNCompany, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
             this, &AccessRightsWidget::signalCbNCompany);

    connect(_ui->cbTypeTemplate, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &AccessRightsWidget::signalTypeTemplate);

    connect(_ui->listTemplate, &QListView::clicked,
            this, &AccessRightsWidget::onClickedListTemplate);

    connect(_ui->btnAddTemplate, &QPushButton::clicked,
            [this]()
    {
        int idCompany = _ui->cbNCompany->model()->index(_ui->cbNCompany->currentIndex(), id_template).data().toInt();
        emit signalBtnClickedInsertTemplate(idCompany);
    });

    connect(_ui->btnSaveTemplate, &QPushButton::clicked,
            [this]()
    {
        int id = _ui->listTemplate->model()->index(_ui->listTemplate->currentIndex().row(), id_template).data().toInt();
        emit signalBtnClickedSaveTemplate(id, _ui->editDescription->text(), _ui->editSms->toPlainText());

        _ui->editDescription->clear();
        _ui->editSms->clear();
    });

    connect(_ui->btnRemoveTemplate, &QPushButton::clicked,
            [this]
    {
        int id = _ui->listTemplate->model()->index(_ui->listTemplate->currentIndex().row(), id_template).data().toInt();
        if (id != 0)
            emit signalBtnClickedRemoveTemplate(id);
        else
            QMessageBox::information(this, "Информация",
                                     "Не выбран шаблон из списка.\nПожалуйста, выберите шаблон.", QMessageBox::Ok);

        _ui->editDescription->clear();
        _ui->editSms->clear();
    });

    hideWidgets();
    connectMethod();
    setPermission();

    _ui->editFind->setEnabled(false);
    _ui->btnSaveUserPermissions->setEnabled(false);

#ifndef QT_DEBUG
    _ui->btnTestReview->hide();
#endif
}

AccessRightsWidget::~AccessRightsWidget()
{
    delete _ui;
}

void AccessRightsWidget::setUsersTableModel(QAbstractItemModel* model)
{
    _ui->tableViewUsers->setModel(model);
}

void AccessRightsWidget::setUsersModel(QAbstractItemModel *model)
{
    _ui->listUsers->setModel(model);

    _ui->editFind->setEnabled(true);
}

void AccessRightsWidget::setDepartamentsModel(QAbstractItemModel *model)
{
    _ui->treeDepartaments->setModel(model);
}

void AccessRightsWidget::setCommandsModel(qat_client::CheckedModel *commandsModel)
{
    _ui->listCommands->setModel(commandsModel);
}

void AccessRightsWidget::setDepartamentUser(QModelIndex indexDepartament)
{
    _ui->treeDepartaments->setCurrentIndex(indexDepartament);
}

void AccessRightsWidget::setResetListCommand()
{
    _ui->listCommands->reset();
}

void AccessRightsWidget::setResetTreeDepartments()
{
    _ui->treeDepartaments->reset();
}

void AccessRightsWidget::setTextDepartment(const QString &text)
{
    _ui->infoLabel->setText(text);
}

void AccessRightsWidget::isEnabledPermissions(bool isEnabled)
{
    _ui->listCommands->setEnabled(isEnabled);
}

void AccessRightsWidget::isEnabledDepartments(bool isEnabled)
{
    _ui->treeDepartaments->setEnabled(isEnabled);
}

void AccessRightsWidget::isEnabledBtnSaveUserPermissions(bool isEnabled)
{
    _ui->btnSaveUserPermissions->setEnabled(!isEnabled);
}

void AccessRightsWidget::isExpandedTreeDepartment()
{
    _ui->treeDepartaments->expandAll();
}

bool AccessRightsWidget::isCurrentListUserIndex()
{
    return _ui->listUsers->currentIndex().isValid();
}

void AccessRightsWidget::hideBtnCompany()
{
    bool hide = _ui->btnAddCompany->isHidden() ? true : false;
    _ui->btnAddCompany->setHidden(!hide);
    _ui->btnUpdateCompany->setHidden(!hide);
    _ui->btnDelCompany->setHidden(!hide);
    _ui->btnSaveCompany->setHidden(hide);
    _ui->btnCancelCompany->setHidden(hide);
    _ui->editCompany->setHidden(hide);
}

void AccessRightsWidget::hideBtnNature()
{
    bool hide = _ui->btnAddNature->isHidden() ? true : false;
    _ui->btnAddNature->setHidden(!hide);
    _ui->btnUpdateNature->setHidden(!hide);
    _ui->btnDelNature->setHidden(!hide);
    _ui->btnSaveNature->setHidden(hide);
    _ui->btnCancelNature->setHidden(hide);
    _ui->editNature->setHidden(hide);
    _ui->tEditNature->setHidden(hide);
    _ui->editDay->setHidden(hide);
    _ui->editRating->setHidden(hide);
    _ui->editCost->setHidden(hide);
    _ui->label_4->setHidden(hide);
    _ui->label_6->setHidden(hide);
    _ui->label_7->setHidden(hide);
    _ui->label_8->setHidden(hide);
    _ui->cbTypeNature->setHidden(hide);
}

void AccessRightsWidget::hideBtnWorker()
{
    bool hide = _ui->btnAddWorker->isHidden() ? true : false;
    _ui->btnAddWorker->setHidden(!hide);
    _ui->btnUpdateWorker->setHidden(!hide);
    _ui->btnDelWorker->setHidden(!hide);
    _ui->btnSaveWorker->setHidden(hide);
    _ui->btnCancelWorker->setHidden(hide);
    _ui->editWorker->setHidden(hide);
}

void AccessRightsWidget::hideBtnDepartments()
{
    bool hide = _ui->btnAddDepartment->isHidden() ? true : false;
    _ui->btnAddDepartment->setHidden(!hide);
    _ui->btnUpdateDepartment->setHidden(!hide);
    _ui->btnDelDepartment->setHidden(!hide);
    _ui->btnSaveDepartment->setHidden(hide);
    _ui->btnCancelDepartment->setHidden(hide);
    _ui->editDepartment->setHidden(hide);
}

void AccessRightsWidget::onClickedListTemplate(const QModelIndex &index)
{
    const int rowIndex = index.row();

    _ui->editDescription->setText(_ui->listTemplate->model()->index(rowIndex, description).data().toString());
    _ui->editSms->setText(_ui->listTemplate->model()->index(rowIndex, message_text).data().toString());
}

void AccessRightsWidget::onBtnDeleteUserClicked()
{
    emit signalDeleteUser(_rowUser);
}

void AccessRightsWidget::setCompanyModel(QAbstractItemModel *model)
{
    _ui->cbCompany->setModel(model);
    _ui->cbNCompany->setModel(model);
}

void AccessRightsWidget::setWorkerModel(QAbstractItemModel *model)
{
    _ui->cbWorker->setModel(model);
}

void AccessRightsWidget::setNatureModel(QAbstractItemModel *model)
{
    _ui->cbNature->setModel(model);
}

void AccessRightsWidget::setDepartamentModel(QAbstractItemModel *model)
{
    _ui->cbDepartment->setModel(model);
}

const int AccessRightsWidget::getModelIndexCompany()
{
    return _ui->cbCompany->model()->index(_ui->cbCompany->currentIndex(), 1).data().toInt();
}

const int AccessRightsWidget::getModelIndexTarget()
{
    return _ui->cbWorker->model()->index(_ui->cbWorker->currentIndex(), 1).data().toInt();
}

void AccessRightsWidget::hideWidgets()
{
    _ui->btnAddNature->setEnabled(false);
    _ui->btnAddDepartment->setEnabled(false);

    _ui->btnUpdateCompany->setEnabled(false);
    _ui->btnUpdateNature->setEnabled(false);
    _ui->btnUpdateWorker->setEnabled(false);
    _ui->btnUpdateDepartment->setEnabled(false);

    _ui->btnDelCompany->setEnabled(false);
    _ui->btnDelNature->setEnabled(false);
    _ui->btnDelWorker->setEnabled(false);
    _ui->btnDelDepartment->setEnabled(false);

    _ui->btnSaveCompany->setHidden(true);
    _ui->btnSaveNature->setHidden(true);
    _ui->btnSaveWorker->setHidden(true);
    _ui->btnSaveDepartment->setHidden(true);

    _ui->btnCancelCompany->setHidden(true);
    _ui->btnCancelNature->setHidden(true);
    _ui->btnCancelWorker->setHidden(true);
    _ui->btnCancelDepartment->setHidden(true);

    _ui->editCompany->setHidden(true);
    _ui->editNature->setHidden(true);
    _ui->editWorker->setHidden(true);
    _ui->editDepartment->setHidden(true);
    _ui->tEditNature->setHidden(true);
    _ui->editDay->setHidden(true);
    _ui->editRating->setHidden(true);
    _ui->editCost->setHidden(true);
    _ui->label_4->setHidden(true);
    _ui->label_6->setHidden(true);
    _ui->label_7->setHidden(true);
    _ui->label_8->setHidden(true);
    _ui->cbTypeNature->setHidden(true);
}

void AccessRightsWidget::setTempalateSmsModel(QAbstractItemModel *model)
{
    _ui->listTemplate->setModel(model);
}

void AccessRightsWidget::onChangedNCompany()
{
    if (_ui->cbNCompany->currentIndex() != -1)
        emit signalCbNCompany(_ui->cbNCompany->currentIndex());
}

void AccessRightsWidget::connectMethod()
{
    auto refreshNature = [=](int row)
    {
        const auto idCompany = _ui->cbCompany->model()->index(_ui->cbCompany->currentIndex(), 1).data().toInt();
        const auto idTarget = _ui->cbWorker->model()->index(_ui->cbWorker->currentIndex(), 1).data().toInt();

        if ((_ui->cbCompany->currentIndex() >= 0))
        {
            _ui->btnAddDepartment->setEnabled(true);
            _ui->btnAddCompany->setEnabled(true);
            _ui->btnUpdateCompany->setEnabled(true);
            _ui->btnDelCompany->setEnabled(true);

            emit signalChangedCompany(idCompany);
        }

        if (_ui->cbWorker->currentIndex() >= 0)
        {
            _ui->btnUpdateWorker->setEnabled(true);
            _ui->btnDelWorker->setEnabled(true);
        }

        if ((_ui->cbCompany->currentIndex() >= 0) && (_ui->cbWorker->currentIndex() >= 0))
        {
            _ui->btnAddNature->setEnabled(true);
            emit signalRefreshNature(idCompany, idTarget);
        }
    };

    connect(_ui->cbCompany, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
             refreshNature);
    connect(_ui->cbWorker, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
             refreshNature);
    connect(_ui->cbNature, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            [this](){
        if (_ui->cbNature->currentIndex() >= 0)
        {
            _ui->btnUpdateNature->setEnabled(true);
            _ui->btnDelNature->setEnabled(true);
        }
    });
    connect(_ui->cbDepartment, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            [this](){
        if (_ui->cbDepartment->currentIndex() >= 0)
        {
            _ui->btnUpdateDepartment->setEnabled(true);
            _ui->btnDelDepartment->setEnabled(true);
        }
    });

    // add
    connect(_ui->btnAddCompany, &QPushButton::clicked, this, &AccessRightsWidget::hideBtnCompany);
    connect(_ui->btnAddNature, &QPushButton::clicked, this, &AccessRightsWidget::hideBtnNature);
    connect(_ui->btnAddWorker, &QPushButton::clicked, this, &AccessRightsWidget::hideBtnWorker);
    connect(_ui->btnAddDepartment, &QPushButton::clicked, this, &AccessRightsWidget::hideBtnDepartments);

    // update
    connect(_ui->btnUpdateCompany, &QPushButton::clicked, [this]() {
        flagUpdateCompany = true;
        _ui->editCompany->setText(_ui->cbCompany->currentText());
        hideBtnCompany();
    });

    connect(_ui->btnUpdateNature, &QPushButton::clicked, [this]() {
        flagUpdateNature = true;
        auto model = _ui->cbNature->model();
        auto essense        = model->index(_ui->cbNature->currentIndex(), 1).data().toString();
        auto day            = model->index(_ui->cbNature->currentIndex(), 2).data().toString();
        auto default_rating = model->index(_ui->cbNature->currentIndex(), 4).data().toString();
        auto cost           = model->index(_ui->cbNature->currentIndex(), 5).data().toFloat();
        auto typeIndex      = model->index(_ui->cbNature->currentIndex(), 6).data().toInt();
        _ui->editNature->setText(_ui->cbNature->currentText());
        _ui->tEditNature->setText(essense);
        _ui->editDay->setText(day);
        _ui->editRating->setText(default_rating);
        _ui->editCost->setText(QString::number(cost, 'f', 2));
        _ui->cbTypeNature->setCurrentIndex(typeIndex);
        hideBtnNature();
    });

    connect(_ui->btnUpdateWorker, &QPushButton::clicked, [this]() {
        flagUpdateWorker = true;
        _ui->editWorker->setText(_ui->cbWorker->currentText());
        hideBtnWorker();
    });

    connect(_ui->btnUpdateDepartment, &QPushButton::clicked, [this]() {
        flagUpdateDepartment = true;
        _ui->editDepartment->setText(_ui->cbDepartment->currentText());
        hideBtnDepartments();
    });

    // cancel
    connect(_ui->btnCancelCompany, &QPushButton::clicked, [this]() {
        flagUpdateCompany = false;
        _ui->editCompany->clear();
        hideBtnCompany();
    });

    connect(_ui->btnCancelNature, &QPushButton::clicked, [this]() {
        flagUpdateNature = false;
        _ui->tEditNature->clear();
        _ui->editDay->clear();
        _ui->editRating->clear();
        _ui->editCost->clear();
        _ui->editNature->clear();
        hideBtnNature();
    });

    connect(_ui->btnCancelWorker, &QPushButton::clicked, [this]() {
        flagUpdateWorker = false;
        _ui->editWorker->clear();
        hideBtnWorker();
    });

    connect(_ui->btnCancelDepartment, &QPushButton::clicked, [this]() {
        flagUpdateDepartment = false;
        _ui->editDepartment->clear();
        hideBtnDepartments();
    });

    // save
    connect(_ui->btnSaveCompany, &QPushButton::clicked, [this]() {
        if (!flagUpdateCompany)
        {
            emit signalSaveAdd(_ui->editCompany->text(), "company");
        }
        else
        {
            auto id = _ui->cbCompany->model()->index(_ui->cbCompany->currentIndex(), 1).data().toInt();
            emit signalSaveUpdate(_ui->editCompany->text(), "company", id);
        }

        flagUpdateCompany = false;
        _ui->editCompany->clear();
        hideBtnCompany();
    });

    // TODO need update signals
    connect(_ui->btnSaveNature, &QPushButton::clicked, [this]() {
        if (!flagUpdateNature)
        {
            emit signalSaveNatureAdd(_ui->editNature->text(),
                                     _ui->editDay->text().toInt(),
                                     _ui->tEditNature->toPlainText(),
                                     "complaints_nature" ,
                                     _ui->editCost->text(),
                                     _ui->editRating->text().toInt(),
                                     _ui->cbTypeNature->currentIndex());
        }
        else
        {
            auto id = _ui->cbNature->model()->index(_ui->cbNature->currentIndex(), 3).data().toInt();
            emit signalSaveNatureUpdate(_ui->editNature->text(), _ui->editDay->text().toInt(),
                                        _ui->tEditNature->toPlainText(),
                                        "complaints_nature",
                                        _ui->editCost->text(),
                                        id,
                                        _ui->editRating->text().toInt(),
                                        _ui->cbTypeNature->currentIndex());
        }

        flagUpdateNature = false;
        _ui->tEditNature->clear();
        _ui->editDay->clear();
        _ui->editNature->clear();
        hideBtnNature();
    });

    connect(_ui->btnSaveWorker, &QPushButton::clicked, [this]() {
        if (!flagUpdateWorker)
        {
            emit signalSaveAdd(_ui->editWorker->text(), "complaints_target");
        }
        else
        {
             auto id = _ui->cbWorker->model()->index(_ui->cbWorker->currentIndex(), 1).data().toInt();
             emit signalSaveUpdate(_ui->editWorker->text(), "complaints_target", id);
        }

        flagUpdateWorker = false;
        _ui->editWorker->clear();
        hideBtnWorker();
    });

    connect(_ui->btnSaveDepartment, &QPushButton::clicked, [this]() {
        if (!flagUpdateDepartment)
        {
            emit signalSaveAdd(_ui->editDepartment->text(), "departments");
        }
        else
        {
            auto id = _ui->cbDepartment->model()->index(_ui->cbDepartment->currentIndex(), 1).data().toInt();
            emit signalSaveUpdate(_ui->editDepartment->text(), "departments", id);
        }

        flagUpdateDepartment = false;
        _ui->editDepartment->clear();
        hideBtnDepartments();
    });

    // delete
    connect(_ui->btnDelCompany, &QPushButton::clicked, [this]() {
        if (_ui->cbCompany->currentIndex() >= 0)
        {
            auto id = _ui->cbCompany->model()->index(_ui->cbCompany->currentIndex(), 1).data().toInt();
            emit signalDelete(id, "company");
        }
    });

    connect(_ui->btnDelNature, &QPushButton::clicked, [this]() {
        if (_ui->cbNature->currentIndex() >= 0)
        {
            auto id = _ui->cbNature->model()->index(_ui->cbNature->currentIndex(), 3).data().toInt();
            emit signalDelete(id, "complaints_nature");
        }
    });

    connect(_ui->btnDelWorker, &QPushButton::clicked, [this]() {
        if (_ui->cbWorker->currentIndex() >= 0)
        {
            auto id = _ui->cbWorker->model()->index(_ui->cbWorker->currentIndex(), 1).data().toInt();
            emit signalDelete(id, "complaints_target");
        }
    });

    connect(_ui->btnDelDepartment, &QPushButton::clicked, [this]() {
        if (_ui->cbDepartment->currentIndex() >= 0)
        {
            auto id = _ui->cbDepartment->model()->index(_ui->cbDepartment->currentIndex(), 1).data().toInt();
            emit signalDelete(id, "departments");
        }
    });
}

void AccessRightsWidget::setPermission()
{
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();

    bool flagAdminComplaints = false;
    bool flagBtnCreateUser   = false;
    bool flagBtnRemoveUser   = false;
    bool flagPermissions     = false;

    for (auto listCommands : mapPermissions)
    {
        if (listCommands.contains(permissions::UserPermission::admin_complaints))
            flagAdminComplaints = true;

        if (listCommands.contains(permissions::UserPermission::createUser))
            flagBtnCreateUser = true;

        if (listCommands.contains(permissions::UserPermission::delete_user))
            flagBtnRemoveUser = true;

        if (listCommands.contains(permissions::UserPermission::save_permissions))
            flagPermissions = true;

        if (listCommands.contains(permissions::UserPermission::updateUser))
            _flagUpdateUser = true;
    }

    if (!flagAdminComplaints)
        _ui->tabAccesRights->removeTab(_ui->tabAccesRights->indexOf(_ui->tabMaskGeneration));

    if (!flagBtnCreateUser)
        _ui->btnCreateUser->setHidden(true);

    if (!flagBtnRemoveUser)
        _ui->btnDeleteUser->setHidden(true);

    if (!flagPermissions)
        _ui->tabAccesRights->removeTab(_ui->tabAccesRights->indexOf(_ui->tabPermissions));
}
