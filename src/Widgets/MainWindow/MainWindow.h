﻿#pragma once

// TODO: убрать контролеры из виджетов http://134.17.26.128:8080/browse/OKK-133
#include "Controllers/MainWindow/MainWindowController.h"


namespace Ui
{
    class MainWindow;
}

namespace qat_client
{

    class CheckedModel;

    class MainWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        explicit MainWindow();
        ~MainWindow();

        void setTabComplaint();
        void setTabReview();

        DriversWidget * getDriversWidget();
        OperatorsWidget * getOperatorsWidget();
        LogistsWidget * getLogistsWidget();
        ClientsWidget * getClientsWidget();
        ComplaintsWidget * getComplaintsWidget();
        ReviewsWidget * getReviewsWidget();
        AccessRightsWidget * getAccessRightsWidget();

        void setCompany(QAbstractItemModel *model);
        void setStatisticTarget(QAbstractItemModel *model);
        void setModelDepartmentsStatistic(QAbstractItemModel *model);
        void setModelSourceStatistic(QAbstractItemModel *model);
        void setModelInfoStatistic(QAbstractItemModel *model);

    public slots:
        void showMessageStatusBar(const QString &msg, quint32 sec = 0);
//        void setValueStatusBar(quint64 complaint, quint64 review);
        void setValueStatusBar(const QMap<int, int> &map, bool flagItem); // TODO true - complaints, false - reviews

    private slots:
        void btnClickedExcel();
        void btnCreateStatistic();
        void btnClickedExcelReviews();
        void btnCreateStatisticReviews();

        // TODO tray icon
        void slotIconActivated(QSystemTrayIcon::ActivationReason reason);

    signals:
        // NOTE: notify UI need to undate user accounts list
        void signalRefreshUsers();
        // NOTE: UI request to create new user
        void signalCreateUser();

        void signalTabChanged(MainWindowController::Settings settings);
        void signalTabChangedGlobal(MainWindowController::Settings settings);
        void signalClickedStatistic(const QDateTime dateStart, const QDateTime dateEnd, QVariantList idsComany, QVariantList idsTarget);
        void signalClickedStatisticReviews(const QDateTime dateStart, const QDateTime dateEnd, QVariantList ids);
        void signalClickedExcel(const QString & nameFile);
        void signalClickedExcelReviews(const QString & nameFile);

    private:
        void setSettings();
        void setPermissionsWidget();
        void getCreateTabWidgets();
        void clearScrollAreaWidget();

        // TODO tray icon
        void createTrayIcon();

        // TODO create statusBar
        void createStatusBar();

    private:
        Ui::MainWindow *_ui;
        qat_client::MainWindowController::Settings _settings;
        QVBoxLayout _layout;
        QList <QWidget*> _listWidgets;

        QSystemTrayIcon *_trayIcon; // TODO tray icon
        QMenu *_trayIconMenu;
        QAction *_maximizeAction;
        QAction *_quitAction;

//        QLabel *_labelReview;
//        QLabel *_labelComplaint;

        bool _flagItemStatusBar = false;
        QList <QLabel*> _listLabelsStatusBar;
        QMap<int, QLabel*> _mapLabelStatusBar;
        QMap<int, QString> _mapNameLabelStatusBar;

        bool _isTray = false;

        // QWidget interface
    protected:
        void closeEvent(QCloseEvent *event) override;
    };

}
