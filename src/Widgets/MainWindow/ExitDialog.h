#pragma once

namespace Ui {
    class ExitDialog;
}

class ExitDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ExitDialog(QWidget *parent = nullptr);
    ~ExitDialog();

private slots:

private:
    Ui::ExitDialog *_ui;
};

