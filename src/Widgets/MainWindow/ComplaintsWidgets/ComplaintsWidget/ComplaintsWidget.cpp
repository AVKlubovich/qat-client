﻿#include "Common.h"

#include "ComplaintsWidget.h"
#include "ui_ComplaintsWidget.h"

#include "Models/CheckedModel.h"
#include "Models/CheckedStandardModel.h"

#include "permissions/UserPermission.h"


using namespace qat_client;

ComplaintsWidget::ComplaintsWidget(QWidget * parent)
    : QWidget(parent)
    , _ui(new Ui::ComplaintsWidget)
    , _companiesModel(nullptr)
{
    _ui->setupUi(this);

    _ui->btnCreate->setIconSize(_ui->btnCreate->size());
    _ui->btnCreate->setIcon(QIcon(":/images/btnAddComplaint"));
    _ui->btnReset->setHidden(true);

    connect(_ui->btnCreate, &QPushButton::clicked,
            this, &ComplaintsWidget::signalCreateComplaint);

    connect(_ui->complaintsTable, &ComplaintsTable::complaintSelected,
            this, [this](const quint64 id) {
        emit complaintSelected(id);
    });

    auto chechStateF = [this](int state)
    {
        if (state == Qt::Unchecked ||
            state == Qt::PartiallyChecked)
            _ui->dateEnd->setEnabled(true);
        else
            _ui->dateEnd->setEnabled(false);
    };
    connect(_ui->chbIsSetDateEnd, &QCheckBox::stateChanged, this, chechStateF);

    connect(_ui->complaintsTable, &QTableView::customContextMenuRequested,
            this, &ComplaintsWidget::signalHideWidget);

    connect(_ui->btnCheckedAll, &QPushButton::clicked,
            this, &ComplaintsWidget::signalCheckedAll);

    connect(_ui->btnUncheckedAll, &QPushButton::clicked,
            this, &ComplaintsWidget::signalUncheckedAll);

    connect(_ui->btnReset, &QPushButton::clicked,
            this, &ComplaintsWidget::signalResetModel);

    connect(_ui->splitter, &QSplitter::splitterMoved,
            this, &ComplaintsWidget::test);

    setPermissionsWidget();
}

ComplaintsWidget::~ComplaintsWidget()
{
    delete _ui;
}

void ComplaintsWidget::refreshClicked()
{
    setSettings();
    emit signalSelectComplaints(_settings);
}

void ComplaintsWidget::setSettings(ComplaintsWidgetController::SettingsComplaints settings, bool first)
{
    _settings = settings;

//    if (_companiesModel != nullptr)
//        _companiesModel->setChecked(settings._companies);

    _ui->editFind->setText(settings._like);

    _ui->dateStart->setDateTime(_settings._dateStart);
    if (_settings._isSetDateEnd)
    {
        _ui->dateEnd->setEnabled(true);
        _ui->dateEnd->setDateTime(_settings._dateEnd);
        _ui->chbIsSetDateEnd->setChecked(false);
    }
    else
    {
        _ui->dateEnd->setEnabled(false);
        _ui->dateEnd->setDateTime(QDateTime::currentDateTime());
        _ui->chbIsSetDateEnd->setChecked(true);
    }

    checkedCompanies(QModelIndex());

    if(!first)
        return;

    connect(_ui->lvCompanies, &QListView::clicked,
            this, &ComplaintsWidget::checkedCompanies);

    connect(qobject_cast<qat_client::CheckedModel *>(_ui->cbNature->model()), &CheckedModel::selectionChanged,
        this, &ComplaintsWidget::refreshClicked);

    connect(qobject_cast<qat_client::CheckedModel *>(_ui->cbStatus->model()), &CheckedModel::selectionChanged,
            this, &ComplaintsWidget::refreshClicked);

    connect(qobject_cast<qat_client::CheckedModel *>(_ui->cbTarget->model()), &CheckedModel::selectionChanged,
            this, &ComplaintsWidget::refreshClicked);

    connect(qobject_cast<qat_client::CheckedModel *>(_ui->cbSources->model()), &CheckedModel::selectionChanged,
            this, &ComplaintsWidget::refreshClicked);

    connect(qobject_cast<qat_client::CheckedStandardModel *>(_ui->cbFeedBack->model()), &CheckedStandardModel::selectionChanged,
            this, &ComplaintsWidget::refreshClicked);

    connect(_ui->editFind, &QLineEdit::textChanged,
            this, &ComplaintsWidget::refreshClicked);

    connect(_ui->chbIsSetDateEnd, &QCheckBox::clicked,
            this, &ComplaintsWidget::refreshClicked);

    connect(_ui->dateStart, &QDateEdit::dateChanged,
            this, &ComplaintsWidget::refreshClicked);

    connect(_ui->dateEnd, &QDateEdit::dateChanged,
            this, &ComplaintsWidget::refreshClicked);
}

void ComplaintsWidget::setComplaintsModel(QAbstractItemModel* model)
{
    QSortFilterProxyModel *proxy = new QSortFilterProxyModel(this);
    proxy->setSourceModel(model);

    _ui->complaintsTable->setModel(proxy);
}

void ComplaintsWidget::setStatusesModel(CheckedModel * statusesModel)
{
    _ui->cbStatus->setModel(statusesModel);
}

void ComplaintsWidget::setTargetsModel(CheckedModel * targetsModel)
{
    _ui->cbTarget->setModel(targetsModel);
}

void ComplaintsWidget::setNatureModel(CheckedModel * natureModel)
{
    _ui->cbNature->setModel(natureModel);
}

void ComplaintsWidget::setSourcesModel(CheckedModel *sourcesModel)
{
    _ui->cbSources->setModel(sourcesModel);
}

void ComplaintsWidget::setFeedBackModel(CheckedStandardModel *feedBackModel)
{
    _ui->cbFeedBack->setModel(feedBackModel);
}

void ComplaintsWidget::setCompaniesModel(CheckedModel * companiesModel)
{
    _ui->lvCompanies->setModel(companiesModel);
    _companiesModel = companiesModel;
}

void ComplaintsWidget::updateComplaints()
{
    _ui->cbTarget->updateTextHints();
    _ui->cbStatus->updateTextHints();
    _ui->cbNature->updateTextHints();
    _ui->cbSources->updateTextHints();
    _ui->cbFeedBack->updateTextHints();

    _ui->complaintsTable->viewport()->update();
    _ui->complaintsTable->update(QModelIndex());

    enum
    {
        id_target = 11,
        feedback,
        days_to_process,
        status,
    };

    _ui->complaintsTable->setColumnHidden(id_target, true);
    _ui->complaintsTable->setColumnHidden(feedback, true);
    _ui->complaintsTable->setColumnHidden(days_to_process, true);
    _ui->complaintsTable->setColumnHidden(status, true);

    resizeTable();
}

void ComplaintsWidget::updateNature()
{
    _ui->cbNature->update();
}

void ComplaintsWidget::updateSources()
{
    _ui->cbSources->update();
}

void ComplaintsWidget::setComplaintInfoWidget(QWidget* widget)
{
    QSplitter * spliter = new QSplitter(Qt::Vertical);
    spliter->addWidget(_ui->complaintsTable);
    spliter->addWidget(widget);

    _ui->gridComplaints->layout()->addWidget(spliter);
    _ui->gridComplaints->setStretch(1, 1); // растяжение только элемента с индексом 1
}

QCheckBox *ComplaintsWidget::getChbIsSetDateEnd()
{
    return _ui->chbIsSetDateEnd;
}

void ComplaintsWidget::selectNextComplaint()
{
    QModelIndex index = _ui->complaintsTable->currentIndex();
    QModelIndex newIndex = _ui->complaintsTable->model()->index(index.row() + 1, index.column(), index.parent());
    _ui->complaintsTable->setCurrentIndex(newIndex);
}

void ComplaintsWidget::updateCompanies()
{
    _ui->lvCompanies->reset();
    checkedCompanies(QModelIndex());
}

void ComplaintsWidget::setResetBtn(bool flag)
{
    _ui->btnReset->setHidden(flag);
}

void ComplaintsWidget::test(int pos, int index)
{
    if (!_splitterPos)
        _splitterPos = pos;

    if (_splitterPos != pos)
        _flagResizeTable = true;
    else
        _flagResizeTable = false;

    resizeTable();
}

void ComplaintsWidget::setSettings()
{
    _settings.clear();
    if (_companiesModel != nullptr)
        _settings._companies = _companiesModel->checkedRealIndex().toList();

    _settings._statuses = qobject_cast<CheckedModel *>(_ui->cbStatus->model())->checkedRealIndex().toList();
    _settings._targets = qobject_cast<CheckedModel *>(_ui->cbTarget->model())->checkedRealIndex().toList();
    _settings._nature = qobject_cast<CheckedModel *>(_ui->cbNature->model())->checkedRealIndex().toList();
    _settings._sources = qobject_cast<CheckedModel *>(_ui->cbSources->model())->checkedRealIndex().toList();
    _settings._dateStart = QDateTime(_ui->dateStart->dateTime());

    if (!_ui->chbIsSetDateEnd->isChecked())
    {
        _settings._isSetDateEnd = true;
        _settings._dateEnd = QDateTime(_ui->dateEnd->dateTime());
    }
    else
    {
        _settings._isSetDateEnd = false;
    }

    _settings._like = _ui->editFind->text();

    QStringList feedBack;
    auto list = qobject_cast<CheckedStandardModel*>(_ui->cbFeedBack->model())->getCheckedList();
    for (auto it : list)
        feedBack.append(QString::number(it));
    _settings._feedback = feedBack;
}

void ComplaintsWidget::checkedCompanies(const QModelIndex &index)
{
    if (_companiesModel == nullptr)
        return;

    if (_companiesModel->checkedCount() == 1)
    {
        _ui->cbNature->setEnabled(true);
        emit signalFillNature();
    }
    else
    {
        _ui->cbNature->clearSelectIndex();
        _ui->cbNature->clear();
        _ui->cbNature->setEnabled(false);
    }

    emit signalUpdateSourceModel();
    refreshClicked();
}

void ComplaintsWidget::setPermissionsWidget()
{
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();
}

void ComplaintsWidget::resizeTable()
{
    if (_flagResizeTableOld == _flagResizeTable)
        return;

    _flagResizeTableOld = _flagResizeTable;
    if (!_flagResizeTable)
    {
        _ui->complaintsTable->horizontalHeader()->resizeSection(0,  65);  // №
        _ui->complaintsTable->horizontalHeader()->resizeSection(1,  50);  // Статус 150
        _ui->complaintsTable->horizontalHeader()->resizeSection(2,  130); // Дата создания
        _ui->complaintsTable->horizontalHeader()->resizeSection(3,  200); // Компания
        _ui->complaintsTable->horizontalHeader()->resizeSection(4,  130); // Откуда
        _ui->complaintsTable->horizontalHeader()->resizeSection(5,  220); // Отдел
        _ui->complaintsTable->horizontalHeader()->resizeSection(6,  100); // Тип 150
        _ui->complaintsTable->horizontalHeader()->resizeSection(7,  50);  // № заказа 100
        _ui->complaintsTable->horizontalHeader()->resizeSection(8,  180); // Характер
        _ui->complaintsTable->horizontalHeader()->resizeSection(9,  100); // На кого 120
        _ui->complaintsTable->horizontalHeader()->resizeSection(10, 220); // ФИО 250
    }
    else
    {
        _ui->complaintsTable->horizontalHeader()->resizeSection(0,  65);  // №
        _ui->complaintsTable->horizontalHeader()->resizeSection(1,  250); // Статус 150
        _ui->complaintsTable->horizontalHeader()->resizeSection(2,  130); // Дата создания
        _ui->complaintsTable->horizontalHeader()->resizeSection(3,  200); // Компания
        _ui->complaintsTable->horizontalHeader()->resizeSection(4,  130); // Откуда
        _ui->complaintsTable->horizontalHeader()->resizeSection(5,  220); // Отдел
        _ui->complaintsTable->horizontalHeader()->resizeSection(6,  150); // Тип 150
        _ui->complaintsTable->horizontalHeader()->resizeSection(7,  100); // № заказа 100
        _ui->complaintsTable->horizontalHeader()->resizeSection(8,  180); // Характер
        _ui->complaintsTable->horizontalHeader()->resizeSection(9,  120); // На кого 120
        _ui->complaintsTable->horizontalHeader()->resizeSection(10, 250); // ФИО 250
    }

    _ui->complaintsTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
}

void ComplaintsWidget::on_btnExcel_clicked()
{
    const QString& fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                          QString("complaints %1.xlsx")
                                                          .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss")),
                                                          tr("Excel (*.xlsx)"));

    if (!fileName.isEmpty())
        emit signalCreateComplaintsExcelFile(fileName);
}
