﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/MainWindow/ComplaintsControllers/ComplaintsWidgetController.h"


namespace Ui
{
    class ComplaintsWidget;
}

namespace qat_client
{

    class CheckedModel;
    class CheckedStandardModel;


    class ComplaintsWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit ComplaintsWidget(QWidget * parent = nullptr);
        ~ComplaintsWidget();

        void setComplaintsModel(QAbstractItemModel* model);
        void setStatusesModel(CheckedModel * statusesModel);
        void setTargetsModel(CheckedModel * targetsModel);
        void setCompaniesModel(CheckedModel * companiesModel);
        void setSettings(ComplaintsWidgetController::SettingsComplaints settings, bool first = false);
        void setNatureModel(CheckedModel * natureModel);
        void setSourcesModel(CheckedModel * sourcesModel);
        void setFeedBackModel(CheckedStandardModel * feedBackModel);

        void updateComplaints();
        void updateNature();
        void updateSources();

        void setComplaintInfoWidget(QWidget* widget);
        QCheckBox * getChbIsSetDateEnd();

        void selectNextComplaint();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);
        void signalSelectComplaints(const ComplaintsWidgetController::SettingsComplaints & settings);
        void signalCreateComplaint();
        void signalFillNature();
        void signalCreateComplaintsExcelFile(const QString & fileName);
        void signalHideWidget();

        void signalCheckedAll();
        void signalUncheckedAll();
        void signalResetModel();

        void signalUpdateSourceModel();

    public slots:
        void updateCompanies();
        void setResetBtn(bool flag);

        void test(int pos, int index);

    private slots:
        void checkedCompanies(const QModelIndex &index);
        void on_btnExcel_clicked();
        void refreshClicked();

    private:
        void setSettings();
        void setPermissionsWidget();

        void resizeTable();

    private:
        Ui::ComplaintsWidget *_ui;
        ComplaintsWidgetController::SettingsComplaints _settings;
        CheckedModel * _companiesModel;

        int _splitterPos = 0;
        bool _flagResizeTable = false;
        bool _flagResizeTableOld = false;
    };

}
