﻿#include "Common.h"
#include "ComplaintsTable.h"


using namespace qat_client;

ComplaintsTable::ComplaintsTable(QWidget * parent)
    : QTableView(parent)
{
}

ComplaintsTable::~ComplaintsTable()
{
}

void ComplaintsTable::currentChanged(const QModelIndex & current, const QModelIndex & previous)
{
    QTableView::currentChanged(current, previous);
    const auto& idIndex = model()->index(current.row(), 0, QModelIndex());
    emit complaintSelected(idIndex.data(Qt::DisplayRole).toULongLong());
}
