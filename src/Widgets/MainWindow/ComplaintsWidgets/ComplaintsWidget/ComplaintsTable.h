﻿#pragma once


namespace qat_client
{

    class ComplaintsTable : public QTableView
    {
        Q_OBJECT

    public:
        explicit ComplaintsTable(QWidget * parent = nullptr);
        ~ComplaintsTable();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);

    protected slots:
        void currentChanged(const QModelIndex & current, const QModelIndex & previous);
    };

}
