﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/MainWindow/ComplaintsControllers/OperatorsWidgetController.h"


namespace Ui
{
    class OperatorsWidget;
}

namespace qat_client
{

    class CheckedModel;

    class OperatorsWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit OperatorsWidget(QWidget * parent = nullptr);
        ~OperatorsWidget();

        void setOperatorsModel(QAbstractItemModel* model);
        void setCompaniesModel(CheckedModel * companiesModel);
        void setNatureModel(CheckedModel * natureModel);
        void setSettings(OperatorsWidgetController::SettingsOperators settings);
        void setComplaintInfoWidget(QWidget* widget);
        void checkedCompanies(const QModelIndex &index);
        void updateOperators();
        void updateNature();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);
        void operatorSelected(const quint64 id);
        void signalSelectOperators(const OperatorsWidgetController::SettingsOperators & settings);
        void signalFillNature();
        void signalHideWidget();
        void signalCreateComplaintsExcelFile(const QString & fileName);

        void signalCheckedAll();
        void signalUncheckedAll();

    public slots:
        void updateCompanies();

    private slots:
        void refreshClicked();
        void on_btnExcel_clicked();

    private:
        void setSettings();
        void setPermissionsWidget();

    private:
        Ui::OperatorsWidget *_ui;
        OperatorsWidgetController::SettingsOperators _settings;
        CheckedModel * _companiesModel;
    };

}
