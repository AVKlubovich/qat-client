﻿#include "Common.h"

#include "OperatorsWidget.h"
#include "ui_OperatorsWidget.h"

#include "Models/CheckedModel.h"

#include "permissions/UserPermission.h"


using namespace qat_client;

OperatorsWidget::OperatorsWidget(QWidget * parent)
    : QWidget(parent)
    , _ui(new Ui::OperatorsWidget)
    , _companiesModel(nullptr)
{
    _ui->setupUi(this);

    connect(_ui->operatorsTable, &OperatorsTable::complaintSelected,
            this, [this](const quint64 id) {
                    emit complaintSelected(id);
                  });

    auto chechStateF = [this](int state)
    {
        if (state == Qt::Unchecked ||
            state == Qt::PartiallyChecked)
            _ui->dateEnd->setEnabled(true);
        else
            _ui->dateEnd->setEnabled(false);

    };
    connect(_ui->chbIsSetDateEnd, &QCheckBox::stateChanged, this, chechStateF);

    connect(_ui->operatorsTable, &QTableView::customContextMenuRequested,
            this, &OperatorsWidget::signalHideWidget);

    connect(_ui->btnCheckedAll, &QPushButton::clicked,
            this, &OperatorsWidget::signalCheckedAll);

    connect(_ui->btnUncheckedAll, &QPushButton::clicked,
            this, &OperatorsWidget::signalUncheckedAll);

    setPermissionsWidget();
}

OperatorsWidget::~OperatorsWidget()
{
    delete _ui;
}

void OperatorsWidget::setOperatorsModel(QAbstractItemModel* model)
{
    _ui->operatorsTable->setModel(model);
}

void OperatorsWidget::setCompaniesModel(CheckedModel * companiesModel)
{
    _ui->lvCompanies->setModel(companiesModel);
    _companiesModel = companiesModel;
}

void OperatorsWidget::setNatureModel(CheckedModel * natureModel)
{
    _ui->cbNature->setModel(natureModel);
}

void OperatorsWidget::refreshClicked()
{
    setSettings();
    emit signalSelectOperators(_settings);
}

void OperatorsWidget::updateOperators()
{
    _ui->operatorsTable->viewport()->update();
    _ui->operatorsTable->update(QModelIndex());

    _ui->operatorsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    _ui->operatorsTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
}

void OperatorsWidget::updateNature()
{
    _ui->cbNature->update();
}

void OperatorsWidget::updateCompanies()
{
    _ui->lvCompanies->reset();
    checkedCompanies(QModelIndex());
}

void OperatorsWidget::setComplaintInfoWidget(QWidget* widget)
{
    QSplitter * spliter = new QSplitter(Qt::Vertical);
    spliter->addWidget(_ui->operatorsTable);
    spliter->addWidget(widget);
    _ui->gridOperators->layout()->addWidget(spliter);
    _ui->gridOperators->setStretch(1, 1); // растяжение только элемента с индексом 1
}

void OperatorsWidget::checkedCompanies(const QModelIndex &index)
{
    if (_companiesModel == nullptr)
        return;

    if (_companiesModel->checkedCount() == 1)
    {
        _ui->cbNature->setEnabled(true);
        emit signalFillNature();
    }
    else
    {
        _ui->cbNature->clear();
        _ui->cbNature->setEnabled(false);
    }

    refreshClicked();
}

void OperatorsWidget::setSettings(OperatorsWidgetController::SettingsOperators settings)
{
    _settings = settings;

    if (_companiesModel != nullptr)
        _companiesModel->setChecked(settings._companies);

    _ui->editFind->setText(settings._like);

    _ui->dateStart->setDateTime(_settings._dateStart);
    if (_settings._isSetDateEnd)
    {
        _ui->dateEnd->setEnabled(true);
        _ui->dateEnd->setDateTime(_settings._dateEnd);
        _ui->chbIsSetDateEnd->setChecked(false);
    }
    else
    {
        _ui->dateEnd->setEnabled(false);
        _ui->dateEnd->setDateTime(QDateTime::currentDateTime());
        _ui->chbIsSetDateEnd->setChecked(true);
    }

    checkedCompanies(QModelIndex());

    connect(_ui->lvCompanies, &QListView::clicked,
            this, &OperatorsWidget::checkedCompanies);

    connect(qobject_cast<qat_client::CheckedModel *>(_ui->cbNature->model()), &CheckedModel::selectionChanged,
        this, &OperatorsWidget::refreshClicked);

    connect(_ui->editFind, &QLineEdit::textChanged,
            this, &OperatorsWidget::refreshClicked);

    connect(_ui->chbIsSetDateEnd, &QCheckBox::clicked,
            this, &OperatorsWidget::refreshClicked);

    connect(_ui->dateStart, &QDateEdit::dateChanged,
            this, &OperatorsWidget::refreshClicked);

    connect(_ui->dateEnd, &QDateEdit::dateChanged,
            this, &OperatorsWidget::refreshClicked);
}

void OperatorsWidget::setSettings()
{
    _settings.clear();
    if (_companiesModel != nullptr)
        _settings._companies = _companiesModel->checkedRealIndex().toList();
    _settings._nature = qobject_cast<qat_client::CheckedModel *>(_ui->cbNature->model())->checkedRealIndex().toList();
    _settings._dateStart = QDateTime(_ui->dateStart->dateTime());
    if (!_ui->chbIsSetDateEnd->isChecked())
    {
        _settings._isSetDateEnd = true;
        _settings._dateEnd = QDateTime(_ui->dateEnd->dateTime());
    }
    else
    {
        _settings._isSetDateEnd = false;
    }

    _settings._like = _ui->editFind->text();
}

void OperatorsWidget::setPermissionsWidget()
{
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();
}

void OperatorsWidget::on_btnExcel_clicked()
{
    const QString& fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               "Operators complaints " + QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss") + ".xlsx",
                               tr("Excel (*.xlsx)"));

    if (!fileName.isEmpty())
        emit signalCreateComplaintsExcelFile(fileName);
}
