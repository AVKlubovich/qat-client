﻿#pragma once


namespace qat_client
{

    class OperatorsTable : public QTableView
    {
        Q_OBJECT

    public:
        explicit OperatorsTable(QWidget * parent = nullptr);
        ~OperatorsTable();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);

    protected slots:
        void currentChanged(const QModelIndex & current, const QModelIndex & previous);
    };

}
