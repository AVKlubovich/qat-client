﻿#include "Common.h"
#include "OperatorsTable.h"


using namespace qat_client;

OperatorsTable::OperatorsTable(QWidget * parent)
    : QTableView(parent)
{
}

OperatorsTable::~OperatorsTable()
{
}

void OperatorsTable::currentChanged(const QModelIndex & current, const QModelIndex & previous)
{
    QTableView::currentChanged(current, previous);
    const auto& idIndex = model()->index(current.row(), 0, QModelIndex());
    emit complaintSelected(idIndex.data(Qt::DisplayRole).toULongLong());
}
