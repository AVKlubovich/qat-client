﻿#include "Common.h"
#include "DriversTable.h"


using namespace qat_client;

DriversTable::DriversTable(QWidget * parent)
    : QTableView(parent)
{
}

DriversTable::~DriversTable()
{
}

void DriversTable::currentChanged(const QModelIndex & current, const QModelIndex & previous)
{
    QTableView::currentChanged(current, previous);
    const auto& idIndex = model()->index(current.row(), 0, QModelIndex());
    emit complaintSelected(idIndex.data(Qt::DisplayRole).toULongLong());
}
