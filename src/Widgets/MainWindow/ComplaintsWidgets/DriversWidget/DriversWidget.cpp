﻿#include "Common.h"

#include "DriversWidget.h"
#include "ui_DriversWidget.h"

#include "Models/CheckedModel.h"

#include "permissions/UserPermission.h"


using namespace qat_client;

DriversWidget::DriversWidget(QWidget * parent)
    : QWidget(parent)
    , _ui(new Ui::DriversWidget)
    , _companiesModel(nullptr)
{
    _ui->setupUi(this);

    connect(_ui->driversTable, &DriversTable::complaintSelected,
            this, [this](const quint64 id) {
                    emit complaintSelected(id);
                  });

    auto chechStateF = [this](int state)
    {
        if (state == Qt::Unchecked ||
            state == Qt::PartiallyChecked)
            _ui->dateEnd->setEnabled(true);
        else
            _ui->dateEnd->setEnabled(false);

    };
    connect(_ui->chbIsSetDateEnd, &QCheckBox::stateChanged, this, chechStateF);

    connect(_ui->driversTable, &QTableView::customContextMenuRequested,
            this, &DriversWidget::signalHideWidget);

    connect(_ui->btnCheckedAll, &QPushButton::clicked,
            this, &DriversWidget::signalCheckedAll);

    connect(_ui->btnUncheckedAll, &QPushButton::clicked,
            this, &DriversWidget::signalUncheckedAll);

    setPermissionsWidget();
}

DriversWidget::~DriversWidget()
{
    delete _ui;
}

void DriversWidget::setDriversModel(QAbstractItemModel* model)
{
    _ui->driversTable->setModel(model);
}

void DriversWidget::setCompaniesModel(CheckedModel * companiesModel)
{
    _ui->lvCompanies->setModel(companiesModel);
    _companiesModel = companiesModel;
}

void DriversWidget::setNatureModel(CheckedModel * natureModel)
{
    _ui->cbNature->setModel(natureModel);
}

void DriversWidget::refreshClicked()
{
    setSettings();
    emit signalSelectDrivers(_settings);
}

void DriversWidget::updateDrivers()
{
    _ui->driversTable->viewport()->update();
    _ui->driversTable->update(QModelIndex());

    _ui->driversTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    _ui->driversTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
}

void DriversWidget::updateNature()
{
    _ui->cbNature->update();
}

void DriversWidget::updateCompanies()
{
    _ui->lvCompanies->reset();
    checkedCompanies(QModelIndex());
}

void DriversWidget::setComplaintInfoWidget(QWidget* widget)
{
    QSplitter * spliter = new QSplitter(Qt::Vertical);
    spliter->addWidget(_ui->driversTable);
    spliter->addWidget(widget);
    _ui->gridDrivers->layout()->addWidget(spliter);
    _ui->gridDrivers->setStretch(1, 1); // растяжение только элемента с индексом 1
}

void DriversWidget::checkedCompanies(const QModelIndex &index)
{
    if (_companiesModel == nullptr)
        return;

    if (_companiesModel->checkedCount() == 1)
    {
        _ui->cbNature->setEnabled(true);
        emit signalFillNature();
    }
    else
    {
        _ui->cbNature->clear();
        _ui->cbNature->setEnabled(false);
    }

    refreshClicked();
}

void DriversWidget::setSettings(DriversWidgetController::SettingsDrivers settings)
{
    _settings = settings;

    if (_companiesModel != nullptr)
        _companiesModel->setChecked(settings._companies);

    _ui->editFind->setText(settings._like);

    _ui->dateStart->setDateTime(_settings._dateStart);
    if (_settings._isSetDateEnd)
    {
        _ui->dateEnd->setEnabled(true);
        _ui->dateEnd->setDateTime(_settings._dateEnd);
        _ui->chbIsSetDateEnd->setChecked(false);
    }
    else
    {
        _ui->dateEnd->setEnabled(false);
        _ui->dateEnd->setDateTime(QDateTime::currentDateTime());
        _ui->chbIsSetDateEnd->setChecked(true);
    }

    checkedCompanies(QModelIndex());

    connect(_ui->lvCompanies, &QListView::clicked,
            this, &DriversWidget::checkedCompanies);

    connect(qobject_cast<qat_client::CheckedModel *>(_ui->cbNature->model()), &CheckedModel::selectionChanged,
        this, &DriversWidget::refreshClicked);

    connect(_ui->editFind, &QLineEdit::textChanged,
            this, &DriversWidget::refreshClicked);

    connect(_ui->chbIsSetDateEnd, &QCheckBox::clicked,
            this, &DriversWidget::refreshClicked);

    connect(_ui->dateStart, &QDateEdit::dateChanged,
            this, &DriversWidget::refreshClicked);

    connect(_ui->dateEnd, &QDateEdit::dateChanged,
            this, &DriversWidget::refreshClicked);
}

void DriversWidget::setSettings()
{
    _settings.clear();
    if (_companiesModel != nullptr)
        _settings._companies = _companiesModel->checkedRealIndex().toList();
    _settings._nature = qobject_cast<qat_client::CheckedModel *>(_ui->cbNature->model())->checkedRealIndex().toList();
    _settings._dateStart = QDateTime(_ui->dateStart->dateTime());
    if (!_ui->chbIsSetDateEnd->isChecked())
    {
        _settings._isSetDateEnd = true;
        _settings._dateEnd = QDateTime(_ui->dateEnd->dateTime());
    }
    else
    {
        _settings._isSetDateEnd = false;
    }

    _settings._like = _ui->editFind->text();
}

void DriversWidget::setPermissionsWidget()
{
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();
}

void DriversWidget::on_btnExcel_clicked()
{
    const QString& fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               "Drivers complaints " + QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss") + ".xlsx",
                               tr("Excel (*.xlsx)"));

    if (!fileName.isEmpty())
        emit signalCreateComplaintsExcelFile(fileName);
}
