﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/MainWindow/ComplaintsControllers/DriversWidgetController.h"


namespace Ui
{
    class DriversWidget;
}

namespace qat_client
{

    class CheckedModel;

    class DriversWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit DriversWidget(QWidget * parent = nullptr);
        ~DriversWidget();

        void setDriversModel(QAbstractItemModel* model);
        void setCompaniesModel(CheckedModel * companiesModel);
        void setNatureModel(CheckedModel * natureModel);
        void setSettings(DriversWidgetController::SettingsDrivers settings);
        void setComplaintInfoWidget(QWidget* widget);
        void checkedCompanies(const QModelIndex &index);
        void updateDrivers();
        void updateNature();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);
        void driverSelected(const quint64 id);
        void signalSelectDrivers(const DriversWidgetController::SettingsDrivers & settings);
        void signalFillNature();
        void signalHideWidget();
        void signalCreateComplaintsExcelFile(const QString & fileName);

        void signalCheckedAll();
        void signalUncheckedAll();

    public slots:
        void updateCompanies();

    private slots:
        void refreshClicked();
        void on_btnExcel_clicked();

    private:
        void setSettings();
        void setPermissionsWidget();

    private:
        Ui::DriversWidget *_ui;
        DriversWidgetController::SettingsDrivers _settings;
        CheckedModel * _companiesModel;
    };

}
