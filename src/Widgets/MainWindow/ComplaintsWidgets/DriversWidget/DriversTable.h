﻿#pragma once


namespace qat_client
{

    class DriversTable : public QTableView
    {
        Q_OBJECT

    public:
        explicit DriversTable(QWidget * parent = nullptr);
        ~DriversTable();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);

    protected slots:
        void currentChanged(const QModelIndex & current, const QModelIndex & previous);
    };

}
