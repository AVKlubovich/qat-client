﻿#pragma once


namespace qat_client
{

    class ClientsTable : public QTableView
    {
        Q_OBJECT

    public:
        explicit ClientsTable(QWidget * parent = nullptr);
        ~ClientsTable();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);

    protected slots:
        void currentChanged(const QModelIndex & current, const QModelIndex & previous);
    };

}
