﻿#include "Common.h"

#include "ClientsWidget.h"
#include "ui_ClientsWidget.h"

#include "Models/CheckedModel.h"

#include "permissions/UserPermission.h"


using namespace qat_client;

ClientsWidget::ClientsWidget(QWidget * parent)
    : QWidget(parent)
    , _ui(new Ui::ClientsWidget)
    , _companiesModel(nullptr)
{
    _ui->setupUi(this);

    connect(_ui->btnRefresh, &QPushButton::clicked,
            this, &ClientsWidget::refreshClicked);

    connect(_ui->clientsTable, &ClientsTable::complaintSelected,
            this, [this](const quint64 id) {
                    emit complaintSelected(id);
                  });

    auto chechStateF = [this](int state)
    {
        if (state == Qt::Unchecked ||
            state == Qt::PartiallyChecked)
        {
            _ui->dateEnd->setEnabled(true);
            _ui->timeEnd->setEnabled(true);
        }
        else
        {
            _ui->dateEnd->setEnabled(false);
            _ui->timeEnd->setEnabled(false);
        }
    };
    connect(_ui->chbIsSetDateEnd, &QCheckBox::stateChanged, this, chechStateF);

    connect(_ui->clientsTable, &QTableView::customContextMenuRequested,
            this, &ClientsWidget::signalHideWidget);

    connect(_ui->lvCompanies, &QListView::clicked, this, &ClientsWidget::checkedCompanies);

    setPermissionsWidget();
}

ClientsWidget::~ClientsWidget()
{
    delete _ui;
}

void ClientsWidget::setClientsModel(QAbstractItemModel* model)
{
    _ui->clientsTable->setModel(model);
}

void ClientsWidget::setCompaniesModel(qat_client::CheckedModel * companiesModel)
{
    _ui->lvCompanies->setModel(companiesModel);
    _companiesModel = companiesModel;
}

void ClientsWidget::setNatureModel(qat_client::CheckedModel * natureModel)
{
    _ui->cbNature->setModel(natureModel);
}

void ClientsWidget::refreshClicked()
{
    setSettings();
    emit signalSelectClients(_settings);
}

void ClientsWidget::updateClients()
{
    _ui->clientsTable->viewport()->update();
    _ui->clientsTable->update(QModelIndex());

    _ui->clientsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void ClientsWidget::updateNature()
{
    _ui->cbNature->update();
}

void ClientsWidget::setComplaintInfoWidget(QWidget* widget)
{
    _ui->gridClients->layout()->addWidget(widget);
}

void ClientsWidget::checkedCompanies(const QModelIndex &index)
{
    if (_companiesModel == nullptr)
        return;

    if (_companiesModel->checkedCount() == 1)
    {
        _ui->cbNature->setEnabled(true);
        emit signalFillNature();
    }
    else
    {
        _ui->cbNature->clear();
        _ui->cbNature->setEnabled(false);
    }
}

void ClientsWidget::setSettings(qat_client::ClientsWidgetController::SettingsClients settings)
{
    _settings = settings;

    if (_companiesModel != nullptr)
        _companiesModel->setChecked(settings._companies);

    _ui->editFind->setText(settings._like);

    _ui->dateStart->setDate(_settings._dateStart.date());
    _ui->timeStart->setTime(_settings._dateStart.time());
    if (_settings._isSetDateEnd)
    {
        _ui->dateEnd->setEnabled(true);
        _ui->timeEnd->setEnabled(true);
        _ui->dateEnd->setDate(_settings._dateEnd.date());
        _ui->timeEnd->setTime(_settings._dateEnd.time());
        _ui->chbIsSetDateEnd->setChecked(false);
    }
    else
    {
        _ui->dateEnd->setEnabled(false);
        _ui->timeEnd->setEnabled(false);
        _ui->dateEnd->setDate(QDate::currentDate());
        _ui->timeEnd->setTime(QTime::currentTime());
        _ui->chbIsSetDateEnd->setChecked(true);
    }

    checkedCompanies(QModelIndex());
}

void ClientsWidget::setSettings()
{
    _settings.clear();
    if (_companiesModel != nullptr)
        _settings._companies = _companiesModel->checkedRealIndex().toList();
    _settings._nature = qobject_cast<qat_client::CheckedModel *>(_ui->cbNature->model())->checkedRealIndex().toList();
    _settings._dateStart = QDateTime(_ui->dateStart->date(), _ui->timeStart->time());
    if (!_ui->chbIsSetDateEnd->isChecked())
    {
        _settings._isSetDateEnd = true;
        _settings._dateEnd = QDateTime(_ui->dateEnd->date(), _ui->timeEnd->time());
    }
    else
    {
        _settings._isSetDateEnd = false;
    }

    _settings._like = _ui->editFind->text();
}

void ClientsWidget::setPermissionsWidget()
{
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();
}
