﻿#include "Common.h"
#include "ClientsTable.h"


using namespace qat_client;

ClientsTable::ClientsTable(QWidget * parent)
    : QTableView(parent)
{
}

ClientsTable::~ClientsTable()
{
}

void ClientsTable::currentChanged(const QModelIndex & current, const QModelIndex & previous)
{
    QTableView::currentChanged(current, previous);
    const auto& idIndex = model()->index(current.row(), 0, QModelIndex());
    emit complaintSelected(idIndex.data(Qt::DisplayRole).toULongLong());
}
