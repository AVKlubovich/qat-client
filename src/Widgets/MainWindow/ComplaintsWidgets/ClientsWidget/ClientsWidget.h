﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/MainWindow/ComplaintsControllers/ClientsWidgetController.h"


namespace Ui
{
    class ClientsWidget;
}

namespace qat_client
{

    class CheckedModel;

    class ClientsWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit ClientsWidget(QWidget * parent = nullptr);
        ~ClientsWidget();

        void setClientsModel(QAbstractItemModel* model);
        void setCompaniesModel(CheckedModel * companiesModel);
        void setNatureModel(CheckedModel * natureModel);
        void setSettings(ClientsWidgetController::SettingsClients settings);
        void setComplaintInfoWidget(QWidget* widget);
        void checkedCompanies(const QModelIndex &index);
        void updateClients();
        void updateNature();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);
        void clientSelected(const quint64 id);
        void signalSelectClients(const ClientsWidgetController::SettingsClients & settings);
        void signalFillNature();
        void signalHideWidget();

    private slots:
        void refreshClicked();

    private:
        void setSettings();
        void setPermissionsWidget();

    private:
        Ui::ClientsWidget *_ui;
        ClientsWidgetController::SettingsClients _settings;
        CheckedModel * _companiesModel;
    };

}
