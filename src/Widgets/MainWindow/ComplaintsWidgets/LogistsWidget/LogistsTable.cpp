﻿#include "Common.h"
#include "LogistsTable.h"


using namespace qat_client;

LogistsTable::LogistsTable(QWidget * parent)
    : QTableView(parent)
{
}

LogistsTable::~LogistsTable()
{
}

void LogistsTable::currentChanged(const QModelIndex & current, const QModelIndex & previous)
{
    QTableView::currentChanged(current, previous);
    const auto& idIndex = model()->index(current.row(), 0, QModelIndex());
    emit complaintSelected(idIndex.data(Qt::DisplayRole).toULongLong());
}
