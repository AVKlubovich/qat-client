﻿#pragma once


namespace qat_client
{

    class LogistsTable : public QTableView
    {
        Q_OBJECT

    public:
        explicit LogistsTable(QWidget * parent = nullptr);
        ~LogistsTable();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);

    protected slots:
        void currentChanged(const QModelIndex & current, const QModelIndex & previous);
    };

}
