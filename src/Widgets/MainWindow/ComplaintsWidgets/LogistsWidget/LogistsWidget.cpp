﻿#include "Common.h"

#include "LogistsWidget.h"
#include "ui_LogistsWidget.h"

#include "Models/CheckedModel.h"

#include "permissions/UserPermission.h"


using namespace qat_client;

LogistsWidget::LogistsWidget(QWidget * parent)
    : QWidget(parent)
    , _ui(new Ui::LogistsWidget)
    , _companiesModel(nullptr)
{
    _ui->setupUi(this);

    connect(_ui->logistsTable, &LogistsTable::complaintSelected,
            this, [this](const quint64 id) {
                    emit complaintSelected(id);
                  });

    auto chechStateF = [this](int state)
    {
        if (state == Qt::Unchecked ||
            state == Qt::PartiallyChecked)
            _ui->dateEnd->setEnabled(true);
        else
            _ui->dateEnd->setEnabled(false);

    };
    connect(_ui->chbIsSetDateEnd, &QCheckBox::stateChanged, this, chechStateF);

    connect(_ui->logistsTable, &QTableView::customContextMenuRequested,
            this, &LogistsWidget::signalHideWidget);

    connect(_ui->btnCheckedAll, &QPushButton::clicked,
            this, &LogistsWidget::signalCheckedAll);

    connect(_ui->btnUncheckedAll, &QPushButton::clicked,
            this, &LogistsWidget::signalUncheckedAll);

    setPermissionsWidget();
}

LogistsWidget::~LogistsWidget()
{
    delete _ui;
}

void LogistsWidget::setLogistsModel(QAbstractItemModel* model)
{
    _ui->logistsTable->setModel(model);
}

void LogistsWidget::setCompaniesModel(CheckedModel * companiesModel)
{
    _ui->lvCompanies->setModel(companiesModel);
    _companiesModel = companiesModel;
}

void LogistsWidget::setNatureModel(CheckedModel * natureModel)
{
    _ui->cbNature->setModel(natureModel);
}

void LogistsWidget::refreshClicked()
{
    setSettings();
    emit signalSelectLogists(_settings);
}

void LogistsWidget::updateLogists()
{
    _ui->logistsTable->viewport()->update();
    _ui->logistsTable->update(QModelIndex());

    _ui->logistsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    _ui->logistsTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
}

void LogistsWidget::updateNature()
{
    _ui->cbNature->update();
}

void LogistsWidget::updateCompanies()
{
    _ui->lvCompanies->reset();
    checkedCompanies(QModelIndex());
}

void LogistsWidget::setComplaintInfoWidget(QWidget* widget)
{
    QSplitter * spliter = new QSplitter(Qt::Vertical);
    spliter->addWidget(_ui->logistsTable);
    spliter->addWidget(widget);
    _ui->gridLogists->layout()->addWidget(spliter);
    _ui->gridLogists->setStretch(1, 1); // растяжение только элемента с индексом 1
}

void LogistsWidget::checkedCompanies(const QModelIndex &index)
{
    if (_companiesModel == nullptr)
        return;

    if (_companiesModel->checkedCount() == 1)
    {
        _ui->cbNature->setEnabled(true);
        emit signalFillNature();
    }
    else
    {
        _ui->cbNature->clear();
        _ui->cbNature->setEnabled(false);
    }

    refreshClicked();
}

void LogistsWidget::setSettings(LogistsWidgetController::SettingsLogists settings)
{
    _settings = settings;

    if (_companiesModel != nullptr)
        _companiesModel->setChecked(settings._companies);

    _ui->editFind->setText(settings._like);

    _ui->dateStart->setDateTime(_settings._dateStart);
    if (_settings._isSetDateEnd)
    {
        _ui->dateEnd->setEnabled(true);
        _ui->dateEnd->setDateTime(_settings._dateEnd);
        _ui->chbIsSetDateEnd->setChecked(false);
    }
    else
    {
        _ui->dateEnd->setEnabled(false);
        _ui->dateEnd->setDateTime(QDateTime::currentDateTime());
        _ui->chbIsSetDateEnd->setChecked(true);
    }

    checkedCompanies(QModelIndex());

    connect(_ui->lvCompanies, &QListView::clicked,
            this, &LogistsWidget::checkedCompanies);

    connect(qobject_cast<qat_client::CheckedModel *>(_ui->cbNature->model()), &CheckedModel::selectionChanged,
        this, &LogistsWidget::refreshClicked);

    connect(_ui->editFind, &QLineEdit::textChanged,
            this, &LogistsWidget::refreshClicked);

    connect(_ui->chbIsSetDateEnd, &QCheckBox::clicked,
            this, &LogistsWidget::refreshClicked);

    connect(_ui->dateStart, &QDateEdit::dateChanged,
            this, &LogistsWidget::refreshClicked);

    connect(_ui->dateEnd, &QDateEdit::dateChanged,
            this, &LogistsWidget::refreshClicked);
}

void LogistsWidget::setSettings()
{
    _settings.clear();
    if (_companiesModel != nullptr)
        _settings._companies = _companiesModel->checkedRealIndex().toList();
    _settings._nature = qobject_cast<qat_client::CheckedModel *>(_ui->cbNature->model())->checkedRealIndex().toList();
    _settings._dateStart = QDateTime(_ui->dateStart->dateTime());
    if (!_ui->chbIsSetDateEnd->isChecked())
    {
        _settings._isSetDateEnd = true;
        _settings._dateEnd = QDateTime(_ui->dateEnd->dateTime());
    }
    else
    {
        _settings._isSetDateEnd = false;
    }

    _settings._like = _ui->editFind->text();
}

void LogistsWidget::setPermissionsWidget()
{
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();
}

void qat_client::LogistsWidget::on_btnExcel_clicked()
{
    const QString& fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               "Logists complaints " + QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss") + ".xlsx",
                               tr("Excel (*.xlsx)"));

    if (!fileName.isEmpty())
        emit signalCreateComplaintsExcelFile(fileName);
}
