﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/MainWindow/ComplaintsControllers/LogistsWidgetController.h"


namespace Ui
{
    class LogistsWidget;
}

namespace qat_client
{

    class CheckedModel;

    class LogistsWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit LogistsWidget(QWidget * parent = nullptr);
        ~LogistsWidget();

        void setLogistsModel(QAbstractItemModel* model);
        void setCompaniesModel(CheckedModel * companiesModel);
        void setNatureModel(CheckedModel * natureModel);
        void setSettings(LogistsWidgetController::SettingsLogists settings);
        void setComplaintInfoWidget(QWidget* widget);
        void checkedCompanies(const QModelIndex &index);
        void updateLogists();
        void updateNature();

    signals:
        //complaints management
        void complaintSelected(const quint64 id);
        void logistSelected(const quint64 id);
        void signalSelectLogists(const LogistsWidgetController::SettingsLogists & settings);
        void signalFillNature();
        void signalHideWidget();
        void signalCreateComplaintsExcelFile(const QString & fileName);

        void signalCheckedAll();
        void signalUncheckedAll();

    public slots:
        void updateCompanies();

    private slots:
        void refreshClicked();
        void on_btnExcel_clicked();

    private:
        void setSettings();
        void setPermissionsWidget();

    private:
        Ui::LogistsWidget *_ui;
        LogistsWidgetController::SettingsLogists _settings;
        CheckedModel * _companiesModel;
    };

}
