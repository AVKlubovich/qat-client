#include "Common.h"
#include "ExitDialog.h"
#include "ui_ExitDialog.h"

ExitDialog::ExitDialog(QWidget *parent) :
    QDialog(parent, Qt::CustomizeWindowHint),
    _ui(new Ui::ExitDialog)
{
    _ui->setupUi(this);
    connect(_ui->btnTrey, &QPushButton::clicked, this, &ExitDialog::accept);
    connect(_ui->btnCloseQat, &QPushButton::clicked, this, &ExitDialog::reject);

    connect(this, &QDialog::finished, this, &ExitDialog::deleteLater);;
}

ExitDialog::~ExitDialog()
{
    delete _ui;
}

