#include "Common.h"

#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "ExitDialog.h"

#include "Models/CheckedModel.h"
#include "permissions/UserPermission.h"


using namespace qat_client;

MainWindow::MainWindow()
    : QMainWindow(nullptr)
    , _ui(new Ui::MainWindow)
{
    _ui->setupUi(this);

    _mapNameLabelStatusBar = {
        {0, "Всего"},
        {1, "Новых"},
        {2, "В работе"},
        {3, "Разобраных"},
        {4, "Требующих оповещения"},
        {5, "Закрытых"},
        {6, "Открытых"},
        {7, "Закрытых"}
    };

    this->showFullScreen();
    createTrayIcon();

    createStatusBar();

    _ui->scrollArea->setLayout(&_layout);

    connect(_ui->infoTabWidget, &QTabWidget::currentChanged,
    this, [this](int tab)
    {
        setSettings();
        emit signalTabChanged(_settings);
    });

    connect(_ui->tabWidgetComplaints, &QTabWidget::currentChanged,
    this, [this](int tab)
    {
        setSettings();
        emit signalTabChanged(_settings);
    });

    connect(_ui->globalTabWidget, &QTabWidget::currentChanged,
    [this](int tab)
    {
        if (tab == _ui->globalTabWidget->indexOf(_ui->tabStatistic))
        {
            _ui->dateEnd->setDateTime(QDateTime::currentDateTime());
            _ui->dateStart->setDate(QDate::currentDate().addDays(-4));

            _ui->dateEndReviews->setDate(QDate::currentDate());
            _ui->timeEndReviews->setTime(QTime::currentTime());
            _ui->dateStartReviews->setDate(QDate::currentDate().addDays(-4));
        }
    });

    connect(_ui->btnCreateStatistic, &QPushButton::clicked,
            this, &MainWindow::btnCreateStatistic);

    connect(_ui->btnExcel, &QPushButton::clicked,
            this, &MainWindow::btnClickedExcel);

    connect(_ui->btnCreateStatisticReviews, &QPushButton::clicked,
            this, &MainWindow::btnCreateStatisticReviews);

    connect(_ui->btnExcelReviews, &QPushButton::clicked,
            this, &MainWindow::btnClickedExcelReviews);

    connect(_ui->tabWidgetStatistic, &QTabWidget::currentChanged,
    [this](int tab)
    {
        clearScrollAreaWidget();
        _ui->btnExcel->setEnabled(false);
        _ui->btnExcelReviews->setEnabled(false);
    });

    setPermissionsWidget();
    _ui->btnExcel->setEnabled(false);
    _ui->btnExcelReviews->setEnabled(false);
}

MainWindow::~MainWindow()
{
    _trayIcon->deleteLater();
    delete _ui;
}

void MainWindow::setTabComplaint()
{
    _ui->infoTabWidget->setCurrentIndex(0);
    _ui->tabWidgetComplaints->setCurrentIndex(_ui->tabWidgetComplaints->count() - 1);
}

void MainWindow::setTabReview()
{
    _ui->infoTabWidget->setCurrentIndex(1);
}

DriversWidget * MainWindow::getDriversWidget()
{
    return _ui->driversWidget;
}

OperatorsWidget * MainWindow::getOperatorsWidget()
{
    return _ui->operatorsWidget;
}

LogistsWidget * MainWindow::getLogistsWidget()
{
    return _ui->logistsWidget;
}

ClientsWidget * MainWindow::getClientsWidget()
{
    return _ui->clientsWidget;
}

ComplaintsWidget * MainWindow::getComplaintsWidget()
{
    return _ui->complaintsWidget;
}

ReviewsWidget * MainWindow::getReviewsWidget()
{
    return _ui->reviewsWidget;
}

AccessRightsWidget * MainWindow::getAccessRightsWidget()
{
    return _ui->accessRightsWidget;
}

void MainWindow::setCompany(QAbstractItemModel *model)
{
    _ui->cbCompany->setModel(model);
}

void MainWindow::setStatisticTarget(QAbstractItemModel *model)
{
    _ui->cbStatisticTarget->setModel(model);
}

void MainWindow::setModelDepartmentsStatistic(QAbstractItemModel *model)
{
    auto label = new QLabel("Незакрытых жалоб в отделах:");
    _listWidgets.append(label);
    _layout.addWidget(label);

    auto treeView = new QTreeView(_ui->scrollArea);
    _listWidgets.append(treeView);
    treeView->setModel(model);
    treeView->setColumnWidth(0, 450);
    _layout.addWidget(treeView);
}

void MainWindow::setModelSourceStatistic(QAbstractItemModel *model)
{
    auto label = new QLabel("Откуда поступили новые жалобы:");
    _listWidgets.append(label);
    _layout.addWidget(label);

    auto treeView = new QTreeView(_ui->scrollArea);
    _listWidgets.append(treeView);
    treeView->setModel(model);
    treeView->setColumnWidth(0, 450);
    _layout.addWidget(treeView);

    _ui->btnExcel->setEnabled(true);
}

void MainWindow::setModelInfoStatistic(QAbstractItemModel *model)
{
    clearScrollAreaWidget();

    auto tableView = new QTableView(_ui->scrollArea);
    _listWidgets.append(tableView);
    tableView->setShowGrid(false);
    tableView->setModel(model);
    tableView->setColumnWidth(0, 450);
    tableView->setColumnWidth(4, 140);
    tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    tableView->setSelectionMode(QAbstractItemView::NoSelection);
    _layout.addWidget(tableView);

    _ui->btnExcelReviews->setEnabled(true);
}

void MainWindow::showMessageStatusBar(const QString &msg, quint32 sec)
{
    _ui->statusBar->showMessage(msg, sec * 1000);
}

void MainWindow::setValueStatusBar(const QMap<int, int> &map, bool flagItem)
{
    if (_flagItemStatusBar != flagItem)
    {
        _flagItemStatusBar = flagItem;

        for (auto widget : _listLabelsStatusBar)
            _ui->statusBar->removeWidget(widget);
        _listLabelsStatusBar.clear();

        _mapLabelStatusBar.clear();

        for (auto it = map.begin(); it != map.end(); ++it)
        {
            auto labelKey = new QLabel(_mapNameLabelStatusBar.value(it.key()), _ui->statusBar);
            _ui->statusBar->addPermanentWidget(labelKey);
            _listLabelsStatusBar.append(labelKey);

            auto labelValue = new QLabel(QString::number(it.value()), _ui->statusBar);
            _ui->statusBar->addPermanentWidget(labelValue);
            _listLabelsStatusBar.append(labelValue);

            _mapLabelStatusBar.insert(it.key(), labelValue);
        }
        return;
    }

    for (auto it = map.begin(); it != map.end(); ++it)
        _mapLabelStatusBar.value(it.key())->setText(QString::number(it.value()));
}

void MainWindow::btnClickedExcel()
{
    const QString& fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               "statistic complaints " + QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss") + ".xlsx",
                               tr("Excel (*.xlsx)"));

    if (!fileName.isEmpty())
        emit signalClickedExcel(fileName);
}

void MainWindow::btnCreateStatistic()
{
    const QDateTime dateStart = _ui->dateStart->dateTime();
    const QDateTime dateEnd  = _ui->dateEnd->dateTime();
    QVariantList listCompany =  qobject_cast<CheckedModel *>(_ui->cbCompany->model())->getCheckedRealId();
    QVariantList listTarget =  qobject_cast<CheckedModel *>(_ui->cbStatisticTarget->model())->getCheckedRealId();

    if (listCompany.count())
    {
        if (listTarget.count())
        {
            emit signalClickedStatistic(dateStart, dateEnd, listCompany, listTarget);
        }
        else
        {
            QMessageBox msg;
            msg.setText("Выберите на кого");
            msg.exec();
        }
    }
    else
    {
        QMessageBox msg;
        msg.setText("Выберите компанию");
        msg.exec();
    }
}

void MainWindow::btnClickedExcelReviews()
{
    const QString& fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               "statistic reviews " + QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss") + ".xlsx",
                               tr("Excel (*.xlsx)"));

    if (!fileName.isEmpty())
        emit signalClickedExcelReviews(fileName);
}

void MainWindow::btnCreateStatisticReviews()
{
    const QDateTime dateStart(_ui->dateStartReviews->date(), _ui->timeStartReviews->time());
    const QDateTime dateEnd(_ui->dateEndReviews->date(), _ui->timeEndReviews->time());

    QVariantList list;
    switch (_ui->cbWhoseDriver->currentIndex()) {
        case 0:
            list << 0 << 1;
            break;
        case 1:
            list << 0 ;
            break;
        case 2:
            list << 1;
            break;
        default:
            break;
    }

    //if (list.count())
        emit signalClickedStatisticReviews(dateStart, dateEnd, list);
//    else
//    {
//        QMessageBox msg;
//        msg.setText("Выберите принадлежность водителя");
//        msg.exec();
    //    }
}

void MainWindow::slotIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::DoubleClick:
        if(_isTray)
        {
            _isTray = false;
            _trayIcon->hide();
            this->showMaximized();
        }
        break;
    default:
        ;
    }
}

void MainWindow::getCreateTabWidgets()
{
    QMap<int, QString> mapComplaints;

    if (_ui->tabClients)
        mapComplaints[_ui->tabWidgetComplaints->indexOf(_ui->tabClients)]   = "clients";

    if (_ui->tabLogists)
        mapComplaints[_ui->tabWidgetComplaints->indexOf(_ui->tabLogists)]   = "logists";

    if (_ui->tabOperators)
        mapComplaints[_ui->tabWidgetComplaints->indexOf(_ui->tabOperators)] = "operators";

    if (_ui->tabDrivers)
        mapComplaints[_ui->tabWidgetComplaints->indexOf(_ui->tabDrivers)]   = "drivers";

    _settings._mapComplaints = mapComplaints;
}

void MainWindow::clearScrollAreaWidget()
{
    for (auto widget : _listWidgets)
        _layout.removeWidget(widget);

    _listWidgets.clear();
}

void MainWindow::createTrayIcon()
{
    _maximizeAction = new QAction(tr("Развернуть"), this);
    connect(_maximizeAction, &QAction::triggered, this, &MainWindow::showMaximized);

    _quitAction = new QAction(tr("&Закрыть"), this);
    connect(_quitAction, &QAction::triggered, qApp, &QApplication::quit);

    _trayIconMenu = new QMenu(this);
    _trayIconMenu->addAction(_maximizeAction);
    _trayIconMenu->addSeparator();
    _trayIconMenu->addAction(_quitAction);

    _trayIcon = new QSystemTrayIcon(this);
    _trayIcon->setContextMenu(_trayIconMenu);
    _trayIcon->setIcon(this->style()->standardIcon(QStyle::SP_ComputerIcon));
    _trayIcon->setToolTip("Отдел контроля качества" "\n"
                          "Для раскрытия приложения кликните 2 раза по значку");

    connect(_trayIcon, &QSystemTrayIcon::activated, this, &MainWindow::slotIconActivated);
    _trayIcon->hide();
}

void MainWindow::createStatusBar()
{
//    _labelComplaint = new QLabel("0", _ui->statusBar);
//    _ui->statusBar->addPermanentWidget(new QLabel("Всего жалоб:", _ui->statusBar));
//    _ui->statusBar->addPermanentWidget(_labelComplaint);

//    _labelReview = new QLabel("0", _ui->statusBar);
//    _ui->statusBar->addPermanentWidget(new QLabel("Всего отзывов:", _ui->statusBar));
//    _ui->statusBar->addPermanentWidget(_labelReview);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    _isTray = true;
    _trayIcon->show();

    auto dialog = new ExitDialog(this);

    if(dialog->exec())
    {
        this->hide();
        event->ignore();
    }
    else
        QApplication::quit();
}

void MainWindow::setSettings()
{
    _settings._tabIndex = _ui->infoTabWidget->currentIndex();
    _settings._tabIndexComplaints = _ui->tabWidgetComplaints->currentIndex();
}

void MainWindow::setPermissionsWidget()
{
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();

    bool flagGlobalTabWidget          = false;
    bool flagGlobalTabWidgetStatistic = false;
    bool flagCompanyTabWidget         = false; // TODO заглушка, не реализованно
    bool flagInfoTabWidget            = false;
    bool flagDriverTabWidget          = false;
    bool flagOperatorTabWidget        = false;
    bool flagLogistTabWidget          = false;
    bool flagClientTabWidget          = false; // TODO заглушка, не реализованно

    for (auto listCommands : mapPermissions)
    {
        if (listCommands.contains(permissions::UserPermission::get_user_list))
            flagGlobalTabWidget = true;

        if (listCommands.contains(permissions::UserPermission::statistic))
            flagGlobalTabWidgetStatistic = true;

//        if (listCommands.contains(permissions::UserPermission::get_user_list))
//            flagCompanyTabWidget = true;

        if (listCommands.contains(permissions::UserPermission::get_select_reviews))
            flagInfoTabWidget = true;

        if (listCommands.contains(permissions::UserPermission::get_select_drivers))
            flagDriverTabWidget = true;

        if (listCommands.contains(permissions::UserPermission::get_select_operators))
            flagOperatorTabWidget = true;

        if (listCommands.contains(permissions::UserPermission::get_select_logists))
            flagLogistTabWidget = true;
    }


    if (!flagGlobalTabWidget)
        _ui->globalTabWidget->removeTab(_ui->globalTabWidget->indexOf(_ui->tabAdmin));

    if (!flagGlobalTabWidgetStatistic)
        _ui->globalTabWidget->removeTab(_ui->globalTabWidget->indexOf(_ui->tabStatistic));

    enum Company
    {
        TF_GF = 0,
        Dostaevskiy
    };

    if (!flagCompanyTabWidget)
        _ui->companiesTabWidget->removeTab(Dostaevskiy);

    enum Info
    {
        Complaints = 0,
        Reviews
    };

    if (!flagInfoTabWidget)
        _ui->infoTabWidget->removeTab(Reviews);

    if (!flagDriverTabWidget)
        _ui->tabWidgetComplaints->removeTab(_ui->tabWidgetComplaints->indexOf(_ui->tabDrivers));

    if (!flagOperatorTabWidget)
        _ui->tabWidgetComplaints->removeTab(_ui->tabWidgetComplaints->indexOf(_ui->tabOperators));

    if (!flagLogistTabWidget)
        _ui->tabWidgetComplaints->removeTab(_ui->tabWidgetComplaints->indexOf(_ui->tabLogists));

    if (!flagClientTabWidget)
        _ui->tabWidgetComplaints->removeTab(_ui->tabWidgetComplaints->indexOf(_ui->tabClients));

    getCreateTabWidgets();
}
