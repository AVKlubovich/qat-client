#include "Common.h"

#include "AttachmentWidget.h"
#include "ui_AttachmentWidget.h"

#include "permissions/UserPermission.h"


using namespace qat_client;

AttachmentWidget::AttachmentWidget(QWidget *parent, QUuid uuid, const QString & fileName, bool uploaded)
    : QWidget(parent)
    , _ui(new Ui::AttachmentWidget)
    , _uuid(uuid)
    , _fileName(fileName)
{
    _ui->setupUi(this);

    //_ui->labelFileName->setText(QString("<a href=\"whatever\">%1</a>").arg(fileName));
    //_ui->labelFileName->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
    _ui->labelFileName->setText(fileName);

    _ui->progressBar->setRange(0,100);
    _ui->progressBar->setValue(0);
    _ui->btnDownload->setVisible(uploaded);
    _ui->btnCancel->setVisible(!uploaded);
    _ui->progressBar->setVisible(!uploaded);

    _ui->btnCancel->setIconSize(_ui->btnCancel->size());
    _ui->btnCancel->setIcon(QIcon(":/images/btnCancel"));

    setPermissionsWidget();
}

AttachmentWidget::~AttachmentWidget()
{
    delete _ui;
}

void AttachmentWidget::onFileUploadStatusUpdate(float percentsComplete)
{
    _ui->progressBar->setValue(static_cast<quint64>(percentsComplete * 100));
    if (percentsComplete >= 1.0f)
    {
        _ui->progressBar->setVisible(false);
        //_ui->btnCancel->setVisible(false);
    }
}

void AttachmentWidget::onFileUploadCancel()
{
    delete this;
}

void AttachmentWidget::on_labelFileName_linkActivated(const QString &link)
{
    emit openAttachedFileClicked(_uuid);
}

void AttachmentWidget::on_btnCancel_clicked()
{
    emit cancelUploadAttachedFileClicked();
}

void AttachmentWidget::on_btnDownload_clicked()
{
    emit dowloadAttachedFileClicked();
}

void AttachmentWidget::setPermissionsWidget()
{
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();
}
