#pragma once


namespace Ui
{
    class AttachmentWidget;
}

namespace qat_client
{

    class AttachmentWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit AttachmentWidget(QWidget *parent, QUuid uuid, const QString & fileName, bool uploaded = false);
        ~AttachmentWidget();

    public slots:
        void onFileUploadStatusUpdate(float percentsComplete);
        void onFileUploadCancel();

    signals:
        void openAttachedFileClicked(QUuid uuid);
        void cancelUploadAttachedFileClicked();
        void dowloadAttachedFileClicked();
        //void removeAttachedFileClicked(QUuid uuid);

    private slots:
        void on_labelFileName_linkActivated(const QString &link);
        void on_btnCancel_clicked();
        void on_btnDownload_clicked();

    private:
        void setPermissionsWidget();

    private:
        Ui::AttachmentWidget *_ui;

        QUuid _uuid;
        QString _fileName;
    };

}
