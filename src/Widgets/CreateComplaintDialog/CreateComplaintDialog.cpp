#include "Common.h"

#include "CreateComplaintDialog.h"
#include "ui_CreateComplaintDialog.h"

#include "Widgets/AttachmentWidget/AttachmentWidget.h"


using namespace qat_client;

CreateComplaintDialog::CreateComplaintDialog(QWidget *parent)
    : QDialog(parent)
    , _ui(new Ui::CreateComplaintDialog)
{
    _ui->setupUi(this);

    connect(this, &QDialog::finished, this, &CreateComplaintDialog::closed);

    connect(_ui->cbCompany, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &CreateComplaintDialog::companyChanged);
    connect(_ui->cbTarget, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &CreateComplaintDialog::targetChanged);
    connect(_ui->cbNatureComplaint, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &CreateComplaintDialog::natureChanged);

    connect(_ui->btnSave, &QPushButton::clicked,
            this, [this]{
        if (_typeNature)
            if (!_ui->cbFeedBack->currentIndex())
            {
                QMessageBox msg;
                msg.setText(tr("Заполните обязательные поля!"));
                msg.exec();

                _ui->cbFeedBack->setStyleSheet("background-color:red;"
                                               "color:white;");
                return;
            }

        emit createClicked();
        this->reject();
    });

    connect(_ui->btnDuplicate, &QPushButton::clicked,
            this, [this]{
        if (_typeNature)
            if (!_ui->cbFeedBack->currentIndex())
            {
                QMessageBox msg;
                msg.setText(tr("Заполните обязательные поля!"));
                msg.exec();

                _ui->cbFeedBack->setStyleSheet("background-color:red;"
                                               "color:white;");
                return;
            }

        emit createClicked();
    });

    _ui->btnFiles->setIconSize(_ui->btnFiles->size());
    _ui->btnFiles->setIcon(QIcon(":/images/btnFiles"));

    _ui->btnDuplicate->setIconSize(_ui->btnDuplicate->size());
    _ui->btnDuplicate->setIcon(QIcon(":/images/btnDuplicate"));

    _ui->btnSave->setIconSize(_ui->btnSave->size());
    _ui->btnSave->setIcon(QIcon(":/images/btnSave"));

    _ui->label->setHidden(true);
    _ui->cbFeedBack->setHidden(true);
}

CreateComplaintDialog::~CreateComplaintDialog()
{
    delete _ui;
}

void CreateComplaintDialog::setCompanyModel(QAbstractItemModel* model)
{
    _ui->cbCompany->setModel(model);
    updateCompany();
}

void CreateComplaintDialog::updateCompany()
{
    _ui->cbCompany->update();
}

void CreateComplaintDialog::setTargetModel(QAbstractItemModel* model)
{
    _ui->cbTarget->setModel(model);
    updateTraget();
}

void CreateComplaintDialog::updateTraget()
{
    _ui->cbTarget->update();
}

void CreateComplaintDialog::setNatureModel(QAbstractItemModel* model)
{
    _ui->cbNatureComplaint->setModel(model);
    updateNature();
}

void CreateComplaintDialog::updateNature()
{
    _ui->cbNatureComplaint->update();
}

void CreateComplaintDialog::updateEssence()
{
    hideFeedBack();

    const auto row = _ui->cbNatureComplaint->currentIndex();
    const auto essence = (row != -1)
                         ? _ui->cbNatureComplaint->model()->data(_ui->cbNatureComplaint->model()->index(row, 3)).toString()
                         : QString();
    _ui->teDescriptionComplaints->setText(essence);
}

const quint64 CreateComplaintDialog::getOrder()
{
    return _ui->leOrderNumber->text().toLongLong();
}

const QString CreateComplaintDialog::getEssence()
{
    return _ui->teDescriptionComplaints->toPlainText();
}

const QString CreateComplaintDialog::getComment()
{
    return _ui->teAdditionalComments->toPlainText();
}

const quint64 CreateComplaintDialog::getCompanyIndex()
{
    return _ui->cbCompany->currentIndex();
}

const quint64 CreateComplaintDialog::getTargetIndex()
{
    return _ui->cbTarget->currentIndex();
}

const quint64 CreateComplaintDialog::getNatureIndex()
{
    return _ui->cbNatureComplaint->currentIndex();
}

const int CreateComplaintDialog::getDaysToReview()
{
    return _ui->cbNatureComplaint->model()->index(_ui->cbNatureComplaint->currentIndex(), 2).data().toInt();
}

const int CreateComplaintDialog::getFeedBack()
{
    if (_typeNature)
        return _ui->cbFeedBack->currentIndex();
    else
        return 0;
}

void CreateComplaintDialog::setIdOrder(const quint64 idOrder)
{
    if (idOrder)
    {
        _ui->leOrderNumber->setText(QString::number(idOrder));
        _ui->leOrderNumber->setEnabled(false);
    }
}

void CreateComplaintDialog::setIdCompany(const quint64 idCompany)
{
    const auto rowCount = _ui->cbCompany->model()->rowCount();
    auto model = _ui->cbCompany->model();
    for (auto row = 0; row < rowCount; row++)
    {
        if (idCompany == model->index(row, model->columnCount() - 1).data().toULongLong())
        {
            _ui->cbCompany->setCurrentIndex(row);
            _ui->cbCompany->setEnabled(false);
            break;
        }
    }
}

void CreateComplaintDialog::setComment(const QString &comment)
{
    _ui->teAdditionalComments->insertPlainText(comment);
}

void CreateComplaintDialog::setFileAttachWidget(QWidget *fileAttachWidget)
{
    _fileWidget = fileAttachWidget;
    connect(_ui->btnFiles, &QPushButton::clicked, _fileWidget, &QWidget::show);
}

void CreateComplaintDialog::hideFeedBack()
{
    auto model = _ui->cbNatureComplaint->model();
    _typeNature = model->data(model->index(_ui->cbNatureComplaint->currentIndex(), model->columnCount() - 1)).toBool();

    _ui->label->setHidden(!_typeNature);
    _ui->cbFeedBack->setHidden(!_typeNature);
}
