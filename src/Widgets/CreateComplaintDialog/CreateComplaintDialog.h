﻿#pragma once


namespace Ui
{
    class CreateComplaintDialog;
}

namespace qat_client
{

    class AttachmentWidget;

    class CreateComplaintDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit CreateComplaintDialog(QWidget *parent = nullptr);
        virtual ~CreateComplaintDialog();

        void setCompanyModel(QAbstractItemModel* model);
        void updateCompany();

        void setTargetModel(QAbstractItemModel* model);
        void updateTraget();

        void setNatureModel(QAbstractItemModel* model);
        void updateNature();

        void updateEssence();

        const quint64 getOrder();
        const QString getEssence();
        const QString getComment();
        const quint64 getCompanyIndex();
        const quint64 getTargetIndex();
        const quint64 getNatureIndex();
        const int getDaysToReview();
        const int getFeedBack();

        void setIdOrder(const quint64 idOrder);
        void setIdCompany(const quint64 idCompany);
        void setComment(const QString& comment);

        void setFileAttachWidget(QWidget *fileAttachWidget);

    private:
        void hideFeedBack();

    signals:
        void companyChanged(const quint64 companyId);
        void targetChanged(const quint64 targetId);
        void natureChanged(const quint64 natureId);
        void createClicked();
        void closed();

    protected:
        Ui::CreateComplaintDialog *_ui;

        QWidget *_fileWidget = nullptr;
        bool _typeNature = false;
    };

}
