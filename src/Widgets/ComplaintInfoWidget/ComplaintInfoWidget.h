﻿#pragma once

#include "Controllers/ComplaintInfoController.h"


namespace file_attach
{
    class FileAttachmentWidget;
}

namespace Ui
{
    class ComplaintInfoWidget;
}

namespace qat_client
{

    class ComplaintInfoWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit ComplaintInfoWidget(QWidget *parent = nullptr);
        ~ComplaintInfoWidget();

        void setComplaintInfoModel(QAbstractItemModel* model);
        void setNatureModel(QAbstractItemModel* model, const int position);
        void setUsersModel(QAbstractItemModel* model, const int position);
        void setStatusModel(QAbstractItemModel* model, const int position);
        void setDepartamentModel(QAbstractItemModel* model, const int position);
        void setTypeModel(QAbstractItemModel* model, const int position);
        void setComments(QVariantList listComments);
        void setHistory(QVariantList listHistory);
        void setButtonLinkModel(QAbstractItemModel* model);
        void setIsWork(const int isWork);
        void setPermissionsWidget(bool hide);
        void setIdComplaint(const quint64 idComplaint);

        void fillComplaintInfo(QSqlQueryModel *model);
        void infoDriverComplaint(QSqlQueryModel *model);
        void setStatistic(QVariantMap mapStatistic);

        void openSmsDialog(QAbstractItemModel* templateModel, QAbstractItemModel *phoneModel);

    private slots:
        void onBtnWorkCliced();
        void onBtnSaveCliced();
        void onBtnCancelClicked();
        void onBtnCallClicked();
        void clearEdits();

        void showFilesWidget();
        void createFileWidget();

        void checkStatus();
        void checkDepartment();

        void onFilesCountChanged(const quint64 filesCount);

    private:
        quint64 realId(QAbstractItemModel *model, const int row);
        quint64 realId(const QComboBox* cBox);

    private slots:
        void openReview();
        void slotCloseComplaint();
        void clickedBtnNatureComplaint();
        void clickedCbNature(int index);

    signals:
        void signalInWork(bool inWork);
        void signalSaveComplaint(ComplaintInfoController::UpdateParametrs settings);
        void signalIdReveiw(const quint64 idReview);
        void signalTargetComplaints(const QString &idTarget, int statusType);
        void signalDownloadHistory();
        void signalBtnChallenge();

        void signalFindComplaints(QVariantList list); // TODO find complaints
        void signalOpenSmsDialog();
        void signalSendSmsToServer(const QString &smsText, const QString &phone, const QDateTime &date, int type, double mark = 0);

    private:
        Ui::ComplaintInfoWidget *_ui;

        quint64 _idComplaint = 0;

        QAbstractItemModel *_complaintInfoModel = nullptr;
        QAbstractItemModel *_linkModel = nullptr;

        file_attach::FileAttachmentWidget *_fileWidget = nullptr;

        ComplaintInfoController::UpdateParametrs _settings;

        QShortcut *_keySave;
        QShortcut *_keyClose;

        quint64 _currentDep = -1;
        quint64 _currentStatus = -1;

        QMap<QPushButton*, QVariantList> _mapBtnList;
    };

}
