#include "Common.h"

#include "ComplaintInfoWidget.h"
#include "ui_ComplaintInfoWidget.h"

#include "CommentForCall.h"

#include "Definitions.h"

#include "permissions/UserPermission.h"

#include "file-attachment/FileAttachmentWidget.h"

#include "Widgets/SmsNotification/DialogNotification.h"
#include "Widgets/AttachmentWidget/AttachmentWidget.h"


using namespace qat_client;

ComplaintInfoWidget::ComplaintInfoWidget(QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::ComplaintInfoWidget)
{
    _ui->setupUi(this);
    _ui->btnChallenge->setHidden(true);

    _keyClose = new QShortcut(this);
    _keyClose->setKey(QKeySequence::Cut);
    connect(_keyClose, &QShortcut::activated, this, &ComplaintInfoWidget::slotCloseComplaint);

    _keySave = new QShortcut(this);
    _keySave->setKey(QKeySequence::Save);
    connect(_keySave, &QShortcut::activated, this, &ComplaintInfoWidget::onBtnSaveCliced);

    _ui->btnCancel->setHidden(true);
    _ui->btnSave->setHidden(true);

    _ui->btnQuestionnaireDriver->setVisible(false);

    connect(_ui->btnWork, &QPushButton::clicked, this, &ComplaintInfoWidget::onBtnWorkCliced);
    _ui->btnWork->setHidden(true);

    connect(_ui->btnSave, &QPushButton::clicked, this, &ComplaintInfoWidget::onBtnSaveCliced);
    connect(_ui->btnCancel, &QPushButton::clicked, this, &ComplaintInfoWidget::onBtnCancelClicked);
    connect(_ui->btnReviewFromComplaint, &QPushButton::clicked, this, &ComplaintInfoWidget::openReview);
    connect(_ui->btnSms, &QPushButton::clicked, this, &ComplaintInfoWidget::signalOpenSmsDialog);
    connect(_ui->cbNature, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &ComplaintInfoWidget::clickedCbNature);

//    _ui->btnCall->setHidden(true);
    connect(_ui->btnCall, &QPushButton::clicked, this, &ComplaintInfoWidget::onBtnCallClicked);

    connect(_ui->btnFiles, &QPushButton::clicked, this, &ComplaintInfoWidget::showFilesWidget);

    connect(_ui->cbDepartament, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &ComplaintInfoWidget::checkStatus);
    connect(_ui->cbStatus, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &ComplaintInfoWidget::checkDepartment);

    enum statusType
    {
        AllComplaints = 0,
        PositiveComplaints,
        NegativeComplaints,
    };

    connect(_ui->tabWidgetComplaint, &QTabWidget::currentChanged,
            [this](int tab)
            {
                if (tab)
                    emit signalDownloadHistory();
            });
}

ComplaintInfoWidget::~ComplaintInfoWidget()
{
    delete _ui;
}

void ComplaintInfoWidget::setComplaintInfoModel(QAbstractItemModel* model)
{
    _complaintInfoModel = model;
}

void ComplaintInfoWidget::setNatureModel(QAbstractItemModel *model, const int position)
{
    _ui->cbNature->setModel(model);
    _ui->cbNature->setCurrentIndex(position);
//    _ui->editCost->setText(QString::number(CONST_RATING - model->data(model->index(position, 2)).toFloat()));
}

void ComplaintInfoWidget::setUsersModel(QAbstractItemModel *model, const int position)
{
    _ui->cbUser->setModel(model);
    _ui->cbUser->setCurrentIndex(position);
}

void ComplaintInfoWidget::setStatusModel(QAbstractItemModel *model, const int position)
{
    _ui->cbStatus->blockSignals(true);
    _ui->cbStatus->setModel(model);
    _ui->cbStatus->setCurrentIndex(position);
    _ui->cbStatus->blockSignals(false);

    _currentStatus = realId(_ui->cbStatus);
}

void ComplaintInfoWidget::setDepartamentModel(QAbstractItemModel *model, const int position)
{
    _ui->cbDepartament->blockSignals(true);
    _ui->cbDepartament->setModel(model);
    _ui->cbDepartament->setCurrentIndex(position);
    _ui->cbDepartament->blockSignals(false);

    _currentDep = realId(_ui->cbDepartament);
}

void ComplaintInfoWidget::setTypeModel(QAbstractItemModel *model, const int position)
{
    _ui->cbType->setModel(model);
    _ui->cbType->setCurrentIndex(position);
}

void ComplaintInfoWidget::setComments(QVariantList listComments)
{
    _ui->tEditTabReviews->clear();
    _ui->tEditComment->clear();
    for (auto element : listComments)
    {
        auto map = element.toMap();
        _ui->tEditTabReviews->append(map["comment"].toString());
    }
}

void ComplaintInfoWidget::setHistory(QVariantList listHistory)
{
    _ui->tEditTabHistory->clear();
    for (auto element : listHistory)
    {
        auto map = element.toMap();
        _ui->tEditTabHistory->append(map["comment"].toString());
    }
}

void ComplaintInfoWidget::setButtonLinkModel(QAbstractItemModel *model)
{
    _linkModel = model;
}

void ComplaintInfoWidget::setIsWork(const int isWork)
{
    if(!isWork)
        _ui->btnWork->setEnabled(true);
    else
        _ui->btnWork->setEnabled(false);
}

void ComplaintInfoWidget::setIdComplaint(const quint64 idComplaint)
{
    _idComplaint = idComplaint;
    createFileWidget();
}

void ComplaintInfoWidget::onBtnWorkCliced()
{
    _ui->btnSave->setHidden(false);
    _ui->btnCancel->setHidden(true);
    _ui->editOrder->setReadOnly(false);
    _ui->tEditReview->setReadOnly(false);
    _ui->tEditComment->setReadOnly(false);
    _ui->cbDepartament->setEnabled(true);
    _ui->cbNature->setEnabled(true);
    _ui->cbStatus->setEnabled(true);
    _ui->cbType->setEnabled(true);
    _ui->cbUser->setEnabled(true);

    emit signalInWork(true);
}

void ComplaintInfoWidget::onBtnSaveCliced()
{
    _settings.id = _ui->editComplaint->text().toLongLong();
    _settings.idDepartament = realId(_ui->cbDepartament->model(), _ui->cbDepartament->currentIndex());
    _settings.idNature = realId(_ui->cbNature->model(), _ui->cbNature->currentIndex());
    _settings.idStatus = realId(_ui->cbStatus->model(), _ui->cbStatus->currentIndex());
    _settings.idType = realId(_ui->cbType->model(), _ui->cbType->currentIndex());
    _settings.idUser = realId(_ui->cbUser->model(), _ui->cbUser->currentIndex());
    _settings.isWork = 0;
    _settings.essence = _ui->tEditReview->toPlainText();
    _settings.comment = _ui->tEditComment->toPlainText();
    _settings.idOrder = _ui->editOrder->text().toLongLong();
    _settings.daysToReview = _ui->cbNature->model()->index(_ui->cbNature->currentIndex(), 1).data().toInt();
    _settings.rating = _ui->editCost->text();

    emit signalSaveComplaint(_settings);
}

void ComplaintInfoWidget::onBtnCancelClicked()
{
    _ui->btnSave->setHidden(true);
    _ui->btnCancel->setHidden(true);
    _ui->btnWork->setHidden(false);

    _ui->editOrder->setReadOnly(true);
    _ui->tEditReview->setReadOnly(true);
    _ui->tEditComment->setReadOnly(true);
    _ui->cbDepartament->setEnabled(false);
    _ui->cbNature->setEnabled(false);
    _ui->cbStatus->setEnabled(false);
    _ui->cbType->setEnabled(false);
    _ui->cbUser->setEnabled(false);

    emit signalInWork(false);
}

void ComplaintInfoWidget::onBtnCallClicked()
{
    CommentForCall *widget = new CommentForCall(_idComplaint);
    widget->setWindowFlags(Qt::Popup);
    widget->show();
}

void ComplaintInfoWidget::fillComplaintInfo(QSqlQueryModel *model)
{
    clearEdits();
    enum InfoComplaintEnum
    {
        id_nature,
        datetime,
        id_user,
        id_type,
        id_status,
        id_department,
        id_user_create,
        name_source,
        id_company,
        id,
        id_order,
        id_target,
        isWork,
        essence,
        end,
        other_user_create,
        rating,
        feedBack
    };

    if(_linkModel->rowCount())
        _ui->btnReviewFromComplaint->setHidden(false);
    else
        _ui->btnReviewFromComplaint->setHidden(true);

    _ui->editComplaint->setText(model->data(model->index(0, id)).toString());
    _ui->editDateOpen->setText(model->data(model->index(0, datetime)).toString());

    if (model->data(model->index(0, other_user_create)).toString().isEmpty())
        _ui->editCreateUser->setText(model->data(model->index(0, id_user_create)).toString());
    else
        _ui->editCreateUser->setText(model->data(model->index(0, other_user_create)).toString());

    _ui->editSource->setText(model->data(model->index(0, name_source)).toString());
    _ui->editOrder->setText(model->data(model->index(0, id_order)).toString());
    _ui->tEditReview->insertPlainText(model->data(model->index(0, essence)).toString());
    _ui->editCost->setText(model->data(model->index(0, rating)).toString());

    switch (model->data(model->index(0, feedBack)).toInt()) {
        case 0:
            _ui->editFeedBack->setText("Не указано");
            break;
        case 1:
            _ui->editFeedBack->setText("Да");
            break;
        case 2:
            _ui->editFeedBack->setText("Нет");
            break;
        default:
            _ui->editFeedBack->clear();
            break;
    }

    const int CONST_NEGATIVE_COMPLAINT = 2;
    auto mapPermissions = permissions::UserPermission::instance().mapPermissions();
    for (auto listCommands : mapPermissions)
        if ((listCommands.contains(permissions::UserPermission::complaint_challenge))
                && (model->data(model->index(0, id_type)).toInt() == CONST_NEGATIVE_COMPLAINT))
        {
            _ui->btnChallenge->setHidden(false);
            connect(_ui->btnChallenge, &QPushButton::clicked, this, &ComplaintInfoWidget::signalBtnChallenge);
            break;
        }

    onBtnWorkCliced();
}

void ComplaintInfoWidget::infoDriverComplaint(QSqlQueryModel *model)
{
    enum
    {
        IdDriver = 0,
        NameDriver,
        NameParkDriver,
        ColumnDriver,
        WhoseDriver,
        DateComeDriver,
        RatingDriver
    };
    const int CONST_ROW_MODEL = 0;

    if (model->rowCount())
    {
        _ui->editIdDriver->setText(model->index(CONST_ROW_MODEL, IdDriver).data().toString());
        if (!model->index(CONST_ROW_MODEL, WhoseDriver).data().toInt())
            _ui->editDriver->setText("Наш");
        else
            _ui->editDriver->setText("Частник");

        const QString park = model->index(CONST_ROW_MODEL, NameParkDriver).data().toString();
        const QString column = model->index(CONST_ROW_MODEL, ColumnDriver).data().toString();
        QString parkColumnStr;
        if (!park.isEmpty() && !column.isEmpty())
        {
            parkColumnStr = QString("%1 / %2").arg(park).arg(column);
        }
        else if (!park.isEmpty())
        {
            parkColumnStr = park;
        }
        else if (!column.isEmpty())
        {
            parkColumnStr = column;
        }
        _ui->editPark_Column->setText(parkColumnStr);

        auto doub = model->index(CONST_ROW_MODEL, RatingDriver).data().toDouble();
        _ui->editRating->setText(QString::number(doub, 'f', 1));
    }
}

void ComplaintInfoWidget::openReview()
{
    const auto idReview = _linkModel->index(0, 0).data().toLongLong();
    emit signalIdReveiw(idReview);
}

void ComplaintInfoWidget::slotCloseComplaint()
{
    _ui->cbStatus->setCurrentText("Закрыта");
    onBtnSaveCliced();
}

void ComplaintInfoWidget::clickedBtnNatureComplaint()
{
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    emit signalFindComplaints(_mapBtnList.value(btn));
}

void ComplaintInfoWidget::clickedCbNature(int index)
{
    auto model = _ui->cbNature->model();
    _ui->editCost->setText(QString::number(CONST_RATING - model->data(model->index(index, 2)).toFloat(), 'f', 1));
}

void ComplaintInfoWidget::openSmsDialog(QAbstractItemModel* templateModel, QAbstractItemModel* phoneModel)
{
    DialogNotification *dialog = new DialogNotification(this);
    dialog->setModelNotification(templateModel);
    dialog->setModelPhones(phoneModel);
    connect(dialog, &DialogNotification::signalSendSms, this, &ComplaintInfoWidget::signalSendSmsToServer);
    dialog->exec();
    disconnect(dialog, &DialogNotification::signalSendSms, this, &ComplaintInfoWidget::signalSendSmsToServer);
}

void ComplaintInfoWidget::clearEdits()
{
    _ui->tEditTabReviews->clear();
    _ui->tEditReview->clear();
    _ui->tEditComment->clear();
    _ui->editCost->clear();

    _mapBtnList.clear();
    delete _ui->widgetLinks->widget();

    _ui->editIdDriver->clear();
    _ui->editDriver->clear();
    _ui->editPark_Column->clear();
    _ui->editPositive->clear();
    _ui->editNegative->clear();
    _ui->editAll->clear();
    _ui->editRating->clear();
    _ui->editSource->clear();
    _ui->editCreateUser->clear();
    _ui->editDateOpen->clear();
    _ui->editComplaint->clear();
    _ui->editOrder->clear();
}

void ComplaintInfoWidget::showFilesWidget()
{
    createFileWidget();
    _fileWidget->show();
}

void ComplaintInfoWidget::createFileWidget()
{
    if (!_fileWidget)
    {
        _fileWidget = new file_attach::FileAttachmentWidget(this);
        _fileWidget->setWindowFlags(Qt::Popup);
        connect(_fileWidget, &file_attach::FileAttachmentWidget::filesCountChanged,
                this, &ComplaintInfoWidget::onFilesCountChanged);
    }
    _fileWidget->loadFilesList(_idComplaint);
}

void ComplaintInfoWidget::checkStatus()
{
    const auto newDep = realId(_ui->cbDepartament);

    quint64 newStatus = 0;

    if (newDep == ID_DEP_QAT)
    {
        newStatus = Statuses::demolished;
    }
    else if (_currentDep == ID_DEP_QAT &&
             newDep != ID_DEP_QAT)
    {
        newStatus = Statuses::in_the_work;
    }

    if (newStatus != 0)
    {
        _ui->cbStatus->blockSignals(true);
        _ui->cbStatus->setCurrentIndex(newStatus - 1);
        _ui->cbStatus->blockSignals(false);
    }
}

void ComplaintInfoWidget::checkDepartment()
{
    const auto newStatus = realId(_ui->cbStatus);

    quint64 newDep = 0;

    if (newStatus == demolished)
    {
        newDep = ID_DEP_QAT;
    }

    if (newDep != 0)
    {
        _ui->cbDepartament->blockSignals(true);
        _ui->cbDepartament->setCurrentIndex(newDep - 1);
        _ui->cbDepartament->blockSignals(false);
    }
}

void ComplaintInfoWidget::onFilesCountChanged(const quint64 filesCount)
{
    _ui->btnFiles->setText(QString("Файлы: %1").arg(filesCount));
}

quint64 ComplaintInfoWidget::realId(QAbstractItemModel *model, const int row)
{
    QModelIndex index = model->index(row, model->columnCount() - 1, QModelIndex());
    return index.data(Qt::DisplayRole).toInt();
}

quint64 ComplaintInfoWidget::realId(const QComboBox* cBox)
{
    const auto model = cBox->model();
    const auto row = cBox->currentIndex();
    const auto index = model->index(row, model->columnCount() - 1, QModelIndex());
    return index.data(Qt::DisplayRole).toInt();
}

void ComplaintInfoWidget::setPermissionsWidget(bool hide)
{
    if (hide)
        _ui->tabWidgetComplaint->removeTab(_ui->tabWidgetComplaint->count() - 1);
}

void ComplaintInfoWidget::setStatistic(QVariantMap mapStatistic)
{
    enum
    {
        AllComplaintsCount = 0,
        PositiveComplaintsCount,
        NegativeComplaintsCount,
    };
    _ui->editAll->setText(mapStatistic[QString::number(AllComplaintsCount)].toString());
    _ui->editNegative->setText(mapStatistic[QString::number(NegativeComplaintsCount)].toString());
    _ui->editPositive->setText(mapStatistic[QString::number(PositiveComplaintsCount)].toString());

    auto map = mapStatistic["natures"].toMap();
    QWidget *widget = new QWidget(_ui->widgetLinks);
    QVBoxLayout *vLayout = new QVBoxLayout(widget);

    for (auto it = map.begin(); it != map.end(); ++it)
    {
         QPushButton *btn = new QPushButton(it.key(), widget);
         connect(btn, &QPushButton::clicked, this, &ComplaintInfoWidget::clickedBtnNatureComplaint);

        _mapBtnList[btn] = it.value().toList();
        vLayout->addWidget(btn);
    }

    widget->setLayout(vLayout);
    _ui->widgetLinks->setWidget(widget);
}
