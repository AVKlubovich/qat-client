#include "Common.h"

#include "CommentForCall.h"
#include "ui_CommentForCall.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/Response.h"

#include "permissions/UserPermission.h"


using namespace qat_client;

CommentForCall::CommentForCall(const int idComplaint, QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::CommentForCall)
    , _idComplaint(idComplaint)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _ui->setupUi(this);

    connect(_ui->btnSendComment, &QPushButton::clicked, this, &CommentForCall::sendComment);

    QStringList items
    {
        "Вопрос решен",
        "Не отвечает",
        "Перезвонить"
    };
    _ui->comboBoxSubject->addItems(items);
}

CommentForCall::~CommentForCall()
{
    delete _ui;
}

void CommentForCall::onSendComment(network::ResponseShp responseObj)
{
    this->deleteLater();
}

void CommentForCall::sendComment()
{
    QString message = QString("Совершен звонок %1 – %2.")
                       .arg(QDateTime::currentDateTime().toString("dd-MM-yyyy HH:mm"))
                       .arg(_ui->comboBoxSubject->currentText());

    const QString &comment = _ui->textEditComment->toPlainText();
    if (!comment.isEmpty())
        message = QString("%1 Комментарий: %2.").arg(message).arg(comment);

    QVariantMap requestMap;
    requestMap["type_command"] = "add_comment";
    requestMap["id"] = _idComplaint;
    requestMap["comment"] = message;
    requestMap["schema"] = "complaints_schema";
    requestMap["id_user"] = permissions::UserPermission::instance().idUser();

    network::RequestShp request(new network::DefaultRequest("add_comment", QVariant::fromValue(requestMap)));
    _requestsManager->execute(request, this, &CommentForCall::onSendComment);
}
