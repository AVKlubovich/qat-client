﻿#pragma once


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace Ui
{
    class CommentForCall;
}

namespace qat_client
{

    class CommentForCall : public QWidget
    {
        Q_OBJECT

    public:
        explicit CommentForCall(const int idComplaint, QWidget *parent = nullptr);
        ~CommentForCall();

        void onSendComment(network::ResponseShp responseObj);

    private slots:
        void sendComment();

    private:
        Ui::CommentForCall *_ui;

        int _idComplaint;

        network::RequestsManagerShp _requestsManager;
    };

}
