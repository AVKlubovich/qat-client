﻿#pragma once


namespace Ui
{
    class LoginDialog;
}

namespace qat_client
{

    class LoginDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit LoginDialog(QWidget *parent = 0);
        ~LoginDialog();

        //Run dialog and request user credentials (login, password).
        bool requestCredentials();
        void requestCredentialsRetry();
        QString getLogin();
        QString getPassword();

    signals:
        void loginAccepted();

    private slots:
        void loginEditChanged(const QString& str);
        void acceptedButtonClicked();

    private:
        Ui::LoginDialog *_ui;
    };

}
