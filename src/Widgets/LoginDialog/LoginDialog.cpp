﻿#include "Common.h"

#include "LoginDialog.h"
#include "ui_LoginDialog.h"


using namespace qat_client;

LoginDialog::LoginDialog(QWidget *parent)
    : QDialog(parent)
    , _ui(new Ui::LoginDialog)
{
    _ui->setupUi(this);

//    auto newPalette = this->palette();
//    QBrush newBrush;
//    newBrush.setTextureImage(QImage(":/images/loginImg.png"));
//    newPalette.setBrush(QPalette::Window, newBrush);
//    this->setPalette(newPalette);

    connect(_ui->loginEdit, SIGNAL(textChanged(const QString&)), this, SLOT(loginEditChanged(const QString&)));
    connect(_ui->buttonBox, SIGNAL(accepted()), this, SLOT(acceptedButtonClicked()));

    //set initial login
    _ui->loginEdit->setText("testLogin");
}

LoginDialog::~LoginDialog()
{
    delete _ui;
}

bool LoginDialog::requestCredentials()
{
    int res = exec();
    return (res == QDialog::Accepted);
}

void LoginDialog::requestCredentialsRetry()
{
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
}

QString LoginDialog::getLogin()
{
    return _ui->loginEdit->text();
}

QString LoginDialog::getPassword()
{
    return _ui->passwordEdit->text();
}

void LoginDialog::loginEditChanged(const QString& str)
{
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!str.isEmpty());
}

void LoginDialog::acceptedButtonClicked()
{
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);
    emit loginAccepted();
}
