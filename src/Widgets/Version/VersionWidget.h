﻿#pragma once


namespace Ui
{
    class VersionWidget;
}

namespace qat_client
{

    class CheckedModel;

    class VersionWidget : public QDialog
    {
        Q_OBJECT

    public:
        explicit VersionWidget(const QString &oldVersion, const QString &newVersion, QWidget *parent = nullptr);
        ~VersionWidget();

    private:
        void startProcessUpdate();
        void message();

    private:
        Ui::VersionWidget *_ui;
    };

}
