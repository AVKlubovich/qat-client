#include "Common.h"

#include "VersionWidget.h"
#include "ui_VersionWidget.h"

#include "Models/CheckedModel.h"
#include "permissions/UserPermission.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"


using namespace qat_client;

VersionWidget::VersionWidget(const QString &oldVersion, const QString &newVersion, QWidget *parent)
    : QDialog(parent)
    , _ui(new Ui::VersionWidget)
{
    _ui->setupUi(this);

    QString text = _ui->label->text();
    _ui->label->setText(text.arg(oldVersion).arg(newVersion));

    connect(_ui->btnUpdate, &QPushButton::clicked,
            this, &VersionWidget::startProcessUpdate);
}

VersionWidget::~VersionWidget()
{
    delete _ui;
}

void VersionWidget::startProcessUpdate()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Info");

    const QString updateString =
            "C:\\TaxoControlUpdate\\runasuser.exe "
            "-u autoc -p 123456 -d gruzf \"C:\\TaxoControlUpdate\\updater.exe\" \"C:\\TaxoControlUpdate\\test.exe\" ";

    QProcess process(this);
    process.start(updateString);
    process.waitForReadyRead();
    process.waitForFinished();

//    if (process.exitCode() == -1)
    {
        const QString updateStringOKK =
                "C:\\TaxoControlUpdate\\updater.exe "
                "\"C:\\TaxoControlUpdate\\QAT-Client.exe\" \"QAT-Client\" "
                "\"" + settings["IpUpdate"].toString() + "\" "
                "\"" + settings["PortUpdate"].toString() + "\" "
                "\"" + settings["IntType"].toString() + "\"";

        auto process = new QProcess();
        process->start(updateStringOKK);

//        QProcess process(this);
//        process.start(updateStringOKK);
//        process.waitForReadyRead();
//        process.waitForFinished();
//        if (process->exitCode() < 0)
//            message();
    }
//    else
//    {
//        const QString updateStringOKK =
//                "C:\\TaxoControlUpdate\\runasuser.exe "
//                "-u autoc -p 123456 -d gruzf \"C:\\TaxoControlUpdate\\updater.exe\" "
//                "\"C:\\TaxoControlUpdate\\QAT-Client.exe\" \"QAT-Client\" "
//                "\"" + settings["IpUpdate"].toString() + "\" "
//                "\"" + settings["PortUpdate"].toString() + "\" "
//                "\"" + settings["IntType"].toString() + "\"";

//        QProcess process(this);
//        process.start(updateStringOKK);
//        process.waitForReadyRead();
//        process.waitForFinished();

//        if (process.exitCode() < 0)
//            message();
//    }

    QApplication::exit(0);
}

void VersionWidget::message()
{
    QMessageBox msg;
    msg.setText("Установите ТК и АК.\n"
                "После установки повторите попытку обновления.");
    msg.exec();
}
