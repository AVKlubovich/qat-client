﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/ComplaintInfoController.h"


namespace database
{
    class DBManager;
}

namespace Ui
{
    class RecallInfoWidget;
}

namespace qat_client
{
    class ReviewInfoEmailWidget;
    class RecallInfoWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit RecallInfoWidget(QWidget *parent = nullptr);
        ~RecallInfoWidget();

        void setRecallInfoModel(QAbstractItemModel* model);
        void setButtonLinkModel(QAbstractItemModel* model);
        void setDriverInfoModel(QAbstractItemModel* model);

        void setStatistic(QVariantMap mapStatistic);
        void setComments(QVariantList listComments);
        void setHistory(QVariantList listHistory);
        void setPermissionsWidget(bool hide);

    public slots:
        void fillComplaintInfo();

    private slots:
        void showEmailWidget();

    private:
        void clearEdits();
        void openComplaint();

    signals:
        void signalClicedCloseRecall(const quint64 id);
        void signalCreateComplaint(const quint64 idOrder,
                                   const quint64 _idCompany,
                                   const quint64 idReview,
                                   const quint64 _idSource,
                                   const QString& comment);
        void signalIdComplaint(const quint64 idComplaint);
        void signalCreateComment(const QString & comment);
        void signalDownloadHistory();

    private slots:
        void slotCloseReview();
        void slotCreateComplaint();

    private:
        Ui::RecallInfoWidget *_ui;
        QAbstractItemModel* _recallInfoModel = nullptr;
        QAbstractItemModel* _linkModel = nullptr;
        quint64 _idRecall;
        quint64 _idCompany;
        quint64 _idSource;
        QString _userEmail;

        QShortcut *_keyCtrlX = nullptr;
        QShortcut *_keyCtrlD = nullptr;

        QMap <int, QLineEdit*> _mapAnsver;
        ReviewInfoEmailWidget* _reviewInfoEmailWidget;
    };

}
