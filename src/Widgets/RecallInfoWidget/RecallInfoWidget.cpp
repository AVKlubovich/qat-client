#include "Common.h"

#include "RecallInfoWidget.h"
#include "ui_RecallInfoWidget.h"

#include "permissions/UserPermission.h"

#include "EmailWidget.h"
#include "ReviewInfoEmailWidget.h"


using namespace qat_client;

RecallInfoWidget::RecallInfoWidget(QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::RecallInfoWidget)
{
    _ui->setupUi(this);

    _mapAnsver = QMap <int, QLineEdit*>
    {
        {3, _ui->editAnswer1},
        {4, _ui->editAnswer2},
        {5, _ui->editAnswer3},
        {6, _ui->editAnswer4},
        {8, _ui->editAnswer5},
        {9, _ui->editAnswer6}
    };

    _ui->btnQuestionnaireDriver->setVisible(false);

//    _ui->btnSendEmail->setHidden(true);
    connect(_ui->btnSendEmail, &QPushButton::clicked, this, &RecallInfoWidget::showEmailWidget);
    connect(_ui->btnCloseRecall, &QPushButton::clicked, this, &RecallInfoWidget::slotCloseReview);
    connect(_ui->btnCreateComplaint, &QPushButton::clicked, this, &RecallInfoWidget::slotCreateComplaint);
    connect(_ui->btnComplaintOfReview, &QPushButton::clicked, this, &RecallInfoWidget::openComplaint);

    connect(_ui->btnSaveComment, &QPushButton::clicked,
            this, [this](){
        emit signalCreateComment(_ui->tEditComment->toPlainText());
    });

    connect(_ui->tabWidgetComplaint, &QTabWidget::currentChanged,
            this, [this](int tab)
            {
                if (tab)
                    emit signalDownloadHistory();
            });

    _keyCtrlX = new QShortcut(this);
    _keyCtrlX->setKey(Qt::CTRL + Qt::Key_X);
    connect(_keyCtrlX, &QShortcut::activated, this, &RecallInfoWidget::slotCloseReview);

    _keyCtrlD = new QShortcut(this);
    _keyCtrlD->setKey(Qt::CTRL + Qt::Key_D);
    connect(_keyCtrlD, &QShortcut::activated, this, &RecallInfoWidget::slotCreateComplaint);

    _reviewInfoEmailWidget = new ReviewInfoEmailWidget(this);
    this->layout()->addWidget(_reviewInfoEmailWidget);
    _reviewInfoEmailWidget->hideWidget();

    connect(_reviewInfoEmailWidget, &ReviewInfoEmailWidget::signalCloseReview,
            this, &RecallInfoWidget::slotCloseReview);
    connect(_reviewInfoEmailWidget, &ReviewInfoEmailWidget::signalSendEmail,
            this, &RecallInfoWidget::showEmailWidget);
    connect(_reviewInfoEmailWidget, &ReviewInfoEmailWidget::signalCreateComplaint,
            this, &RecallInfoWidget::slotCreateComplaint);
    connect(_reviewInfoEmailWidget, &ReviewInfoEmailWidget::signalViewComplaint,
            this, &RecallInfoWidget::openComplaint);
    connect(_reviewInfoEmailWidget, &ReviewInfoEmailWidget::signalAppendComment,
            this, &RecallInfoWidget::signalCreateComment);
    connect(_reviewInfoEmailWidget, &ReviewInfoEmailWidget::signalClickedTabHistory,
            this, &RecallInfoWidget::signalDownloadHistory);
}

RecallInfoWidget::~RecallInfoWidget()
{
    delete _ui;
}

void RecallInfoWidget::setRecallInfoModel(QAbstractItemModel *model)
{
    _recallInfoModel = model;
    _reviewInfoEmailWidget->setInfoModel(_recallInfoModel);
}

void RecallInfoWidget::setButtonLinkModel(QAbstractItemModel *model)
{
    _linkModel = model;
    _reviewInfoEmailWidget->setLinkModel(_linkModel);
}

void RecallInfoWidget::setDriverInfoModel(QAbstractItemModel *model)
{
    enum
    {
        IdDriver,
        NameDriver,
        namePark,
        WhoseDriver,
        DateComeDriver,
        RatingDriver,
        NumberAuto
    };

    const int CONST_ROW_MODEL = 0;

    if (model->rowCount())
    {
        _ui->editIdDriver->setText(model->index(CONST_ROW_MODEL, IdDriver).data().toString());
        _ui->editDriver->setText(model->index(CONST_ROW_MODEL, NameDriver).data().toString());
        _ui->editAccessoryDriver->setText(model->index(CONST_ROW_MODEL, WhoseDriver).data().toString());
        _ui->editPark_Column->setText(model->index(CONST_ROW_MODEL, namePark).data().toString());
        auto doub = model->index(CONST_ROW_MODEL, RatingDriver).data().toDouble();
        _ui->editRating->setText(QString::number(doub, 'f', 1));
        _ui->editAuto->setText(model->index(CONST_ROW_MODEL, NumberAuto).data().toString());
    }
}

void RecallInfoWidget::fillComplaintInfo()
{
    clearEdits();
    enum InfoComplaintEnum
    {
        id,
        id_order,
        datetime,
        id_company,
        id_status,
        id_source,
        comment,
        type,
        id_question,
        answer,
        user_email,
        end
    };

    _idRecall = _recallInfoModel->data(_recallInfoModel->index(0, id)).toLongLong();
    _idCompany = _recallInfoModel->data(_recallInfoModel->index(0, id_company)).toLongLong();
    _idSource = _recallInfoModel->data(_recallInfoModel->index(0, id_source)).toLongLong();

    switch (_idSource) {
        case 10:
        case 11:
            _reviewInfoEmailWidget->readInfoModel();
            _ui->widgetReviewInfo->hide();
            _reviewInfoEmailWidget->show();
            break;
        default:
            _reviewInfoEmailWidget->hideWidget();
            _ui->widgetReviewInfo->show();
            break;
    }

    const auto status = _recallInfoModel->data(_recallInfoModel->index(0, id_status)).toLongLong();
    if (status)
        _ui->btnCloseRecall->setHidden(true);
    else
        _ui->btnCloseRecall->setHidden(false);

    if(_linkModel->rowCount())
    {
        _ui->btnComplaintOfReview->setHidden(false);
        _ui->btnCreateComplaint->setHidden(true);
    }
    else
    {
        _ui->btnComplaintOfReview->setHidden(true);
        _ui->btnCreateComplaint->setHidden(false);
    }

    _userEmail = _recallInfoModel->data(_recallInfoModel->index(0, user_email)).toString();
    // TODO проверка на email
    if (!_userEmail.isEmpty())
        _ui->btnSendEmail->show();
    else
        _ui->btnSendEmail->hide();

    _ui->editRecall->setText(_recallInfoModel->data(_recallInfoModel->index(0, id)).toString());
    _ui->editOrder->setText(_recallInfoModel->data(_recallInfoModel->index(0, id_order)).toString());
    _ui->editDateCreate->setText(_recallInfoModel->data(_recallInfoModel->index(0, datetime)).toString());
    _ui->tEditReview->insertPlainText(_recallInfoModel->data(_recallInfoModel->index(0, comment)).toString());
    _ui->editType->setText(_recallInfoModel->data(_recallInfoModel->index(0, type)).toString());

    for (auto i = 0; i < _recallInfoModel->rowCount(); i++)
    {
        const auto idQuestion = _recallInfoModel->data(_recallInfoModel->index(i, id_question)).toInt();
        if (_mapAnsver.contains(idQuestion))
            _mapAnsver.value(idQuestion)->setText(_recallInfoModel->data(_recallInfoModel->index(i, answer)).toString());
    }
}

void RecallInfoWidget::showEmailWidget()
{
//    EmailWidget* widget = new EmailWidget("sanya.dedckov@yandex.ru", _idCompany, this);
    EmailWidget* widget = new EmailWidget(_userEmail, _idCompany, this);
    widget->setWindowFlags(Qt::Popup);
    widget->show();
}

void RecallInfoWidget::clearEdits()
{
    _ui->tEditTabReviews->clear();
    _ui->tEditReview->clear();
    _ui->tEditComment->clear();

    _ui->editRecall->clear();
    _ui->editOrder->clear();
    _ui->editDateCreate->clear();
    _ui->editAuto->clear();
    _ui->editAnswer1->clear();
    _ui->editAnswer2->clear();
    _ui->editAnswer3->clear();
    _ui->editAnswer4->clear();
    _ui->editAnswer5->clear();
    _ui->editAnswer6->clear();
    _ui->editIdDriver->clear();
    _ui->editDriver->clear();
    _ui->editAccessoryDriver->clear();
    _ui->editPark_Column->clear();
    _ui->editAll->clear();
    _ui->editPositive->clear();
    _ui->editNegative->clear();
    _ui->editCharacters->clear();
    _ui->editRating->clear();
}

void RecallInfoWidget::openComplaint()
{
    const auto idComplaint = _linkModel->index(0, 0).data().toLongLong();
    emit signalIdComplaint(idComplaint);
}

void RecallInfoWidget::slotCloseReview()
{
    emit signalClicedCloseRecall(_idRecall);
}

void RecallInfoWidget::slotCreateComplaint()
{
    const auto idOrder = _ui->editOrder->text().toLongLong();
    const auto comment = _ui->tEditReview->toPlainText();
    emit signalClicedCloseRecall(_idRecall);
    emit signalCreateComplaint(idOrder, _idCompany, _idRecall, _idSource, comment);
}

void RecallInfoWidget::setPermissionsWidget(bool hide)
{
    if (hide)
        _ui->tabWidgetComplaint->removeTab(_ui->tabWidgetComplaint->count() - 1);
}

void RecallInfoWidget::setStatistic(QVariantMap mapStatistic)
{
    enum
    {
        AllComplaintsCount = 0,
        PositiveComplaintsCount,
        NegativeComplaintsCount,
    };
    _ui->editAll->setText(mapStatistic[QString::number(AllComplaintsCount)].toString());
    _ui->editNegative->setText(mapStatistic[QString::number(NegativeComplaintsCount)].toString());
    _ui->editPositive->setText(mapStatistic[QString::number(PositiveComplaintsCount)].toString());

    auto list = mapStatistic["natures"].toList();
    for (auto element : list)
    {
        auto map = element.toMap();
        _ui->editCharacters->append(map["nature"].toString());
    }
}

void RecallInfoWidget::setComments(QVariantList listComments)
{
    _ui->tEditTabReviews->clear();
    for (auto element : listComments)
    {
        auto map = element.toMap();
        _ui->tEditTabReviews->append(map["comment"].toString());
    }

    _reviewInfoEmailWidget->setVariantListComments(listComments);
}

void RecallInfoWidget::setHistory(QVariantList listHistory)
{
    _ui->tEditTabHistory->clear();
    for (auto element : listHistory)
    {
        auto map = element.toMap();
        _ui->tEditTabHistory->append(map["comment"].toString());
    }

    _reviewInfoEmailWidget->setVariantListHistory(listHistory);
}
