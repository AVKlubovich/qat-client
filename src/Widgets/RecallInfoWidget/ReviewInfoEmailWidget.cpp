#include "Common.h"
#include "ReviewInfoEmailWidget.h"
#include "ui_ReviewInfoEmailWidget.h"

#include "permissions/UserPermission.h"
#include "EmailWidget.h"

using namespace qat_client;

ReviewInfoEmailWidget::ReviewInfoEmailWidget(QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::ReviewInfoEmailWidget)
{
    _ui->setupUi(this);
    connect(_ui->btnCloseRecall, &QPushButton::clicked, this, &ReviewInfoEmailWidget::signalCloseReview);
    connect(_ui->btnComplaintOfReview, &QPushButton::clicked, this, &ReviewInfoEmailWidget::signalViewComplaint);
    connect(_ui->btnCreateComplaint, &QPushButton::clicked, this, &ReviewInfoEmailWidget::signalCreateComplaint);
    connect(_ui->btnSendEmail, &QPushButton::clicked, this, &ReviewInfoEmailWidget::signalSendEmail);
    connect(_ui->btnSaveComment, &QPushButton::clicked, [this]
    {
         emit signalAppendComment(_ui->tEditComment->toPlainText());
    });

    connect(_ui->tabWidgetComplaint, &QTabWidget::currentChanged,
            this, [this](int tab)
            {
                if (tab)
                    emit signalClickedTabHistory();
            });
}

ReviewInfoEmailWidget::~ReviewInfoEmailWidget()
{
    delete _ui;
}

void ReviewInfoEmailWidget::hideWidget()
{
    clearEdits();
    this->hide();
}

void ReviewInfoEmailWidget::setInfoModel(QAbstractItemModel *model)
{
    _infoModel = model;
}

void ReviewInfoEmailWidget::setLinkModel(QAbstractItemModel *model)
{
    _linkModel = model;
}

void ReviewInfoEmailWidget::readInfoModel()
{
    clearEdits();
    enum InfoComplaintEnum
    {
        id,
        id_order,
        datetime,
        id_company,
        id_status,
        id_source,
        comment,
        type,
        id_question,
        answer,
        user_email,
        end
    };

    auto userEmail = _infoModel->data(_infoModel->index(0, user_email)).toString();
    if (!userEmail.isEmpty())
        _ui->btnSendEmail->show();
    else
        _ui->btnSendEmail->hide();

    const auto status = _infoModel->data(_infoModel->index(0, id_status)).toLongLong();
    if (status)
        _ui->btnCloseRecall->setHidden(true);
    else
        _ui->btnCloseRecall->setHidden(false);

    if(_linkModel->rowCount())
    {
        _ui->btnComplaintOfReview->setHidden(false);
        _ui->btnCreateComplaint->setHidden(true);
    }
    else
    {
        _ui->btnComplaintOfReview->setHidden(true);
        _ui->btnCreateComplaint->setHidden(false);
    }

    _ui->editRecall->setText(         _infoModel->data(_infoModel->index(0, id)).toString());
    _ui->editDateCreate->setText(     _infoModel->data(_infoModel->index(0, datetime)).toString());
    _ui->tEditReview->insertPlainText(_infoModel->data(_infoModel->index(0, comment)).toString());
    _ui->editType->setText(           _infoModel->data(_infoModel->index(0, type)).toString());
}

void ReviewInfoEmailWidget::setVariantListComments(const QVariantList &listComments)
{
    _ui->tEditTabReviews->clear();
    for (auto element : listComments)
    {
        auto map = element.toMap();
        _ui->tEditTabReviews->append(map["comment"].toString());
    }
}

void ReviewInfoEmailWidget::setVariantListHistory(const QVariantList &listHistory)
{
    _ui->tEditTabHistory->clear();
    for (auto element : listHistory)
    {
        auto map = element.toMap();
        _ui->tEditTabHistory->append(map["comment"].toString());
    }
}

void ReviewInfoEmailWidget::clearEdits()
{
    _ui->editDateCreate->clear();
    _ui->editRecall->clear();
    _ui->editType->clear();

    _ui->tEditComment->clear();
    _ui->tEditReview->clear();
    _ui->tEditTabHistory->clear();
    _ui->tEditTabReviews->clear();
}
