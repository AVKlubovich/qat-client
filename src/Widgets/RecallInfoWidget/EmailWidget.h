﻿#pragma once


namespace Ui
{
    class EmailWidget;
}

namespace qat_client
{

    class EmailWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit EmailWidget(const QString& emailAddress, const quint64 idCompany, QWidget *parent = nullptr);
        ~EmailWidget();

    public slots:
        void sendEmailClicked();
        void checkBtnVisible();

    private slots:
        void onSendEmail(QNetworkReply *reply);

    private:
        Ui::EmailWidget *_ui;
        QString _emailAddress;
        quint64 _idCompany;
    };

}
