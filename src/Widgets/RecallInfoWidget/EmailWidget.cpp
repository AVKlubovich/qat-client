#include "Common.h"

#include "EmailWidget.h"
#include "ui_EmailWidget.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"


using namespace qat_client;

EmailWidget::EmailWidget(const QString &emailAddress, const quint64 idCompany, QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::EmailWidget)
    , _emailAddress(emailAddress)
    , _idCompany(idCompany)
{
    _ui->setupUi(this);

    _ui->labelEmail->setText(_emailAddress);

    connect(_ui->btnSendEmail, &QPushButton::clicked, this, &EmailWidget::sendEmailClicked);
    connect(_ui->textEditEmail, &QTextEdit::textChanged, this, &EmailWidget::checkBtnVisible);
}

EmailWidget::~EmailWidget()
{
    delete _ui;
}

void EmailWidget::sendEmailClicked()
{
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart typeCommandPart;
    typeCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"type_command\""));
    typeCommandPart.setBody("send_email");
    multiPart->append(typeCommandPart);

    const QString& json = QString("{\"email_address\":\"%1\", \"message\":\"%2\", \"id_company\":\"%3\"}")
                          .arg(_emailAddress)
                          .arg(_ui->textEditEmail->toPlainText())
                          .arg(_idCompany);
    QHttpPart jsonPart;
    jsonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"json\""));
    jsonPart.setBody(json.toUtf8());
    multiPart->append(jsonPart);

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("PHP");
    QUrl url(settings["Url"].toString());
    QNetworkRequest request(url);
    QNetworkAccessManager *networkManager = new QNetworkAccessManager();
    connect(networkManager, &QNetworkAccessManager::finished, this, &EmailWidget::onSendEmail);
    QNetworkReply *reply = networkManager->post(request, multiPart);
    multiPart->setParent(reply);

    _ui->textEditEmail->clear();

    this->hide();
}

void EmailWidget::checkBtnVisible()
{
    bool isVisible = _ui->textEditEmail->toPlainText().count() > 2;
    _ui->btnSendEmail->setEnabled(isVisible);
}

void EmailWidget::onSendEmail(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        qDebug() << "[EMAIL ERROR]" << reply->errorString();
        QMessageBox::critical(nullptr, tr("Error"), tr("Error"));
        return;
    }
    else
        qDebug() << "[EMAIL REPLAY]" << reply->readAll();
    reply->deleteLater();

    this->deleteLater();
}
