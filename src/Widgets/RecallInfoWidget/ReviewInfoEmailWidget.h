﻿#pragma once

// TODO: убрать контроллеры из виджетов
#include "Controllers/ComplaintInfoController.h"


namespace database
{
    class DBManager;
}

namespace Ui
{
    class ReviewInfoEmailWidget;
}

namespace qat_client
{

    class ReviewInfoEmailWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit ReviewInfoEmailWidget(QWidget *parent = nullptr);
        ~ReviewInfoEmailWidget();

        void hideWidget();
        void setInfoModel(QAbstractItemModel *model);
        void setLinkModel(QAbstractItemModel *model);
        void readInfoModel();

        void setVariantListComments(const QVariantList &listComments);
        void setVariantListHistory(const QVariantList &listHistory);

    private:
        Ui::ReviewInfoEmailWidget *_ui;
        QAbstractItemModel *_infoModel = nullptr;
        QAbstractItemModel *_linkModel = nullptr;

    signals:
        void signalViewComplaint();
        void signalCloseReview();
        void signalSendEmail();
        void signalCreateComplaint();
        void signalAppendComment(const QString &str);
        void signalClickedTabHistory();

    private:
        void clearEdits();
    };

}
