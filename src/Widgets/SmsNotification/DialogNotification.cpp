#include "Common.h"
#include "DialogNotification.h"
#include "ui_DialogNotification.h"

#include "MessageDialog/TypeMessageWidget.h"
#include "Definitions.h"


using namespace qat_client;

DialogNotification::DialogNotification(QWidget *parent)
    : QDialog(parent)
    , _ui(new Ui::DialogNotification)
{
    _ui->setupUi(this);

    connect(this, &DialogNotification::finished,
            this, &DialogNotification::deleteLater);

    connect(_ui->cbNotification, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &DialogNotification::displaySmsText);

    connect(_ui->chbNotification, &QCheckBox::clicked,
            this, &DialogNotification::onClickedChbNotification);

//    connect(_ui->btnSend, &QPushButton::clicked,
//            this, &DialogNotification::onBtnSendClicked);

    connect(_ui->btnSend, &QPushButton::clicked, [this]
    {
        auto dialog = new TypeMessageWidget(this);
        connect(dialog, &TypeMessageWidget::signalSendSMS, this, &DialogNotification::onBtnSendClicked);
        dialog->exec();

        disconnect(dialog, &TypeMessageWidget::signalSendSMS, this, &DialogNotification::onBtnSendClicked);
    });
}

DialogNotification::~DialogNotification()
{
    delete _ui;
}

void DialogNotification::setModelNotification(QAbstractItemModel *model)
{
    _ui->cbNotification->setModel(model);
}

void DialogNotification::setModelPhones(QAbstractItemModel *model)
{
    _ui->cbPhones->setModel(model);
}

void DialogNotification::displaySmsText(int index)
{
    _ui->editSMS->setText(_ui->cbNotification->model()->index(index, 2).data().toString());
    _ui->dtNotification->setMinimumDateTime(QDateTime::currentDateTime());
}

void DialogNotification::onClickedChbNotification(bool checked)
{
    if (checked)
        _ui->dtNotification->setEnabled(false);
    else
        _ui->dtNotification->setEnabled(true);
}

void DialogNotification::onBtnSendClicked(int typeMessage, double mark)
{
    QStringList listPhone = _ui->cbPhones->currentText().split(" ");

    QDateTime date;
    if (_ui->chbNotification->isChecked())
        date = QDateTime::currentDateTime();
    else
        date = _ui->dtNotification->dateTime();

    emit signalSendSms(_ui->editSMS->toPlainText(), listPhone.first(), date, typeMessage, mark);
    accept();
}
