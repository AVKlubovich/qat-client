#pragma once


namespace Ui
{
    class TypeMessageWidget;
}

namespace qat_client
{

    class TypeMessageWidget : public QDialog
    {
        Q_OBJECT

    public:
        explicit TypeMessageWidget(QWidget *parent = nullptr);
        ~TypeMessageWidget();

    signals:
        void signalSendSMS(int type, double mark);

    private:
        Ui::TypeMessageWidget *_ui;
    };

}
