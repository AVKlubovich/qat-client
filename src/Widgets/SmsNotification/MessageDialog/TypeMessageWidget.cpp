#include "Common.h"
#include "TypeMessageWidget.h"
#include "ui_TypeMessageWidget.h"

#include "Definitions.h"


using namespace qat_client;

TypeMessageWidget::TypeMessageWidget(QWidget *parent)
    : QDialog(parent, Qt::CustomizeWindowHint)
    , _ui(new Ui::TypeMessageWidget)
{
    _ui->setupUi(this);
    connect(_ui->btnCancel, &QPushButton::clicked, this, &TypeMessageWidget::reject);
    connect(_ui->btnSend, &QPushButton::clicked, [this]
    {
        if (!_ui->cbTypeMessage->currentIndex())
        {
            QMessageBox::information(this, "Предупреждение","Не указан тип сообщения", QMessageBox::Ok);
            return;
        }

        if ((_ui->cbTypeMessage->currentIndex() == 2) && (_ui->editMark->text().isEmpty()))
        {
            QMessageBox::information(this, "Предупреждение","Не указана оценка", QMessageBox::Ok);
            return;
        }

        const auto mark = _ui->editMark->text().replace(1,1,".").toDouble();
        emit signalSendSMS(_ui->cbTypeMessage->currentIndex(), mark);
        accept();
    });

    connect(_ui->cbTypeMessage, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [this](int index)
    {
        if (index == 2)
            _ui->editMark->setHidden(false);
        else
            _ui->editMark->setHidden(true);
    });

    auto validator = new QDoubleValidator(0.0, 4.9, 1, this);
    validator->setNotation( QDoubleValidator::StandardNotation );
    _ui->editMark->setValidator(validator);

    _ui->editMark->setHidden(true);
}

TypeMessageWidget::~TypeMessageWidget()
{
    delete _ui;
}
