#pragma once


namespace Ui
{
    class DialogNotification;
}

namespace qat_client
{

    class DialogNotification : public QDialog
    {
        Q_OBJECT

    public:
        explicit DialogNotification(QWidget *parent = nullptr);
        ~DialogNotification();

        void setModelNotification(QAbstractItemModel *model);
        void setModelPhones(QAbstractItemModel *model);

    signals:
        void signalSendSms(const QString &smsText, const QString &phone, const QDateTime &date, int type, double mark = 0);

    private slots:
        void displaySmsText(int index);
        void onClickedChbNotification(bool checked);
        void onBtnSendClicked(int typeMessage = 0, double mark = 0);

    private:
        Ui::DialogNotification *_ui;
    };

}
