﻿#include "CreateUserDialog.h"
#include "ui_CreateUserDialog.h"


using namespace qat_client;

CreateUserDialog::CreateUserDialog(QWidget *parent)
    : QDialog(parent)
    , _ui(new Ui::CreateUserDialog)
{
    _ui->setupUi(this);

    flagStatus = true;

    connect(_ui->buttonBox, SIGNAL(accepted()),
            this, SLOT(acceptedButtonClicked()));

    connect(_ui->cbCompany, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &CreateUserDialog::signalIdComapny);

    connect(this, &CreateUserDialog::finished,
            this, &CreateUserDialog::clearVariables);
}

CreateUserDialog::~CreateUserDialog()
{
    delete _ui;
}

void CreateUserDialog::getUserData(QString *name,
                                   QString *userName,
                                   QString *password,
                                   QString *position,
                                   QString *company,
                                   QString *department)
{
    *name = _ui->editName->text();
    *userName = _ui->editLogin->text();
    *password= _ui->editPassword->text();
    *position = _ui->editPosition->text();
    *company = _ui->cbCompany->currentText();
    *department = _ui->cbDepartment->currentText();
}

void CreateUserDialog::setCompanyModel(QAbstractItemModel *model)
{
    _ui->cbCompany->setModel(model);
}

void CreateUserDialog::setDepartmentModel(QAbstractItemModel *model)
{
    _ui->cbDepartment->setModel(model);
}

void CreateUserDialog::setDataUser(network::User &user)
{
    _ui->editName->setText(user.name());
    _ui->editLogin->setText(user.login());
    _ui->editPassword->setText(user.password());
    _ui->editPosition->setText(user.officer());
    _company = user.company();
    _department = user.department();

    flagStatus = false;
}

void CreateUserDialog::setDisabledButtonSave()
{
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(false);
}

void CreateUserDialog::acceptedButtonClicked()
{
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);

    if (flagStatus)
        emit onCreateUser();
    else
        emit onUpdateUser();
}

void CreateUserDialog::clearVariables()
{
    _company.clear();
    _department.clear();

    _ui->editName->clear();
    _ui->editLogin->clear();
    _ui->editPassword->clear();
    _ui->editPosition->clear();
    _ui->cbCompany->clear();
    _ui->cbDepartment->clear();

    flagStatus = true;
}

int CreateUserDialog::exec()
{
    _ui->cbCompany->setCurrentText(_company);
    _ui->cbDepartment->setCurrentText(_department);
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    int res = QDialog::exec();
    return res;
}

void CreateUserDialog::execRetry()
{
    Q_ASSERT(this->isVisible());
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
}
