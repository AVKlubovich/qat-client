﻿#pragma once

#include "network-core/RequestsManager/Users/Users.h"


namespace Ui
{
    class CreateUserDialog;
}

namespace qat_client
{
    class CreateUserDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit CreateUserDialog(QWidget *parent = nullptr);
        ~CreateUserDialog();

        void getUserData(QString *name,
                         QString *login,
                         QString *password,
                         QString *position,
                         QString *company,
                         QString *department);

        void setCompanyModel(QAbstractItemModel* model);
        void setDepartmentModel(QAbstractItemModel* model);

        void setDataUser(network::User &user);

        void setDisabledButtonSave();

        //Display dialog for request new user parameters.
        //Return 0 if cancel dialog.
        int exec() override;
        //Reset dialog controls for repeat enter of user parameters.
        //Return 0 if cancel dialog.
        void execRetry();

    signals:
        void onCreateUser();
        void onUpdateUser();
        void signalIdComapny(int index);

    private slots:
        void acceptedButtonClicked();
        void clearVariables();

    private:
        Ui::CreateUserDialog *_ui;
        QString _company;
        QString _department;

        bool flagStatus;
    };
}
