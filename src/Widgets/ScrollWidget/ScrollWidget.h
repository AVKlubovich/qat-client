﻿#pragma once


namespace Ui
{
    class ScrollWidget;
}

namespace qat_client
{

    class ScrollWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit ScrollWidget(QWidget *parent = nullptr);
        ~ScrollWidget();

        void setWidget(QWidget *widget);
        void clearWidget();

    private:
        Ui::ScrollWidget *_ui;
    };

}
