﻿#include "ScrollWidget.h"
#include "ui_ScrollWidget.h"


using namespace qat_client;

ScrollWidget::ScrollWidget(QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::ScrollWidget)
{
    _ui->setupUi(this);
}

ScrollWidget::~ScrollWidget()
{
    delete _ui;
}

void ScrollWidget::setWidget(QWidget *widget)
{
    _ui->scrollArea->setWidget(widget);
}
