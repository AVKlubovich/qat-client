﻿#pragma once


namespace database
{
    class DBManager;
}

namespace Ui
{
    class WorkerInformationWidget;
}

namespace qat_client
{

    class WorkerInformationWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit WorkerInformationWidget(QWidget *parent = nullptr);
        ~WorkerInformationWidget();

        void setInfoModel(QSqlQueryModel *model);
        void setPhonesModel(QSqlQueryModel *model);
        void setStatistic(QVariantMap mapStatistic);
        void setComments(QVariantList listComments);
        void setHistory(QVariantList listHistory);

        void loadDriverPhoto();
        void setPermissionsWidget(bool hide);

    private slots:
        void onLoadDriverPhoto(QNetworkReply *reply);

        void onBtnDriverCV();
        void onLoadDriverCV(QNetworkReply *reply);

        void downloadPhotoFinish(const QString& url);
        void downloadPhotoError(const QString& err);

        void downloadCVFinish(const QString& url);
        void downloadCVError(const QString& err);

        void onBtnExcelClicked();

    private:
        void clearWidgets();

    signals:
        void signalTargetComplaints(const QString& idTarget, int statusType);
        void signalSaveComment(const QString& comment);
        void signalDownloadHistory();
        void startDownload(const QString downloadUrl);

        void signalCreateComplaintsExcelFile(const QString& fileName);

    private:
        Ui::WorkerInformationWidget *_ui;

        quint64 _driverId;
        quint64 _companyId;
    };

}
