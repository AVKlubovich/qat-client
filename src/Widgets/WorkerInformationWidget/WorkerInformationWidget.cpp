#include "Common.h"

#include "WorkerInformationWidget.h"
#include "ui_WorkerInformationWidget.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"

#include "FileDownloader.h"


using namespace qat_client;

WorkerInformationWidget::WorkerInformationWidget(QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::WorkerInformationWidget)
{
    _ui->setupUi(this);

    enum statusType
    {
        AllComplaints = 0,
        PositiveComplaints,
        NegativeComplaints,
    };

    connect(_ui->btnSaveComments, &QPushButton::clicked, this, [this](){
        emit signalSaveComment(_ui->tEditComment->toPlainText());
    });

    connect(_ui->btnDriverCV, &QPushButton::clicked, this, &WorkerInformationWidget::onBtnDriverCV);
    connect(_ui->tabWidgetComplaint, &QTabWidget::currentChanged,
            this, [this](int tab)
            {
                if (tab)
                    emit signalDownloadHistory();
            });

    connect(_ui->btnDownloadComplaintExcel, &QPushButton::clicked,
            this, &WorkerInformationWidget::onBtnExcelClicked);
}

WorkerInformationWidget::~WorkerInformationWidget()
{
    delete _ui;
}

void WorkerInformationWidget::setInfoModel(QSqlQueryModel *model)
{
    clearWidgets();

    enum
    {
        idDriver = 0,
        driverName,
        namePark,
        driverColumn,
        dateComeDriver,
        numberCar,
        modelCar,
        colorCar,
        idCompany
    };

    _ui->editIdDriver->setText(model->index(0, idDriver).data().toString());
    _ui->editFioDriver->setText(model->index(0, driverName).data().toString());
    _ui->editHiredDriver->setText(model->index(0, dateComeDriver).data().toString());
    _ui->editAutoNumber->setText(model->index(0, numberCar).data().toString());
    _ui->editAutoBrand->setText(model->index(0, modelCar).data().toString());

    const QString park = model->index(0, namePark).data().toString();
    const QString column = model->index(0, driverColumn).data().toString();
    QString parkColumnStr;
    if (!park.isEmpty() && !column.isEmpty())
    {
        parkColumnStr = QString("%1 / %2").arg(park).arg(column);
    }
    else if (!park.isEmpty())
    {
        parkColumnStr = park;
    }
    else if (!column.isEmpty())
    {
        parkColumnStr = column;
    }
    _ui->editPark->setText(parkColumnStr);

    _driverId = model->index(0, idDriver).data().toULongLong();
    _companyId = model->index(0, idCompany).data().toULongLong();

    loadDriverPhoto();
}

void WorkerInformationWidget::setPhonesModel(QSqlQueryModel *model)
{
    for (auto i = 0; i < model->rowCount(); ++i)
        _ui->editAdditionalPhoneNumbers->append(model->index(i, model->columnCount() - 1).data().toString());
}

void WorkerInformationWidget::setStatistic(QVariantMap mapStatistic)
{
    enum
    {
        AllComplaintsCount = 0,
        PositiveComplaintsCount,
        NegativeComplaintsCount,
    };
    _ui->editCountComplaints->setText(mapStatistic[QString::number(AllComplaintsCount)].toString());
    _ui->editCountNegativeComplaints->setText(mapStatistic[QString::number(NegativeComplaintsCount)].toString());
    _ui->editCountPositiveComplaints->setText(mapStatistic[QString::number(PositiveComplaintsCount)].toString());

    auto list = mapStatistic["natures"].toList();
    for (auto element : list)
    {
        auto map = element.toMap();
        _ui->ediNegativeCharacters->append(map["nature"].toString());
    }
}

void WorkerInformationWidget::setComments(QVariantList listComments)
{
    _ui->tEditTabReviews->clear();
    for (auto element : listComments)
    {
        auto map = element.toMap();
        _ui->tEditTabReviews->append(map["comment"].toString());
    }
}

void WorkerInformationWidget::setHistory(QVariantList listHistory)
{
    _ui->tEditTabHistory->clear();
    for (auto element : listHistory)
    {
        auto map = element.toMap();
        _ui->tEditTabHistory->append(map["comment"].toString());
    }
}

void WorkerInformationWidget::loadDriverPhoto()
{
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart typeCommandPart;
    typeCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"type_command\""));
    typeCommandPart.setBody("get_driver_photo");
    multiPart->append(typeCommandPart);

    QHttpPart jsonPart;
    const auto& json = QString("{\"type_command\":\"get_driver_photo\", \"driver_id\":\"%1\", \"id_company\":\"%2\"}")
                       .arg(_driverId)
                       .arg(_companyId);
    jsonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"json\""));
    jsonPart.setBody(json.toUtf8());
    multiPart->append(jsonPart);

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("PHP");
    QUrl url(settings["Url"].toString());
    QNetworkRequest request(url);
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    QNetworkReply *reply = networkManager->post(request, multiPart);
    connect(networkManager, &QNetworkAccessManager::finished, this, &WorkerInformationWidget::onLoadDriverPhoto);
}

void WorkerInformationWidget::onLoadDriverPhoto(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        qDebug() << reply->errorString();
        QMessageBox::critical(nullptr, tr("Error"), tr("Drivers photo do not load"));
        return;
    }

    auto bArray = reply->readAll();
    bArray = bArray.right(bArray.size() - bArray.indexOf("{"));
    const auto doc = QJsonDocument::fromJson(bArray);
    auto jObj = doc.object();
    const auto& outData = jObj.toVariantMap();

    if (!outData.contains("url_photo_driver"))
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("Drivers photo do not load"));
        return;
    }

    const auto& photoUrl = outData["url_photo_driver"].toString();

    auto downloadThread = new QThread(this);
    auto downloader = new FileDownloader();
    downloader->moveToThread(downloadThread);

    connect(downloader, &FileDownloader::destroyed, downloadThread, &QThread::quit);
    connect(downloadThread, &QThread::finished, downloadThread, &QThread::deleteLater);
    connect(this, &WorkerInformationWidget::startDownload, downloader, &FileDownloader::downloadFile);
    connect(downloader, &FileDownloader::finish, this, &WorkerInformationWidget::downloadPhotoFinish);
    connect(downloader, &FileDownloader::error, this, &WorkerInformationWidget::downloadPhotoError);

    downloadThread->start();

    emit startDownload(photoUrl);
}

void WorkerInformationWidget::onBtnDriverCV()
{
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart typeCommandPart;
    typeCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"type_command\""));
    typeCommandPart.setBody("get_driver_cv");
    multiPart->append(typeCommandPart);

    QHttpPart jsonPart;
    const auto& json = QString("{\"type_command\":\"get_driver_cv\", \"driver_id\":\"%1\", \"id_company\":\"%2\"}")
                       .arg(_driverId)
                       .arg(_companyId);
    jsonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"json\""));
    jsonPart.setBody(json.toUtf8());
    multiPart->append(jsonPart);

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("PHP");
    QUrl url(settings["Url"].toString());
    QNetworkRequest request(url);
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    QNetworkReply *reply = networkManager->post(request, multiPart);
    connect(networkManager, &QNetworkAccessManager::finished, this, &WorkerInformationWidget::onLoadDriverCV);
}

void WorkerInformationWidget::onLoadDriverCV(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        qDebug() << reply->errorString();
        QMessageBox::critical(nullptr, tr("Error"), tr("Drivers cv do not load"));
        return;
    }

    auto bArray = reply->readAll();
    bArray = bArray.right(bArray.size() - bArray.indexOf("{"));
    const auto doc = QJsonDocument::fromJson(bArray);
    auto jObj = doc.object();
    const auto& outData = jObj.toVariantMap();

    if (!outData.contains("url_cv_driver"))
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("Drivers cv do not load"));
        return;
    }

    const auto& cvUrl = outData["url_cv_driver"].toString();

    auto downloadThread = new QThread(this);
    auto downloader = new FileDownloader();
    downloader->moveToThread(downloadThread);

    connect(downloadThread, &QThread::finished, downloader, &FileDownloader::deleteLater);
    connect(downloader, &FileDownloader::destroyed, downloadThread, &QThread::quit);
    connect(this, &WorkerInformationWidget::startDownload, downloader, &FileDownloader::downloadFile);
    connect(downloader, &FileDownloader::finish, this, &WorkerInformationWidget::downloadCVFinish);
    connect(downloader, &FileDownloader::error, this, &WorkerInformationWidget::downloadCVError);

    downloadThread->start();

    emit startDownload(cvUrl);
}

void WorkerInformationWidget::downloadPhotoFinish(const QString &url)
{
    _ui->labelPhotoDriver->setStyleSheet(QString("image: url(%1);").arg(url));

    sender()->deleteLater();
}

void WorkerInformationWidget::downloadPhotoError(const QString &err)
{
    qDebug() << err;
}

void WorkerInformationWidget::downloadCVFinish(const QString &url)
{
    QDesktopServices::openUrl(url);
}

void WorkerInformationWidget::downloadCVError(const QString &err)
{
    qDebug() << err;
}

void WorkerInformationWidget::onBtnExcelClicked()
{
    const QString fileName = QFileDialog::getSaveFileName(
                                 this, tr("Save File"),
                                 "Drivers complaints " + QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss") + ".xlsx",
                                 tr("Excel (*.xlsx)"));

    if (!fileName.isEmpty())
        emit signalCreateComplaintsExcelFile(fileName);
}

void WorkerInformationWidget::clearWidgets()
{
    _ui->ediNegativeCharacters->clear();
    _ui->editAdditionalPhoneNumbers->clear();
    _ui->editAutoBrand->clear();
    _ui->editAutoNumber->clear();
    _ui->editCountComplaints->clear();
    _ui->editCountPositiveComplaints->clear();
    _ui->editFioDriver->clear();
    _ui->editHiredDriver->clear();
    _ui->editIdDriver->clear();
    _ui->editPark->clear();
    _ui->editPhoneNumber->clear();
    _ui->tEditComment->clear();
    _ui->tEditTabHistory->clear();
    _ui->tEditTabReviews->clear();
}

void WorkerInformationWidget::setPermissionsWidget(bool hide)
{
    if (hide)
        _ui->tabWidgetComplaint->removeTab(_ui->tabWidgetComplaint->count() - 1);
}
