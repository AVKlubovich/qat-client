#pragma once


namespace qat_client
{

    class File;
    typedef QSharedPointer<File> FileShp;


    class FileDownloader : public QObject
    {
        Q_OBJECT

    public:
        FileDownloader(QObject *parent = nullptr);
        ~FileDownloader() = default;

    public slots:
        void downloadFile(const QString& downloadUrl);

    private slots:
        void downloadReadyRead();
        void downloadFinished();
        void slotError(QNetworkReply::NetworkError error);
        void slotSslErrors(QList<QSslError> errors);

    private:
        void prepareOutputFile(const QString &fileName);

    signals:
        void finish(const QString& url);
        void error(const QString& err);

    private:
        QNetworkAccessManager *_manager;
        QFile *_outputFile;
    };

    typedef QSharedPointer<FileDownloader> FileDownloaderShp;

}
