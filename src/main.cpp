#include "Common.h"

#include "Core/Core.h"


void setTextCodec(const QString& encodingName)
{
    QTextCodec *codec = QTextCodec::codecForName(encodingName.toStdString().c_str());
    QTextCodec::setCodecForLocale(codec);
}

int main(int argc, char *argv[])
{
//    Q_INIT_RESOURCE(images);
    QApplication a(argc, argv);

    setTextCodec("UTF-8");

    auto& core = qat_client::Core::instance();
    if (!core.init())
    {
        core.done();
        return -1;
    }

    core.run();

    a.exec();

    core.done();

    return 0;
}
