﻿DROP TABLE if exists clients;
DROP TABLE if exists commands;
DROP TABLE if exists company;
DROP TABLE if exists complaint_files;
DROP TABLE if exists complaint_to_review;
DROP TABLE if exists complaints;
DROP TABLE if exists complaints_comment;
DROP TABLE if exists complaints_nature;
DROP TABLE if exists complaints_orders_cars;
DROP TABLE if exists complaints_orders_driver_phones;
DROP TABLE if exists complaints_orders_drivers;
DROP TABLE if exists complaints_orders_logists;
DROP TABLE if exists complaints_orders_operators;
DROP TABLE if exists complaints_source;
DROP TABLE if exists complaints_status;
DROP TABLE if exists complaints_target;
DROP TABLE if exists complaints_type;
DROP TABLE if exists customers;
DROP TABLE if exists departments;
DROP TABLE if exists employees;
DROP TABLE if exists hours_worked;
DROP TABLE if exists info;
DROP TABLE if exists notification;
DROP TABLE if exists notification_methods;
DROP TABLE if exists parks;
DROP TABLE if exists permissions;
DROP TABLE if exists reviews;
DROP TABLE if exists reviews_answer;
DROP TABLE if exists reviews_comment;
DROP TABLE if exists reviews_files;
DROP TABLE if exists reviews_orders_cars;
DROP TABLE if exists reviews_orders_driver_phones;
DROP TABLE if exists reviews_orders_drivers;
DROP TABLE if exists reviews_orders_logists;
DROP TABLE if exists reviews_orders_operators;
DROP TABLE if exists reviews_source;
DROP TABLE if exists users;

CREATE TABLE company (
  id               bigint NOT NULL,
  name             text,
  api              text,
  questionary_url  text,
  isVisible integer
);

CREATE UNIQUE INDEX company_id
ON company (id);

CREATE TABLE complaints (
  id              bigint NOT NULL,
  id_company      bigint,
  id_order        bigint,
  id_status       integer,
  id_source       integer,
  id_nature       integer,
  id_department   bigint,
  id_customer     bigint,
  id_user_create  bigint,
  id_user_close   bigint,
  date_create     timestamp,
  date_close      timestamp,
  id_user         bigint,
  essence         text,
  id_type         bigint,
  is_work         smallint,
  days_to_process timestamp,
  other_user_create text,
  date_order      date,
  client_number   text,
  rating          float,
  feedback        int
);

CREATE UNIQUE INDEX complaints_id
ON complaints (id);

CREATE TABLE complaint_files (
  id bigserial NOT NULL,
  id_complaint bigint,
  file_uuid uuid,
  file_name text,
  file_size bigint,
  time_create timestamp,
  file_chunks integer
);

CREATE TABLE complaints_nature (
  id           bigint NOT NULL,
  id_company   bigint,
  id_target    integer,
  data_nature  text,
  sequence     integer,
  id_user        bigint,
  id_visiable    integer,
  hint_essence   text,
  days_to_review integer,
  default_rating integer,
  cost           float,
  type           int
);

CREATE UNIQUE INDEX complaints_nature_id
ON complaints_nature (id);

CREATE TABLE complaints_source (
  id           bigint NOT NULL,
  id_company   bigint,
  name_source  text,
  id_user      bigint
);

CREATE TABLE complaints_status (
  id           bigint NOT NULL,
  name_status  text,
  sequence     integer,
  id_user bigint
);

CREATE TABLE complaints_target (
  id           bigint NOT NULL,
  data_target  text,
  id_user bigint,
  isVisible integer
);

CREATE UNIQUE INDEX complaints_target_id
ON complaints_target (id);

CREATE TABLE complaints_comment (
  id            bigint NOT NULL,
  id_complaint  bigint,
  comment       text,
  date_create   timestamp,
  id_user       bigint
);


CREATE TABLE complaints_type (
  id bigint,
  name text
);

CREATE TABLE customers (
  id           bigint NOT NULL,
  id_company   bigint,
  id_customer  bigint,
  phone        text,
  email        text,
  name         text
);

CREATE TABLE employees (
  id           bigint NOT NULL,
  id_company   bigint,
  id_target    bigint,
  name         text
);

CREATE TABLE hours_worked (
  id_user     bigint,
  date_start  timestamp,
  date_close  timestamp
);

CREATE TABLE info (
  version        text,
  version_proxy  text
);

CREATE TABLE notification (
  id                    bigint NOT NULL,
  id_complaint          bigint,
  id_notification_type  integer,
  id_user bigint
);

CREATE TABLE notification_methods (
  id                 bigint NOT NULL,
  notification_type  text,
  id_user bigint
);

CREATE TABLE reviews (
  id           bigint NOT NULL,
  id_company   bigint,
  id_order     bigint,
  id_source    bigint,
  id_customer  bigint,
  date_create  timestamp,
  id_user      bigint,
  id_status    bigint,
  comment      text,
  type         integer,
  user_phone   text,
  user_email   text
);

CREATE UNIQUE INDEX reviews_id
ON reviews (id);
PRAGMA encoding = "UTF-8";

CREATE TABLE reviews_answer (
  id           bigint NOT NULL,
  id_reviews   bigint,
  id_question  bigint,
  answer       text,
  id_user bigint
);

CREATE TABLE reviews_files (
  id           bigint NOT NULL,
  id_reviews   bigint,
  file_name    text,
  date_create  timestamp,
  id_user      bigint,
  client_name  text,
  client_phone text,
  client_email text
);

CREATE TABLE reviews_source (
  id           bigint NOT NULL,
  id_company   bigint,
  name_source  text,
  id_user      bigint
);

CREATE TABLE "reviews_comment" (
  id           bigint NOT NULL,
  id_reviews   bigint,
  comment      text,
  date_create  timestamp,
  id_user      bigint
);

CREATE TABLE "departments" (
  id bigint,
  id_parent bigint,
  name text,
  id_company bigint,
  isVisible integer
);

CREATE UNIQUE INDEX departments_id
ON departments (id);

CREATE TABLE clients (
  id               bigint NOT NULL,
  id_company       bigint,
  id_complaint     bigint,
  fio      		   text,
  phone			   text,
  date_create      timestamp,
  id_order         bigint,
  id_customer 	   bigint,
  id_nature 	   bigint
);

CREATE UNIQUE INDEX clients_id
ON clients (id);

CREATE TABLE users (
  id bigint,
  name text,
  id_department bigint,
  id_company bigint,
  isVisible integer
);

CREATE TABLE permissions (
  id_user bigint,
  id_department bigint,
  id_command bigint
);

CREATE TABLE commands (
  id bigint,
  name text,
  permission_description text
);

CREATE TABLE "complaint_to_review" (
  id_complaint bigint,
  id_review    bigint,
  id           bigint
);

CREATE TABLE complaints_orders_cars
(
  id bigint NOT NULL,
  id_complaint bigint NOT NULL,
  id_car        bigint,
  "number"      text,
  model         text,
  color         text
);

CREATE UNIQUE INDEX car_id
ON complaints_orders_cars (id_complaint);

CREATE TABLE complaints_orders_driver_phones
(
  id bigint NOT NULL,
  id_complaint bigint NOT NULL,
  phone text
);

CREATE TABLE complaints_orders_drivers
(
  id bigint NOT NULL,
  id_complaint bigint NOT NULL,
  id_worker bigint,
  surname text,
  middlename text,
  firstname text,
  whose smallint,
  id_park bigint,
  "column" integer,
  id_city bigint,
  date_come date,
  rating real,
  rating_count bigint,
  form_type text
);

CREATE UNIQUE INDEX driver_id
ON complaints_orders_drivers (id_complaint);

CREATE TABLE complaints_orders_logists
(
  id bigint NOT NULL,
  id_complaint bigint NOT NULL,
  id_worker bigint,
  fio text,
  phone text
);

CREATE UNIQUE INDEX logist_id
ON complaints_orders_logists (id_complaint);

CREATE TABLE complaints_orders_operators
(
  id bigint NOT NULL,
  id_complaint bigint NOT NULL,
  id_worker bigint,
  fio text,
  phone text
);

CREATE UNIQUE INDEX operator_id
ON complaints_orders_operators (id_complaint);

CREATE TABLE reviews_orders_cars
(
  id_review bigint NOT NULL,
  id_car    bigint,
  "number"  text,
  model     text,
  color     text,
  id        bigint
);

CREATE UNIQUE INDEX cars_id_review
ON reviews_orders_cars (id_review);

CREATE TABLE reviews_orders_driver_phones
(
  id_review bigint NOT NULL,
  phone     text,
  id        bigint
);

CREATE TABLE reviews_orders_drivers
(
  id_review  bigint NOT NULL,
  id_worker  bigint,
  surname    text,
  middlename text,
  firstname  text,
  whose      smallint,
  id_park    bigint,
  "column"   integer,
  id_city    bigint,
  date_come  date,
  rating     real,
  rating_count  bigint,
  form_type     text,
  id            bigint
);

CREATE UNIQUE INDEX driver_id_review
ON reviews_orders_drivers (id_review);

CREATE TABLE reviews_orders_logists
(
  id_review bigint NOT NULL,
  id_worker bigint,
  fio       text,
  phone     text,
  id        bigint
);

CREATE UNIQUE INDEX logist_id_review
ON reviews_orders_logists (id_review);

CREATE TABLE reviews_orders_operators
(
  id_review bigint NOT NULL,
  id_worker bigint,
  fio       text,
  phone     text,
  id        bigint
);

CREATE UNIQUE INDEX operator_id_review
ON reviews_orders_operators (id_review);

CREATE TABLE parks
(
  id bigint NOT NULL,
  park_name text,
  id_company bigint NOT NULL
)
