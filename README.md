
# qat-client

## Описание проекта
Система для работы с жалобами

## Используемые технологии
[C++14](https://ru.wikipedia.org/wiki/C%2B%2B14)  
[Qt5](https://ru.wikipedia.org/wiki/Qt)

## Системные требования
Windows

## Документация
[Соглашения по оформлению кода](https://drive.google.com/open?id=0B48GpktEZIksWjNjTmdWcW53Rnc)

## Список контрибьюторов
[@github/maximp](../../../../maximp)  
[@github/avklubovich](../../../../avklubovich)  
[@github/AlexandrDedckov](../../../../AlexandrDedckov)
